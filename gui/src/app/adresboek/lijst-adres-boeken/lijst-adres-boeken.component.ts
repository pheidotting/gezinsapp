import {Component, CUSTOM_ELEMENTS_SCHEMA, OnInit} from '@angular/core';
import {AdresBoek, AdresBoeken, AdresboekService, AlgemeneResponse} from '../../../api/adresboek';
import {ActivatedRoute, Router} from "@angular/router";
import {CommonModule} from "@angular/common";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {SideBarComponent} from "../../common/side-bar/side-bar.component";
import {HeaderMetToevoegButtonComponent} from "../../common/header-met-toevoeg-button/header-met-toevoeg-button.component";
import {OpslaanEnAfbrekenButtonsComponent} from "../../common/opslaan-en-afbreken-buttons/opslaan-en-afbreken-buttons.component";
import {ModalComponent} from "../../common/modal/modal.component";

@Component({
  selector: 'app-lijst-adres-boeken',
  templateUrl: './lijst-adres-boeken.component.html',
  styleUrl: './lijst-adres-boeken.component.css',
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SideBarComponent,
    HeaderMetToevoegButtonComponent,
    OpslaanEnAfbrekenButtonsComponent,
    ModalComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class LijstAdresBoekenComponent implements OnInit {
  modalTonen: boolean = false;
  adresBoeken: AdresBoek[] = [];
  teVerwijderenAdresBoek: AdresBoek | undefined;

  constructor(
    private activatedRoute: ActivatedRoute,
    private adresboekService: AdresboekService,
    private router: Router
  ) {
  }

  ngOnInit(): void {
    this.activatedRoute.data.subscribe((response: any) => {
      this.adresBoeken = response.adresBoeken.adresBoeken;
    });
  }

  voegAdresBoekToe() {
    this.router.navigate(['adresboek', '0', 'nieuw'])
  }

  verwijder(id: number) {
    this.adresboekService.verwijderAdresBoek(id).subscribe((response: AlgemeneResponse) => {
      this.adresboekService.lijstAdresBoeken().subscribe((adresBoeken: AdresBoeken) => {
        this.adresBoeken = adresBoeken.adresBoeken;
      });
    });
  }

  getOmschrijving(adresBoek: AdresBoek | undefined): string {
    return '';
  }

  doeVerwijder() {
  }

  verbergModal() {
  }

  gezinslid(gezinslidsid: number | undefined): string {
    return '';
  }
}

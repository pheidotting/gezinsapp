package nl.gezinsapp.mapper;

import nl.gezinsapp.domain.VerlanglijstItem;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;

import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;

@Mapper(componentModel = "CDI")
public abstract class VerlanglijstItemMapper {
    public abstract VerlanglijstItem map(org.openapi.quarkus.verlanglijstje_openapi_yaml.model.VerlanglijstjeItem verlanglijstjeItem);

    public abstract VerlanglijstItem update(org.openapi.quarkus.verlanglijstje_openapi_yaml.model.VerlanglijstjeItem verlanglijstjeItem, @MappingTarget VerlanglijstItem verlanglijstItem);

    protected LocalDateTime map(OffsetDateTime value) {
        return value == null ? null : value.toLocalDateTime();
    }

    protected OffsetDateTime map(LocalDateTime value) {
        return value == null ? null : OffsetDateTime.of(value, ZoneOffset.ofHours(2));
    }
}

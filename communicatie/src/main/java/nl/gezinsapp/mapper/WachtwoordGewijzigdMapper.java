package nl.gezinsapp.mapper;

import nl.gezinsapp.messaging.model.WachtwoordGewijzigd;
import nl.gezinsapp.model.CommWachtwoordGewijzigd;
import org.mapstruct.Mapper;

@Mapper(componentModel = "CDI")
public interface WachtwoordGewijzigdMapper {
    CommWachtwoordGewijzigd map(WachtwoordGewijzigd wijzigWachtwoordAangemaakt);
}

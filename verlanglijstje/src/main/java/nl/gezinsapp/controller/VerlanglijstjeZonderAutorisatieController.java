package nl.gezinsapp.controller;

import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.gezinsapp.commons.aspect.MethodeLogging;
import nl.gezinsapp.commons.aspect.TrackAndTraceId;
import nl.gezinsapp.domain.Verlanglijst;
import nl.gezinsapp.mapper.VerlanglijstMapper;
import org.openapi.quarkus.verlanglijstje_openapi_yaml.api.VerlanglijstjeZonderAutorisatieApi;
import org.openapi.quarkus.verlanglijstje_openapi_yaml.model.AlgemeneResponse;
import org.openapi.quarkus.verlanglijstje_openapi_yaml.model.Verlanglijstje;

import java.time.LocalDateTime;

@Slf4j
@RequiredArgsConstructor
public class VerlanglijstjeZonderAutorisatieController implements VerlanglijstjeZonderAutorisatieApi {
    private final VerlanglijstMapper verlanglijstMapper;

    @Override
    @Transactional
    @TrackAndTraceId
    @MethodeLogging
    public Verlanglijstje leesVerlanglijstje(String code) {
        var lijst = Verlanglijst.find("code", code).firstResultOptional();

        if (lijst.isPresent()) {
            return verlanglijstMapper.maps((Verlanglijst) lijst.get());
        } else {
            return new Verlanglijstje();
        }
    }

    @Override
    @Transactional
    @TrackAndTraceId
    @MethodeLogging
    public AlgemeneResponse streepItemAfVerlanglijstje(String code, Long id) {
        var lijst = Verlanglijst.find("code", code).firstResultOptional();

        if (lijst.isPresent()) {
            ((Verlanglijst) lijst.get()).getVerlanglijstItems().stream()
                    .filter(verlanglijstItem -> verlanglijstItem.getId().equals(id))
                    .findFirst().ifPresent(verlanglijstItem -> verlanglijstItem.setDatumAfgestreept(LocalDateTime.now()));
        }
        return new AlgemeneResponse();
    }
}

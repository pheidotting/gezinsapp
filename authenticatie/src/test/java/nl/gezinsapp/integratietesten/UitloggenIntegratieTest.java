package nl.gezinsapp.integratietesten;

import io.quarkus.test.junit.QuarkusTest;
import lombok.extern.slf4j.Slf4j;
import nl.gezinsapp.model.RefreshToken;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.openapi.quarkus.authenticatie_openapi_yaml.model.UitloggenRequest;

import java.time.Instant;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertTrue;

@Slf4j
@QuarkusTest
class UitloggenIntegratieTest extends AbstractIntegratieTest {
    @Test
    @DisplayName("Test Uitloggen")
    void test() {
        var refreshToken = new RefreshToken(
                "accessToken",
                33L,
                Instant.now());
        var token = UUID.randomUUID();
        refreshTokenRepository.set(refreshToken, token);

        assertTrue(refreshTokenRepository.get(token).isPresent());

        UitloggenRequest uitloggenRequest = new UitloggenRequest();
        uitloggenRequest.setRefreshToken(token.toString());

        postViaApiNoResponse("/api/authorisatie/uitloggen", uitloggenRequest, null);

        assertTrue(refreshTokenRepository.get(token).isEmpty());
    }
}

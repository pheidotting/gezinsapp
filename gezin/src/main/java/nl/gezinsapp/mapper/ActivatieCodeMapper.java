package nl.gezinsapp.mapper;

import nl.gezinsapp.model.ActivatieCode;
import org.mapstruct.Mapper;
import org.openapi.quarkus.gezin_openapi_yaml.model.ActiveringsCode;
@Mapper(componentModel = "CDI")
public interface ActivatieCodeMapper {
    ActiveringsCode map(ActivatieCode activatieCode);
}

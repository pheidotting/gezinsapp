import {Pipe, PipeTransform} from '@angular/core';
import {GezinslidGeslachtEnum} from '../../api/gezin';

@Pipe({
  name: 'maakGeslachtEnumOp',
  standalone: true
})
export class MaakGeslachtEnumOpPipe implements PipeTransform {

  transform(geslacht: GezinslidGeslachtEnum, ...args: unknown[]): unknown {
    return geslacht.substring(0, 1).toUpperCase() + geslacht.substring(1).toLowerCase();
  }

}

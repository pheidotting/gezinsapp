package nl.gezinsapp.service;

import jakarta.enterprise.context.RequestScoped;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.gezinsapp.ActivatieCodeNietMeegegevenException;
import nl.gezinsapp.ActivatieCodeOnjuistException;
import nl.gezinsapp.commons.aspect.MethodeLogging;
import nl.gezinsapp.messaging.model.GezinAangemeld;
import nl.gezinsapp.messaging.sender.Sender;
import nl.gezinsapp.model.Gezin;
import nl.gezinsapp.model.Gezinslid;
import nl.gezinsapp.model.SoortGezinslid;
import nl.gezinsapp.model.repository.ActivatieCodeRepository;
import nl.gezinsapp.model.repository.GezinRepository;
import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.openapi.quarkus.authenticatie_openapi_yaml.api.WachtwoordApi;
import org.openapi.quarkus.authenticatie_openapi_yaml.model.GenereerSaltEnHashWachtwoordRequest;
import org.openapi.quarkus.gezin_openapi_yaml.model.ControleerActiveringsCode;

import java.util.List;
import java.util.Optional;

import static nl.gezinsapp.service.CommunicatieMessagingObjectUtil.vulObject;

@Slf4j
@RequestScoped
@RequiredArgsConstructor
public class GezinService {
    private final GezinRepository gezinRepository;
    private final Sender sender;
    private final ActivatieCodeRepository activatieCodeRepository;
    @RestClient
    private WachtwoordApi wachtwoordApi;

    @Transactional
    @MethodeLogging
    public Gezin opslaan(Gezin gezin) {
        gezin.getGezinsleden().forEach(gezinslid -> gezinslid.setGezin(gezin));
        log.info("Opslaan {}", gezin);
        gezinRepository.persist(gezin);

        return gezin;
    }

    @MethodeLogging
    public Gezin aanmeldenGezin(Gezin gezin, ControleerActiveringsCode controleerActiveringsCode) throws ActivatieCodeOnjuistException, ActivatieCodeNietMeegegevenException {
        if (controleerActiveringsCode == null) {
            throw new ActivatieCodeNietMeegegevenException();
        }
        var activatieCode = activatieCodeRepository.get(controleerActiveringsCode.getId());
        if (activatieCode.isEmpty() || !activatieCode.get().equals(controleerActiveringsCode.getCode())) {
            throw new ActivatieCodeOnjuistException();
        }

        var eersteGezinslid = vindGezinslid(gezin);

        var genereerSaltEnHashWachtwoordResponse = wachtwoordApi.genereerSaltEnHashWachtwoord(
                new GenereerSaltEnHashWachtwoordRequest()
                        .emailadres(eersteGezinslid.getEmailadres())
                        .wachtwoord(eersteGezinslid.getWachtwoord())
        );

        eersteGezinslid.setSalt(genereerSaltEnHashWachtwoordResponse.getSalt());
        eersteGezinslid.setWachtwoord(genereerSaltEnHashWachtwoordResponse.getHashwachtwoord());
        var gezinOpgeslagen = opslaan(gezin);

        var gezinAngemeld = vulObject(eersteGezinslid,
                GezinAangemeld.builder().build()
        );

        sender.verstuurGezinAangemeld(gezinAngemeld);

        return gezinOpgeslagen;
    }

    @Transactional
    @MethodeLogging
    public Optional<Gezin> findById(Long id) {
        return gezinRepository.findByIdOptional(id);
    }

    @Transactional
    @MethodeLogging
    public List<Gezin> findAll() {
        return gezinRepository.findAll().stream().toList();
    }

    protected Gezinslid vindGezinslid(Gezin gezin) {
        return gezin.getGezinsleden().stream()
                .filter(gezinslid -> gezinslid.getSoortGezinslid().equals(SoortGezinslid.OUDER))
                .filter(gezinslid -> gezinslid.getEmailadres() != null && !"".equals(gezinslid.getEmailadres()))
                .findFirst().orElse(new Gezinslid());
    }
}

package nl.gezinsapp.service;

import io.quarkus.qute.Template;
import jakarta.enterprise.context.ApplicationScoped;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.gezinsapp.model.CommJarige;
import org.openapi.quarkus.gezin_openapi_yaml.model.LeesGezin;

@Slf4j
@ApplicationScoped
@RequiredArgsConstructor
public class JarigeVandaagService extends AbstractService<CommJarige> {
    private final Template jarigeVandaag;

    @Override
    public void vulCommObject(CommJarige commJarige, LeesGezin gezin) {
        gezin.getGezinsleden().stream()
                .filter(gezinslid -> gezinslid.getId() == commJarige.getGezinslidId())
                .findFirst()
                .ifPresent(gezinslid -> {
                    commJarige.setAchternaam(gezinslid.getAchternaam());
                    commJarige.setTussenvoegsel(gezinslid.getTussenvoegsel());
                    commJarige.setVoornaam(gezinslid.getVoornaam());
                });

    }

    @Override
    public Template getTemplate() {
        return jarigeVandaag;
    }

    @Override
    public String getOnderwerp() {
        return "Gefeliciteerd met je verjaardag!";
    }
}

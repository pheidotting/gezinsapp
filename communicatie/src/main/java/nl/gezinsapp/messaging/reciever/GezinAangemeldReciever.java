package nl.gezinsapp.messaging.reciever;

import nl.gezinsapp.commons.messaging.AbstractReciever;

import io.smallrye.common.annotation.Blocking;
import jakarta.enterprise.context.ApplicationScoped;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.gezinsapp.commons.aspect.MethodeLogging;
import nl.gezinsapp.mapper.GezinAangemeldMapper;
import nl.gezinsapp.messaging.model.GezinAangemeld;
import nl.gezinsapp.service.GezinAangemeldService;
import org.eclipse.microprofile.reactive.messaging.Incoming;
import org.eclipse.microprofile.reactive.messaging.Message;
import org.slf4j.MDC;

import java.util.concurrent.CompletionStage;
@Slf4j
@ApplicationScoped
@RequiredArgsConstructor
public class GezinAangemeldReciever extends AbstractReciever<GezinAangemeld> {
    private final GezinAangemeldService gezinAangemeldService;
    private final GezinAangemeldMapper gezinAangemeldMapper;

    @Incoming("gezin-aangemeld")
    @MethodeLogging
    @Blocking
    public CompletionStage<Void> verwerk(Message<GezinAangemeld> m) {
        var gezinAangemeld = getObject(m, GezinAangemeld.class);

        if(gezinAangemeld.getTrackAndTraceId()!=null){
            MDC.put("trackAndTraceId",gezinAangemeld.getTrackAndTraceId().toString());
        }

        log.info("Ontvangen : {}", gezinAangemeld);

        gezinAangemeldService.maakEmail(gezinAangemeldMapper.map(gezinAangemeld));

        return m.ack();
    }


}

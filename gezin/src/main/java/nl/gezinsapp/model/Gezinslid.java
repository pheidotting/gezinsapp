package nl.gezinsapp.model;

import jakarta.persistence.Column;
import jakarta.persistence.DiscriminatorValue;
import jakarta.persistence.Entity;
import jakarta.persistence.Enumerated;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.time.LocalDate;

import static jakarta.persistence.EnumType.STRING;

@Entity
@Getter
@Setter
@ToString(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@DiscriminatorValue(value = "G")
public class Gezinslid extends Gebruiker {
    @ManyToOne
    @JoinColumn(name = "GEZIN")
    @ToString.Exclude
    private Gezin gezin;
    @Enumerated(STRING)
    @Column(name = "SOORTGEZINSLID")
    private SoortGezinslid soortGezinslid;
    @Enumerated(STRING)
    @Column(name = "GESLACHT")
    private Geslacht geslacht;
    @Column(name = "GEBOORTEDATUM")
    private LocalDate geboorteDatum;
    @Column(name = "avataraanwezig")
    private boolean avatarAanwezig;

    @Override
    public String rol() {
        return this.getClass().getSimpleName();
    }
}

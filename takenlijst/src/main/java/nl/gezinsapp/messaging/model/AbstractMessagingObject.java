package nl.gezinsapp.messaging.model;


import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.UUID;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode
public abstract class AbstractMessagingObject {
    private String emailadres;
    private Long gezinsId;
    private Long gezinslidId;
    private UUID trackAndTraceId;
}

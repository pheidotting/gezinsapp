package nl.gezinsapp.integratietesten;

import io.quarkus.test.junit.QuarkusTest;
import lombok.extern.slf4j.Slf4j;
import nl.gezinsapp.integratietesten.common.QueueUtil;
import nl.gezinsapp.integratietesten.common.TestGegevens;
import nl.gezinsapp.messaging.model.Jarige;
import nl.gezinsapp.messaging.model.JarigenVandaag;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.openapi.quarkus.gezin_openapi_yaml.model.Gezin;
import org.openapi.quarkus.gezin_openapi_yaml.model.Gezinslid;

import java.time.Duration;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static io.restassured.RestAssured.given;
import static org.awaitility.Awaitility.await;
import static org.junit.jupiter.api.Assertions.*;
import io.quarkus.test.common.QuarkusTestResource;

@Slf4j
@QuarkusTest
@QuarkusTestResource(MockServer.class)
class JarigenVandaagIntegratieTest extends AbstractIntegratieTest {
    @ConfigProperty(name = "mp.messaging.incoming.jarigenvandaag.exchange.name")
    private String jarigenVandaagExchange;
    @ConfigProperty(name = "rabbitmq-host")
    private String rabbitHost;
    @ConfigProperty(name = "rabbitmq-port")
    private String rabbitPort;
    @ConfigProperty(name = "rabbitmq-username")
    private String rabbitUsername;
    @ConfigProperty(name = "rabbitmq-password")
    private String rabbitPassword;

    @Test
    @DisplayName("Test een e-mail voor een jarige")
    void testWijzigWachtwoord() {
        maakDatabaseLeeg();
        given().when().delete("http://localhost:" + mailpitContainer.getMappedPort(8025) + "/api/v1/messages");

        TestGegevens.setRabbitHost(rabbitHost);
        TestGegevens.setRabbitPort(Integer.valueOf(rabbitPort));
        TestGegevens.setRabbitUser(rabbitUsername);
        TestGegevens.setRabbitPassword(rabbitPassword);

        var gezin = new Gezin();
        gezin.setNaam("Jarigen : The Simpsons");
        var gezinslid = new Gezinslid();
        gezinslid.setEmailadres("b@c.d");
        gezinslid.setSoortGezinslid(Gezinslid.SoortGezinslidEnum.OUDER);
        gezinslid.setId(46L);
        gezinslid.setVoornaam("Jan");
        gezinslid.setAchternaam("Jansen");
        gezin.getGezinsleden().add(gezinslid);
        mockGezin(3L, gezin);

        var jarige = new Jarige();
        jarige.setGezinslidId(46L);
        jarige.setGezinsId(3L);
        var bericht = new JarigenVandaag(List.of(jarige));
        maakJarigenVandaagQueueUtil().stuurBerichtNaarExchange(bericht);

        await()
                .atMost(40, TimeUnit.SECONDS)
                .pollInterval(Duration.ofMillis(500))
                .until(() -> {
                    var email = leesEmail();
                    assertEquals("Gefeliciteerd met je verjaardag!", email.getOnderwerp());
                    assertEquals("b@c.d\n" +
                            "Jan\n" +
                            "\n" +
                            "Jansen", email.getTekst());
                    assertNotNull(email.getTijdstipaangemaakt());
                    assertNull(email.getTijdstipverstuurd());
                    return true;
                });

        given().when().get("/verstuurmails");

        var response = given()
                .when().get("http://localhost:" + mailpitContainer.getMappedPort(8025) + "/api/v1/messages");
        assertTrue(response.getBody().prettyPrint().contains("\"messages_count\": 1,"));
        assertTrue(response.getBody().prettyPrint().contains("\"Address\": \"gezinsapp@heidotting.nl\""));
        assertTrue(response.getBody().prettyPrint().contains("\"Address\": \"b@c.d\""));
        assertTrue(response.getBody().prettyPrint().contains("\"Subject\": \"Gefeliciteerd met je verjaardag!\","));
        assertTrue(response.getBody().prettyPrint().contains("\"Snippet\": \"b@c.d Jan Jansen\""));

        var email = leesEmail();
        assertNotNull(email.getTijdstipaangemaakt());
        assertNotNull(email.getTijdstipverstuurd());
    }

    private QueueUtil<JarigenVandaag> maakJarigenVandaagQueueUtil() {
        return new QueueUtil<>() {
            @Override
            public String getExchange() {
                return jarigenVandaagExchange;
            }

            @Override
            public Class<JarigenVandaag> getClazz() {
                return JarigenVandaag.class;
            }
        };
    }
}

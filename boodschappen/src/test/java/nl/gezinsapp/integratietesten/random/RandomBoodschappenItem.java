package nl.gezinsapp.integratietesten.random;

import org.openapi.quarkus.boodschappen_openapi_yaml.model.BoodschappenItem;
import uk.co.jemos.podam.api.DefaultClassInfoStrategy;
import uk.co.jemos.podam.api.PodamFactory;
import uk.co.jemos.podam.api.PodamFactoryImpl;

public class RandomBoodschappenItem {
    private static PodamFactory factory;

    private static PodamFactory getFactory() {
        if (factory == null) {
            factory = new PodamFactoryImpl();
            DefaultClassInfoStrategy classInfoStrategy = DefaultClassInfoStrategy.getInstance();
            classInfoStrategy.addExcludedField(BoodschappenItem.class, "id");
            classInfoStrategy.addExcludedField(BoodschappenItem.class, "gezin");
            classInfoStrategy.addExcludedField(nl.gezinsapp.model.BoodschappenItem.class, "id");
            classInfoStrategy.addExcludedField(nl.gezinsapp.model.BoodschappenItem.class, "gezin");
            factory.setClassStrategy(classInfoStrategy);
        }
        return factory;
    }

    public static BoodschappenItem randomDtoBoodschappenItem() {
        return getFactory().manufacturePojo(BoodschappenItem.class);
    }

    public static nl.gezinsapp.model.BoodschappenItem randomBoodschappenItem(Long gezin) {
        var item = getFactory().manufacturePojo(nl.gezinsapp.model.BoodschappenItem.class);
        item.setGezin(gezin);
        return item;
    }
}

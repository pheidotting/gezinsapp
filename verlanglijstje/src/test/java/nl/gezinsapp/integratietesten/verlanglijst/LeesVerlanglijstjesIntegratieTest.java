package nl.gezinsapp.integratietesten.verlanglijst;

import com.fasterxml.jackson.core.JsonProcessingException;
import io.quarkus.test.junit.QuarkusTest;
import lombok.extern.slf4j.Slf4j;
import nl.gezinsapp.integratietesten.AbstractIntegratieTest;
import nl.gezinsapp.integratietesten.MockServer;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockserver.model.JsonBody;
import org.openapi.quarkus.gezin_openapi_yaml.model.Gezin;
import org.openapi.quarkus.gezin_openapi_yaml.model.Gezinslid;
import org.openapi.quarkus.verlanglijstje_openapi_yaml.model.Verlanglijstjes;

import static nl.gezinsapp.integratietesten.random.RandomVerlanglijst.randomDomainVerlanglijst;
import static nl.gezinsapp.integratietesten.random.RandomVerlanglijstItem.randomDomainVerlanglijstItem;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockserver.model.HttpRequest.request;
import static org.mockserver.model.HttpResponse.response;
import io.quarkus.test.common.QuarkusTestResource;

@Slf4j
@QuarkusTest
@QuarkusTestResource(MockServer.class)
class LeesVerlanglijstjesIntegratieTest extends AbstractIntegratieTest {
    @Test
    @DisplayName("Haal alle verlanglijstjes van 1 gezin op")
    void test() throws JsonProcessingException {
        maakDatabaseLeeg();

        var accesstoken = maakToken();
        var jwtResponse = mockCheckJwt(accesstoken);

        MockServer.getMockServerClient()
                .when(
                        request()
                                .withMethod("GET")
                                .withPath("/api/gezin/" + jwtResponse.getGebruiker().getGezin())
                )
                .respond(
                        response()
                                .withStatusCode(200)
                                .withHeader("Content-Type", "application/json")
                                .withBody(JsonBody.json(gezin(jwtResponse.getGebruiker().getGezin(), jwtResponse.getGebruiker().getId())))
                );

        var verlanglijst1 = randomDomainVerlanglijst(jwtResponse.getGebruiker().getId());
        var verlanglijst2 = randomDomainVerlanglijst(33L);

        opslaan(verlanglijst1);
        opslaan(verlanglijst2);

        var item = randomDomainVerlanglijstItem();
        item.setVerlanglijst(verlanglijst1);
        opslaan(item);

        var lijsten = leesViaApi(Verlanglijstjes.class, "/api/verlanglijstje/verlanglijstjes", accesstoken);

        assertEquals(1, lijsten.getVerlanglijstjes().size());
        assertEquals(verlanglijst1.getOmschrijving(), lijsten.getVerlanglijstjes().get(0).getOmschrijving());
        assertEquals(1, lijsten.getVerlanglijstjes().get(0).getVerlanglijstItems().size());
        assertEquals(item.getOmschrijving(), lijsten.getVerlanglijstjes().get(0).getVerlanglijstItems().get(0).getOmschrijving());
    }

    private Gezin gezin(Long id, Long eersteLidId) {
        var gezin = new Gezin();
        gezin.setId(id);

        var gezinslid1 = new Gezinslid();
        gezinslid1.setId(eersteLidId);

        var gezinslid2 = new Gezinslid();
        gezinslid2.setId(3L);

        gezin.getGezinsleden().add(gezinslid1);
        gezin.getGezinsleden().add(gezinslid2);

        return gezin;
    }
}

package nl.gezinsapp.service;

import jakarta.enterprise.context.RequestScoped;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.gezinsapp.WijzigWachtwoordCodeOnjuistException;
import nl.gezinsapp.commons.aspect.MethodeLogging;
import nl.gezinsapp.messaging.model.WachtwoordGewijzigd;
import nl.gezinsapp.messaging.model.WijzigWachtwoordCodeAangemaakt;
import nl.gezinsapp.messaging.sender.Sender;
import nl.gezinsapp.model.Gebruiker;
import nl.gezinsapp.model.WijzigWachtwoord;
import nl.gezinsapp.model.WijzigWachtwoordCode;
import nl.gezinsapp.model.repository.WijzigWachtwoordCodeRepository;
import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.openapi.quarkus.authenticatie_openapi_yaml.api.WachtwoordApi;
import org.openapi.quarkus.authenticatie_openapi_yaml.model.GenereerSaltEnHashWachtwoordRequest;

import java.util.UUID;

import static nl.gezinsapp.service.CommunicatieMessagingObjectUtil.vulObject;

@RequestScoped
@Slf4j
@RequiredArgsConstructor
public class WijzigWachtwoordService {
    private final WijzigWachtwoordCodeRepository activatieCodeRepository;
    private final GenereerCodeService genereerCodeService;
    private final Sender sender;
    @RestClient
    private WachtwoordApi wachtwoordApi;

    @MethodeLogging
    public WijzigWachtwoordCode genereerWijzigWachtwoordCode(String email) {
        var activatieCode = activatieCodeRepository.set(genereerCodeService.genereerCode());

        Gebruiker.find("emailadres", email).firstResultOptional()
                .ifPresentOrElse(panacheEntityBase -> {
                    var wijzigWachtwoordCodeAangemaakt = vulObject((Gebruiker) panacheEntityBase, WijzigWachtwoordCodeAangemaakt.builder()
                            .code(activatieCode.getCode())
                            .build());

                    log.info("{}", wijzigWachtwoordCodeAangemaakt.getCode());
                    sender.verstuurWijzigWachtwoordCodeAangemaakt(wijzigWachtwoordCodeAangemaakt);
                }, () -> log.debug("{} niet gevonden", email));

        return activatieCode;
    }

    @MethodeLogging
    public boolean controleerWijzigWachtwoordCode(UUID id, String code) {
        var codeInDb = activatieCodeRepository.get(id);

        if (codeInDb.isPresent()) {
            return codeInDb.get().equals(code);
        } else {
            return false;
        }
    }

    @MethodeLogging
    @Transactional
    public void wijzigWachtwoord(WijzigWachtwoord wijzigWachtwoord) throws WijzigWachtwoordCodeOnjuistException {
        var codeAkkoord = controleerWijzigWachtwoordCode(UUID.fromString(wijzigWachtwoord.getCodeid()), wijzigWachtwoord.getCode());

        if (!codeAkkoord) {
            throw new WijzigWachtwoordCodeOnjuistException();
        }

        Gebruiker.find("emailadres", wijzigWachtwoord.getEmail()).firstResultOptional()
                .ifPresent(panacheEntityBase -> {
                    var gebruiker = (Gebruiker) panacheEntityBase;

                    var genereerSaltEnHashWachtwoordResponse = wachtwoordApi.genereerSaltEnHashWachtwoord(
                            new GenereerSaltEnHashWachtwoordRequest()
                                    .salt(gebruiker.getSalt())
                                    .emailadres(gebruiker.getEmailadres())
                                    .wachtwoord(wijzigWachtwoord.getWachtwoord())
                    );
                    gebruiker.setSalt(genereerSaltEnHashWachtwoordResponse.getSalt());
                    gebruiker.setWachtwoord(genereerSaltEnHashWachtwoordResponse.getHashwachtwoord());

                    gebruiker.persist();

                    var wachtwoordGewijzigd = vulObject(gebruiker, WachtwoordGewijzigd.builder().build());

                    sender.verstuurWachtwoordGewijzigd(wachtwoordGewijzigd);
                });
    }
}

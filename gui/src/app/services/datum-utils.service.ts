import {Injectable} from '@angular/core';
import moment from 'moment';

@Injectable({
  providedIn: 'root'
})
export class DatumUtils {

  public volgendeKeerFromMoment(momentDatum: moment.Moment): moment.Moment {
    let nu: moment.Moment = moment();
    let m: moment.Moment;

    m = momentDatum.clone().year(nu.year());
    if (m.isBefore(nu)) {
      m.year(nu.year() + 1);
    }
    return m;
  }

  volgendeKeer(datum: string | undefined): moment.Moment {
    let momentDatum: moment.Moment = this.asMoment(datum);
    return this.volgendeKeerFromMoment(momentDatum);
  }

  asMoment(datum: string | undefined): moment.Moment {
    if (datum != undefined) {
      return moment(datum);
    } else {
      return moment();
    }
  }

  maakDatumOp(datum: string | undefined): string {
    return this.asMoment(datum).format('DD-MM-YYYY');
  }

  maakTijdOp(datum: string | undefined): string {
    return this.maakMomentTijdOp(this.asMoment(datum));
  }

  maakMomentTijdOp(datum: moment.Moment): string {
    return datum.format('HH:mm');
  }

  leeftijdFromMoment(datum: moment.Moment): number {
    return this.volgendeKeerFromMoment(datum.clone()).diff(datum, 'years');
  }

  leeftijd(datum: string | undefined): number {
    if (datum != undefined) {
      return this.leeftijdFromMoment(this.asMoment(datum))
    } else {
      return 0;
    }
  }

}

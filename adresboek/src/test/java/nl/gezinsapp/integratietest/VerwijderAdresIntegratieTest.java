package nl.gezinsapp.integratietest;

import com.fasterxml.jackson.core.JsonProcessingException;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.openapi.quarkus.adresboek_openapi_yaml.model.AlgemeneResponse;

import static nl.gezinsapp.integratietest.random.RandomAdresBoek.randomAdres;
import static nl.gezinsapp.integratietest.random.RandomAdresBoek.randomAdresBoek;
import static org.jboss.resteasy.reactive.RestResponse.StatusCode.FORBIDDEN;
import static org.jboss.resteasy.reactive.RestResponse.StatusCode.OK;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

@Slf4j
@QuarkusTest
@QuarkusTestResource(MockServer.class)
class VerwijderAdresIntegratieTest extends AbstractIntegratieTest {
    @Test
    @DisplayName("VerwijderAdresBoek")
    void test() throws JsonProcessingException {
        var accesstoken = maakToken();
        var jwtResponse = mockCheckJwt(accesstoken);

        maakDatabaseLeeg();

        var item = randomAdresBoek();
        item.setAdressen(null);
        item.setGezin(jwtResponse.getGebruiker().getGezin());
        opslaan(item);

        var adres = randomAdres();
        adres.setAdresBoek(item);
        opslaan(adres);

        var response = deleteViaApi(AlgemeneResponse.class, "/api/adresboek/adres", adres.getId(), accesstoken, OK);

        assertNull(response.getId());
        assertEquals(0, leesAdressen(item).size());
    }

    @Test
    @DisplayName("VerwijderAdresBoek met check op gezin")
    void testMetFoutGezin() throws JsonProcessingException {
        var accesstoken = maakToken();
        mockCheckJwt(accesstoken);

        maakDatabaseLeeg();

        var item = randomAdresBoek();
        item.setAdressen(null);
        item.setGezin(99L);
        opslaan(item);

        var adres = randomAdres();
        adres.setAdresBoek(item);
        opslaan(adres);

        assertNull(deleteViaApi(null, "/api/adresboek/adres", adres.getId(), accesstoken, FORBIDDEN));
    }
}

import {CanActivateFn, Router} from '@angular/router';
import {TokenService} from "../services/token.service";
import {inject} from "@angular/core";
import {map} from "rxjs";

export const beheerGuard: CanActivateFn = (route, state) => {
  var tokenService: TokenService = inject(TokenService);
  var router = inject(Router);

  return tokenService.accessToken.pipe(map((accessToken: string | null) => {
    let decodedJWT = JSON.parse(window.atob(accessToken!.split('.')[1]));

    if (decodedJWT.soortGebruiker.toLowerCase() == 'beheerder') {
      return true;
    } else {
      router.navigate(['/inloggen']);
      return false;
    }
  }));

};

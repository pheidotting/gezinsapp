package nl.gezinsapp.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.openapi.quarkus.kalender_openapi_yaml.model.SoortDienst;

@Mapper(componentModel = "CDI")
public interface SoortDienstMapper {
    SoortDienst map(nl.gezinsapp.model.SoortDienst soortDienst);
    nl.gezinsapp.model.SoortDienst map(SoortDienst soortDienst);
    nl.gezinsapp.model.SoortDienst update(@MappingTarget nl.gezinsapp.model.SoortDienst bestaandeSoortDienst, SoortDienst soortDienst);
}

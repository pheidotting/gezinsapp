package nl.gezinsapp.service;

import jakarta.enterprise.context.ApplicationScoped;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.gezinsapp.commons.aspect.MethodeLogging;
import nl.gezinsapp.exception.GebruikerNietGevondenException;
import nl.gezinsapp.mapper.GebruikerMapper;
import nl.gezinsapp.model.GebruikerImpl;
import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.openapi.quarkus.gezin_openapi_yaml.api.GebruikerApi;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;


@Slf4j
@ApplicationScoped
@RequiredArgsConstructor
public class GebruikerService {
    @RestClient
    private GebruikerApi gebruikerApi;

    private final GebruikerMapper gebruikerMapper;

    @MethodeLogging
    public GebruikerImpl getGebruiker(String subject) throws GebruikerNietGevondenException {
        var gebruiker = gebruikerApi.leesGebruiker(subject);

        if (gebruiker != null) {
            return gebruikerMapper.map(gebruiker);
        } else {
            throw new GebruikerNietGevondenException("Geen Gebruiker(s) gevonden met subject " + subject);
        }
    }

    @MethodeLogging
    public GebruikerImpl getGebruiker(Long id) throws GebruikerNietGevondenException {
        log.info("getGebruiker with id {}", id);
        var gebruiker = gebruikerApi.leesGebruikerOpId(id);

        if (gebruiker != null) {
            return gebruikerMapper.map(gebruiker);
        } else {
            throw new GebruikerNietGevondenException("Geen Gebruiker(s) gevonden met ID " + id);
        }
    }

    @MethodeLogging
    public GebruikerImpl setEmailadresEnWachtwoord(GebruikerImpl inloggendeGebruiker, String emailadres, String wachtwoord) {
        inloggendeGebruiker.setEmailadres(emailadres);
        try {
            setHashWachtwoord(inloggendeGebruiker, wachtwoord);
        } catch (NoSuchAlgorithmException e) {
            log.error("Fout opgetreden", e);
        }
        return inloggendeGebruiker;
    }

    @MethodeLogging
    public void setHashWachtwoord(GebruikerImpl gebruiker, String wachtwoord) throws NoSuchAlgorithmException {
        gebruiker.setWachtwoord(hash(wachtwoord + gebruiker.getSalt()));
    }

    @MethodeLogging
    public String hash(String tekst) throws NoSuchAlgorithmException {
        var md = MessageDigest.getInstance("SHA-512");
        // Change this to "UTF-16" if needed
        md.update(tekst.getBytes(StandardCharsets.UTF_8));

        return new BigInteger(1, md.digest()).toString(16);
    }
}

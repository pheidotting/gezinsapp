import {Injectable} from '@angular/core';
import {Gezinslid} from "../../api/gezin";
import moment from "moment";
import {TokenService} from "./token.service";
import {Observable, Observer} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class GezinslidUtil {

  constructor(
    private tokenService: TokenService
  ) {
  }

  maakNaamOp(gezinslid: Gezinslid): String {
    let naam: string = '';
    if (gezinslid != null) {
      naam += gezinslid.voornaam.trim();

      if (gezinslid.tussenvoegsel != null && gezinslid.tussenvoegsel != undefined) {
        naam += ' ' + gezinslid.tussenvoegsel.trim();
      }
      naam += ' ' + gezinslid.achternaam.trim();
    }
    return naam.trim();
  }

  sorteer(gezinsleden: Gezinslid[]): Gezinslid[] {
    return gezinsleden.sort((lid1: Gezinslid, lid2: Gezinslid): number => {
      let datum1: moment.Moment = moment(lid1.geboorteDatum);
      let datum2: moment.Moment = moment(lid2.geboorteDatum);
      if (datum1.isBefore(datum2)) {
        return -1;
      } else if (datum2.isBefore(datum1)) {
        return 1;
      } else {
        return 0;
      }
    });
  }

  haalIngelogdGezinslid(gezinsleden: Gezinslid[]): Observable<Gezinslid> {
    return new Observable((observer: Observer<Gezinslid>) => {
      this.tokenService.accessToken.subscribe((token: string | null) => {
        let decodedJWT = JSON.parse(window.atob(token!.split('.')[1]));

        let gezinslid = gezinsleden.filter((gezinslid: Gezinslid) => {
          return gezinslid.id == decodedJWT.userId;
        })[0];
        observer.next(gezinslid);
      });
    });
  }
}

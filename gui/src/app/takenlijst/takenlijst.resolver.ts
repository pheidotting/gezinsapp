import {ActivatedRouteSnapshot, Resolve} from '@angular/router';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {LijstTaken, TakenlijstService} from '../../api/takenlijst';


@Injectable({
  providedIn: 'root'
})
export class LijstTakenResolver implements Resolve<LijstTaken> {
  constructor(
    private readonly takenlijstService: TakenlijstService
  ) {
  }

  resolve(route: ActivatedRouteSnapshot): Observable<LijstTaken> {
    return this.takenlijstService.alleTaken();
  }
}

package nl.gezinsapp.integratietesten.verlanglijst;

import com.fasterxml.jackson.core.JsonProcessingException;
import io.quarkus.test.junit.QuarkusTest;
import lombok.extern.slf4j.Slf4j;
import nl.gezinsapp.integratietesten.AbstractIntegratieTest;
import nl.gezinsapp.integratietesten.MockServer;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockserver.model.JsonBody;
import org.openapi.quarkus.gezin_openapi_yaml.model.AlgemeneResponse;
import org.openapi.quarkus.gezin_openapi_yaml.model.Gezin;
import org.openapi.quarkus.gezin_openapi_yaml.model.Gezinslid;

import static nl.gezinsapp.integratietesten.random.RandomVerlanglijst.randomDomainVerlanglijst;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockserver.model.HttpRequest.request;
import static org.mockserver.model.HttpResponse.response;
import io.quarkus.test.common.QuarkusTestResource;

@Slf4j
@QuarkusTest
@QuarkusTestResource(MockServer.class)
class VerwijderVerlanglijstjesIntegratieTest extends AbstractIntegratieTest {
    @Test
    @DisplayName("Haal alle verlanglijstjes van 1 gezin op")
    void test() throws JsonProcessingException {
        maakDatabaseLeeg();

        var accesstoken = maakToken();
        var jwtResponse = mockCheckJwt(accesstoken);

        MockServer.getMockServerClient()
                .when(
                        request()
                                .withMethod("GET")
                                .withPath("/api/gezin/" + jwtResponse.getGebruiker().getGezin())
                )
                .respond(
                        response()
                                .withStatusCode(200)
                                .withHeader("Content-Type", "application/json")
                                .withBody(JsonBody.json(gezin(jwtResponse.getGebruiker().getGezin(), jwtResponse.getGebruiker().getId())))
                );

        var verlanglijst1 = randomDomainVerlanglijst(jwtResponse.getGebruiker().getId());
        var verlanglijst2 = randomDomainVerlanglijst(33L);

        opslaan(verlanglijst1);
        opslaan(verlanglijst2);

        assertEquals(2,aantalVerlanglijsten());

        var response = deleteViaApi(AlgemeneResponse.class, "/api/verlanglijstje/verlanglijstjes",verlanglijst1.getId() ,accesstoken);
        assertEquals(verlanglijst1.getId(),response.getId());
        assertNull(response.getFoutmeldingen());

        assertEquals(1,aantalVerlanglijsten());

         response = deleteViaApi(AlgemeneResponse.class, "/api/verlanglijstje/verlanglijstjes",verlanglijst2.getId() ,accesstoken);
        assertNull(response.getId());
assertEquals(1,response.getFoutmeldingen().getMeldingen().size());
assertEquals("Betreffende verlanglijst is geen onderdeel van het huidige gezin.",response.getFoutmeldingen().getMeldingen().get(0));
    }

    private Gezin gezin(Long id, Long eersteLidId) {
        var gezin = new Gezin();
        gezin.setId(id);

        var gezinslid1 = new Gezinslid();
        gezinslid1.setId(eersteLidId);

        var gezinslid2 = new Gezinslid();
        gezinslid2.setId(3L);

        gezin.getGezinsleden().add(gezinslid1);
        gezin.getGezinsleden().add(gezinslid2);

        return gezin;
    }
}

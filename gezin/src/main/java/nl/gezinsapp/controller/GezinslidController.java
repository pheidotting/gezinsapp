package nl.gezinsapp.controller;

import io.smallrye.common.annotation.RunOnVirtualThread;
import jakarta.transaction.Transactional;
import jakarta.ws.rs.Path;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.gezinsapp.commons.aspect.AlsGezinslid;
import nl.gezinsapp.commons.aspect.MethodeLogging;
import nl.gezinsapp.commons.aspect.TrackAndTraceId;
import nl.gezinsapp.mapper.GezinslidMapper;
import nl.gezinsapp.model.repository.GezinRepository;
import nl.gezinsapp.model.repository.GezinslidRepository;
import nl.gezinsapp.service.GezinslidService;
import org.openapi.quarkus.gezin_openapi_yaml.api.GezinslidApi;
import org.openapi.quarkus.gezin_openapi_yaml.model.AlgemeneResponse;
import org.openapi.quarkus.gezin_openapi_yaml.model.Foutmeldingen;
import org.openapi.quarkus.gezin_openapi_yaml.model.GezinslidMetId;
import org.openapi.quarkus.gezin_openapi_yaml.model.GezinslidZonderId;
import org.openapi.quarkus.gezin_openapi_yaml.model.VerwijderGezinslid;

import static nl.gezinsapp.util.FoutmeldingUtil.mapExceptionNaarFoutmeldingen;

@Slf4j
@Path("/api/gezin/gezinslid")
@RequiredArgsConstructor
public class GezinslidController implements GezinslidApi {
    private final GezinslidRepository gezinslidRepository;
    private final GezinslidService gezinslidService;
    private final GezinRepository gezinRepository;
    private final GezinslidMapper gezinslidMapper;

    @Override
    @RunOnVirtualThread
    @AlsGezinslid
    @MethodeLogging
    @Transactional
    @TrackAndTraceId
    public AlgemeneResponse verwijderGezinslid(VerwijderGezinslid verwijderGezinslid) {
        gezinslidService.verwijder(verwijderGezinslid.getId());

        return new AlgemeneResponse().foutmeldingen(new Foutmeldingen());
    }

    @Override
    @Transactional
    @RunOnVirtualThread
    @AlsGezinslid
    @MethodeLogging
    @TrackAndTraceId
    public AlgemeneResponse opslaanGezinslid(GezinslidMetId gezinslidMetId) {
        try {
            gezinslidService.opslaan(gezinslidMapper.update(gezinslidRepository.findById(gezinslidMetId.getId()), gezinslidMetId), gezinslidMetId.getGezin());
        } catch (Exception e) {
            return new AlgemeneResponse().foutmeldingen(new Foutmeldingen().meldingen(mapExceptionNaarFoutmeldingen(e)));
        }

        return new AlgemeneResponse().foutmeldingen(new Foutmeldingen());
    }

    @Override
    @Transactional
    @RunOnVirtualThread
    @AlsGezinslid
    @MethodeLogging
    @TrackAndTraceId
    public AlgemeneResponse opslaanNieuwGezinslid(GezinslidZonderId gezinslidZonderId) {
        try {
            gezinslidService.opslaan(gezinslidMapper.map(gezinslidZonderId), gezinslidZonderId.getGezin());
        } catch (Exception e) {
            return new AlgemeneResponse().foutmeldingen(new Foutmeldingen().meldingen(mapExceptionNaarFoutmeldingen(e)));
        }

        return new AlgemeneResponse().foutmeldingen(new Foutmeldingen());
    }
}

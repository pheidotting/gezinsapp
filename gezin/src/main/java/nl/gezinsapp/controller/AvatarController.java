package nl.gezinsapp.controller;

import jakarta.transaction.Transactional;
import jakarta.ws.rs.core.Response;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.gezinsapp.aspect.ThreadVariabelen;
import nl.gezinsapp.commons.aspect.AlsGezinslid;
import nl.gezinsapp.commons.aspect.MethodeLogging;
import nl.gezinsapp.commons.aspect.TrackAndTraceId;
import nl.gezinsapp.service.AvatarService;
import org.openapi.quarkus.gezin_openapi_yaml.api.AvatarApi;
import org.openapi.quarkus.gezin_openapi_yaml.model.AlgemeneResponse;
import org.openapi.quarkus.gezin_openapi_yaml.model.Foutmeldingen;

import java.io.File;
import java.io.IOException;

@Slf4j
@RequiredArgsConstructor
public class AvatarController implements AvatarApi {
    private final AvatarService avatarService;

    @Override
    @Transactional
    @TrackAndTraceId
    @MethodeLogging
    public File downloadAvatar(Long gezinslidId) {
        return avatarService.download(gezinslidId);
    }

    @Override
    @Transactional
    @TrackAndTraceId
    @MethodeLogging
    @AlsGezinslid
    public AlgemeneResponse uploadAvatar(UploadAvatarMultipartForm multipartForm) {
        var gebruiker = ThreadVariabelen.get();

        var response = new AlgemeneResponse();
        try {
            avatarService.upload(gebruiker, multipartForm.gezinslidId, multipartForm.bijlage);
        } catch (IOException e) {
            response.setFoutmeldingen(new Foutmeldingen());
            response.getFoutmeldingen().addMeldingenItem(e.getMessage());
        }

        return response;
    }

    @Override
    @Transactional
    @TrackAndTraceId
    @MethodeLogging
    @AlsGezinslid
    public Response verwijderAvatar(Long gezinslidId) {
        var gebruiker = ThreadVariabelen.get();

        try {
            avatarService.verwijder(gebruiker, gezinslidId);
        } catch (IOException e) {
            log.error("Fout opgetreden {}", e.getMessage());
        }

        return Response.ok().build();
    }
}

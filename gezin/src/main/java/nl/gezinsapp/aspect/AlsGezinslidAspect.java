package nl.gezinsapp.aspect;

import io.vertx.core.http.HttpServerRequest;
import jakarta.annotation.Priority;
import jakarta.interceptor.AroundInvoke;
import jakarta.interceptor.Interceptor;
import jakarta.interceptor.InvocationContext;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.HttpHeaders;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.gezinsapp.commons.aspect.AlsGezinslid;
import nl.gezinsapp.commons.aspect.NietIngelogdException;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.openapi.quarkus.authenticatie_openapi_yaml.api.JwtApi;
import org.openapi.quarkus.authenticatie_openapi_yaml.model.CheckJWTRequest;
import org.openapi.quarkus.authenticatie_openapi_yaml.model.Gebruiker;
import org.slf4j.MDC;

import static jakarta.ws.rs.core.HttpHeaders.AUTHORIZATION;

@Slf4j
@AlsGezinslid
@Priority(2020)
@Interceptor
@RequiredArgsConstructor
public class AlsGezinslidAspect {
    private final HttpServerRequest request;
    @Context
    HttpHeaders headers;
    @ConfigProperty(name = "quarkus.application.name")
    private String applicatieNaam;
    @ConfigProperty(name = "quarkus.application.omgeving")
    private String omgeving;
    @RestClient
    private JwtApi jwtApi;

    @AroundInvoke
    Object logInvocation(InvocationContext context) throws Exception {
        MDC.put("applicatie-naam", applicatieNaam);
        MDC.put("omgeving", omgeving);
        var token = headers.getHeaderString(AUTHORIZATION);

        if (token == null) {
            throw new NietIngelogdException();
        } else {
            token = token.replace("Bearer", "").trim();
            var jwtResponse = jwtApi.checkJwt(new CheckJWTRequest().accessToken(token));

            if (jwtResponse.getGebruiker() == null) {
                throw new NietIngelogdException();
            }

            MDC.put("user-id", jwtResponse.getGebruiker().getId().toString());

            ThreadVariabelen.set(jwtResponse.getGebruiker());

            log.info("Ingelogde gebruiker akkoord, gebruiker : {} {} ({}) {}", jwtResponse.getGebruiker().getId(), maakNaamOp(jwtResponse.getGebruiker()), jwtResponse.getGebruiker().getGezin(), Thread.currentThread().getId());
        }

        var ctx = context.proceed();

        ThreadVariabelen.clear();

        return ctx;
    }

    private String maakNaamOp(Gebruiker gebruiker) {
        var builder = new StringBuilder(gebruiker.getVoornaam());
        if (gebruiker.getTussenvoegsel() != null) {
            builder.append(" ");
            builder.append(gebruiker.getTussenvoegsel());
        }
        builder.append(" ");
        builder.append(gebruiker.getAchternaam());

        return builder.toString();
    }
}

package nl.gezinsapp.integratietesten.verlanglijstzonderautorisatie;

import io.quarkus.test.junit.QuarkusTest;
import lombok.extern.slf4j.Slf4j;
import nl.gezinsapp.integratietesten.AbstractIntegratieTest;
import nl.gezinsapp.integratietesten.MockServer;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.openapi.quarkus.verlanglijstje_openapi_yaml.model.AlgemeneResponse;

import static nl.gezinsapp.integratietesten.random.RandomVerlanglijst.randomDomainVerlanglijst;
import static nl.gezinsapp.integratietesten.random.RandomVerlanglijstItem.randomDomainVerlanglijstItem;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import io.quarkus.test.common.QuarkusTestResource;

@Slf4j
@QuarkusTest
@QuarkusTestResource(MockServer.class)
class StreepItemAfOpVerlanglijstjeItemIntegratieTest extends AbstractIntegratieTest {
    @Test
    @DisplayName("Streep een item af van een verlanglijstje met code zonder ingelogd te zijn")
    void test() {
        maakDatabaseLeeg();

        var verlanglijst = randomDomainVerlanglijst(99L);
        assertNotNull(verlanglijst.getCode());
        opslaan(verlanglijst);

        var domainItem = randomDomainVerlanglijstItem();
        domainItem.setVerlanglijst(verlanglijst);
        domainItem.setDatumAfgestreept(null);
        opslaan(domainItem);

        patchViaApi(AlgemeneResponse.class, "/api/verlanglijstje/verlanglijstje-zonder-autorisatie/" + verlanglijst.getCode(), domainItem.getId(), null);

        var gelezen = leesVerlanglijstItem(domainItem.getId());
        assertNotNull(gelezen.getDatumAfgestreept());
    }

    @Test
    @DisplayName("Streep een item af van een verlanglijstje met code zonder ingelogd te zijn met onjuiste code")
    void test2() {
        maakDatabaseLeeg();

        var verlanglijst = randomDomainVerlanglijst(99L);
        assertNotNull(verlanglijst.getCode());
        opslaan(verlanglijst);

        var domainItem = randomDomainVerlanglijstItem();
        domainItem.setDatumAfgestreept(null);
        domainItem.setVerlanglijst(verlanglijst);
        opslaan(domainItem);

        patchViaApi(AlgemeneResponse.class, "/api/verlanglijstje/verlanglijstje-zonder-autorisatie/abc", domainItem.getId(), null);

        var gelezen = leesVerlanglijstItem(domainItem.getId());
        assertNull(gelezen.getDatumAfgestreept());
    }
}

package nl.gezinsapp.messaging.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;


@Getter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class JarigenVandaag {
    private List<Jarige> jarigen = new ArrayList<>();
}

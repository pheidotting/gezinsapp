import {Taak} from "../../../api/takenlijst";

export interface TaakOmToeTeWijzen extends Taak {
  gezinsledenToewijzen: Array<{
    id: number,
    voornaam: string,
    geselecteerd: boolean
  }>;
}

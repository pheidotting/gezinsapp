package nl.gezinsapp.controller;

import jakarta.annotation.PostConstruct;
import nl.gezinsapp.commons.aspect.MethodeLogging;
import org.openapi.quarkus.authenticatie_openapi_yaml.api.WachtwoordSterkteApi;
import org.openapi.quarkus.authenticatie_openapi_yaml.model.WachtwoordSterkte;
import org.passay.CharacterRule;
import org.passay.EnglishCharacterData;
import org.passay.EnglishSequenceData;
import org.passay.IllegalSequenceRule;
import org.passay.LengthRule;
import org.passay.PasswordData;
import org.passay.PasswordValidator;
import org.passay.PropertiesMessageResolver;
import org.passay.RepeatCharactersRule;
import org.passay.WhitespaceRule;

import java.io.IOException;
import java.util.Properties;

public class WachtwoordSterkteController implements WachtwoordSterkteApi {
    private PasswordValidator validator;

    @PostConstruct
    public void init() throws IOException {
        var props = new Properties();
        props.load(getClass().getClassLoader().getResourceAsStream("messages.properties"));
        var resolver = new PropertiesMessageResolver(props);

        this.validator = new PasswordValidator(
                resolver,
                new LengthRule(8, Integer.MAX_VALUE),

                new CharacterRule(EnglishCharacterData.UpperCase, 1),
                new CharacterRule(EnglishCharacterData.LowerCase, 1),
                new CharacterRule(EnglishCharacterData.Digit, 1),
                new CharacterRule(EnglishCharacterData.Special, 1),
                new RepeatCharactersRule(3),
                new IllegalSequenceRule(EnglishSequenceData.Alphabetical, 5, false),
                new IllegalSequenceRule(EnglishSequenceData.Numerical, 5, false),
                new IllegalSequenceRule(EnglishSequenceData.USQwerty, 5, false),
                new WhitespaceRule());
    }

    @Override
    @MethodeLogging
    public WachtwoordSterkte wachtwoordSterkte(String wachtwoord) {
        var result = this.validator.validate(new PasswordData(wachtwoord));

        var sterkte = new WachtwoordSterkte();
        sterkte.setValide(result.isValid());
        sterkte.setMeldingen(this.validator.getMessages(result));
        return sterkte;
    }
}

import {Pipe, PipeTransform} from '@angular/core';
import {GezinslidSoortGezinslidEnum} from '../../api/gezin';

@Pipe({
  name: 'maakSoortOuderEnumOp',
  standalone: true
})
export class MaakSoortOuderEnumOpPipe implements PipeTransform {

  transform(soortGezinslidEnum: GezinslidSoortGezinslidEnum, ...args: unknown[]): unknown {
    return soortGezinslidEnum.substring(0, 1).toUpperCase() + soortGezinslidEnum.substring(1).toLowerCase();
  }

}

import {ActivatedRouteSnapshot, ResolveFn, RouterStateSnapshot} from '@angular/router';
import {inject} from '@angular/core';
import {AfspraakService, LijstAfspraken} from '../../../api/kalender';
import moment from "moment/moment";

export const afspraakServiceAlleAfsprakenResolver: ResolveFn<LijstAfspraken> = (route: ActivatedRouteSnapshot, state: RouterStateSnapshot) => {
  const afspraakService = inject(AfspraakService);

  let nu: moment.Moment = moment();
  let overEenJaar: moment.Moment = moment().add(1, "year");

  return afspraakService.alleAfspraken(nu.format('YYYY-MM-DD'), overEenJaar.format('YYYY-MM-DD'));
}

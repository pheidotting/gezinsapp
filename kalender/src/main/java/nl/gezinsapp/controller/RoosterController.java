package nl.gezinsapp.controller;

import io.smallrye.common.annotation.RunOnVirtualThread;
import jakarta.transaction.Transactional;
import jakarta.ws.rs.Path;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.gezinsapp.aspect.ThreadVariabelen;
import nl.gezinsapp.commons.aspect.AlsGezinslid;
import nl.gezinsapp.commons.aspect.MethodeLogging;
import nl.gezinsapp.commons.aspect.TrackAndTraceId;
import nl.gezinsapp.mapper.SoortDienstMapper;
import nl.gezinsapp.model.Afspraak;
import nl.gezinsapp.model.SoortDienst;
import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.openapi.quarkus.gezin_openapi_yaml.api.GezinApi;
import org.openapi.quarkus.kalender_openapi_yaml.api.RoosterApi;
import org.openapi.quarkus.kalender_openapi_yaml.model.AlgemeneResponse;
import org.openapi.quarkus.kalender_openapi_yaml.model.SoortenDiensten;

@Slf4j
@Path("/api/kalender/rooster/soort-diensten")
@RequiredArgsConstructor
public class RoosterController implements RoosterApi {
    private final SoortDienstMapper soortDienstMapper;
    @RestClient
    private GezinApi gezinApi;

    @Override
    @Transactional
    @TrackAndTraceId
    @AlsGezinslid
    @RunOnVirtualThread
    @MethodeLogging
    public SoortenDiensten soortenDiensten(Long id) {
        return new SoortenDiensten().soortenDiensten(SoortDienst.findAll().stream()
                .map(SoortDienst.class::cast)
                .filter(soortDienst -> soortDienst.getGezinslid() != null)
                .filter(soortDienst -> soortDienst.getGezinslid().equals(id))
                .map(soortDienstMapper::map)
                .toList());
    }

    @Override
    @Transactional
    @TrackAndTraceId
    @AlsGezinslid
    @RunOnVirtualThread
    @MethodeLogging
    public AlgemeneResponse nieuweSoortenDienstenOpslaan(org.openapi.quarkus.kalender_openapi_yaml.model.SoortDienst soortDienst) {
        soortDienst.id(null);
        var sd = soortDienstMapper.map(soortDienst);
        sd.persist();
        return new AlgemeneResponse().id(sd.getId());
    }

    @Override
    @Transactional
    @TrackAndTraceId
    @AlsGezinslid
    @RunOnVirtualThread
    @MethodeLogging
    public AlgemeneResponse bestaandeSoortenDienstenOpslaan(org.openapi.quarkus.kalender_openapi_yaml.model.SoortDienst soortDienst) {
        var sd = soortDienstMapper.update(SoortDienst.findById(soortDienst.getId()), soortDienst);
        sd.persist();
        return new AlgemeneResponse().id(sd.getId());
    }

    @Override
    @Transactional
    @TrackAndTraceId
    @AlsGezinslid
    @RunOnVirtualThread
    @MethodeLogging
    public AlgemeneResponse verwijderSoortenDienst(Long id) {
        var sd = SoortDienst.findById(id);
        Afspraak.delete("soortDienst", sd);
        sd.delete();
        return new AlgemeneResponse();
    }

    @Override
    @Transactional
    @TrackAndTraceId
    @AlsGezinslid
    @RunOnVirtualThread
    @MethodeLogging
    public SoortenDiensten soortenDienstenBijGezin() {
        var gezinId = ThreadVariabelen.get().getGezin();
        var gezin = gezinApi.leesGezin(gezinId);

        var result = new SoortenDiensten();

        gezin.getGezinsleden().forEach(gezinslid ->
                SoortDienst.find("gezinslid", gezinslid.getId()).list()
                        .stream().map(panacheEntityBase -> (SoortDienst) panacheEntityBase)
                        .forEach(soortDienst -> result.getSoortenDiensten().add(soortDienstMapper.map(soortDienst))));

        return result;
    }
}

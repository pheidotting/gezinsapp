package nl.gezinsapp.model.enums;

public enum Terugkeer {
    NIET,
    JAARLIJKS,
    MAANDELIJKS
}

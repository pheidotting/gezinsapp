package nl.gezinsapp.service;

import jakarta.enterprise.context.ApplicationScoped;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.gezinsapp.commons.aspect.MethodeLogging;
import nl.gezinsapp.exception.TokenRefreshException;
import nl.gezinsapp.model.RefreshToken;
import nl.gezinsapp.model.repository.RefreshTokenRepository;

import java.time.Instant;
import java.util.Optional;
import java.util.UUID;

@Slf4j
@ApplicationScoped
@RequiredArgsConstructor
public class RefreshTokenService {
    private final RefreshTokenRepository refreshTokenRepository;

    @MethodeLogging
    public RefreshToken createRefreshToken(String accessToken, Long userId, UUID token) {
        var refreshToken = new RefreshToken(
                accessToken,
                userId,
                Instant.now().plusSeconds(86400L)
        );
        refreshTokenRepository.set(refreshToken, token);

        return refreshToken;
    }

    @MethodeLogging
    public RefreshToken verifyExpiration(RefreshToken token) {
        if (token.getExpiryDate().compareTo(Instant.now()) < 0) {
            log.info("Token expired");
            refreshTokenRepository.remove(token.getAccessToken());
            throw new TokenRefreshException(token.getAccessToken(), "Refresh token was expired. Please make a new signin request");
        }

        return token;
    }

    @MethodeLogging
    public Optional<RefreshToken> get(UUID key) {
        return refreshTokenRepository.get(key);
    }

    @MethodeLogging
    public Optional<RefreshToken> get(Long gebruikerId) {
        return refreshTokenRepository.get(gebruikerId);
    }
}

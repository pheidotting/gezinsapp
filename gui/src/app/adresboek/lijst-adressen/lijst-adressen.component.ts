import {Component, CUSTOM_ELEMENTS_SCHEMA, EventEmitter, Input, Output} from '@angular/core';
import {Adres, AdresboekService, AlgemeneResponse} from '../../../api/adresboek';
import {CSVService} from "../../services/csv.service";
import {CommonModule} from "@angular/common";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {HeaderMetToevoegButtonComponent} from "../../common/header-met-toevoeg-button/header-met-toevoeg-button.component";
import {StraatHuisnummerPipe} from "../../pipes/straat-huisnummer.pipe";
import {OpslaanEnAfbrekenButtonsComponent} from "../../common/opslaan-en-afbreken-buttons/opslaan-en-afbreken-buttons.component";

@Component({
  selector: 'app-lijst-adressen',
  templateUrl: './lijst-adressen.component.html',
  styleUrl: './lijst-adressen.component.css',
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HeaderMetToevoegButtonComponent,
    StraatHuisnummerPipe,
    OpslaanEnAfbrekenButtonsComponent
  ],
  // providers:[provideHttpClient(withInterceptorsFromDi())],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class LijstAdressenComponent {
  @Input() adressen: Adres[] = [];
  @Input() omschrijving: string | undefined;
  @Input() toevoegButtonTonen: boolean = true;

  @Output() nieuwAdresEventEmitter: EventEmitter<void> = new EventEmitter;
  @Output() gewijzigdeEventEmitter: EventEmitter<void> = new EventEmitter;
  @Output() startBewerkenEventEmitter: EventEmitter<number> = new EventEmitter<number>;

  constructor(
    private adresboekService: AdresboekService,
    private csvService: CSVService
  ) {
  }

  get getAdressen(): Adres[] {
    this.adressen.sort((a1: Adres, a2: Adres) => {
      if (a1 == undefined || a1.naam == undefined) {
        return 1;
      }
      if (a2 == undefined || a2.naam == undefined) {
        return -1;
      }
      return a1.naam.localeCompare(a2.naam);
    });
    return this.adressen;
  }

  nieuwAdres() {
    this.nieuwAdresEventEmitter.emit();
  }

  verwijder(id: number) {
    this.adresboekService.verwijderAdres(id).subscribe((response: AlgemeneResponse) => {
      this.gewijzigdeEventEmitter.emit();
    })
  }

  startBewerken(id: number) {
    this.startBewerkenEventEmitter.emit(id);
  }

  exporteerNaarCsv() {
    this.csvService.csvExporteren(this.adressen, this.omschrijving!);
  }
}

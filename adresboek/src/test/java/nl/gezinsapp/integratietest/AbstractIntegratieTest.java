package nl.gezinsapp.integratietest;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.restassured.RestAssured;
import io.restassured.config.ObjectMapperConfig;
import io.restassured.config.RestAssuredConfig;
import jakarta.transaction.Transactional;
import nl.gezinsapp.model.Adres;
import nl.gezinsapp.model.AdresBoek;
import org.mockserver.model.JsonBody;
import org.openapi.quarkus.authenticatie_openapi_yaml.model.CheckJWTRequest;
import org.openapi.quarkus.authenticatie_openapi_yaml.model.JwtResponse;

import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.List;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

import static io.restassured.RestAssured.given;
import static jakarta.ws.rs.core.HttpHeaders.AUTHORIZATION;
import static org.awaitility.Awaitility.await;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockserver.model.HttpRequest.request;
import static org.mockserver.model.HttpResponse.response;

public abstract class AbstractIntegratieTest {
    private final String BEARER = "";

    protected <T> T getViaApi(Class<T> clazz, String url, String token, int verwachteStatusCode) {
        AtomicReference<T> gezinF = new AtomicReference<T>();

        await()
                .atMost(2, TimeUnit.MINUTES)
                .pollInterval(Duration.ofMillis(500))
                .until(() -> {
                    var requestSpecification = given()
                            .when();
                    if (token != null) {
                        requestSpecification.header(AUTHORIZATION, String.format("%s%s", BEARER, token));
                    }
                    var response = requestSpecification.get(url);
                    System.out.println("response.statusCode() " + response.statusCode());
                    System.out.println("response.statusCode() " + response.asPrettyString());
                    if (response.statusCode() == verwachteStatusCode && clazz != null) {
                        gezinF.set(response.then().extract().response().as(clazz));
                    }
                    return response.statusCode() == verwachteStatusCode;
                });

        return gezinF.get();
    }

    protected <T> T postViaApi(Class<T> clazz, String url, Object payload, String token) {
        RestAssured.config = RestAssuredConfig.config().objectMapperConfig(new ObjectMapperConfig().jackson2ObjectMapperFactory(
                (cls, charset) -> {
                    var df = new SimpleDateFormat("dd-MM-yyyy");
                    var objectMapper = new ObjectMapper().findAndRegisterModules();
                    objectMapper.setDateFormat(df);
                    return objectMapper;
                }
        ));
        var requestSpecification = given()
                .when();
        if (token != null) {
            requestSpecification.header(AUTHORIZATION, String.format("%s%s", BEARER, token));
        }
        requestSpecification.header("Content-Type", "application/json");
        requestSpecification.body(payload);
        var response = requestSpecification.post(url);
        System.out.println("response.statusCode(" + response.statusCode() + ")");

        assertEquals(200, response.statusCode());
        return response.as(clazz);
    }

    protected <T> T putViaApi(Class<T> clazz, String url, Object payload, String token) {
        RestAssured.config = RestAssuredConfig.config().objectMapperConfig(new ObjectMapperConfig().jackson2ObjectMapperFactory(
                (cls, charset) -> {
                    var df = new SimpleDateFormat("dd-MM-yyyy");
                    var objectMapper = new ObjectMapper().findAndRegisterModules();
                    objectMapper.setDateFormat(df);
                    return objectMapper;
                }
        ));
        var requestSpecification = given()
                .when();
        if (token != null) {
            requestSpecification.header(AUTHORIZATION, String.format("%s%s", BEARER, token));
        }
        requestSpecification.header("Content-Type", "application/json");

        requestSpecification.body(payload);
        var response = requestSpecification.put(url);
        System.out.println("response.statusCode(" + response.statusCode() + ")");

        assertEquals(200, response.statusCode());
        return response.as(clazz);
    }

    protected <T> T patchViaApi(Class<T> clazz, String url, Object payload, String token, int verwachteStatusCode) {
        RestAssured.config = RestAssuredConfig.config().objectMapperConfig(new ObjectMapperConfig().jackson2ObjectMapperFactory(
                (cls, charset) -> {
                    var df = new SimpleDateFormat("dd-MM-yyyy");
                    var objectMapper = new ObjectMapper().findAndRegisterModules();
                    objectMapper.setDateFormat(df);
                    return objectMapper;
                }
        ));
        var requestSpecification = given().when();
        if (token != null) {
            requestSpecification.header(AUTHORIZATION, String.format("%s%s", BEARER, token));
        }
        requestSpecification.header("Content-Type", "application/json");

        requestSpecification.body(payload);
        var response = requestSpecification.patch(url);
        System.out.println("response.statusCode(" + response.statusCode() + ")");

        assertEquals(verwachteStatusCode, response.statusCode());
        if (clazz == null) {
            return null;
        }
        return response.as(clazz);
    }

    protected <T> T deleteViaApi(Class<T> clazz, String url, Object payload, String token, int verwachteStatusCode) {
        RestAssured.config = RestAssuredConfig.config().objectMapperConfig(new ObjectMapperConfig().jackson2ObjectMapperFactory(
                (cls, charset) -> {
                    var df = new SimpleDateFormat("dd-MM-yyyy");
                    var objectMapper = new ObjectMapper().findAndRegisterModules();
                    objectMapper.setDateFormat(df);
                    return objectMapper;
                }
        ));
        var requestSpecification = given().when();
        if (token != null) {
            requestSpecification.header(AUTHORIZATION, String.format("%s%s", BEARER, token));
        }
        requestSpecification.header("Content-Type", "application/json");

        requestSpecification.body(payload);
        var response = requestSpecification.delete(url);
        System.out.println("response.statusCode(" + response.statusCode() + ")");

        assertEquals(verwachteStatusCode, response.statusCode());
        if (clazz == null) {
            return null;
        }
        return response.as(clazz);
    }

    @Transactional
    public void maakDatabaseLeeg() {
        Adres.deleteAll();
        AdresBoek.deleteAll();
    }

    @Transactional
    public void opslaan(AdresBoek adresBoek) {
        adresBoek.persist();
    }

    @Transactional
    public void opslaan(Adres adres) {
        adres.persist();
    }

    @Transactional
    public AdresBoek leesAdresBoek(Long id) {
        return AdresBoek.findById(id);
    }

    @Transactional
    public List<AdresBoek> leesAdresBoeken() {
        return AdresBoek.findAll().list();
    }

    @Transactional
    public List<Adres> leesAdressen(AdresBoek adresBoek) {
        return Adres.find("adresBoek", adresBoek).list();
    }

    @Transactional
    public Adres leesAdres(Long id) {
        return Adres.findById(id);
    }

    public String maakToken() {
        return Jwts.builder()
                .setSubject("a@b.c")
                .signWith(SignatureAlgorithm.HS512,
                        UUID.randomUUID().toString().replace("-", "") +
                                UUID.randomUUID().toString().replace("-", "") +
                                UUID.randomUUID().toString().replace("-", "")
                )
                .compact();
    }

    public JwtResponse mockCheckJwt(String token) throws JsonProcessingException {
        var jwtResponse = new JwtResponse();
        jwtResponse.setGebruiker(new org.openapi.quarkus.authenticatie_openapi_yaml.model.Gebruiker());
        jwtResponse.getGebruiker().setVoornaam("John");
        jwtResponse.getGebruiker().setAchternaam("Doe");
        jwtResponse.getGebruiker().setId(new Random().nextLong());
        jwtResponse.getGebruiker().setGezin(new Random().nextLong());
        System.out.println(String.format("Gemockte gebruiker: %s : %s %s %s (%s)", jwtResponse.getGebruiker().getId(), jwtResponse.getGebruiker().getVoornaam(), jwtResponse.getGebruiker().getTussenvoegsel(), jwtResponse.getGebruiker().getAchternaam(), jwtResponse.getGebruiker().getGezin()));
        MockServer.getMockServerClient()
                .when(
                        request()
                                .withMethod("POST")
                                .withPath("/api/authorisatie/checkjwt")
                                .withBody(new ObjectMapper().writeValueAsString(new CheckJWTRequest().accessToken(token)))
                )
                .respond(
                        response()
                                .withStatusCode(200)
                                .withHeader("Content-Type", "application/json")
                                .withBody(JsonBody.json(jwtResponse))
                );

        return jwtResponse;
    }
}

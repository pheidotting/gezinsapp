package nl.gezinsapp.integratietesten;

import com.fasterxml.jackson.core.JsonProcessingException;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.openapi.quarkus.boodschappen_openapi_yaml.model.AlgemeneResponse;
import org.openapi.quarkus.boodschappen_openapi_yaml.model.VerwijderBoodschappenItem;

import static nl.gezinsapp.integratietesten.random.RandomBoodschappenItem.randomBoodschappenItem;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

@Slf4j
@QuarkusTest
@QuarkusTestResource(MockServer.class)
class VerwijderBoodschappenItemsIntegratieTest extends AbstractIntegratieTest {
    @Test
    @DisplayName("Verwijder een BoodschappenItem")
    void test() throws JsonProcessingException {
        var accesstoken = maakToken();
        var jwtResponse = mockCheckJwt(accesstoken);

        maakDatabaseLeeg();

        var item1 = randomBoodschappenItem(jwtResponse.getGebruiker().getGezin());

        opslaanBoodschappenItem(item1);

        var response = deleteViaApi(AlgemeneResponse.class, "/api/boodschappen/item", new VerwijderBoodschappenItem().id(item1.getId()), accesstoken);
        assertNull(response.getFoutmeldingen());
        assertEquals(item1.getId(), response.getId());

        assertNull(leesBoodschappenItem(item1.getId()));
    }
}

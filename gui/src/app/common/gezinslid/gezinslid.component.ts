import {Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, SimpleChanges} from '@angular/core';
import {AlgemeneResponse, AvatarService, Gezin, Gezinslid, GezinslidSoortGezinslidEnum} from "../../../api/gezin";
import {Observable, Subscription, timer} from "rxjs";
import {Foutmeldingen} from "../../model/foutmeldingen";
import {MeldingenComponent} from "../meldingen/meldingen.component";
import {FormsModule} from "@angular/forms";
import {AvatarComponent} from "../avatar/avatar.component";
import {NgIf} from "@angular/common";

@Component({
  selector: 'app-gezinslid',
  templateUrl: './gezinslid.component.html',
  imports: [
    MeldingenComponent,
    FormsModule,
    AvatarComponent,
    NgIf
  ]
})
export class GezinslidComponent implements OnInit, OnDestroy, OnChanges {
  @Input({required: true}) gezinslid!: Gezinslid;
  @Input({required: true}) gezin!: Gezin;
  @Input() avatar: boolean = false;
  @Output() opslaanEventEmitter: EventEmitter<void> = new EventEmitter<void>();
  @Output() achternaamGewijzigdEventEmitter: EventEmitter<void> = new EventEmitter<void>();
  @Output() tussenvoegselGewijzigdEventEmitter: EventEmitter<void> = new EventEmitter<void>();

  fileFile: string = '';
  timestamp: number = (new Date()).getTime();
  foutmeldingen: string[] = [];
  meldingen: string[] = [];
  goedmeldingen: string[] = [];
  private preOpslaanSubscription: Subscription | undefined;
  @Input() preOpslaanObservable: Observable<void> | undefined;

  constructor(
    private avatarService: AvatarService
  ) {
  }

  ngOnInit(): void {
    let aantalOuders = this.gezin.gezinsleden.filter((gezinslid: Gezinslid) => {
      return gezinslid.soortGezinslid == GezinslidSoortGezinslidEnum.Ouder;
    }).length;

    let soortLid: GezinslidSoortGezinslidEnum = aantalOuders > 1 ? GezinslidSoortGezinslidEnum.Kind : GezinslidSoortGezinslidEnum.Ouder;
    if (this.gezinslid.achternaam == '') {
      if (this.gezin.gezinsleden.length > 0) {
        this.gezinslid.achternaam = this.gezin.gezinsleden[0].achternaam;
        this.gezinslid.soortGezinslid = soortLid;
      }
    }
    this.preOpslaanSubscription = this.preOpslaanObservable!.subscribe(() => this.preOpslaan());
  }

  ngOnDestroy() {
    this.preOpslaanSubscription!.unsubscribe();
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.ngOnInit();
  }

  preOpslaan() {
    this.foutmeldingen = [];
    if (this.gezinslid.achternaam == '') {
      this.foutmeldingen.push(Foutmeldingen.ACHTERNAAM_MAG_NIET_LEEG_ZIJN);
    }
    if (this.gezinslid.voornaam == '') {
      this.foutmeldingen.push(Foutmeldingen.VOORNAAM_MAG_NIET_LEEG_ZIJN);
    }
    if (this.gezinslid.geslacht == null) {
      this.foutmeldingen.push(Foutmeldingen.GESLACHT_MAG_NIET_LEEG_ZIJN);
    }
    if (this.gezinslid.soortGezinslid == null) {
      this.foutmeldingen.push(Foutmeldingen.SOORT_MAG_NIET_LEEG_ZIJN);
    }

    if (this.foutmeldingen.length == 0) {
      this.opslaanEventEmitter.emit();
    }
  }

  maakNaamOp(gezinslid: Gezinslid): String {
    let naam: string = '';
    if (gezinslid != null) {
      naam += gezinslid.voornaam;

      if (gezinslid.tussenvoegsel != null && gezinslid.tussenvoegsel != undefined) {
        naam += ' ' + gezinslid.tussenvoegsel.trim();
      }
      naam += ' ' + gezinslid.achternaam;

      if (naam.trim() != '') {
        naam = "(" + naam.trim();
        if (gezinslid.soortGezinslid != null) {
          naam += ', ' + gezinslid.soortGezinslid.substring(0, 1).toUpperCase() + gezinslid.soortGezinslid.substring(1).toLowerCase();
        }
        naam += ")";
      }
    }
    return naam.trim();
  }

  achternaamGewijzigd(): void {
    this.achternaamGewijzigdEventEmitter.emit();
  }

  tussenvoegselGewijzigd(): void {
    this.tussenvoegselGewijzigdEventEmitter.emit();
  }

  handleFileInput(files: FileList):
    void {
    if (files != null) {
      if (files.item(0)!.size > 4000000) {
        this.foutmeldingen.push('Het opgegeven bestand is groter dan 4MB');
      } else {
        this.avatarService.uploadAvatar(this.gezinslid.id, files.item(0)!).subscribe((response: AlgemeneResponse) => {
            this.gezinslid.avatarAanwezig = true;
            this.timestamp = (new Date()).getTime();
          },
          error => {
            this.foutmeldingen.push(error);
            timer(10000)
              .subscribe(() => {
                this.foutmeldingen = [];
              });
          });
      }
    }
  }

  verwijderAvatar(): void {
    this.avatarService.verwijderAvatar(this.gezinslid.id).subscribe((response: any) => {
      this.gezinslid.avatarAanwezig = false;
      this.timestamp = (new Date()).getTime();
    });
  }
}

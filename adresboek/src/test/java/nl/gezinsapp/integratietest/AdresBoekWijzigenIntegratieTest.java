package nl.gezinsapp.integratietest;

import com.fasterxml.jackson.core.JsonProcessingException;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.openapi.quarkus.adresboek_openapi_yaml.model.AlgemeneResponse;

import static nl.gezinsapp.integratietest.random.RandomAdresBoek.randomAdresBoek;
import static nl.gezinsapp.integratietest.random.RandomAdresBoek.randomDtoAdresBoek;
import static org.jboss.resteasy.reactive.RestResponse.StatusCode.FORBIDDEN;
import static org.jboss.resteasy.reactive.RestResponse.StatusCode.OK;
import static org.junit.jupiter.api.Assertions.*;

@Slf4j
@QuarkusTest
@QuarkusTestResource(MockServer.class)
class AdresBoekWijzigenIntegratieTest extends AbstractIntegratieTest {
    @Test
    @DisplayName("Sla een gewijzigd AdresBoek op")
    void test() throws JsonProcessingException {
        var accesstoken = maakToken();
        var jwtResponse = mockCheckJwt(accesstoken);

        maakDatabaseLeeg();

        var item = randomAdresBoek();
        item.setGezin(jwtResponse.getGebruiker().getGezin());
        opslaan(item);

        var gewijzigd = randomDtoAdresBoek();
        gewijzigd.setId(item.getId());
        gewijzigd.setGezin(item.getGezin());
        var response = patchViaApi(AlgemeneResponse.class, "/api/adresboek/adresboek", gewijzigd, accesstoken, OK);

        System.out.println(response);
        var itemGelezen = leesAdresBoek(response.getId());
        assertNotNull(itemGelezen);
        assertEquals(jwtResponse.getGebruiker().getGezin(), itemGelezen.getGezin());
        assertEquals(gewijzigd.getGezinslid(), itemGelezen.getGezinslid());
        assertEquals(gewijzigd.getOmschrijving(), itemGelezen.getOmschrijving());
    }

    @Test
    @DisplayName("Sla een gewijzigd AdresBoek op, gezin klopt niet")
    void testFoutGezin() throws JsonProcessingException {
        var accesstoken = maakToken();
        mockCheckJwt(accesstoken);

        maakDatabaseLeeg();

        var item = randomAdresBoek();
        item.setGezin(99L);
        opslaan(item);

        var gewijzigd = randomDtoAdresBoek();
        gewijzigd.setId(item.getId());
        assertNull(patchViaApi(null, "/api/adresboek/adresboek", gewijzigd, accesstoken, FORBIDDEN));
    }
}

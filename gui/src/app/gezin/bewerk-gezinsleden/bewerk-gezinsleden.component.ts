import {Component, Input, OnInit, Output} from '@angular/core';
import {
  AlgemeneResponse,
  Gezin,
  GezinService,
  Gezinslid,
  GezinslidGeslachtEnum,
  GezinslidMetId,
  GezinslidMetIdGeslachtEnum,
  GezinslidMetIdSoortGezinslidEnum,
  GezinslidService,
  GezinslidSoortGezinslidEnum,
  GezinslidZonderId,
  GezinslidZonderIdGeslachtEnum,
  GezinslidZonderIdSoortGezinslidEnum,
  LeesGezin
} from "../../../api/gezin";
import {ActivatedRoute, Router} from "@angular/router";
import {Subject} from "rxjs";
import {SideBarComponent} from "../../common/side-bar/side-bar.component";
import {GezinslidComponent} from "../../common/gezinslid/gezinslid.component";
import {OpslaanEnAfbrekenButtonsComponent} from "../../common/opslaan-en-afbreken-buttons/opslaan-en-afbreken-buttons.component";

@Component({
  selector: 'app-bewerk-gezinsleden',
  templateUrl: './bewerk-gezinsleden.component.html',
  imports: [
    SideBarComponent,
    GezinslidComponent,
    OpslaanEnAfbrekenButtonsComponent
  ]
})
export class BewerkGezinsledenComponent implements OnInit {
  @Input({required: true}) gezinslid: Gezinslid = {
    voornaam: '',
    achternaam: '',
    soortGezinslid: GezinslidSoortGezinslidEnum.Ouder,
    geslacht: GezinslidGeslachtEnum.Man,
    id: 0
  };
  gezin: Gezin = {
    naam: '',
    gezinsleden: []
  };
  @Output() preOpslaanEventEmitter: Subject<void> = new Subject<void>();
  opgeslagen: boolean = false;

  constructor(
    private router: Router,
    private gezinService: GezinService,
    private gezinslidService: GezinslidService,
    private activatedRoute: ActivatedRoute,
  ) {
  }

  ngOnInit(): void {
    this.gezinService.leesHuidigGezin().subscribe((gezin: LeesGezin) => {
      this.gezin = gezin;
      this.activatedRoute.url.subscribe(url => {
        if (url.length > 1) {
          let id = url[1].path;
          this.gezinslid = this.gezin.gezinsleden.filter((lid: Gezinslid) => {
            return lid.id == +id;
          })[0];
        }
      });
    });
  }

  opslaan(): void {
    this.preOpslaanEventEmitter.next();
  }

  doeOpslaan(): void {
    if (!this.opgeslagen) {
      this.opgeslagen = true;
      if (this.gezinslid.id == null || this.gezinslid.id == 0) {
        let gezinslidZonderId: GezinslidZonderId = {
          achternaam: this.gezinslid.achternaam,
          geslacht: GezinslidZonderIdGeslachtEnum.Man,
          soortGezinslid: GezinslidZonderIdSoortGezinslidEnum.Kind,
          voornaam: ""
        };
        gezinslidZonderId.gezin = this.gezin.id;
        gezinslidZonderId.achternaam = this.gezinslid.achternaam;
        gezinslidZonderId.tussenvoegsel = this.gezinslid.tussenvoegsel;
        gezinslidZonderId.voornaam = this.gezinslid.voornaam;
        // @ts-ignore
        gezinslidZonderId.soortGezinslid = GezinslidZonderIdSoortGezinslidEnum[this.maakSoortOuderEnumOp(this.gezinslid.soortGezinslid)];
        gezinslidZonderId.emailadres = this.gezinslid.emailadres;
        gezinslidZonderId.geboorteDatum = this.gezinslid.geboorteDatum;
        // @ts-ignore
        gezinslidZonderId.geslacht = GezinslidZonderIdGeslachtEnum[this.maakGeslachtEnumOp(this.gezinslid.geslacht)];
        this.gezinslidService.opslaanNieuwGezinslid(gezinslidZonderId).subscribe((response: AlgemeneResponse) => {
          this.router.navigate(['gezinsleden']);
        });
      } else {
        let gezinslidMetId: GezinslidMetId = {
          id: 0,
          achternaam: this.gezinslid.achternaam,
          geslacht: GezinslidMetIdGeslachtEnum.Man,
          soortGezinslid: GezinslidMetIdSoortGezinslidEnum.Kind,
          voornaam: ""
        };
        gezinslidMetId.id = this.gezinslid.id;
        gezinslidMetId.gezin = this.gezin.id;
        gezinslidMetId.achternaam = this.gezinslid.achternaam;
        gezinslidMetId.tussenvoegsel = this.gezinslid.tussenvoegsel;
        gezinslidMetId.voornaam = this.gezinslid.voornaam;
        // @ts-ignore
        gezinslidMetId.soortGezinslid = GezinslidMetIdSoortGezinslidEnum[this.maakSoortOuderEnumOp(this.gezinslid.soortGezinslid)];
        gezinslidMetId.emailadres = this.gezinslid.emailadres;
        gezinslidMetId.geboorteDatum = this.gezinslid.geboorteDatum;
        // @ts-ignore
        gezinslidMetId.geslacht = GezinslidMetIdGeslachtEnum[this.maakGeslachtEnumOp(this.gezinslid.geslacht)];
        this.gezinslidService.opslaanGezinslid(gezinslidMetId).subscribe((a: AlgemeneResponse) => {
          this.router.navigate(['gezinsleden']);
        });
      }
    }
  }

  afbreken(): void {
    this.router.navigate(['dashboard']);
  }

  maakSoortOuderEnumOp(soortGezinslidEnum: GezinslidSoortGezinslidEnum): string {
    return soortGezinslidEnum.substring(0, 1).toUpperCase() + soortGezinslidEnum.substring(1).toLowerCase();
  }

  maakGeslachtEnumOp(geslacht: GezinslidGeslachtEnum): string {
    return geslacht.substring(0, 1).toUpperCase() + geslacht.substring(1).toLowerCase();
  }
}

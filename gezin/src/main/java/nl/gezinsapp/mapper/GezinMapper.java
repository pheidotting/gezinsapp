package nl.gezinsapp.mapper;

import jakarta.inject.Inject;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.openapi.quarkus.gezin_openapi_yaml.model.AanmeldenGezin;
import org.openapi.quarkus.gezin_openapi_yaml.model.Gezin;
import org.openapi.quarkus.gezin_openapi_yaml.model.LeesGezin;
import org.openapi.quarkus.gezin_openapi_yaml.model.OpslaanGezin;
@Mapper(componentModel = "CDI")
public abstract class GezinMapper {
    @Inject
    protected GezinslidMapper gezinslidMapper;

    @Mapping(target = "gezinsleden", expression = """
                java(gezin.getGezinsleden().stream().map(new java.util.function.Function<Gezinslid, org.openapi.quarkus.gezin_openapi_yaml.model.Gezinslid>() { 
                @Override
                public org.openapi.quarkus.gezin_openapi_yaml.model.Gezinslid apply(Gezinslid gezinslid) {
                    return gezinslidMapper.map(gezinslid);
                }
            }).toList())
            """)
    public abstract Gezin map(nl.gezinsapp.model.Gezin gezin);

    public abstract nl.gezinsapp.model.Gezin map(OpslaanGezin gezin, @MappingTarget nl.gezinsapp.model.Gezin source);

    public abstract nl.gezinsapp.model.Gezin map(AanmeldenGezin aanmeldenGezin);

    public abstract LeesGezin mapLeesGezin(nl.gezinsapp.model.Gezin gezin);

    Long map(Gezin gezin) {
        return gezin == null ? null : gezin.getId();
    }

    nl.gezinsapp.model.Gezin map(Long value) {
        return null;
    }
}

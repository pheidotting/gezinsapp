package nl.gezinsapp.service;

import io.quarkus.qute.Template;
import jakarta.enterprise.context.ApplicationScoped;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.gezinsapp.model.CommWachtwoordGewijzigd;
import org.openapi.quarkus.gezin_openapi_yaml.model.LeesGezin;

@Slf4j
@ApplicationScoped
@RequiredArgsConstructor
public class WachtwoordGewijzigdService extends AbstractService<CommWachtwoordGewijzigd> {
    private final Template wachtwoordGewijzigd;

    @Override
    public void vulCommObject(CommWachtwoordGewijzigd commWachtwoordGewijzigd, LeesGezin gezin) {

    }

    @Override
    public Template getTemplate() {
        return wachtwoordGewijzigd;
    }

    @Override
    public String getOnderwerp() {
        return "Wachtwoord gewijzigd";
    }
}

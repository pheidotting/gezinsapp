import {Pipe, PipeTransform} from '@angular/core';
import {Adres} from '../../api/adresboek';

@Pipe({
  name: 'straatHuisnummer',
  standalone: true
})
export class StraatHuisnummerPipe implements PipeTransform {

  transform(adres: Adres, ...args: unknown[]): unknown {
    let result = adres.straat + ' ' + adres.huisnummer;
    if (adres.toevoeging != null && adres.toevoeging != '') {
      result += ' ' + adres.toevoeging;
    }
    return result;
  }

}

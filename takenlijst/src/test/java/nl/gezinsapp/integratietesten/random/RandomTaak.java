package nl.gezinsapp.integratietesten.random;

import org.openapi.quarkus.takenlijst_openapi_yaml.model.Taak;
import uk.co.jemos.podam.api.DefaultClassInfoStrategy;
import uk.co.jemos.podam.api.PodamFactory;
import uk.co.jemos.podam.api.PodamFactoryImpl;

public class RandomTaak {
    private static PodamFactory factory;

    private static PodamFactory getFactory() {
        if (factory == null) {
            factory = new PodamFactoryImpl();
            DefaultClassInfoStrategy classInfoStrategy = DefaultClassInfoStrategy.getInstance();
            classInfoStrategy.addExcludedField(Taak.class, "id");
            classInfoStrategy.addExcludedField(Taak.class, "gezinsleden");
            classInfoStrategy.addExcludedField(nl.gezinsapp.domain.Taak.class, "id");
            classInfoStrategy.addExcludedField(nl.gezinsapp.domain.Taak.class, "gezinsleden");
            factory.setClassStrategy(classInfoStrategy);
        }
        return factory;
    }

    public static Taak randomTaak() {
        return getFactory().manufacturePojo(Taak.class);
    }

    public static nl.gezinsapp.domain.Taak randomDomainTaak() {
        return getFactory().manufacturePojo(nl.gezinsapp.domain.Taak.class);
    }

}

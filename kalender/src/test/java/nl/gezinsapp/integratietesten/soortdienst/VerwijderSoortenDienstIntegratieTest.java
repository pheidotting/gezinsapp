package nl.gezinsapp.integratietesten.soortdienst;

import com.fasterxml.jackson.core.JsonProcessingException;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;
import lombok.extern.slf4j.Slf4j;
import nl.gezinsapp.integratietesten.AbstractIntegratieTest;
import nl.gezinsapp.integratietesten.MockServer;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.openapi.quarkus.kalender_openapi_yaml.model.AlgemeneResponse;

import static nl.gezinsapp.integratietesten.common.random.RandomAfspraak.randomAfspraak;
import static nl.gezinsapp.integratietesten.common.random.SoortDienstDatum.randomSoortDienst;
import static org.junit.jupiter.api.Assertions.*;

@Slf4j
@QuarkusTest
@QuarkusTestResource(MockServer.class)
class VerwijderSoortenDienstIntegratieTest extends AbstractIntegratieTest {

    @Test
    @DisplayName("Lees een opgeslagen datum")
    void test() throws JsonProcessingException {
        maakDatabaseLeeg();

        var accesstoken = maakToken();
        var jwtResponse = mockCheckJwt(accesstoken);

        var soortDienst = randomSoortDienst();
        opslaan(soortDienst);

        var afspraak = randomAfspraak();
        afspraak.setSoortDienst(soortDienst);
        opslaan(afspraak);

        assertFalse(leesAlleSoortDienst().isEmpty());
        assertFalse(leesAlleAfspraken().isEmpty());

        var response = deleteViaApi(AlgemeneResponse.class, "/api/kalender/rooster/soort-diensten", soortDienst.getId(), accesstoken);
        assertNull(response.getFoutmeldingen());
        assertNull(response.getId());

        assertTrue(leesAlleSoortDienst().isEmpty());
        assertTrue(leesAlleAfspraken().isEmpty());
    }
    }

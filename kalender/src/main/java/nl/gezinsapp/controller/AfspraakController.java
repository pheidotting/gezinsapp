package nl.gezinsapp.controller;

import io.smallrye.common.annotation.RunOnVirtualThread;
import jakarta.transaction.Transactional;
import jakarta.ws.rs.Path;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.gezinsapp.aspect.ThreadVariabelen;
import nl.gezinsapp.commons.aspect.AlsGezinslid;
import nl.gezinsapp.commons.aspect.MethodeLogging;
import nl.gezinsapp.commons.aspect.TrackAndTraceId;
import nl.gezinsapp.mapper.AfspraakMapper;
import nl.gezinsapp.model.SoortDienst;
import nl.gezinsapp.model.enums.Terugkeer;
import nl.gezinsapp.service.AfspraakService;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.openapi.quarkus.kalender_openapi_yaml.api.AfspraakApi;
import org.openapi.quarkus.kalender_openapi_yaml.model.Afspraak;
import org.openapi.quarkus.kalender_openapi_yaml.model.AlgemeneResponse;
import org.openapi.quarkus.kalender_openapi_yaml.model.LijstAfspraken;

import java.time.LocalDate;

@Slf4j
@Path("/api/kalender/afspraak")
@RequiredArgsConstructor
public class AfspraakController implements AfspraakApi {
    private final AfspraakMapper afspraakMapper;
    private final AfspraakService afspraakService;

    @Override
    @Transactional
    @TrackAndTraceId
    @AlsGezinslid
    @RunOnVirtualThread
    @MethodeLogging
    public AlgemeneResponse nieuweAfspraak(Afspraak afspraak) {
        log.info(ReflectionToStringBuilder.toString(afspraak));
        var afs = afspraakMapper.map(afspraak);
        if (afspraak.getSoortDienst() != null) {
            afs.setSoortDienst(SoortDienst.findById(afspraak.getSoortDienst()));
        }
        log.info(ReflectionToStringBuilder.toString(afs));
        if (afs.getTerugkeer() == null) {
            afs.setTerugkeer(Terugkeer.NIET);
        }
        afs.setGezin(ThreadVariabelen.get().getGezin());
        afs.persist();

        return new AlgemeneResponse().id(afs.getId());
    }

    @Override
    @TrackAndTraceId
    @AlsGezinslid
    @RunOnVirtualThread
    @MethodeLogging
    public LijstAfspraken alleAfspraken(LocalDate start, LocalDate eind) {
        return new LijstAfspraken().afspraken(
                afspraakService.alleAfspraken(start, eind, ThreadVariabelen.get().getGezin())
                        .stream()
                        .map(afspraakMapper::map)
                        .toList()
        );
    }

    @Override
    @Transactional
    @TrackAndTraceId
    @AlsGezinslid
    @RunOnVirtualThread
    @MethodeLogging
    public AlgemeneResponse opslaanAfspraak(Afspraak afspraak) {
        var bestaandeAfspraak = nl.gezinsapp.model.Afspraak.findByIdOptional(afspraak.getId())
                .map(panacheEntityBase -> (nl.gezinsapp.model.Afspraak) panacheEntityBase)
                .orElse(new nl.gezinsapp.model.Afspraak());
        var afs = afspraakMapper.update(bestaandeAfspraak, afspraak);
        if (afspraak.getSoortDienst() != null) {
            afs.setSoortDienst(SoortDienst.findById(afspraak.getSoortDienst()));
        }
        if (afs.getTerugkeer() == null) {
            afs.setTerugkeer(Terugkeer.NIET);
        }
        afs.setGezin(ThreadVariabelen.get().getGezin());
        afs.persist();
        return new AlgemeneResponse().id(bestaandeAfspraak.getId());
    }

    @Override
    @Transactional
    @TrackAndTraceId
    @AlsGezinslid
    @RunOnVirtualThread
    @MethodeLogging
    public AlgemeneResponse verwijderAfspraak(Long id) {
        nl.gezinsapp.model.Afspraak.deleteById(id);
        return new AlgemeneResponse();
    }
}

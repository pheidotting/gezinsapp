package nl.gezinsapp.common.random;

import nl.gezinsapp.model.GebruikerImpl;
import org.openapi.quarkus.gezin_openapi_yaml.model.Gezinslid;
import uk.co.jemos.podam.api.DefaultClassInfoStrategy;
import uk.co.jemos.podam.api.PodamFactory;
import uk.co.jemos.podam.api.PodamFactoryImpl;

public class RandomGezinslid {
    private static PodamFactory factory;

    private static PodamFactory getFactory() {
        if (factory == null) {
            factory = new PodamFactoryImpl();
            DefaultClassInfoStrategy classInfoStrategy = DefaultClassInfoStrategy.getInstance();
            classInfoStrategy.addExcludedField(Gezinslid.class, "id");
            classInfoStrategy.addExcludedField(Gezinslid.class, "gezin");
            factory.setClassStrategy(classInfoStrategy);
        }
        return factory;
    }


    public static Gezinslid randomMsgGezinslid() {
        return getFactory().manufacturePojo(Gezinslid.class);
    }

    public static GebruikerImpl randomGebruikerImpl() {
        return getFactory().manufacturePojo(GebruikerImpl.class);
    }
}

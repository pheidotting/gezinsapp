package nl.gezinsapp.integratietesten;

import com.fasterxml.jackson.core.JsonProcessingException;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.openapi.quarkus.boodschappen_openapi_yaml.model.LijstBoodschappenItems;

import static nl.gezinsapp.integratietesten.random.RandomBoodschappenItem.randomBoodschappenItem;
import static org.junit.jupiter.api.Assertions.assertEquals;

@Slf4j
@QuarkusTest
@QuarkusTestResource(MockServer.class)
class LijstBoodschappenItemsIntegratieTest extends AbstractIntegratieTest {
    @Test
    @DisplayName("Toon alle boodschappen voor een gezin")
    void test() throws JsonProcessingException {
        var accesstoken = maakToken();
        var jwtResponse = mockCheckJwt(accesstoken);

        maakDatabaseLeeg();

        var item1 = randomBoodschappenItem(jwtResponse.getGebruiker().getGezin());
        item1.setGehaald(false);
        var item2 = randomBoodschappenItem(2L);
        item2.setGehaald(false);
        var item3 = randomBoodschappenItem(jwtResponse.getGebruiker().getGezin());
        item3.setGehaald(false);

        opslaanBoodschappenItem(item1);
        opslaanBoodschappenItem(item2);
        opslaanBoodschappenItem(item3);

        var response = leesViaApi(LijstBoodschappenItems.class, "/api/boodschappen/item", accesstoken);

        assertEquals(2, response.getBoodschappenitems().size());
        assertEquals(jwtResponse.getGebruiker().getGezin(), response.getBoodschappenitems().get(0).getGezin());
        assertEquals(jwtResponse.getGebruiker().getGezin(), response.getBoodschappenitems().get(1).getGezin());
    }
}

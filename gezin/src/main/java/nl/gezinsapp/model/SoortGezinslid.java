package nl.gezinsapp.model;

public enum SoortGezinslid {
    OUDER, KIND
}

import {Injectable} from '@angular/core';
import {DatumMetGebeurtenissen} from "../common/model/datum-met-gebeurtenissen.model";
import {TijdMetGebeurtenissen} from "../common/model/tijd-met-gebeurtenissen.model";
import {Gebeurtenis} from "../common/model/gebeurtenis.model";
import moment from "moment/moment";
import {Gezinslid} from "../../api/gezin";

@Injectable({
  providedIn: 'root'
})
export class DatumMetGebeurtenissenService {

  constructor() {
  }

  flatten(gebeurtenissen: DatumMetGebeurtenissen[]): DatumMetGebeurtenissen[] {
    let result: DatumMetGebeurtenissen[] = [];

    const datums = new Set<string>();

    gebeurtenissen
      .filter((datumMetGebeurtenissen: DatumMetGebeurtenissen) => {
        return datumMetGebeurtenissen.datum != undefined;
      })
      .forEach((datumMetGebeurtenissen: DatumMetGebeurtenissen) => {
        datums.add(datumMetGebeurtenissen.datum!);
      });

    datums.forEach((s: string) => {
      result.push({
        datum: s,
        tijdMetGebeurtenissen: []
      })
    });

    gebeurtenissen.forEach((datumMetGebeurtenissen: DatumMetGebeurtenissen) => {
      let index = [...datums].findIndex(s => {
        return s == datumMetGebeurtenissen.datum;
      });

      datumMetGebeurtenissen.tijdMetGebeurtenissen.forEach((tijdMetGebeurtenissen: TijdMetGebeurtenissen) => {
        let tijdstipindex = result[index].tijdMetGebeurtenissen.findIndex((res: TijdMetGebeurtenissen) => {
          return res.van == tijdMetGebeurtenissen.van && res.tot == tijdMetGebeurtenissen.tot;
        });

        if (tijdstipindex == -1) {
          let nieuw: TijdMetGebeurtenissen = {gebeurtenissen: []};
          nieuw.van = tijdMetGebeurtenissen.van;
          nieuw.tot = tijdMetGebeurtenissen.tot;
          tijdMetGebeurtenissen.gebeurtenissen.forEach((gebeurtenis: Gebeurtenis) => {
            nieuw.gebeurtenissen.push(gebeurtenis);
          });
          result[index].tijdMetGebeurtenissen.push(tijdMetGebeurtenissen);
        } else {
          tijdMetGebeurtenissen.gebeurtenissen.forEach((gebeurtenis: Gebeurtenis) => {
            let gebeurtenisindex = result[index].tijdMetGebeurtenissen[tijdstipindex].gebeurtenissen.findIndex((gb: Gebeurtenis) => {
              return gb.soortDatum == gebeurtenis.soortDatum
                && gb.omschrijving == gebeurtenis.omschrijving;
            });

            if (gebeurtenisindex == -1) {
              result[index].tijdMetGebeurtenissen[tijdstipindex].gebeurtenissen.push(gebeurtenis);
            } else {
              let gl: Set<Gezinslid> = new Set<Gezinslid>();
              result[index].tijdMetGebeurtenissen[tijdstipindex].gebeurtenissen[gebeurtenisindex].gezinsleden!.forEach((lid: Gezinslid) => {
                gl.add(lid);
              });
              gebeurtenis.gezinsleden!.forEach((lid: Gezinslid) => {
                gl.add(lid);
              });
              result[index].tijdMetGebeurtenissen[tijdstipindex].gebeurtenissen[gebeurtenisindex].gezinsleden = [...gl];
            }
          });
        }
      });
    });

    let sorted = this.sortDatumMetGebeurtenissen(result);
    sorted.forEach((datumMetGebeurtenissen: DatumMetGebeurtenissen) => {
      datumMetGebeurtenissen.tijdMetGebeurtenissen = this.sortTijdMetGebeurtenissen(datumMetGebeurtenissen);
    });
    return sorted;
  }

  private sortDatumMetGebeurtenissen(datumMetGebeurtenissens: DatumMetGebeurtenissen[]): DatumMetGebeurtenissen[] {
    return datumMetGebeurtenissens.sort((tijdstip1: DatumMetGebeurtenissen, tijdstip2: DatumMetGebeurtenissen): number => {
      let datum1: moment.Moment = moment(tijdstip1.datum);
      let datum2: moment.Moment = moment(tijdstip2.datum);
      if (datum1.isBefore(datum2)) {
        return -1;
      } else if (datum2.isBefore(datum1)) {
        return 1;
      } else {
        return 0;
      }
    });
  }

  private sortTijdMetGebeurtenissen(datumMetGebeurtenissen: DatumMetGebeurtenissen): TijdMetGebeurtenissen[] {
    return datumMetGebeurtenissen.tijdMetGebeurtenissen.sort((tijdstip1: TijdMetGebeurtenissen, tijdstip2: TijdMetGebeurtenissen): number => {
      let van1: moment.Moment = moment(datumMetGebeurtenissen.datum + ' ' + tijdstip1.van);
      let van2: moment.Moment = moment(datumMetGebeurtenissen.datum + ' ' + tijdstip2.van);
      if (van1.isBefore(van2)) {
        return -1;
      } else if (van2.isBefore(van1)) {
        return 1;
      } else {
        let tot1: moment.Moment = moment(datumMetGebeurtenissen.datum + ' ' + tijdstip1.tot);
        let tot2: moment.Moment = moment(datumMetGebeurtenissen.datum + ' ' + tijdstip2.tot);
        if (tot1.isBefore(tot2)) {
          return -1;
        } else if (tot2.isBefore(tot1)) {
          return 1;
        } else {
          return 0;
        }
      }
    });
  }
}

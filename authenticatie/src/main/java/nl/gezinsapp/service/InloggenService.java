package nl.gezinsapp.service;

import jakarta.enterprise.context.ApplicationScoped;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.gezinsapp.commons.aspect.MethodeLogging;
import nl.gezinsapp.exception.GebruikerNietGevondenException;
import nl.gezinsapp.exception.OnjuistWachtwoordException;
import nl.gezinsapp.exception.TeLangGeledenException;
import nl.gezinsapp.model.GebruikerImpl;
import nl.gezinsapp.model.InlogPoging;
import nl.gezinsapp.model.InloggenRequest;
import nl.gezinsapp.model.Resultaat;

import java.time.LocalDateTime;

@Slf4j
@ApplicationScoped
@RequiredArgsConstructor
public class InloggenService {
    private final GebruikerService gebruikerService;
    private final InlogPogingService inlogPogingService;
    private final RefreshTokenService refreshTokenService;

    @MethodeLogging
    public GebruikerImpl inloggen(InloggenRequest inloggenRequest, InlogPoging inlogPoging) throws OnjuistWachtwoordException, GebruikerNietGevondenException, TeLangGeledenException {
        var gebruikerUitDatabase = gebruikerService.getGebruiker(inloggenRequest.emailadres());
        var laatsteInlogPoging = inlogPogingService.haalLaatsteInlogPoging(gebruikerUitDatabase.getId());
        if (gebruikerUitDatabase != null) {
            inlogPoging = inlogPogingService.setGebruiker(inlogPoging, gebruikerUitDatabase.getId());
        }
        log.debug("gebruikerUitDatabase {}", gebruikerUitDatabase);

        var token = refreshTokenService.get(gebruikerUitDatabase.getId());

        if (!token.isPresent()) {
            if (laatsteInlogPoging.isPresent()) {
                var ip = laatsteInlogPoging.get();
                var weekgeleden = LocalDateTime.now().minusWeeks(2);
                if (ip.getResultaat() == Resultaat.GELUKT && ip.getTijdstip().isBefore(weekgeleden)) {
                    inlogPogingService.setResultaat(inlogPoging, Resultaat.TWEE_FACTOR_NODIG);
                    throw new TeLangGeledenException();
                }
            }
        }

        var inloggendeGebruiker = new GebruikerImpl();
        inloggendeGebruiker.setSalt(gebruikerUitDatabase.getSalt());
        inloggendeGebruiker = gebruikerService.setEmailadresEnWachtwoord(inloggendeGebruiker, inloggenRequest.emailadres(), inloggenRequest.wachtwoord());

        var wachtwoord = gebruikerUitDatabase.getWachtwoord();
        if (wachtwoord != null && !wachtwoord.equals(inloggendeGebruiker.getWachtwoord())) {
            inlogPogingService.setResultaat(inlogPoging, Resultaat.FOUT_WACHTWOORD);
            throw new OnjuistWachtwoordException();
        }

        return gebruikerUitDatabase;
    }

}

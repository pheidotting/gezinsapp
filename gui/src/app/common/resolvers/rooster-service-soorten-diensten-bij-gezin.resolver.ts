import {ActivatedRouteSnapshot, ResolveFn, RouterStateSnapshot} from '@angular/router';
import {inject} from '@angular/core';
import {RoosterService, SoortenDiensten,} from "../../../api/kalender";

export const roosterServiceSoortenDienstenBijGezin: ResolveFn<SoortenDiensten> = (route: ActivatedRouteSnapshot, state: RouterStateSnapshot) => {
  const roosterService = inject(RoosterService);

  return roosterService.soortenDienstenBijGezin();
}

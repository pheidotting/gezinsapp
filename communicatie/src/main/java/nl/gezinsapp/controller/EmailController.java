package nl.gezinsapp.controller;

import io.smallrye.common.annotation.RunOnVirtualThread;
import jakarta.transaction.Transactional;
import jakarta.ws.rs.Path;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.gezinsapp.commons.aspect.AlsBeheerder;
import nl.gezinsapp.commons.aspect.MethodeLogging;
import nl.gezinsapp.commons.aspect.TrackAndTraceId;
import nl.gezinsapp.domain.Email;
import nl.gezinsapp.mapper.EmailMapper;
import nl.gezinsapp.service.ActivatieCodeAangemaaktService;
import org.openapi.quarkus.communicatie_openapi_yaml.api.EmailApi;
import org.openapi.quarkus.communicatie_openapi_yaml.model.Emails;

@Slf4j
@RequiredArgsConstructor
@Path("/api/communicatie/leesemails/{soort}")
public class EmailController implements EmailApi {
    private final ActivatieCodeAangemaaktService activatieCodeAangemaaktService;
    private final EmailMapper emailMapper;

    @Override
    @Transactional
    @TrackAndTraceId
    @AlsBeheerder
    @RunOnVirtualThread
    @MethodeLogging
    public Emails leesEmails(String soort) {
        String onderwerp = "";
        if (soort.equalsIgnoreCase("ACTIVATIECODE")) {
            onderwerp = activatieCodeAangemaaktService.getOnderwerp();
        }

        return new Emails().emails(Email.find("onderwerp", onderwerp).list()
                .stream().map(Email.class::cast)
                .map(emailMapper::map)
                .toList());
    }
}

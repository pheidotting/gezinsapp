package nl.gezinsapp.controller;

import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.HttpHeaders;
import jakarta.ws.rs.core.Response;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.gezinsapp.commons.aspect.MethodeLogging;
import nl.gezinsapp.commons.aspect.TrackAndTraceId;
import nl.gezinsapp.mapper.InloggenRequestMapper;
import nl.gezinsapp.model.repository.RefreshTokenRepository;
import nl.gezinsapp.service.VoorbereidenVoorInloggenService;
import org.openapi.quarkus.authenticatie_openapi_yaml.api.InEnUitloggenApi;
import org.openapi.quarkus.authenticatie_openapi_yaml.model.InloggenRequest;
import org.openapi.quarkus.authenticatie_openapi_yaml.model.JwtResponse;
import org.openapi.quarkus.authenticatie_openapi_yaml.model.UitloggenRequest;

import java.util.UUID;

@Slf4j
@RequiredArgsConstructor
public class InEnUitloggenController implements InEnUitloggenApi {
    private final RefreshTokenRepository refreshTokenRepository;
    private final InloggenRequestMapper inloggenRequestMapper;
    private final VoorbereidenVoorInloggenService voorbereidenVoorInloggenService;
    @Context
    private HttpHeaders headers;

    @Override
    @MethodeLogging
    @TrackAndTraceId
    public JwtResponse inloggen(InloggenRequest inloggenRequest) {
        log.info("Poging tot inloggen met e-mailadres : {}", inloggenRequest.getEmailadres());
        return voorbereidenVoorInloggenService.inloggen(inloggenRequestMapper.map(inloggenRequest), headers);
    }

    @Override
    @MethodeLogging
    @TrackAndTraceId
    public Response uitloggen(UitloggenRequest uitloggenRequest) {
        log.info("refreshToken {} uitloggen", uitloggenRequest.getRefreshToken());
        refreshTokenRepository.remove(UUID.fromString(uitloggenRequest.getRefreshToken()));

        return Response.ok().build();
    }
}

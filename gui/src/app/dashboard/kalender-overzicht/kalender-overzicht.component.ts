import {Component, OnInit} from '@angular/core';
import {GezinService, LeesGezin} from "../../../api/gezin";
import {Afspraak, AfspraakService, LijstAfspraken, RoosterService, SoortDienst, SoortenDiensten,} from "../../../api/kalender";
import moment from 'moment';
import {DatumUtils} from "../../services/datum-utils.service";
import {DatumMetGebeurtenissen} from "../../common/model/datum-met-gebeurtenissen.model";
import {SoortDatumEnum} from "../../common/model/gebeurtenis.model";
import {AfspraakNaarDatumMetGebeurtenisService} from "../../services/afspraak-naar-datum-met-gebeurtenis.service";
import {MaakDagOpPipe} from "../../pipes/maak-dag-op.pipe";
import {MaakDatumOpPipe} from "../../pipes/maak-datum-op.pipe";
import {GezinslidVoornaamPipe} from "../../pipes/gezinslid-voornaam.pipe";
import {AvatarComponent} from "../../common/avatar/avatar.component";
import {VerjaardagNaarDatumMetGebeurtenisService} from "../../services/verjaardag-naar-datum-met-gebeurtenis.service";
import {DatumMetGebeurtenissenService} from "../../services/datum-met-gebeurtenissen.service";

@Component({
  selector: 'app-kalender-overzicht',
  templateUrl: './kalender-overzicht.component.html',
  imports: [
    MaakDagOpPipe,
    MaakDatumOpPipe,
    GezinslidVoornaamPipe,
    AvatarComponent
  ]
})
export class KalenderOverzichtComponent implements OnInit {
  gezin?: LeesGezin;
  lijstAfspraken: LijstAfspraken = {
    afspraken: []
  };
  lijstGebeurtenissen: DatumMetGebeurtenissen[] = [];
  soortenDiensten: SoortDienst[] = [];

  constructor(
    private gezinService: GezinService,
    private readonly datumUtils: DatumUtils,
    public afspraakService: AfspraakService,
    private roosterservice: RoosterService,
    private readonly afspraakNaarDatumMetGebeurtenisService: AfspraakNaarDatumMetGebeurtenisService,
    private readonly verjaardagNaarDatumMetGebeurtenisService: VerjaardagNaarDatumMetGebeurtenisService,
    private readonly datumMetGebeurtenissenService: DatumMetGebeurtenissenService
  ) {
  }

  ngOnInit(): void {
    this.gezinService.leesHuidigGezin().subscribe((res: LeesGezin) => {
      this.roosterservice.soortenDienstenBijGezin().subscribe((soortenDiensten: SoortenDiensten) => {
        this.soortenDiensten = soortenDiensten.soortenDiensten;
        this.gezin = res;
        let nu: moment.Moment = moment();
        let overEenJaar: moment.Moment = moment().add(1, "year");

        this.afspraakService.alleAfspraken(nu.format('YYYY-MM-DD'), overEenJaar.format('YYYY-MM-DD')).subscribe((lijstAfspraken: LijstAfspraken) => {
          this.lijstAfspraken = lijstAfspraken;
          this.verwerkDatums();
        });
      });
    });
  }

  verwerkDatums(): void {
    this.lijstGebeurtenissen = [];
    if (this.gezin != null) {
      this.verjaardagNaarDatumMetGebeurtenisService.transform(this.gezin, this.lijstGebeurtenissen).forEach((datumMetGebeurtenissen: DatumMetGebeurtenissen) => {
        this.lijstGebeurtenissen.push(datumMetGebeurtenissen);
      });
    }
    this.lijstAfspraken.afspraken.forEach((afspraak: Afspraak) => {
      this.lijstGebeurtenissen.push(this.afspraakNaarDatumMetGebeurtenisService.transform(afspraak, this.lijstGebeurtenissen, this.gezin!.gezinsleden, this.soortenDiensten));
    });
    this.lijstGebeurtenissen = this.datumMetGebeurtenissenService.flatten(this.lijstGebeurtenissen);
  }

  protected readonly SoortDatumEnum = SoortDatumEnum;
  protected readonly JSON = JSON;
}

import {Component, Input} from '@angular/core';
import {GezinslidUtil} from "../../../services/gezinslid-util.service";
import moment from "moment";
import {RoosterService, SoortenDiensten} from '../../../../api/kalender';
import {SoortDienst, SoortDienstDagEnum} from "../../../../api/kalender/model/soortDienst";
import {Gezinslid} from '../../../../api/gezin';
import {AlgemeneResponse} from "../../../../api/gezin/model/algemeneResponse";
import {MeldingenComponent} from "../../../common/meldingen/meldingen.component";
import {AvatarComponent} from "../../../common/avatar/avatar.component";
import {SoortDienstRegelComponent} from "../soort-dienst-regel/soort-dienst-regel.component";
import {ModalComponent} from "../../../common/modal/modal.component";
import {NgIf} from "@angular/common";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-soort-dienst',
  templateUrl: './soort-dienst.component.html',
  imports: [
    MeldingenComponent,
    AvatarComponent,
    SoortDienstRegelComponent,
    ModalComponent,
    NgIf
  ]
})
export class SoortDienstComponent {
  @Input() gezinslid?: Gezinslid;
  @Input() index?: number;
  tonen: string = '';
  soortenDiensten?: SoortenDiensten;
  teVerwijderenSoortDienst?: SoortDienst;
  foutmeldingen: string[] = [];
  meldingen: string[] = [];
  goedmeldingen: string[] = [];

  verwijderModalTonen: boolean = false;

  constructor(
    private readonly activatedRoute: ActivatedRoute,
    public readonly gezinslidUtil: GezinslidUtil,
    private readonly roosterService: RoosterService
  ) {
  }

  maakNaamOp(gezinslid?: Gezinslid): String {
    if (gezinslid != null) {
      return this.gezinslidUtil.maakNaamOp(gezinslid);
    } else {
      return '';
    }
  }

  toggle() {
    if (this.tonen == '') {
      this.haalDataUitResolver();
      this.tonen = 'show';
      if (this.soortenDiensten?.soortenDiensten.length == 0) {
        this.nieuw();
      }
    } else {
      this.tonen = '';
    }
  }

  haalDataUitResolver(): void {
    this.activatedRoute.data.subscribe((response: any) => {
      this.soortenDiensten = JSON.parse(JSON.stringify(response.soortenDiensten));
      this.soortenDiensten!.soortenDiensten = this.soortenDiensten!.soortenDiensten
        .filter((soortDienst: SoortDienst) => {
          return soortDienst.gezinslid == this.gezinslid!.id;
        });
      this.verwerkData();
    });
  }

  haalDataVanServer(): void {
    this.roosterService.soortenDiensten(this.gezinslid!.id).subscribe((soortenDiensten: SoortenDiensten) => {
      this.soortenDiensten = soortenDiensten;
      this.verwerkData();
    });
  }

  verwerkData(): void {
    const weekdayOrder: SoortDienstDagEnum[] = Object.values(SoortDienstDagEnum);
    this.soortenDiensten!.soortenDiensten = this.soortenDiensten!.soortenDiensten.sort((a: SoortDienst, b: SoortDienst) => {
      let sort: number = weekdayOrder.indexOf(a.dag!) - weekdayOrder.indexOf(b.dag!);
      if (sort == 0) {
        let vanA: string = moment(moment().format('YYYY-MM-DD') + ' ' + a.van).format('HHMM');
        let vanB: string = moment(moment().format('YYYY-MM-DD') + ' ' + b.van).format('HHMM');
        let result: number = +vanA - +vanB;
        if (result == 0) {
          let totA: string = moment(moment().format('YYYY-MM-DD') + ' ' + a.tot).format('HHMM');
          let totB: string = moment(moment().format('YYYY-MM-DD') + ' ' + b.tot).format('HHMM');
          return +totA - +totB;
        }
        return result;
      }
      return sort;
    });
  }

  nieuw(): void {
    this.soortenDiensten?.soortenDiensten.push({
      id: 0,
      omschrijving: '',
      van: '',
      tot: '',
      gezinslid: this.gezinslid!.id
    })
  }

  voegFoutmeldingToe(melding: string): void {
    this.foutmeldingen.push(melding);
  }

  resetFoutmeldingen(): void {
    this.foutmeldingen = [];
  }

  setTeVerwijderen(soortDienst: SoortDienst): void {
    this.verwijderModalTonen = true;
    this.teVerwijderenSoortDienst = soortDienst;
  }

  verwijderSoortDienst(): void {
    this.roosterService.verwijderSoortenDienst(this.teVerwijderenSoortDienst!.id).subscribe((response: AlgemeneResponse) => {
      this.haalDataVanServer();
      this.verwijderModalTonen = false;
    });
  }

  verwijderModalVerbergen(): void {
    this.verwijderModalTonen = false;
  }
}

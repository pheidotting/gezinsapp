import {TestBed} from '@angular/core/testing';

import {GezinnenComponent} from './gezinnen.component';
import {ActivatedRoute, provideRouter, RouterModule} from "@angular/router";
import {provideHttpClient, withInterceptorsFromDi} from "@angular/common/http";
import {HttpTestingController, provideHttpClientTesting} from "@angular/common/http/testing";
import {of} from "rxjs";

describe('GezinnenComponent', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [GezinnenComponent,
        RouterModule.forRoot([])],
      providers: [provideRouter([]), provideHttpClient(withInterceptorsFromDi()), provideHttpClientTesting()]
    });
  });

  it('should create and load data', () => {
    let fixture = TestBed.createComponent(GezinnenComponent);
    let httpTestingController = TestBed.inject(HttpTestingController);

    let activatedRoute: ActivatedRoute = fixture.debugElement.injector.get(ActivatedRoute);

    activatedRoute.data = of({
      gezinnen: {
        gezinnen: [
          {
            naam: 'A'
          },
          {
            naam: 'B'
          }
        ]
      }
    });

    fixture.detectChanges();

    let app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
    expect(app.gezinnen.length).toBe(2);
  });

  it('toggle tonen', () => {
    let fixture = TestBed.createComponent(GezinnenComponent);

    fixture.detectChanges();

    let app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();

    app.tonen[5] = '';

    app.toggle(5);

    expect(app.tonen[5]).toEqual('show');

    app.toggle(5);

    expect(app.tonen[5]).toEqual('');
  });
});

package nl.gezinsapp.service;

import nl.gezinsapp.model.enums.Dag;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class AfspraakServiceTest {
    private AfspraakService afspraakService = new AfspraakService();

    @Test
    void getMonths() {
        var result = afspraakService.getMonths(LocalDate.of(2024, 1, 1), LocalDate.of(2024, 5, 1));
        assertEquals(5, result.size());
        result = afspraakService.getMonths(LocalDate.of(2024, 1, 1), LocalDate.of(2025, 5, 1));
        assertEquals(17, result.size());
    }

    @Test
    void valtTussenDag() {
        var start = LocalDate.of(2024, 12, 2);
        var drie = LocalDate.of(2024, 12, 3);
        var negen = LocalDate.of(2024, 12, 9);
        var tien = LocalDate.of(2024, 12, 10);
        var eind = LocalDate.of(2024, 12, 11);

        assertEquals(List.of(start, negen), afspraakService.valtTussen(Dag.MA, start, eind));
        assertEquals(List.of(drie, tien), afspraakService.valtTussen(Dag.DI, start, eind));
        assertEquals(List.of(), afspraakService.valtTussen(Dag.WO, start, drie));
        assertEquals(List.of(), afspraakService.valtTussen(Dag.DO, start, drie));
    }

    @Test
    void valtTussenDag2() {
        var start = LocalDate.of(2024, 12, 2);
        var eind = LocalDate.of(2024, 12, 2);

        assertEquals(List.of(start), afspraakService.valtTussen(Dag.MA, start, eind));
        assertEquals(List.of(), afspraakService.valtTussen(Dag.DI, start, eind));
        assertEquals(List.of(), afspraakService.valtTussen(Dag.WO, start, eind));
        assertEquals(List.of(), afspraakService.valtTussen(Dag.DO, start, eind));
    }
}
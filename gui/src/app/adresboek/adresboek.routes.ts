import {Routes} from '@angular/router';
import {LijstAdresBoekenComponent} from "./lijst-adres-boeken/lijst-adres-boeken.component";
import {adresBoekGuard} from "../guards/adres-boek.guard";
import {LijstAdresBoekenResolver} from "./lijst-adres-boeken.resolver";
import {AdresBoekComponent} from "./adres-boek/adres-boek.component";
import {AdresBoekResolver} from "./adres-boek.resolver";
import {lijstGezinsledenResolver} from "../gezin/lijst-gezinsleden.resolver";

export const ADRESBOEK_ROUTES: Routes = [
  {
    path: '',
    component: LijstAdresBoekenComponent,
    canActivate: [adresBoekGuard],
    resolve: {
      adresBoeken: LijstAdresBoekenResolver,
      gezin: lijstGezinsledenResolver
    }
  },
  {
    path: ':id',
    component: AdresBoekComponent,
    canActivate: [adresBoekGuard],
    resolve: {
      adresBoek: AdresBoekResolver,
      gezin: lijstGezinsledenResolver
    }
  },
  {
    path: ':id/nieuw',
    component: AdresBoekComponent,
    canActivate: [adresBoekGuard],
    resolve: {
      gezin: lijstGezinsledenResolver
    }
  }
]

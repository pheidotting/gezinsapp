package nl.gezinsapp.integratietesten.common.random;

import nl.gezinsapp.model.SoortDienst;
import uk.co.jemos.podam.api.DefaultClassInfoStrategy;
import uk.co.jemos.podam.api.PodamFactory;
import uk.co.jemos.podam.api.PodamFactoryImpl;

public class SoortDienstDatum {
    private static PodamFactory factory;

    private static PodamFactory getFactory() {
        if (factory == null) {
            factory = new PodamFactoryImpl();
            DefaultClassInfoStrategy classInfoStrategy = DefaultClassInfoStrategy.getInstance();
            classInfoStrategy.addExcludedField(org.openapi.quarkus.kalender_openapi_yaml.model.SoortDienst.class, "id");
            classInfoStrategy.addExcludedField(org.openapi.quarkus.kalender_openapi_yaml.model.SoortDienst.class, "gezinslid");
            classInfoStrategy.addExcludedField(SoortDienst.class, "id");
            classInfoStrategy.addExcludedField(SoortDienst.class, "dag");
            factory.setClassStrategy(classInfoStrategy);
        }
        return factory;
    }

    public static SoortDienst randomSoortDienst() {
        return getFactory().manufacturePojo(SoortDienst.class);
    }

    public static org.openapi.quarkus.kalender_openapi_yaml.model.SoortDienst randomMsgSoortDienst() {
        var msg = getFactory().manufacturePojo(org.openapi.quarkus.kalender_openapi_yaml.model.SoortDienst.class);
        var domain = randomSoortDienst();
        msg.setVan(domain.getVan().toString());
        msg.setTot(domain.getTot().toString());
        return msg;
    }
}

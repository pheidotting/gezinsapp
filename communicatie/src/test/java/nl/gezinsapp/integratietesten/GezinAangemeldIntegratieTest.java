package nl.gezinsapp.integratietesten;

import io.quarkus.test.junit.QuarkusTest;
import lombok.extern.slf4j.Slf4j;
import nl.gezinsapp.integratietesten.common.QueueUtil;
import nl.gezinsapp.integratietesten.common.TestGegevens;
import nl.gezinsapp.messaging.model.GezinAangemeld;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.openapi.quarkus.gezin_openapi_yaml.model.Gezin;
import org.openapi.quarkus.gezin_openapi_yaml.model.Gezinslid;

import java.time.Duration;
import java.util.concurrent.TimeUnit;

import static io.restassured.RestAssured.given;
import static org.awaitility.Awaitility.await;
import static org.junit.jupiter.api.Assertions.*;
import io.quarkus.test.common.QuarkusTestResource;

@Slf4j
@QuarkusTest
@QuarkusTestResource(MockServer.class)
public class GezinAangemeldIntegratieTest extends AbstractIntegratieTest {
    @ConfigProperty(name = "mp.messaging.incoming.gezin-aangemeld.exchange.name")
    private String exchange;
    @ConfigProperty(name = "rabbitmq-host")
    private String rabbitHost;
    @ConfigProperty(name = "rabbitmq-port")
    private String rabbitPort;
    @ConfigProperty(name = "rabbitmq-username")
    private String rabbitUsername;
    @ConfigProperty(name = "rabbitmq-password")
    private String rabbitPassword;

    @Test
    @DisplayName("Test een correcte aanmelding van het gezin")
    void test() {
        final var gezinId = 33L;

        maakDatabaseLeeg();
given()         .when().delete("http://localhost:" + mailpitContainer.getMappedPort(8025) + "/api/v1/messages");

        TestGegevens.setRabbitHost(rabbitHost);
        TestGegevens.setRabbitPort(Integer.valueOf(rabbitPort));
        TestGegevens.setRabbitUser(rabbitUsername);
        TestGegevens.setRabbitPassword(rabbitPassword);

        var queueUtil = maakGezinAangemeldQueueUtil();
        var gezin = new Gezin();
        gezin.setNaam("GezinAangemeld : The Simpsons");
        var gezinslid = new Gezinslid();
        gezinslid.setEmailadres("a@b.c");
        gezinslid.setSoortGezinslid(Gezinslid.SoortGezinslidEnum.OUDER);
        gezin.getGezinsleden().add(gezinslid);
        mockGezin(gezinId, gezin);

        var bericht = new GezinAangemeld();
        bericht.setGezinsId(gezinId);

        queueUtil.stuurBerichtNaarExchange(bericht);

        await()
                .atMost(40, TimeUnit.SECONDS)
                .pollInterval(Duration.ofMillis(500))
                .until(() -> {
                    var email = leesEmail();
                    assertEquals("Welkom bij de gezinsapp!", email.getOnderwerp());
                    assertEquals("<h1>a@b.c</h1>\n" +
                            "<h1>GezinAangemeld : The Simpsons</h1>", email.getTekst());
                    assertNotNull(email.getTijdstipaangemaakt());
                    assertNull(email.getTijdstipverstuurd());
                    return true;
                });

    given().when().get("/verstuurmails");

        var response = given()
                .when().get("http://localhost:" + mailpitContainer.getMappedPort(8025) + "/api/v1/messages");
        assertTrue(response.getBody().prettyPrint().contains("\"messages_count\": 1,"));
        assertTrue(response.getBody().prettyPrint().contains("\"Address\": \"gezinsapp@heidotting.nl\""));
        assertTrue(response.getBody().prettyPrint().contains("\"Address\": \"a@b.c\""));
        assertTrue(response.getBody().prettyPrint().contains("\"Subject\": \"Welkom bij de gezinsapp!\","));
        assertTrue(response.getBody().prettyPrint().contains("\"Snippet\": \"<h1>a@b.c</h1> <h1>GezinAangemeld : The Simpsons</h1>\""));

        var email = leesEmail();
        assertNotNull(email.getTijdstipaangemaakt());
        assertNotNull(email.getTijdstipverstuurd());
    }

    private QueueUtil<GezinAangemeld> maakGezinAangemeldQueueUtil() {
        return new QueueUtil<>() {
            @Override
            public String getExchange() {
                return exchange;
            }

            @Override
            public Class<GezinAangemeld> getClazz() {
                return GezinAangemeld.class;
            }
        };
    }

}

module.exports = {
  platform: 'gitlab',
  endpoint: 'https://gitlab.com/api/v4/',
  autodiscover: true,
  token: "glpat-7MJS_GuYD-CdWBgt6Z7u",
  assignees: [
    'pheidotting'
  ],
  baseBranches: [
    'development'
  ],
  labels: [
    'renovate'
  ],
  extends: [
    'config:base'
  ],
  prConcurrentLimit: 50,
  fileMatch: [
    "(^|/|\\.)pom\\.xml$",
    "^(((\\.mvn)|(\\.m2))/)?settings\\.xml$"
  ],
  versioning: "maven"
};

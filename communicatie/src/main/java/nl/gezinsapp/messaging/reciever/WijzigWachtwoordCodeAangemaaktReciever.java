package nl.gezinsapp.messaging.reciever;

import nl.gezinsapp.commons.messaging.AbstractReciever;

import io.smallrye.common.annotation.Blocking;
import jakarta.enterprise.context.ApplicationScoped;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.gezinsapp.commons.aspect.MethodeLogging;
import nl.gezinsapp.mapper.WijzigWachtwoordCodeAangemaaktMapper;
import nl.gezinsapp.messaging.model.WijzigWachtwoordCodeAangemaakt;
import nl.gezinsapp.service.WijzigWachtwoordCodeAangemaaktService;
import org.eclipse.microprofile.reactive.messaging.Incoming;
import org.eclipse.microprofile.reactive.messaging.Message;
import org.slf4j.MDC;

import java.util.concurrent.CompletionStage;
@Slf4j
@ApplicationScoped
@RequiredArgsConstructor
public class WijzigWachtwoordCodeAangemaaktReciever extends AbstractReciever<WijzigWachtwoordCodeAangemaakt> {
    private final WijzigWachtwoordCodeAangemaaktService wijzigWachtwoordCodeAangemaaktService;
    private final WijzigWachtwoordCodeAangemaaktMapper wijzigWachtwoordCodeAangemaaktMapper;

    @Incoming("wijzigwachtwoordcode-aangemaakt")
    @MethodeLogging
    @Blocking
    public CompletionStage<Void> verwerk(Message<WijzigWachtwoordCodeAangemaakt> m) {
        var wijzigWachtwoordCodeAangemaakt = getObject(m, WijzigWachtwoordCodeAangemaakt.class);

        if(wijzigWachtwoordCodeAangemaakt.getTrackAndTraceId()!=null){
            MDC.put("trackAndTraceId",wijzigWachtwoordCodeAangemaakt.getTrackAndTraceId().toString());
        }

        log.info("Ontvangen : {}", wijzigWachtwoordCodeAangemaakt);

        wijzigWachtwoordCodeAangemaaktService.maakEmail(wijzigWachtwoordCodeAangemaaktMapper.map(wijzigWachtwoordCodeAangemaakt));

        return m.ack();
    }
}

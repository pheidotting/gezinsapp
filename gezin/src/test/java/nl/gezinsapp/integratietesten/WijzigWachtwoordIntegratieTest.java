package nl.gezinsapp.integratietesten;

import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;
import lombok.extern.slf4j.Slf4j;
import nl.gezinsapp.integratietesten.common.QueueUtil;
import nl.gezinsapp.integratietesten.common.TestGegevens;
import nl.gezinsapp.messaging.model.WachtwoordGewijzigd;
import nl.gezinsapp.messaging.model.WijzigWachtwoordCodeAangemaakt;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.openapi.quarkus.gezin_openapi_yaml.model.AlgemeneResponse;
import org.openapi.quarkus.gezin_openapi_yaml.model.VraagWijzigWachtwoordCodeOp;
import org.openapi.quarkus.gezin_openapi_yaml.model.WijzigWachtwoord;
import org.openapi.quarkus.gezin_openapi_yaml.model.WijzigWachtwoordCode;

import static nl.gezinsapp.integratietesten.common.random.RandomGezin.randomDomainGezin;
import static nl.gezinsapp.integratietesten.common.random.RandomGezinslid.randomDomainGezinslid;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

@Slf4j
@QuarkusTest
@QuarkusTestResource(MockServer.class)
class WijzigWachtwoordIntegratieTest extends AbstractIntegratieTest {
    @ConfigProperty(name = "mp.messaging.outgoing.wijzigwachtwoordcode-aangemaakt.exchange.name")
    private String wijzigwachtwoordcodeAaangemaaktExchange;
    @ConfigProperty(name = "mp.messaging.outgoing.wachtwoordgewijzigd.exchange.name")
    private String wachtwoordgewijzigdExchange;
    @ConfigProperty(name = "rabbitmq-host")
    private String rabbitHost;
    @ConfigProperty(name = "rabbitmq-port")
    private String rabbitPort;
    @ConfigProperty(name = "rabbitmq-username")
    private String rabbitUsername;
    @ConfigProperty(name = "rabbitmq-password")
    private String rabbitPassword;

    @Test
    @DisplayName("Test het wijzigen van het wachtwoord")
    void testWijzigWachtwoord() {
        final String EMAILADRES = "a@b.c";

        TestGegevens.setRabbitHost(rabbitHost);
        TestGegevens.setRabbitPort(Integer.valueOf(rabbitPort));
        TestGegevens.setRabbitUser(rabbitUsername);
        TestGegevens.setRabbitPassword(rabbitPassword);

        var wijzigWachtwoordCodeAangemaaktQueueUtil = maakWijzigWachtwoordCodeAangemaaktQueueUtil();
        var wachtwoordGewijzigdQueueUtil = maakWachtwoordGewijzigdQueueUtil();

        maakDatabaseLeeg();

        var gezin = randomDomainGezin();
        opslaanGezin(gezin);
        var gezinslid = randomDomainGezinslid();
        gezinslid.setGezin(gezin);
        gezinslid.setEmailadres(EMAILADRES);
        opslaanGezinslid(gezinslid);

        var vraagWijzigWachtwoordCodeOp = new VraagWijzigWachtwoordCodeOp().email(EMAILADRES);
        var wijzigWachtwoordCodeDto = postViaApi(WijzigWachtwoordCode.class, "/api/gezin/wijzigwachtwoordcode/genereer", vraagWijzigWachtwoordCodeOp, null, null);
        var wijzigWachtwoordCodeAangemaakt = wijzigWachtwoordCodeAangemaaktQueueUtil.vangBericht();
        assertEquals(vraagWijzigWachtwoordCodeOp.getEmail(), wijzigWachtwoordCodeAangemaakt.get().getEmailadres());
        var code = wijzigWachtwoordCodeAangemaakt.get().getCode();

        var wijzigWachtwoord = new WijzigWachtwoord()
                .code(code)
                .email(gezinslid.getEmailadres())
                .codeid(wijzigWachtwoordCodeDto.getId().toString())
                .wachtwoord("nieuwwachtwoord");
        var response = postViaApi(AlgemeneResponse.class, "/api/gezin/wijzigwachtwoord", wijzigWachtwoord, null, null);
        assertNull(response.getFoutmeldingen());
        assertNull(response.getId());

        var wachtwoordGewijzigdOptional = wachtwoordGewijzigdQueueUtil.vangBericht();
        assertEquals(gezinslid.getEmailadres(), wachtwoordGewijzigdOptional.get().getEmailadres());
    }

    private QueueUtil<WijzigWachtwoordCodeAangemaakt> maakWijzigWachtwoordCodeAangemaaktQueueUtil() {
        return new QueueUtil<>() {
            @Override
            public String getExchange() {
                return wijzigwachtwoordcodeAaangemaaktExchange;
            }

            @Override
            public Class<WijzigWachtwoordCodeAangemaakt> getClazz() {
                return WijzigWachtwoordCodeAangemaakt.class;
            }
        };
    }

    private QueueUtil<WachtwoordGewijzigd> maakWachtwoordGewijzigdQueueUtil() {
        return new QueueUtil<>() {
            @Override
            public String getExchange() {
                return wachtwoordgewijzigdExchange;
            }

            @Override
            public Class<WachtwoordGewijzigd> getClazz() {
                return WachtwoordGewijzigd.class;
            }
        };
    }
}

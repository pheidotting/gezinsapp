package nl.gezinsapp.service;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.transaction.Transactional;
import jakarta.ws.rs.core.HttpHeaders;
import lombok.RequiredArgsConstructor;
import nl.gezinsapp.commons.aspect.MethodeLogging;
import nl.gezinsapp.exception.GebruikerNietGevondenException;
import nl.gezinsapp.exception.OnjuistWachtwoordException;
import nl.gezinsapp.exception.TeLangGeledenException;
import nl.gezinsapp.mapper.GebruikerMapper;
import nl.gezinsapp.model.GebruikerImpl;
import nl.gezinsapp.model.InloggenRequest;
import nl.gezinsapp.model.Resultaat;
import nl.gezinsapp.utils.JwtUtils;
import org.openapi.quarkus.authenticatie_openapi_yaml.model.JwtResponse;

import java.util.Optional;
import java.util.UUID;

import static java.util.Optional.empty;
import static java.util.Optional.ofNullable;
import static org.openapi.quarkus.authenticatie_openapi_yaml.model.JwtResponse.StatusEnum.*;

@ApplicationScoped
@RequiredArgsConstructor
public class VoorbereidenVoorInloggenService {
    private final InloggenService inloggenService;
    private final JwtUtils jwtUtils;
    private final RefreshTokenService refreshTokenService;
    private final GebruikerMapper gebruikerMapper;
    private final InlogPogingService inlogPogingService;

    @MethodeLogging
    @Transactional
    public JwtResponse inloggen(InloggenRequest inloggenRequest, HttpHeaders headers) {
        var inlogPoging = inlogPogingService.maakInlogPoging(headers);
        Optional<GebruikerImpl> gebruiker;
        JwtResponse.StatusEnum status = null;
        try {
            gebruiker = ofNullable(inloggenService.inloggen(inloggenRequest, inlogPoging));
        } catch (GebruikerNietGevondenException | OnjuistWachtwoordException e) {
            gebruiker = empty();
            status = GEBRUIKER_NIET_GEVONDEN_OF_ONJUIST_WACHTWOORD;
        } catch (TeLangGeledenException e) {
            gebruiker = empty();
            status = TWEE_FACTOR_NODIG;
        }

        var jwtResponse = new JwtResponse();
        if (gebruiker.isPresent()) {
            status = GELUKT;
            inlogPogingService.setResultaat(inlogPoging, Resultaat.GELUKT);
            var jwt = jwtUtils.generateJwtToken(gebruiker.get());

            var token = UUID.randomUUID();
            refreshTokenService.createRefreshToken(jwt, gebruiker.get().getId(), token);

            jwtResponse.accessToken(jwt);
            jwtResponse.refreshToken(token);
            jwtResponse.status(status);
            jwtResponse.setGebruiker(gebruikerMapper.map(gebruiker.get()));
        }
        jwtResponse.status(status);

        return jwtResponse;
    }
}

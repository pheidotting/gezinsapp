package nl.gezinsapp.domain;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;
import jakarta.persistence.Column;
import jakarta.persistence.Convert;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.Singular;
import lombok.ToString;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "TAAK")
@Getter
@Setter
@ToString(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Taak extends PanacheEntityBase {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;
    @Column(name = "GEZINSLEDEN")
    @Convert(converter = ListConverter.class)
    @Singular("gezinslid")
    private List<Long> gezinsleden = new ArrayList<>();
    @Column(name = "GEZIN")
    private Long gezin;
    @Column(name = "OMSCHRIJVING")
    private String omschrijving;
    @Column(name = "DEADLINE")
    private LocalDateTime deadline;
    @Column(name = "TIJDSTIPAFGESTREEPT")
    private LocalDateTime tijdstipAfgestreept;
}

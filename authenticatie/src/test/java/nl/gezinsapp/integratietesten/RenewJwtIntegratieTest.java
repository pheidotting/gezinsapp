package nl.gezinsapp.integratietesten;

import io.quarkus.test.junit.QuarkusTest;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockserver.model.JsonBody;
import org.openapi.quarkus.authenticatie_openapi_yaml.model.InloggenRequest;
import org.openapi.quarkus.authenticatie_openapi_yaml.model.JwtResponse;
import org.openapi.quarkus.authenticatie_openapi_yaml.model.TokenRefreshRequest;

import java.security.NoSuchAlgorithmException;
import java.util.UUID;

import static nl.gezinsapp.common.random.RandomGezinslid.randomGebruikerImpl;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockserver.model.HttpRequest.request;
import static org.mockserver.model.HttpResponse.response;

@Slf4j
@QuarkusTest
class RenewJwtIntegratieTest extends AbstractIntegratieTest {
    @Test
    @DisplayName("Test Check JWT")
    void test() throws NoSuchAlgorithmException {
        var salt = gebruikerService.hash(UUID.randomUUID().toString());

        var mail = "email5";
        var wachtwoord = "wachtwoord";
        var gezinslid = randomGebruikerImpl();
        gezinslid.setEmailadres(mail);
        gezinslid.setWachtwoord(wachtwoord);
        gezinslid.setSalt(salt);
        gebruikerService.setHashWachtwoord(gezinslid, wachtwoord);
        var trackAndTraceId = UUID.randomUUID().toString();

        MockServer.getMockServerClient()
                .when(
                        request()
                                .withMethod("GET")
                                .withPath("/api/gezin/gebruiker/" + mail)
                                .withHeader("trackAndTraceId", trackAndTraceId)
                )
                .respond(
                        response()
                                .withStatusCode(200)
                                .withHeader("Content-Type", "application/json")
                                .withBody(JsonBody.json(gezinslid))
                );
        MockServer.getMockServerClient()
                .when(
                        request()
                                .withMethod("GET")
                                .withPath("/api/gezin/gebruiker-op-id/" + gezinslid.getId())
                                .withHeader("trackAndTraceId", trackAndTraceId)
                )
                .respond(
                        response()
                                .withStatusCode(200)
                                .withHeader("Content-Type", "application/json")
                                .withBody(JsonBody.json(gezinslid))
                );

        var inloggenRequest = new InloggenRequest();
        inloggenRequest.setEmailadres(mail);
        inloggenRequest.setWachtwoord(wachtwoord);

        var response = postViaApi(JwtResponse.class, "/api/authorisatie/inloggen", inloggenRequest, null, trackAndTraceId);

        assertEquals("GELUKT", response.getStatus().name());

        var tokenRefreshRequest = new TokenRefreshRequest();
        tokenRefreshRequest.setRefreshToken(response.getRefreshToken());

        var jwtResponse = postViaApi(JwtResponse.class, "/api/authorisatie/renewjwt", tokenRefreshRequest, null, trackAndTraceId);
        assertEquals(gezinslid.getId(), jwtResponse.getGebruiker().getId());
        assertNotNull(jwtResponse.getAccessToken());
        assertNotNull(jwtResponse.getRefreshToken());
    }
}

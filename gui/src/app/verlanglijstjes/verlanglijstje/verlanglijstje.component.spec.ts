import {fakeAsync, TestBed, tick} from "@angular/core/testing";
import {ActivatedRoute, provideRouter, RouterModule} from "@angular/router";
import {provideHttpClient, withInterceptorsFromDi} from "@angular/common/http";
import {provideHttpClientTesting} from "@angular/common/http/testing";
import {of} from "rxjs";
import {VerlanglijstjeComponent} from "./verlanglijstje.component";

describe('VerlanglijstjeComponent', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [VerlanglijstjeComponent,
        RouterModule.forRoot([])],
      providers: [provideRouter([]), provideHttpClient(withInterceptorsFromDi()), provideHttpClientTesting()]
    });
  });
  it('should create', fakeAsync(() => {
    let fixture = TestBed.createComponent(VerlanglijstjeComponent);

    let app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));

  it('should load data on startup', fakeAsync(() => {
    let fixture = TestBed.createComponent(VerlanglijstjeComponent);

    let activatedRoute: ActivatedRoute = fixture.debugElement.injector.get(ActivatedRoute);

    activatedRoute.data = of({
      gezin: {
        naam: 'Heidotting',
        gezinsleden: [{
          id: 2,
          voornaam: 'v',
          tussenvoegsel: 't',
          achternaam: 'a'
        }]
      }, verlanglijstje: {
        verlanglijstjes: [
          {
            omschrijving: 'abc',
            verlanglijstItems: [
              {
                omschrijving: 'def'
              }
            ]
          }
        ]
      }
    });

    fixture.detectChanges();
    tick();

    let app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
    expect(app.gezin.naam).toBe('Heidotting');
    expect(app.verlanglijstjes.length).toBe(1);
    expect(app.verlanglijstjes[0].omschrijving).toBe('abc');
    expect(app.verlanglijstjes[0].verlanglijstItems.length).toBe(1);
    expect(app.verlanglijstjes[0].verlanglijstItems[0].omschrijving).toBe('def');
    expect(app.gezinslid(2)).toEqual('v t a');
  }));
});

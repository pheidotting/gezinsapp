import {TestBed} from '@angular/core/testing';
import {CanActivateFn} from '@angular/router';

import {adresBoekGuard} from './adres-boek.guard';

describe('adresBoekGuard', () => {
  const executeGuard: CanActivateFn = (...guardParameters) =>
    TestBed.runInInjectionContext(() => adresBoekGuard(...guardParameters));

  beforeEach(() => {
    TestBed.configureTestingModule({});
  });

  it('should be created', () => {
    expect(executeGuard).toBeTruthy();
  });
});

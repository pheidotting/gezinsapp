import {Gezinslid} from "../../../api/gezin";


export class Gebeurtenis {
  gezinsleden?: Gezinslid[];
  omschrijving?: string;
  soortDatum?: SoortDatumEnum;
}

export enum SoortDatumEnum {
  Huwelijk = 'HUWELIJK',
  Verjaardag = 'VERJAARDAG',
  Overig = 'OVERIG'
};

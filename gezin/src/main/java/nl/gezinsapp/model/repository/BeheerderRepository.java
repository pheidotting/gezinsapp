package nl.gezinsapp.model.repository;

import io.quarkus.hibernate.orm.panache.PanacheRepository;
import jakarta.enterprise.context.ApplicationScoped;
import nl.gezinsapp.model.Beheerder;

@ApplicationScoped
public class BeheerderRepository implements PanacheRepository<Beheerder> {
}

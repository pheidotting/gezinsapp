import {TijdMetGebeurtenissen} from "./tijd-met-gebeurtenissen.model";

export class DatumMetGebeurtenissen {
  datum?: string;
  tijdMetGebeurtenissen: TijdMetGebeurtenissen[] = [];
}

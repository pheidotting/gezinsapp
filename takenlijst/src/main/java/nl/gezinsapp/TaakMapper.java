package nl.gezinsapp;

import nl.gezinsapp.domain.Taak;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;

import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.List;

@Mapper(componentModel = "CDI")
public abstract class TaakMapper {
    public abstract Taak map(org.openapi.quarkus.takenlijst_openapi_yaml.model.Taak takenlijst);

    public abstract List<org.openapi.quarkus.takenlijst_openapi_yaml.model.Taak> map(List<Taak> takenlijst);

    public abstract Taak update(org.openapi.quarkus.takenlijst_openapi_yaml.model.Taak takenlijst, @MappingTarget Taak bestaand);

    protected LocalDateTime map(OffsetDateTime value) {
        return value == null ? null : value.toLocalDateTime();
    }

    protected OffsetDateTime map(LocalDateTime value) {
        return value == null ? null : OffsetDateTime.of(value, ZoneOffset.ofHours(2));
    }
}

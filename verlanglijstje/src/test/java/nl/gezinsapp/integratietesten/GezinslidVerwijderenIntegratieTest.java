package nl.gezinsapp.integratietesten;

import com.fasterxml.jackson.core.JsonProcessingException;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;
import lombok.extern.slf4j.Slf4j;
import nl.gezinsapp.integratietesten.common.QueueUtil;
import nl.gezinsapp.integratietesten.common.TestGegevens;
import nl.gezinsapp.messaging.model.GezinslidVerwijderd;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.UUID;
import java.util.concurrent.TimeUnit;

import static nl.gezinsapp.integratietesten.random.RandomVerlanglijst.randomDomainVerlanglijst;
import static org.awaitility.Awaitility.await;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

@Slf4j
@QuarkusTest
@QuarkusTestResource(MockServer.class)
class GezinslidVerwijderenIntegratieTest extends AbstractIntegratieTest {
    @ConfigProperty(name = "mp.messaging.incoming.gezinslidverwijderd.exchange.name")
    private String exchange;
    @ConfigProperty(name = "rabbitmq-host")
    private String rabbitHost;
    @ConfigProperty(name = "rabbitmq-port")
    private String rabbitPort;
    @ConfigProperty(name = "rabbitmq-username")
    private String rabbitUsername;
    @ConfigProperty(name = "rabbitmq-password")
    private String rabbitPassword;

    @Test
    @DisplayName("Test dat de verlanglijstjes worden verwijderd")
    void test() throws JsonProcessingException {
        TestGegevens.setRabbitHost(rabbitHost);
        TestGegevens.setRabbitPort(Integer.valueOf(rabbitPort));
        TestGegevens.setRabbitUser(rabbitUsername);
        TestGegevens.setRabbitPassword(rabbitPassword);

        maakDatabaseLeeg();

        var gezinslid1 = 33L;
        var gezinslid2 = 46L;

        var accesstoken = maakToken();
        mockCheckJwt(accesstoken);

        var verlanglijst1 = randomDomainVerlanglijst(gezinslid1);
        opslaan(verlanglijst1);
        var verlanglijst2 = randomDomainVerlanglijst(gezinslid2);
        opslaan(verlanglijst2);

        queue().stuurBerichtNaarExchange(new GezinslidVerwijderd("a@b.c", 1L, 2L, UUID.randomUUID(), gezinslid1));

        await()
                .atMost(5, TimeUnit.SECONDS)
                .until(() -> {
                    try {
                        assertNull(leesVerlanglijst(verlanglijst1.getId()).getGezinslid());
                        assertNotNull(leesVerlanglijst(verlanglijst2.getId()).getGezinslid());
                        return true;
                    } catch (AssertionError ae) {
                        return false;
                    }
                });
    }

    private QueueUtil<GezinslidVerwijderd> queue() {
        return new QueueUtil<>() {
            @Override
            public String getExchange() {
                return exchange;
            }

            @Override
            public Class<GezinslidVerwijderd> getClazz() {
                return GezinslidVerwijderd.class;
            }
        };
    }
}


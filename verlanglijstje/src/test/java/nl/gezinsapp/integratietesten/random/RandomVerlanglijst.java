package nl.gezinsapp.integratietesten.random;

import nl.gezinsapp.domain.Verlanglijst;
import org.openapi.quarkus.verlanglijstje_openapi_yaml.model.Verlanglijstje;
import uk.co.jemos.podam.api.DefaultClassInfoStrategy;
import uk.co.jemos.podam.api.PodamFactory;
import uk.co.jemos.podam.api.PodamFactoryImpl;

public class RandomVerlanglijst {
    private static PodamFactory factory;

    private static PodamFactory getFactory() {
        if (factory == null) {
            factory = new PodamFactoryImpl();
            DefaultClassInfoStrategy classInfoStrategy = DefaultClassInfoStrategy.getInstance();
            classInfoStrategy.addExcludedField(Verlanglijstje.class, "id");
            classInfoStrategy.addExcludedField(Verlanglijstje.class, "gezinslid");
            classInfoStrategy.addExcludedField(Verlanglijstje.class, "verlanglijstItems");
            classInfoStrategy.addExcludedField(Verlanglijst.class, "id");
            classInfoStrategy.addExcludedField(Verlanglijst.class, "gezinslid");
            classInfoStrategy.addExcludedField(Verlanglijst.class, "verlanglijstItems");
            factory.setClassStrategy(classInfoStrategy);
        }
        return factory;
    }

    public static Verlanglijstje randomMsgVerlanglijst() {
        return getFactory().manufacturePojo(Verlanglijstje.class);
    }
    public static Verlanglijstje randomMsgVerlanglijst(Long gezinslidId) {
        var lijst = getFactory().manufacturePojo(Verlanglijstje.class);
        lijst.setGezinslid(gezinslidId);

        return lijst;
    }

    public static Verlanglijst randomDomainVerlanglijst(Long gezinslidId) {
        var lijst = getFactory().manufacturePojo(Verlanglijst.class);
        lijst.setGezinslid(gezinslidId);

        return lijst;
    }
}

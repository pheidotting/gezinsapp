package nl.gezinsapp.controller;

import io.smallrye.common.annotation.RunOnVirtualThread;
import jakarta.transaction.Transactional;
import jakarta.ws.rs.Path;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.gezinsapp.aspect.ThreadVariabelen;
import nl.gezinsapp.client.PostcodeClient;
import nl.gezinsapp.commons.aspect.AlsGezinslid;
import nl.gezinsapp.commons.aspect.ForbiddenException;
import nl.gezinsapp.commons.aspect.MethodeLogging;
import nl.gezinsapp.commons.aspect.TrackAndTraceId;
import nl.gezinsapp.mapper.AdresBoekMapper;
import nl.gezinsapp.mapper.AdresMapper;
import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.openapi.quarkus.adresboek_openapi_yaml.api.AdresboekApi;
import org.openapi.quarkus.adresboek_openapi_yaml.model.Adres;
import org.openapi.quarkus.adresboek_openapi_yaml.model.AdresBijPostcode;
import org.openapi.quarkus.adresboek_openapi_yaml.model.AdresBoek;
import org.openapi.quarkus.adresboek_openapi_yaml.model.AdresBoekMetAdressen;
import org.openapi.quarkus.adresboek_openapi_yaml.model.AdresBoeken;
import org.openapi.quarkus.adresboek_openapi_yaml.model.AlgemeneResponse;
import org.openapi.quarkus.adresboek_openapi_yaml.model.Foutmeldingen;

@Slf4j
@Path("/api/adresboek")
@RequiredArgsConstructor
public class AdresBoekController implements AdresboekApi {
    private final AdresBoekMapper adresBoekMapper;
    private final AdresMapper adresMapper;
    @RestClient
    private PostcodeClient postcodeClient;

    @Override
    @TrackAndTraceId
    @AlsGezinslid
    @RunOnVirtualThread
    @MethodeLogging
    public AdresBijPostcode adresBijPostcode(String postcode, String huisnummer) {
        log.info("Adres ophalen bij postcode {} en huisnummer {}", postcode, huisnummer);

        if (postcode != null && huisnummer != null) {
            var response = postcodeClient.haalAdresOp(postcode, huisnummer);

            return new AdresBijPostcode()
                    .straat(response.getString("street"))
                    .postcode(response.getString("postcode"))
                    .huisnummer(response.getString("house_number"))
                    .plaats(response.getString("city"));
        } else {
            return new AdresBijPostcode();
        }
    }

    @Override
    @Transactional
    @TrackAndTraceId
    @AlsGezinslid
    @RunOnVirtualThread
    @MethodeLogging
    public AlgemeneResponse adresBoekToevoegen(AdresBoek adresBoek) {
        var gebruiker = ThreadVariabelen.get();

        var opTeSlaan = adresBoekMapper.map(adresBoek);
        opTeSlaan.setGezin(gebruiker.getGezin());
        opTeSlaan.persist();

        return new AlgemeneResponse().id(opTeSlaan.getId());
    }

    @Override
    @Transactional
    @TrackAndTraceId
    @AlsGezinslid
    @RunOnVirtualThread
    @MethodeLogging
    public AlgemeneResponse wijzigAdresBoek(AdresBoek adresBoek) {
        var gebruiker = ThreadVariabelen.get();

        var response = new AlgemeneResponse();

        nl.gezinsapp.model.AdresBoek.findByIdOptional(adresBoek.getId())
                .ifPresentOrElse(entity -> {
                            var adr = (nl.gezinsapp.model.AdresBoek) entity;

                            if (!adr.getGezin().equals(gebruiker.getGezin())) {
                                throw new ForbiddenException();
                            }

                            var opTeSlaan = adresBoekMapper.update(adresBoek, adr);
                            opTeSlaan.setGezin(gebruiker.getGezin());
                            opTeSlaan.persist();

                            response.setId(adresBoek.getId());
                        },
                        () -> response.setFoutmeldingen(new Foutmeldingen().addMeldingenItem(String.format("AdresBoek met id %s werd niet gevonden.", adresBoek.getId()))));

        return response;
    }


    @Override
    @Transactional
    @TrackAndTraceId
    @AlsGezinslid
    @RunOnVirtualThread
    @MethodeLogging
    public AdresBoeken lijstAdresBoeken() {
        var gebruiker = ThreadVariabelen.get();

        var result = new AdresBoeken();
        result.setAdresBoeken(nl.gezinsapp.model.AdresBoek.find("gezin", gebruiker.getGezin())
                .stream()
                .map(nl.gezinsapp.model.AdresBoek.class::cast)
                .map(adresBoekMapper::map)
                .toList());

        return result;
    }

    @Override
    @Transactional
    @TrackAndTraceId
    @AlsGezinslid
    @RunOnVirtualThread
    @MethodeLogging
    public AlgemeneResponse verwijderAdresBoek(Long id) {
        var response = new AlgemeneResponse();

        nl.gezinsapp.model.AdresBoek.findByIdOptional(id)
                .ifPresent(panacheEntityBase -> {
                    var adresBoek = (nl.gezinsapp.model.AdresBoek) panacheEntityBase;
                    if (!adresBoek.getGezin().equals(ThreadVariabelen.get().getGezin())) {
                        throw new ForbiddenException();
                    }

                    nl.gezinsapp.model.AdresBoek.deleteById(id);
                });

        nl.gezinsapp.model.AdresBoek.deleteById(id);

        return response;
    }

    @Override
    @Transactional
    @TrackAndTraceId
    @AlsGezinslid
    @RunOnVirtualThread
    @MethodeLogging
    public AdresBoekMetAdressen leesAdresBoek(Long id) {
        final AdresBoekMetAdressen[] result = {null};
        nl.gezinsapp.model.AdresBoek.findByIdOptional(id)
                .ifPresent(entityBase -> {
                    var adresBoek = (nl.gezinsapp.model.AdresBoek) entityBase;
                    if (!adresBoek.getGezin().equals(ThreadVariabelen.get().getGezin())) {
                        throw new ForbiddenException();
                    }
                    result[0] = adresBoekMapper.mapMetAdressen(adresBoek);
                });
        return result[0];
    }

    @Override
    @Transactional
    @TrackAndTraceId
    @AlsGezinslid
    @RunOnVirtualThread
    @MethodeLogging
    public AlgemeneResponse adresToevoegen(Adres adres) {
        var adresBoek = (nl.gezinsapp.model.AdresBoek) nl.gezinsapp.model.AdresBoek.findById(adres.getAdresBoek());

        if (!adresBoek.getGezin().equals(ThreadVariabelen.get().getGezin())) {
            throw new ForbiddenException();
        }

        var gemapt = adresMapper.map(adres);
        gemapt.setPostcode(gemapt.getPostcode().substring(0, 6));

        gemapt.setAdresBoek(adresBoek);
        gemapt.persist();

        return new AlgemeneResponse().id(gemapt.getId());
    }

    @Override
    @Transactional
    @TrackAndTraceId
    @AlsGezinslid
    @RunOnVirtualThread
    @MethodeLogging
    public AlgemeneResponse verwijderAdres(Long id) {
        nl.gezinsapp.model.Adres.findByIdOptional(id)
                .ifPresent(panacheEntityBase -> {
                    var adres = (nl.gezinsapp.model.Adres) panacheEntityBase;

                    if (!adres.getAdresBoek().getGezin().equals(ThreadVariabelen.get().getGezin())) {
                        throw new ForbiddenException();
                    }

                    adres.delete();
                });


        return new AlgemeneResponse();
    }

    @Override
    @Transactional
    @TrackAndTraceId
    @AlsGezinslid
    @RunOnVirtualThread
    @MethodeLogging
    public AlgemeneResponse wijzigAdres(Adres adr) {
        nl.gezinsapp.model.Adres.findByIdOptional(adr.getId())
                .ifPresent(panacheEntityBase -> {
                    var adres = (nl.gezinsapp.model.Adres) panacheEntityBase;
                    var adresBoek = adres.getAdresBoek();
                    log.info("Gezin {}", adres.getAdresBoek().getGezin());
                    log.info("Gezin {}", ThreadVariabelen.get().getGezin());

                    if (!adres.getAdresBoek().getGezin().equals(ThreadVariabelen.get().getGezin())) {
                        throw new ForbiddenException();
                    }

                    var gewijzigd = adresMapper.update(adr, adres);
                    gewijzigd.setPostcode(gewijzigd.getPostcode().substring(0, 6));

                    gewijzigd.setAdresBoek(adresBoek);

                    gewijzigd.persist();
                });

        return new AlgemeneResponse();
    }
}

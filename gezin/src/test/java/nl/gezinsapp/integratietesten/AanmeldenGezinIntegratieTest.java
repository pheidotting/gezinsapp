package nl.gezinsapp.integratietesten;

import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;
import lombok.extern.slf4j.Slf4j;
import nl.gezinsapp.integratietesten.common.QueueUtil;
import nl.gezinsapp.integratietesten.common.TestGegevens;
import nl.gezinsapp.messaging.model.GezinAangemeld;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockserver.client.server.MockServerClient;
import org.mockserver.model.JsonBody;
import org.openapi.quarkus.authenticatie_openapi_yaml.model.GenereerSaltEnHashWachtwoordResponse;
import org.openapi.quarkus.gezin_openapi_yaml.model.AanmeldenGezin;
import org.openapi.quarkus.gezin_openapi_yaml.model.AlgemeneResponse;
import org.openapi.quarkus.gezin_openapi_yaml.model.ControleerActiveringsCode;
import org.openapi.quarkus.gezin_openapi_yaml.model.Gezinslid;
import uk.co.jemos.podam.api.PodamFactoryImpl;

import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockserver.model.HttpRequest.request;
import static org.mockserver.model.HttpResponse.response;
import static org.openapi.quarkus.gezin_openapi_yaml.model.Gezinslid.SoortGezinslidEnum.KIND;
import static org.openapi.quarkus.gezin_openapi_yaml.model.Gezinslid.SoortGezinslidEnum.OUDER;

@Slf4j
@QuarkusTest
@QuarkusTestResource(MockServer.class)
class AanmeldenGezinIntegratieTest extends AbstractIntegratieTest {
    @ConfigProperty(name = "mp.messaging.outgoing.gezin-aangemeld.exchange.name")
    private String exchange;
    @ConfigProperty(name = "rabbitmq-host")
    private String rabbitHost;
    @ConfigProperty(name = "rabbitmq-port")
    private String rabbitPort;
    @ConfigProperty(name = "rabbitmq-username")
    private String rabbitUsername;
    @ConfigProperty(name = "rabbitmq-password")
    private String rabbitPassword;

    @Test
    @DisplayName("Test een correcte aanmelding van het gezin")
    void test() {
        TestGegevens.setRabbitHost(rabbitHost);
        TestGegevens.setRabbitPort(Integer.valueOf(rabbitPort));
        TestGegevens.setRabbitUser(rabbitUsername);
        TestGegevens.setRabbitPassword(rabbitPassword);

        var queueUtil = maakGezinAangemeldQueueUtil();

        maakDatabaseLeeg();

        var genereerSaltEnHashWachtwoordResponse = new GenereerSaltEnHashWachtwoordResponse()
                .salt("salt")
                .emailadres("emailadres")
                .hashwachtwoord("wachtwoord");

        MockServer.getMockServerClient()
                .when(
                        request()
                                .withMethod("GET")
                                .withPath("/api/authorisatie/gebruiker")
                                .withHeader("trackAndTraceId")
                )
                .respond(
                        response()
                                .withStatusCode(200)
                                .withHeader("Content-Type", "application/json")
                                .withBody(JsonBody.json(genereerSaltEnHashWachtwoordResponse))
                );

        var activatieCode = activatieCodeRepository.set("123456");

        var aanmeldenGezin = new AanmeldenGezin();
        var controleerActiveringsCode = new ControleerActiveringsCode();
        controleerActiveringsCode.setId(activatieCode.getId());
        controleerActiveringsCode.setCode(activatieCode.getCode());
        aanmeldenGezin.setActiveringsCode(controleerActiveringsCode);
        aanmeldenGezin
                .naam("Heidotting & Co")
                .gezinsleden(List.of(maakGezinslid(OUDER), maakGezinslid(KIND)));

        var result = putViaApi(AlgemeneResponse.class, "/api/gezin", aanmeldenGezin, null, null);

        assertEquals(1, gezinRepository.findAll().list().size());
        assertEquals(2, gezinslidRepository.findAll().list().size());

        assertNotNull(result.getId());
        assertNull(result.getFoutmeldingen());

        var gezinAangemeld = queueUtil.vangBericht();
        gezinAangemeld.ifPresentOrElse(gezinAangemeld1 -> {
            assertEquals(aanmeldenGezin.getGezinsleden().get(0).getEmailadres(), gezinAangemeld1.getEmailadres());
        }, () -> fail("Geen GezinAangemeld ontvangen"));
    }

    @Test
    @DisplayName("Test een aanmelding van het gezin waarbij de activatiecode onjuist is")
    void test1() {
        TestGegevens.setRabbitHost(rabbitHost);
        TestGegevens.setRabbitPort(Integer.valueOf(rabbitPort));
        TestGegevens.setRabbitUser(rabbitUsername);
        TestGegevens.setRabbitPassword(rabbitPassword);

        maakDatabaseLeeg();

        var activatieCode = activatieCodeRepository.set("123456");

        var aanmeldenGezin = new AanmeldenGezin();
        var controleerActiveringsCode = new ControleerActiveringsCode();
        controleerActiveringsCode.setId(activatieCode.getId());
        controleerActiveringsCode.setCode("234567");
        aanmeldenGezin.setActiveringsCode(controleerActiveringsCode);
        aanmeldenGezin
                .naam("Heidotting & Co")
                .gezinsleden(List.of(maakGezinslid(OUDER), maakGezinslid(KIND)));

        var result = putViaApi(AlgemeneResponse.class, "/api/gezin", aanmeldenGezin, null, null);

        assertEquals(0, gezinRepository.findAll().list().size());
        assertEquals(0, gezinslidRepository.findAll().list().size());

        assertNull(result.getId());
        assertNotNull(result.getFoutmeldingen());
        assertEquals(1, result.getFoutmeldingen().getMeldingen().size());
        assertEquals("Activatiecode niet juist", result.getFoutmeldingen().getMeldingen().get(0));
    }

    @Test
    @DisplayName("Test een aanmelding van het gezin waarbij de activatiecode niet is opgevraagd")
    void test2() {
        TestGegevens.setRabbitHost(rabbitHost);
        TestGegevens.setRabbitPort(Integer.valueOf(rabbitPort));
        TestGegevens.setRabbitUser(rabbitUsername);
        TestGegevens.setRabbitPassword(rabbitPassword);

        maakDatabaseLeeg();

        var aanmeldenGezin = new AanmeldenGezin();
        aanmeldenGezin
                .naam("Heidotting & Co")
                .gezinsleden(List.of(maakGezinslid(OUDER), maakGezinslid(KIND)));

        var result = putViaApi(AlgemeneResponse.class, "/api/gezin", aanmeldenGezin, null, null);

        assertEquals(0, gezinRepository.findAll().list().size());
        assertEquals(0, gezinslidRepository.findAll().list().size());

        assertNull(result.getId());
        assertNotNull(result.getFoutmeldingen());
        assertEquals(1, result.getFoutmeldingen().getMeldingen().size());
        assertEquals("Activatiecode niet ingevuld", result.getFoutmeldingen().getMeldingen().get(0));
    }

    private QueueUtil<GezinAangemeld> maakGezinAangemeldQueueUtil() {
        return new QueueUtil<>() {
            @Override
            public String getExchange() {
                return exchange;
            }

            @Override
            public Class<GezinAangemeld> getClazz() {
                return GezinAangemeld.class;
            }
        };
    }

    private Gezinslid maakGezinslid(Gezinslid.SoortGezinslidEnum soort) {
        return new PodamFactoryImpl().manufacturePojo(Gezinslid.class).id(null).soortGezinslid(soort);
    }
}

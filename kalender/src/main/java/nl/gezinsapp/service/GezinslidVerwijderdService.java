package nl.gezinsapp.service;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import nl.gezinsapp.commons.aspect.MethodeLogging;

@ApplicationScoped
@RequiredArgsConstructor
public class GezinslidVerwijderdService {
    private final AfspraakService afspraakService;
    private final SoortDienstService soortDienstService;

    @MethodeLogging
    @Transactional
    public void verwerkVerwijderdgezinslid(Long gezinslidId) {
        afspraakService.verwijderAfsprakenBijGezinslid(gezinslidId);
        soortDienstService.verwijderSoortDienstBijGezinslid(gezinslidId);
    }
}

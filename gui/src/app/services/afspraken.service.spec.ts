import {TestBed} from '@angular/core/testing';

import {AfsprakenService} from './afspraken.service';
import {Afspraak, AfspraakSoortAfspraakEnum, AlgemeneResponse} from '../../api/kalender';
import {EventInput} from "@fullcalendar/core";

describe('AfsprakenService', () => {
  let service: AfsprakenService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AfsprakenService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('mapAfspraakNaarEventInput hele dag', () => {
    let afspraak: Afspraak = {
      omschrijving: 'abc',
      heledag: true,
      vanDatum: 'vanDatum',
      totDatum: 'totDatum',
      vanTijd: 'vanTijd',
      totTijd: 'totTijd',
      soortAfspraak: AfspraakSoortAfspraakEnum.Huwelijk
    };

    let result: EventInput = service.mapAfspraakNaarEventInput(afspraak);
    expect(result.title).toEqual(afspraak.omschrijving);
    expect(result.allDay).toEqual(afspraak.heledag);
    expect(result.start).toEqual(afspraak.vanDatum);
    expect(result.end).toEqual(afspraak.totDatum);
    expect(result.extendedProps!['soortAfspraak']).toEqual('HUWELIJK');
  });
  it('mapAfspraakNaarEventInput geen hele dag', () => {
    let afspraak: Afspraak = {
      omschrijving: 'abc',
      heledag: false,
      vanDatum: 'vanDatum',
      totDatum: 'totDatum',
      vanTijd: 'vanTijd',
      totTijd: 'totTijd',
      soortAfspraak: AfspraakSoortAfspraakEnum.Verjaardag
    };

    let result: EventInput = service.mapAfspraakNaarEventInput(afspraak);
    expect(result.title).toEqual(afspraak.omschrijving);
    expect(result.allDay).toEqual(afspraak.heledag);
    expect(result.start).toEqual(afspraak.vanTijd);
    expect(result.end).toEqual(afspraak.totTijd);
    expect(result.extendedProps!['soortAfspraak']).toEqual('VERJAARDAG');
  });
  it('setNieuweOfTeWijzigenAfspraak', () => {
    let extendedProps: any = {
      soortAfspraak: 'HUWELIJK',
      terugkeer: 'NIET',
      gezinsleden: [1]
    }
    let clickInfo: any = {
      event: {
        id: 2,
        allDay: true,
        startStr: '2024-07-04T14:30',
        endStr: '2024-07-04T15:40',
        title: 'Test',
        display: 'HUWELIJK',
        extendedProps: extendedProps
      }
    };

    service.gezinsleden = [{id: 1, checked: false}];

    service.setNieuweOfTeWijzigenAfspraak(clickInfo);

    expect(service.nieuweOfTeWijzigenAfspraak.id).toEqual(2);
    expect(service.nieuweOfTeWijzigenAfspraak.heledag).toEqual(true);
    expect(service.nieuweOfTeWijzigenAfspraak.vanDatum).toEqual('2024-07-04');
    expect(service.nieuweOfTeWijzigenAfspraak.totDatum).toEqual('2024-07-04');
    expect(service.nieuweOfTeWijzigenAfspraak.vanTijd).toEqual('2024-07-04T14:30');
    expect(service.nieuweOfTeWijzigenAfspraak.totTijd).toEqual('2024-07-04T15:40');
    expect(service.nieuweOfTeWijzigenAfspraak.omschrijving).toEqual('Test');
    expect(service.nieuweOfTeWijzigenAfspraak.soortAfspraak).toEqual(AfspraakSoortAfspraakEnum.Huwelijk);
    expect(service.nieuweOfTeWijzigenAfspraak.gezinsleden).toEqual([1]);
  });
  it('checkAfspraak met lege omschrijving', () => {
    let result: string[] = service.checkAfspraak();

    expect(result.length).toEqual(1);
    expect(result[0]).toEqual('Omschrijving moet gevuld zijn');
  });
  it('checkAfspraak met gevulde omschrijving, totDatum ligt voor vanDatum', () => {
    service.nieuweOfTeWijzigenAfspraak.omschrijving = 'gevuld';
    service.nieuweOfTeWijzigenAfspraak.vanDatum = '2024-07-02';
    service.nieuweOfTeWijzigenAfspraak.totDatum = '2024-07-01';
    let result: string[] = service.checkAfspraak();

    expect(result.length).toEqual(1);
    expect(result[0]).toEqual('Begindatum moet voor einddatum liggen');
  });
  it('checkAfspraak met gevulde omschrijving, totTijd ligt voor vanTijd', () => {
    service.nieuweOfTeWijzigenAfspraak.omschrijving = 'gevuld';
    service.nieuweOfTeWijzigenAfspraak.vanTijd = '2024-07-01T09:01';
    service.nieuweOfTeWijzigenAfspraak.totTijd = '2024-07-01T09:00';
    let result: string[] = service.checkAfspraak();

    expect(result.length).toEqual(1);
    expect(result[0]).toEqual('Begintijd moet voor eindtijd liggen');
  });
  it('zetKlaarVoorOpslaan, hele dag', () => {
    service.nieuweOfTeWijzigenAfspraak.vanTijd = '2024-07-01T09:00';
    service.nieuweOfTeWijzigenAfspraak.totTijd = '2024-07-01T10:00';
    service.nieuweOfTeWijzigenAfspraak.vanDatum = '2024-07-01';
    service.nieuweOfTeWijzigenAfspraak.totDatum = '2024-07-02';
    service.nieuweOfTeWijzigenAfspraak.heledag = true;

    service.zetKlaarVoorOpslaan();

    expect(service.nieuweOfTeWijzigenAfspraak.vanTijd).toBeUndefined();
    expect(service.nieuweOfTeWijzigenAfspraak.totTijd).toBeUndefined();
    expect(service.nieuweOfTeWijzigenAfspraak.vanDatum).toEqual('2024-07-01');
    expect(service.nieuweOfTeWijzigenAfspraak.totDatum).toEqual('2024-07-02');
  });
  it('zetKlaarVoorOpslaan, niet hele dag', () => {
    service.nieuweOfTeWijzigenAfspraak.vanTijd = '2024-07-01T09:00';
    service.nieuweOfTeWijzigenAfspraak.totTijd = '2024-07-01T10:00';
    service.nieuweOfTeWijzigenAfspraak.vanDatum = '2024-07-01';
    service.nieuweOfTeWijzigenAfspraak.totDatum = '2024-07-02';
    service.nieuweOfTeWijzigenAfspraak.heledag = false;

    service.zetKlaarVoorOpslaan();

    expect(service.nieuweOfTeWijzigenAfspraak.vanTijd).toEqual('2024-07-01T09:00:00-02:00');
    expect(service.nieuweOfTeWijzigenAfspraak.totTijd).toEqual('2024-07-01T10:00:00-02:00');
    expect(service.nieuweOfTeWijzigenAfspraak.vanDatum).toBeUndefined();
    expect(service.nieuweOfTeWijzigenAfspraak.totDatum).toBeUndefined();
  });
  it('setNieuweOfTeWijzigenAfspraakVanDateSelectArg', () => {
    let clickInfo: any = {
      allDay: true,
      startStr: '2024-07-04T14:30',
      endStr: '2024-07-04T15:40',
      title: 'Test',
      display: 'HUWELIJK'
    };

    service.setNieuweOfTeWijzigenAfspraakVanDateSelectArg(clickInfo);

    expect(service.nieuweOfTeWijzigenAfspraak.heledag).toEqual(true);
    expect(service.nieuweOfTeWijzigenAfspraak.vanDatum).toEqual('2024-07-04T14:30');
    expect(service.nieuweOfTeWijzigenAfspraak.totDatum).toEqual('2024-07-04T14:30');
    expect(service.nieuweOfTeWijzigenAfspraak.vanTijd).toEqual('2024-07-04T14:30T09:00');
    expect(service.nieuweOfTeWijzigenAfspraak.totTijd).toEqual('2024-07-04T14:30T10:00');
  });
  it('verwerkAfspraak', () => {
    service.nieuweOfTeWijzigenAfspraak.omschrijving = 'omschr';
    service.nieuweOfTeWijzigenAfspraak.heledag = true;

    let response: AlgemeneResponse = {
      foutmeldingen: {
        meldingen: []
      },
      id: 33
    };
    let result: EventInput = service.verwerkAfspraak(response);

    expect(result.id).toEqual('33');
    expect(service.nieuweOfTeWijzigenAfspraak.omschrijving).toEqual('');
    expect(service.nieuweOfTeWijzigenAfspraak.heledag).toBeFalse();
    expect(service.nieuweOfTeWijzigenAfspraak.id).toBeUndefined();
  });
});

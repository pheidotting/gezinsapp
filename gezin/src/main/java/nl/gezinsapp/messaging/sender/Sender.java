package nl.gezinsapp.messaging.sender;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import nl.gezinsapp.commons.aspect.MethodeLogging;
import nl.gezinsapp.messaging.model.ActivatieCodeAangemaakt;
import nl.gezinsapp.messaging.model.GezinAangemeld;
import nl.gezinsapp.messaging.model.GezinslidVerwijderd;
import nl.gezinsapp.messaging.model.JarigenVandaag;
import nl.gezinsapp.messaging.model.WachtwoordGewijzigd;
import nl.gezinsapp.messaging.model.WijzigWachtwoordCodeAangemaakt;
import org.eclipse.microprofile.reactive.messaging.Channel;
import org.eclipse.microprofile.reactive.messaging.Emitter;

@ApplicationScoped
public class Sender {
    @Inject
    @Channel("activatiecode-aangemaakt")
    private Emitter<ActivatieCodeAangemaakt> activatieCodeAangemaaktEmitter;
    @Inject
    @Channel("gezin-aangemeld")
    private Emitter<GezinAangemeld> gezinAangemeldEmitter;
    @Inject
    @Channel("wijzigwachtwoordcode-aangemaakt")
    private Emitter<WijzigWachtwoordCodeAangemaakt> wijzigWachtwoordCodeAangemaaktEmitter;
    @Inject
    @Channel("wachtwoordgewijzigd")
    private Emitter<WachtwoordGewijzigd> wachtwoordGewijzigdEmitter;
    @Inject
    @Channel("jarigenvandaag")
    private Emitter<JarigenVandaag> jarigenVandaagEmitter;
    @Inject
    @Channel("gezinslidverwijderd")
    private Emitter<GezinslidVerwijderd> gezinslidVerwijderdEmitter;

    @MethodeLogging
    public void verstuurActivatieCodeAangemaakt(ActivatieCodeAangemaakt activatieCodeAangemaakt) {
        activatieCodeAangemaaktEmitter.send(activatieCodeAangemaakt);
    }

    @MethodeLogging
    public void verstuurGezinAangemeld(GezinAangemeld gezinAangemeld) {
        gezinAangemeldEmitter.send(gezinAangemeld);
    }

    @MethodeLogging
    public void verstuurWijzigWachtwoordCodeAangemaakt(WijzigWachtwoordCodeAangemaakt wijzigWachtwoordCodeAangemaakt) {
        wijzigWachtwoordCodeAangemaaktEmitter.send(wijzigWachtwoordCodeAangemaakt);
    }

    @MethodeLogging
    public void verstuurWachtwoordGewijzigd(WachtwoordGewijzigd wachtwoordGewijzigd) {
        wachtwoordGewijzigdEmitter.send(wachtwoordGewijzigd);
    }

    @MethodeLogging
    public void verstuurJarigenVandaag(JarigenVandaag jarigenVandaag) {
        jarigenVandaagEmitter.send(jarigenVandaag);
    }

    @MethodeLogging
    public void verstuurGezinslidVerwijderd(GezinslidVerwijderd gezinslidVerwijderd) {
        gezinslidVerwijderdEmitter.send(gezinslidVerwijderd);
    }
}

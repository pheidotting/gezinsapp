package nl.gezinsapp.service;

import io.quarkus.qute.Template;
import io.quarkus.qute.TemplateInstance;
import jakarta.transaction.Transactional;
import lombok.extern.slf4j.Slf4j;
import nl.gezinsapp.commons.aspect.MethodeLogging;
import nl.gezinsapp.domain.Email;
import nl.gezinsapp.model.CommObject;
import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.openapi.quarkus.gezin_openapi_yaml.api.GezinApi;
import org.openapi.quarkus.gezin_openapi_yaml.model.Gezinslid;
import org.openapi.quarkus.gezin_openapi_yaml.model.LeesGezin;

import java.time.LocalDateTime;

@Slf4j
public abstract class AbstractService<T extends CommObject> {
    @RestClient
    protected GezinApi gezinApi;

    public abstract Template getTemplate();

    public abstract String getOnderwerp();

    public abstract void vulCommObject(T t, LeesGezin gezin);

    @MethodeLogging
    public void maakEmail(T t) {
        if (t.getGezinsId() != null) {
            var gezin = gezinApi.leesGezin(t.getGezinsId());
            if (t.getEmailadres() == null) {
                if (t.getGezinslidId() == null) {
                    gezin.getGezinsleden().stream()
                            .filter(gezinslid -> gezinslid.getSoortGezinslid() == Gezinslid.SoortGezinslidEnum.OUDER)
                            .filter(gezinslid -> gezinslid.getEmailadres() != null)
                            .findFirst()
                            .ifPresent(gezinslid -> t.setEmailadres(gezinslid.getEmailadres()));
                } else {
                    gezin.getGezinsleden().stream()
                            .filter(gezinslid -> gezinslid.getId() == t.getGezinslidId())
                            .findFirst()
                            .ifPresent(gezinslid -> t.setEmailadres(gezinslid.getEmailadres()));
                }
            }
            vulCommObject(t, gezin);
        }

        final TemplateInstance[] templateInstances = {getTemplate().instance()};

        t.velden().keySet().forEach(s -> templateInstances[0] = templateInstances[0].data(s, t.velden().get(s)));

        var email = new Email(null, null, t.getEmailadres(), LocalDateTime.now(), null, getOnderwerp(), templateInstances[0].render());
        opslaanEmail(email);
        if (t.getEmailadres() == null) {
            log.error("E-mailadres is leeg, e-mail is wel opgeslagen");
        }
    }

    @MethodeLogging
    @Transactional
    public void opslaanEmail(Email email) {
        email.persist();
    }
}

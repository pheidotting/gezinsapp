package nl.gezinsapp.integratietesten;

import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;
import jakarta.inject.Inject;
import lombok.extern.slf4j.Slf4j;
import nl.gezinsapp.integratietesten.common.QueueUtil;
import nl.gezinsapp.integratietesten.common.TestGegevens;
import nl.gezinsapp.messaging.model.WijzigWachtwoordCodeAangemaakt;
import nl.gezinsapp.model.repository.WijzigWachtwoordCodeRepository;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.junit.jupiter.api.Test;
import org.openapi.quarkus.gezin_openapi_yaml.model.VraagWijzigWachtwoordCodeOp;
import org.openapi.quarkus.gezin_openapi_yaml.model.WijzigWachtwoordCode;

import static nl.gezinsapp.integratietesten.common.random.RandomGezin.randomDomainGezin;
import static nl.gezinsapp.integratietesten.common.random.RandomGezinslid.randomDomainGezinslid;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@Slf4j
@QuarkusTest
@QuarkusTestResource(MockServer.class)
class GenereerWijzigWachtwoordCodeIntegratieTest extends AbstractIntegratieTest {
    @ConfigProperty(name = "mp.messaging.outgoing.wijzigwachtwoordcode-aangemaakt.exchange.name")
    private String exchange;
    @ConfigProperty(name = "rabbitmq-host")
    private String rabbitHost;
    @ConfigProperty(name = "rabbitmq-port")
    private String rabbitPort;
    @ConfigProperty(name = "rabbitmq-username")
    private String rabbitUsername;
    @ConfigProperty(name = "rabbitmq-password")
    private String rabbitPassword;
    @Inject
    private WijzigWachtwoordCodeRepository wijzigWachtwoordCodeRepository;

    @Test
    void test() {
        final String EMAILADRES = "a@b.c";
        TestGegevens.getQueues().put("opslaanbedrijfrequest", exchange);

        TestGegevens.setRabbitHost(rabbitHost);
        TestGegevens.setRabbitPort(Integer.valueOf(rabbitPort));
        TestGegevens.setRabbitUser(rabbitUsername);
        TestGegevens.setRabbitPassword(rabbitPassword);

        var gezin = randomDomainGezin();
        opslaanGezin(gezin);
        var gezinslid = randomDomainGezinslid();
        gezinslid.setEmailadres(EMAILADRES);
        gezinslid.setGezin(gezin);
        opslaanGezinslid(gezinslid);

        var queueUtil = mmaakWijzigWachtwoordCodeAangemaaktQueueUtil();

        var vraagWijzigWachtwoordCodeOp = new VraagWijzigWachtwoordCodeOp().email(EMAILADRES);
        var wijzigWachtwoordCodeDto = postViaApi(WijzigWachtwoordCode.class, "/api/gezin/wijzigwachtwoordcode/genereer", vraagWijzigWachtwoordCodeOp, null, null);

        var wijzigWachtwoordCode = wijzigWachtwoordCodeRepository.get(wijzigWachtwoordCodeDto.getId());
        assertTrue(wijzigWachtwoordCode.isPresent());

        var wijzigWachtwoordCodeAangemaakt = queueUtil.vangBericht();
        assertEquals(wijzigWachtwoordCode.get(), wijzigWachtwoordCodeAangemaakt.get().getCode());
        assertEquals(vraagWijzigWachtwoordCodeOp.getEmail(), wijzigWachtwoordCodeAangemaakt.get().getEmailadres());
    }

    private QueueUtil<WijzigWachtwoordCodeAangemaakt> mmaakWijzigWachtwoordCodeAangemaaktQueueUtil() {
        return new QueueUtil<>() {
            @Override
            public String getExchange() {
                return exchange;
            }

            @Override
            public Class<WijzigWachtwoordCodeAangemaakt> getClazz() {
                return WijzigWachtwoordCodeAangemaakt.class;
            }
        };
    }
}

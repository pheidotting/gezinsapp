package nl.gezinsapp.messaging.reciever;

import io.smallrye.common.annotation.Blocking;
import jakarta.enterprise.context.ApplicationScoped;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.gezinsapp.commons.aspect.MethodeLogging;
import nl.gezinsapp.commons.messaging.AbstractReciever;
import nl.gezinsapp.messaging.model.GezinslidVerwijderd;
import nl.gezinsapp.service.GezinslidVerwijderdService;
import org.eclipse.microprofile.reactive.messaging.Incoming;
import org.slf4j.MDC;

@Slf4j
@ApplicationScoped
@RequiredArgsConstructor
public class GezinslidVerwijderdReciever extends AbstractReciever<GezinslidVerwijderd> {
    private final GezinslidVerwijderdService gezinslidVerwijderdService;

    @Incoming("gezinslidverwijderd")
    @MethodeLogging
    @Blocking
    public void verwerk(byte[] m) {
        var gezinslidVerwijderd = mapS(new String(m), GezinslidVerwijderd.class);

        if (gezinslidVerwijderd.getTrackAndTraceId() != null) {
            MDC.put("trackAndTraceId", gezinslidVerwijderd.getTrackAndTraceId().toString());
        }

        log.info("Ontvangen : {}", gezinslidVerwijderd);

        gezinslidVerwijderdService.verwerkVerwijderdgezinslid(gezinslidVerwijderd.getId());
    }
}

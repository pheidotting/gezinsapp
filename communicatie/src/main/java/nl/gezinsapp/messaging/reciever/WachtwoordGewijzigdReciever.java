package nl.gezinsapp.messaging.reciever;

import nl.gezinsapp.commons.messaging.AbstractReciever;

import io.smallrye.common.annotation.Blocking;
import jakarta.enterprise.context.ApplicationScoped;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.gezinsapp.commons.aspect.MethodeLogging;
import nl.gezinsapp.mapper.WachtwoordGewijzigdMapper;
import nl.gezinsapp.messaging.model.WachtwoordGewijzigd;
import nl.gezinsapp.service.WachtwoordGewijzigdService;
import org.eclipse.microprofile.reactive.messaging.Incoming;
import org.eclipse.microprofile.reactive.messaging.Message;
import org.slf4j.MDC;

import java.util.concurrent.CompletionStage;
@Slf4j
@ApplicationScoped
@RequiredArgsConstructor
public class WachtwoordGewijzigdReciever extends AbstractReciever<WachtwoordGewijzigd> {
    private final WachtwoordGewijzigdService wachtwoordGewijzigdService;
    private final WachtwoordGewijzigdMapper wachtwoordGewijzigdMapper;

    @Incoming("wachtwoordgewijzigd")
    @MethodeLogging
    @Blocking
    public CompletionStage<Void> verwerk(Message<WachtwoordGewijzigd> m) {
        var wachtwoordGewijzigd = getObject(m, WachtwoordGewijzigd.class);

        if(wachtwoordGewijzigd.getTrackAndTraceId()!=null){
            MDC.put("trackAndTraceId",wachtwoordGewijzigd.getTrackAndTraceId().toString());
        }

        log.info("Ontvangen : {}", wachtwoordGewijzigd);

        wachtwoordGewijzigdService.maakEmail(wachtwoordGewijzigdMapper.map(wachtwoordGewijzigd));

        return m.ack();
    }
}

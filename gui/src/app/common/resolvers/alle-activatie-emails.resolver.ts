import {ActivatedRouteSnapshot, ResolveFn, RouterStateSnapshot} from '@angular/router';
import {inject} from '@angular/core';
import {Emails, EmailService} from '../../../api/communicatie';

export const alleActivatieEmailsResolver: ResolveFn<Emails> = (route: ActivatedRouteSnapshot, state: RouterStateSnapshot) => {
  const emailService = inject(EmailService);

  return emailService.leesEmails('ACTIVATIECODE');
}

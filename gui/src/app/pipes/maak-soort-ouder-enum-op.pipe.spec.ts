import {GezinslidSoortGezinslidEnum} from '../../api/gezin';
import {MaakSoortOuderEnumOpPipe} from './maak-soort-ouder-enum-op.pipe';

describe('MaakSoortOuderEnumOpPipe', () => {
  it('create an instance', () => {
    const pipe = new MaakSoortOuderEnumOpPipe();
    expect(pipe).toBeTruthy();

    expect(pipe.transform(GezinslidSoortGezinslidEnum.Ouder)).toBe('Ouder');
    expect(pipe.transform(GezinslidSoortGezinslidEnum.Kind)).toBe('Kind');
  });
});

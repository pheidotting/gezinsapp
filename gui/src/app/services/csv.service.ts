import {Injectable} from '@angular/core';
import {Adres} from '../../api/adresboek';

@Injectable({
  providedIn: 'root'
})
export class CSVService {

  private convertToCSV(objArray: string | { naam: string; huisnummer: number; straat: number; postcode: boolean; plaats: string; }[]
    , headerList: { [p: string]: any }): string {
    let array = typeof objArray != 'object' ? JSON.parse(objArray) : objArray;
    let str = '';
    let row = '';
    for (let index in headerList) {
      row += headerList[index] + ', ';
    }
    row = row.slice(0, -1);
    str += row.substring(0, row.length - 1) + '\r\n';
    // @ts-ignore
    for (let i = 0; i < array.length; i++) {
      let line = "";
      for (let index in headerList) {
        let head = headerList[index];
        line += array[i][head] + ',';
      }
      str += line.substring(0, line.length - 1) + "\r\n";
    }
    return str;
  }

  public csvExporteren(adressen: Adres[], omschrijving: string) {
    let csvData = this.convertToCSV(JSON.stringify(adressen), [
      'naam', 'straat', 'huisnummer', 'toevoeging', 'postcode', 'plaats']);
    console.log(csvData)
    let blob = new Blob(['\ufeff' + csvData], {
      type: 'text/csv;charset=utf-8;'
    });
    let dwldLink = document.createElement('a');
    let url = URL.createObjectURL(blob);
    let isSafariBrowser = navigator.userAgent.indexOf(
        'Safari') != -1 &&
      navigator.userAgent.indexOf('Chrome') == -1;

    // If Safari open in new window to
    // save file with random filenaam.
    if (isSafariBrowser) {
      dwldLink.setAttribute("target", "_blank");
    }
    dwldLink.setAttribute("href", url);
    dwldLink.setAttribute("download", omschrijving + ".csv");
    dwldLink.style.visibility = "hidden";
    document.body.appendChild(dwldLink);
    dwldLink.click();
    document.body.removeChild(dwldLink);
  }
}

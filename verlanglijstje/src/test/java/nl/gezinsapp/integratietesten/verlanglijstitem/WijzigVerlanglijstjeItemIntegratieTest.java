package nl.gezinsapp.integratietesten.verlanglijstitem;

import com.fasterxml.jackson.core.JsonProcessingException;
import io.quarkus.test.junit.QuarkusTest;
import lombok.extern.slf4j.Slf4j;
import nl.gezinsapp.integratietesten.AbstractIntegratieTest;
import nl.gezinsapp.integratietesten.MockServer;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockserver.model.JsonBody;
import org.openapi.quarkus.gezin_openapi_yaml.model.Gezin;
import org.openapi.quarkus.gezin_openapi_yaml.model.Gezinslid;
import org.openapi.quarkus.verlanglijstje_openapi_yaml.model.AlgemeneResponse;

import static nl.gezinsapp.integratietesten.random.RandomVerlanglijst.randomDomainVerlanglijst;
import static nl.gezinsapp.integratietesten.random.RandomVerlanglijstItem.randomDomainVerlanglijstItem;
import static nl.gezinsapp.integratietesten.random.RandomVerlanglijstItem.randomMsgVerlanglijstItem;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockserver.model.HttpRequest.request;
import static org.mockserver.model.HttpResponse.response;
import io.quarkus.test.common.QuarkusTestResource;

@Slf4j
@QuarkusTest
@QuarkusTestResource(MockServer.class)
class WijzigVerlanglijstjeItemIntegratieTest extends AbstractIntegratieTest {
    @Test
    @DisplayName("Sla een gewijzigde Verlanglijst Item op")
    void test() throws JsonProcessingException {
        maakDatabaseLeeg();

        var accesstoken = maakToken();
        var jwtResponse = mockCheckJwt(accesstoken);

        MockServer.getMockServerClient()
                .when(
                        request()
                                .withMethod("GET")
                                .withPath("/api/gezin/" + jwtResponse.getGebruiker().getGezin())
                )
                .respond(
                        response()
                                .withStatusCode(200)
                                .withHeader("Content-Type", "application/json")
                                .withBody(JsonBody.json(gezin(jwtResponse.getGebruiker().getGezin(), jwtResponse.getGebruiker().getId())))
                );

        var verlanglijst = randomDomainVerlanglijst(jwtResponse.getGebruiker().getId());
        opslaan(verlanglijst);

        var domainItem = randomDomainVerlanglijstItem();
        domainItem.setVerlanglijst(verlanglijst);
        opslaan(domainItem);

        var item = randomMsgVerlanglijstItem();
        item.setVerlanglijstje(verlanglijst.getId());
        item.setId(domainItem.getId());

        var response = patchViaApi(AlgemeneResponse.class, "/api/verlanglijstje/verlanglijstjeitem", item, accesstoken);

        var itemGelezen = leesVerlanglijstItem(response.getId());
        assertNotNull(itemGelezen);
        assertEquals(item.getOmschrijving(), itemGelezen.getOmschrijving());
    }    @Test
    @DisplayName("Sla een gewijzigde Verlanglijst Item op, lijst is niet van ingelogde gezin")
    void test2() throws JsonProcessingException {
        maakDatabaseLeeg();

        var accesstoken = maakToken();
        var jwtResponse = mockCheckJwt(accesstoken);

        MockServer.getMockServerClient()
                .when(
                        request()
                                .withMethod("GET")
                                .withPath("/api/gezin/" + jwtResponse.getGebruiker().getGezin())
                )
                .respond(
                        response()
                                .withStatusCode(200)
                                .withHeader("Content-Type", "application/json")
                                .withBody(JsonBody.json(gezin(jwtResponse.getGebruiker().getGezin(), jwtResponse.getGebruiker().getId())))
                );

        var verlanglijst = randomDomainVerlanglijst(99L);
        opslaan(verlanglijst);

        var domainItem = randomDomainVerlanglijstItem();
        domainItem.setVerlanglijst(verlanglijst);
        opslaan(domainItem);

        var item = randomMsgVerlanglijstItem();
        item.setVerlanglijstje(verlanglijst.getId());
        item.setId(domainItem.getId());

        var response = patchViaApi(AlgemeneResponse.class, "/api/verlanglijstje/verlanglijstjeitem", item, accesstoken);
        assertNull(response.getId());
        assertEquals(1,response.getFoutmeldingen().getMeldingen().size());
        assertEquals("Betreffende verlanglijst item is geen onderdeel van het huidige gezin.",response.getFoutmeldingen().getMeldingen().get(0));
    }
    private Gezin gezin(Long id, Long eersteLidId) {
        var gezin = new Gezin();
        gezin.setId(id);

        var gezinslid1 = new Gezinslid();
        gezinslid1.setId(eersteLidId);

        var gezinslid2 = new Gezinslid();
        gezinslid2.setId(3L);

        gezin.getGezinsleden().add(gezinslid1);
        gezin.getGezinsleden().add(gezinslid2);

        return gezin;
    }
}

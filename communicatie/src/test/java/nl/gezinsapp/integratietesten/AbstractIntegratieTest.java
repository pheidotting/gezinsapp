package nl.gezinsapp.integratietesten;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.restassured.RestAssured;
import io.restassured.config.ObjectMapperConfig;
import io.restassured.config.RestAssuredConfig;
import jakarta.transaction.Transactional;
import nl.gezinsapp.domain.Email;
import org.junit.Rule;
import org.junit.jupiter.api.BeforeAll;
import org.mockserver.model.JsonBody;
import org.openapi.quarkus.authenticatie_openapi_yaml.model.CheckJWTRequest;
import org.openapi.quarkus.authenticatie_openapi_yaml.model.Gebruiker;
import org.openapi.quarkus.authenticatie_openapi_yaml.model.JwtResponse;
import org.openapi.quarkus.gezin_openapi_yaml.model.Gezin;
import org.openapi.quarkus.gezin_openapi_yaml.model.Gezinslid;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.utility.DockerImageName;

import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

import static io.restassured.RestAssured.given;
import static jakarta.ws.rs.core.HttpHeaders.AUTHORIZATION;
import static org.awaitility.Awaitility.await;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockserver.model.HttpRequest.request;
import static org.mockserver.model.HttpResponse.response;

public abstract class AbstractIntegratieTest {
    private final String BEARER = "";

    @Rule
    public static GenericContainer mailpitContainer = new GenericContainer(DockerImageName.parse("axllent/mailpit"));

    @BeforeAll
    public static void init() {
        List<String> portBindings = new ArrayList<>();
        portBindings.add("1025:1025");
        mailpitContainer.setPortBindings(portBindings);
        mailpitContainer.addExposedPort(8025);
        mailpitContainer.start();
    }

    protected <T> T leesViaApi(Class<T> clazz, String url, String token) {
        AtomicReference<T> gezinF = new AtomicReference<T>();

        await()
                .atMost(40, TimeUnit.SECONDS)
                .pollInterval(Duration.ofMillis(500))
                .until(() -> {
                    var requestSpecification = given()
                            .when();
                    if (token != null) {
                        requestSpecification.header(AUTHORIZATION, String.format("%s%s", BEARER, token));
                    }
                    var response = requestSpecification.get(url);
                    System.out.println("response.statusCode() " + response.statusCode());
                    System.out.println("response.statusCode() " + response.asPrettyString());
                    if (response.statusCode() == 200) {
                        gezinF.set(response.then().extract().response().as(clazz));
                    }
                    return response.statusCode() == 200;
                });

        return gezinF.get();
    }

    protected <T> T postViaApi(Class<T> clazz, String url, Object payload, String token) {
        RestAssured.config = RestAssuredConfig.config().objectMapperConfig(new ObjectMapperConfig().jackson2ObjectMapperFactory(
                (cls, charset) -> {
                    var df = new SimpleDateFormat("dd-MM-yyyy");
                    var objectMapper = new ObjectMapper().findAndRegisterModules();
                    objectMapper.setDateFormat(df);
                    return objectMapper;
                }
        ));
        var requestSpecification = given()
                .when();
        if (token != null) {
            requestSpecification.header(AUTHORIZATION, String.format("%s%s", BEARER, token));
        }
        requestSpecification.header("Content-Type", "application/json");
        requestSpecification.body(payload);
        var response = requestSpecification.post(url);
        System.out.println("response.statusCode(" + response.statusCode() + ")");

        assertEquals(200, response.statusCode());
        return response.as(clazz);
    }

    protected <T> T putViaApi(Class<T> clazz, String url, Object payload, String token) {
        RestAssured.config = RestAssuredConfig.config().objectMapperConfig(new ObjectMapperConfig().jackson2ObjectMapperFactory(
                (cls, charset) -> {
                    var df = new SimpleDateFormat("dd-MM-yyyy");
                    var objectMapper = new ObjectMapper().findAndRegisterModules();
                    objectMapper.setDateFormat(df);
                    return objectMapper;
                }
        ));
        var requestSpecification = given()
                .when();
        if (token != null) {
            requestSpecification.header(AUTHORIZATION, String.format("%s%s", BEARER, token));
        }
        requestSpecification.header("Content-Type", "application/json");

        requestSpecification.body(payload);
        var response = requestSpecification.put(url);
        System.out.println("response.statusCode(" + response.statusCode() + ")");

        assertEquals(200, response.statusCode());
        return response.as(clazz);
    }

    protected <T> T patchViaApi(Class<T> clazz, String url, Object payload, String token) {
        RestAssured.config = RestAssuredConfig.config().objectMapperConfig(new ObjectMapperConfig().jackson2ObjectMapperFactory(
                (cls, charset) -> {
                    var df = new SimpleDateFormat("dd-MM-yyyy");
                    var objectMapper = new ObjectMapper().findAndRegisterModules();
                    objectMapper.setDateFormat(df);
                    return objectMapper;
                }
        ));
        var requestSpecification = given()
                .when();
        if (token != null) {
            requestSpecification.header(AUTHORIZATION, String.format("%s%s", BEARER, token));
        }
        requestSpecification.header("Content-Type", "application/json");

        requestSpecification.body(payload);
        var response = requestSpecification.patch(url);
        System.out.println("response.statusCode(" + response.statusCode() + ")");

        assertEquals(200, response.statusCode());
        return response.as(clazz);
    }

    protected <T> T deleteViaApi(Class<T> clazz, String url, Object payload, String token) {
        RestAssured.config = RestAssuredConfig.config().objectMapperConfig(new ObjectMapperConfig().jackson2ObjectMapperFactory(
                (cls, charset) -> {
                    var df = new SimpleDateFormat("dd-MM-yyyy");
                    var objectMapper = new ObjectMapper().findAndRegisterModules();
                    objectMapper.setDateFormat(df);
                    return objectMapper;
                }
        ));
        var requestSpecification = given()
                .when();
        if (token != null) {
            requestSpecification.header(AUTHORIZATION, String.format("%s%s", BEARER, token));
        }
        requestSpecification.header("Content-Type", "application/json");

        requestSpecification.body(payload);
        var response = requestSpecification.delete(url);
        System.out.println("response.statusCode(" + response.statusCode() + ")");

        assertEquals(200, response.statusCode());
        return response.as(clazz);
    }

    @Transactional
    public void maakDatabaseLeeg() {
        Email.deleteAll();
    }

    @Transactional
    public void opslaan(Email email) {
        email.persist();
    }

    public String maakToken() {
        return Jwts.builder()
                .setSubject("a@b.c")
                .signWith(SignatureAlgorithm.HS512,
                        UUID.randomUUID().toString().replace("-", "") +
                                UUID.randomUUID().toString().replace("-", "") +
                                UUID.randomUUID().toString().replace("-", "")
                )
                .compact();
    }

    public JwtResponse mockCheckJwt(String token) throws JsonProcessingException {
        return mockCheckJwt(token, false);
    }

    public JwtResponse mockCheckJwt(String token, boolean beheerder) throws JsonProcessingException {
        var jwtResponse = new JwtResponse();
        jwtResponse.setGebruiker(new org.openapi.quarkus.authenticatie_openapi_yaml.model.Gebruiker());
        jwtResponse.getGebruiker().setVoornaam("John");
        jwtResponse.getGebruiker().setAchternaam("Doe");
        jwtResponse.getGebruiker().setId(new Random().nextLong());
        jwtResponse.getGebruiker().setGezin(new Random().nextLong());
        jwtResponse.getGebruiker().setSoort(Gebruiker.SoortEnum.GEZINSLID);
        if (beheerder) {
            jwtResponse.getGebruiker().setSoort(Gebruiker.SoortEnum.BEHEERDER);
        }
        System.out.println(String.format("Gemockte gebruiker: %s : %s %s %s (%s)", jwtResponse.getGebruiker().getId(), jwtResponse.getGebruiker().getVoornaam(), jwtResponse.getGebruiker().getTussenvoegsel(), jwtResponse.getGebruiker().getAchternaam(), jwtResponse.getGebruiker().getGezin()));
        MockServer.getMockServerClient()
                .when(
                        request()
                                .withMethod("POST")
                                .withPath("/api/authorisatie/checkjwt")
                                .withBody(new ObjectMapper().writeValueAsString(new CheckJWTRequest().accessToken(token)))
                )
                .respond(
                        response()
                                .withStatusCode(200)
                                .withHeader("Content-Type", "application/json")
                                .withBody(JsonBody.json(jwtResponse))
                );

        return jwtResponse;
    }

    public void mockGezin(Long gezinsId, String emailadres) {
        var gezin = new Gezin();
        var gezinslid = new Gezinslid();
        gezinslid.setEmailadres(emailadres);
        gezin.getGezinsleden().add(gezinslid);

        mockGezin(gezinsId, gezin);
    }

    public void mockGezin(Long gezinsId, Gezin gezin) {
        MockServer.getMockServerClient()
                .when(
                        request()
                                .withMethod("GET")
                                .withPath("/api/gezin/" + gezinsId)
                )
                .respond(
                        response()
                                .withStatusCode(200)
                                .withHeader("Content-Type", "application/json")
                                .withBody(JsonBody.json(gezin))
                );
    }

    @Transactional
    public Email leesEmail() {
        return Email.findAll().firstResult();
    }
}

describe('Boodschappen Component', () => {
  it('Toon een boodschappenlijstje', () => {
    cy.intercept('GET', '/api/boodschappen/item', {
      statusCode: 200,
      body: {
        "boodschappenitems": [{"gehaald": false, "gezin": 1, "id": 1, "omschrijving": "Broodjes", "winkel": "Jumbo"}, {
          "gehaald": false,
          "gezin": 1,
          "id": 2,
          "omschrijving": "Boter",
          "winkel": "Jumbo"
        }]
      },
    });

    cy.intercept('PUT', '/api/boodschappen/item', {
      statusCode: 200,
      body: {
        "id": 9
      },
    });
    cy.intercept('PATCH', '/api/boodschappen/item', {
      statusCode: 200,
      body: {
        "id": 9
      },
    });

    cy.visit('http://localhost:4200');
    cy.get('#boodschappen_link').click();

    cy.get('#omschrijving-0').should('have.text', 'Broodjes');
    cy.get('#omschrijving-1').should('have.text', 'Boter');
    cy.get('#winkel-0').should('have.text', 'Jumbo');
    cy.get('#winkel-1').should('have.text', 'Jumbo');

    cy.get('#nieuweomschrijving').type('Yoghurt');
    cy.get('#nieuwewinkel').type('Lidl');
    cy.get('#nieuwewinkel').type('{enter}');

    cy.get('#omschrijving-0').should('have.text', 'Broodjes');
    cy.get('#omschrijving-1').should('have.text', 'Boter');
    cy.get('#omschrijving-2').should('have.text', 'Yoghurt');
    cy.get('#winkel-0').should('have.text', 'Jumbo');
    cy.get('#winkel-1').should('have.text', 'Jumbo');
    cy.get('#winkel-2').should('have.text', 'Lidl');

    cy.get('#nieuweomschrijving').type('Koekjes');
    cy.get('#nieuwewinkel').clear();
    cy.get('#nieuwewinkel').type('Plus');
    cy.get('#nieuweomschrijving').type('{enter}');
    cy.get('#nieuwewinkel').should('have.value', 'Plus');

    cy.get('#omschrijving-0').should('have.text', 'Broodjes');
    cy.get('#omschrijving-1').should('have.text', 'Boter');
    cy.get('#omschrijving-2').should('have.text', 'Yoghurt');
    cy.get('#omschrijving-3').should('have.text', 'Koekjes');
    cy.get('#winkel-0').should('have.text', 'Jumbo');
    cy.get('#winkel-1').should('have.text', 'Jumbo');
    cy.get('#winkel-2').should('have.text', 'Lidl');
    cy.get('#winkel-3').should('have.text', 'Plus');

    cy.intercept('GET', '/api/boodschappen/item', {
      statusCode: 200,
      body: {
        "boodschappenitems": [{"gehaald": false, "gezin": 1, "id": 1, "omschrijving": "Broodjes", "winkel": "Jumbo"}, {
          "gehaald": false,
          "gezin": 1,
          "id": 2,
          "omschrijving": "Boter",
          "winkel": "Jumbo"
        }, {
          "gehaald": false,
          "gezin": 1,
          "id": 3,
          "omschrijving": "Koekjes",
          "winkel": "Plus"
        }]
      },
    });

    cy.get('#afvinken-2').check();

    cy.get('#omschrijving-0').should('have.text', 'Broodjes');
    cy.get('#omschrijving-1').should('have.text', 'Boter');
    cy.get('#omschrijving-2').should('have.text', 'Koekjes');
    cy.get('#winkel-0').should('have.text', 'Jumbo');
    cy.get('#winkel-1').should('have.text', 'Jumbo');
    cy.get('#winkel-2').should('have.text', 'Plus');

    cy.get('#wijzig-1').click();

    cy.intercept('GET', '/api/boodschappen/item', {
      statusCode: 200,
      body: {
        "boodschappenitems": [{"gehaald": false, "gezin": 1, "id": 1, "omschrijving": "Broodjes", "winkel": "Jumbo"}, {
          "gehaald": false,
          "gezin": 1,
          "id": 2,
          "omschrijving": "Bier",
          "winkel": "Sligro"
        }, {
          "gehaald": false,
          "gezin": 1,
          "id": 3,
          "omschrijving": "Koekjes",
          "winkel": "Plus"
        }]
      },
    });

    cy.get('#wijzigomschrijving-1').type('Bier');
    cy.get('#wijzigwinkel-1').type('Aldi');
    cy.get('#wijzig-1').click();

    cy.get('#omschrijving-0').should('have.text', 'Broodjes');
    cy.get('#omschrijving-1').should('have.text', 'Bier');
    cy.get('#omschrijving-2').should('have.text', 'Koekjes');
    cy.get('#winkel-0').should('have.text', 'Jumbo');
    cy.get('#winkel-1').should('have.text', 'Sligro');
    cy.get('#winkel-2').should('have.text', 'Plus');

    cy.screenshot();
  })
})

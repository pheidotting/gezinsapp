import {fakeAsync, TestBed, tick} from "@angular/core/testing";
import {ActivatedRoute, provideRouter, RouterModule} from "@angular/router";
import {provideHttpClient, withInterceptorsFromDi} from "@angular/common/http";
import {provideHttpClientTesting} from "@angular/common/http/testing";
import {of} from "rxjs";
import {VerlanglijstComponent} from "./verlanglijst.component";

describe('VerlanglijstComponent', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [VerlanglijstComponent,
        RouterModule.forRoot([])],
      providers: [provideRouter([]), provideHttpClient(withInterceptorsFromDi()), provideHttpClientTesting()]
    });
  });
  it('should create', fakeAsync(() => {
    let fixture = TestBed.createComponent(VerlanglijstComponent);

    let app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));

  it('should load data on startup', fakeAsync(() => {
    let fixture = TestBed.createComponent(VerlanglijstComponent);

    let activatedRoute: ActivatedRoute = fixture.debugElement.injector.get(ActivatedRoute);

    activatedRoute.data = of({
      gezin: {
        naam: 'Heidotting',
        gezinsleden: [{
          id: 2,
          voornaam: 'v',
          tussenvoegsel: 't',
          achternaam: 'a'
        }]
      }, verlanglijstje: {
        verlanglijstjes: [
          {
            id: 3,
            omschrijving: 'abc',
            verlanglijstItems: [
              {
                omschrijving: 'def'
              }
            ]
          }
        ]
      }
    });

    fixture.detectChanges();
    tick();

    let app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
    expect(app.gezin.naam).toBe('Heidotting');
    expect(app.verlanglijstjes.length).toBe(1);
    expect(app.verlanglijstjes[0].omschrijving).toBe('abc');
    expect(app.verlanglijstjes[0].verlanglijstItems.length).toBe(1);
    expect(app.verlanglijstjes[0].verlanglijstItems[0].omschrijving).toBe('def');
  }));
});

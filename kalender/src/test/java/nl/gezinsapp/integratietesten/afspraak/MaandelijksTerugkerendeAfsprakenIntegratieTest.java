package nl.gezinsapp.integratietesten.afspraak;

import com.fasterxml.jackson.core.JsonProcessingException;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;
import lombok.extern.slf4j.Slf4j;
import nl.gezinsapp.integratietesten.AbstractIntegratieTest;
import nl.gezinsapp.integratietesten.MockServer;
import nl.gezinsapp.model.Afspraak;
import nl.gezinsapp.model.enums.Terugkeer;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockserver.model.JsonBody;
import org.openapi.quarkus.authenticatie_openapi_yaml.model.JwtResponse;
import org.openapi.quarkus.gezin_openapi_yaml.model.Gezin;
import org.openapi.quarkus.kalender_openapi_yaml.model.LijstAfspraken;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockserver.model.HttpRequest.request;
import static org.mockserver.model.HttpResponse.response;

@Slf4j
@QuarkusTest
@QuarkusTestResource(MockServer.class)
class MaandelijksTerugkerendeAfsprakenIntegratieTest extends AbstractIntegratieTest {
    @Test
    @DisplayName("Haal alle afspraken op")
    void test() throws JsonProcessingException {
        maakDatabaseLeeg();

        var accesstoken = maakToken();
        var jwtResponse = mockCheckJwt(accesstoken);

        var afspraak1 = new Afspraak(null, "afsp1", true, null, null, LocalDate.of(2024, 1, 5), LocalDate.of(2024, 1, 5), null, Terugkeer.MAANDELIJKS, jwtResponse.getGebruiker().getGezin(), null, null);

        opslaan(afspraak1);

        var gezin = new Gezin();

        MockServer.getMockServerClient()
                .when(
                        request()
                                .withMethod("GET")
                                .withPath("/api/gezin/" + jwtResponse.getGebruiker().getGezin())
                )
                .respond(
                        response()
                                .withStatusCode(200)
                                .withHeader("Content-Type", "application/json")
                                .withBody(JsonBody.json(gezin))
                );

        testAfspraak1(afspraak1, jwtResponse, accesstoken);
    }

    private void testAfspraak1(Afspraak afspraak, JwtResponse jwtResponse, String accesstoken) {
        var response = leesViaApi(LijstAfspraken.class, "/api/kalender/afspraak/2024-02-05/2024-04-05", accesstoken);
        assertEquals(3, response.getAfspraken().size());
        assertEquals(afspraak.getOmschrijving(), response.getAfspraken().get(0).getOmschrijving());
        assertEquals(LocalDate.of(2024, 2, 5), response.getAfspraken().get(0).getVanDatum());
        assertEquals(LocalDate.of(2024, 2, 5), response.getAfspraken().get(0).getTotDatum());
        assertEquals(jwtResponse.getGebruiker().getGezin(), response.getAfspraken().get(0).getGezin());
        assertEquals(afspraak.getOmschrijving(), response.getAfspraken().get(1).getOmschrijving());
        assertEquals(LocalDate.of(2024, 3, 5), response.getAfspraken().get(1).getVanDatum());
        assertEquals(LocalDate.of(2024, 3, 5), response.getAfspraken().get(1).getTotDatum());
        assertEquals(jwtResponse.getGebruiker().getGezin(), response.getAfspraken().get(1).getGezin());
        assertEquals(afspraak.getOmschrijving(), response.getAfspraken().get(2).getOmschrijving());
        assertEquals(LocalDate.of(2024, 4, 5), response.getAfspraken().get(2).getVanDatum());
        assertEquals(LocalDate.of(2024, 4, 5), response.getAfspraken().get(2).getTotDatum());
        assertEquals(jwtResponse.getGebruiker().getGezin(), response.getAfspraken().get(2).getGezin());

    }
}

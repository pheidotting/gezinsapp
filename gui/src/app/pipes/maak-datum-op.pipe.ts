import {Pipe, PipeTransform} from '@angular/core';
import moment from "moment/moment";

@Pipe({
  name: 'maakDatumOp',
  standalone: true
})
export class MaakDatumOpPipe implements PipeTransform {

  transform(datum: string | undefined): string {
    if (datum == undefined) {
      return '';
    }
    return moment(datum).format('DD-MM-YYYY');
  }
}

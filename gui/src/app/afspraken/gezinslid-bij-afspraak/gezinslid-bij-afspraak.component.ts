import {Component, Input} from '@angular/core';
import {FormsModule} from "@angular/forms";
import {AvatarComponent} from "../../common/avatar/avatar.component";

@Component({
  selector: 'app-gezinslid-bij-afspraak',
  imports: [
    FormsModule,
    AvatarComponent
  ],
  templateUrl: './gezinslid-bij-afspraak.component.html'
})
export class GezinslidBijAfspraakComponent {
  @Input() gezinsleden: [{ id?: number, naam?: string, checked: boolean }] = [{checked: false}];

}

import {MaakGeslachtEnumOpPipe} from './maak-geslacht-enum-op.pipe';
import {GezinslidGeslachtEnum} from "../../api/gezin";

describe('MaakGeslachtEnumOpPipe', () => {
  it('create an instance', () => {
    const pipe = new MaakGeslachtEnumOpPipe();
    expect(pipe).toBeTruthy();

    expect(pipe.transform(GezinslidGeslachtEnum.Man)).toBe('Man');
    expect(pipe.transform(GezinslidGeslachtEnum.Vrouw)).toBe('Vrouw');
    expect(pipe.transform(GezinslidGeslachtEnum.Onbelangrijk)).toBe('Onbelangrijk');
  });
});

import {MaakDatumOpPipe} from './maak-datum-op.pipe';

describe('MaakDatumOpPipe', () => {
  it('check opmaak', () => {
    const pipe = new MaakDatumOpPipe();

    let datum: string | undefined = '2024-10-08T14:31';

    expect(pipe).toBeTruthy();
    expect(pipe.transform(datum)).toEqual('08-10-2024');
  });
  it('check opmaak null datum', () => {
    const pipe = new MaakDatumOpPipe();

    let datum: string | undefined = undefined;

    expect(pipe).toBeTruthy();
    expect(pipe.transform(datum)).toEqual('');
  });
});

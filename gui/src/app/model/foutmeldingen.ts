export enum Foutmeldingen {
  GEZINSNAAM_MAG_NIET_LEEG_ZIJN = 'Gezinsnaam mag niet leeg zijn',
  ER_MOETEN_WEL_GEZINSLEDEN_AANWEZIG_ZIJN = 'Er moeten wel gezinsleden aanwezig zijn',
  VOORNAAM_MAG_NIET_LEEG_ZIJN = 'Voornaam mag niet leeg zijn',
  ACHTERNAAM_MAG_NIET_LEEG_ZIJN = 'Achternaam mag niet leeg zijn',
  OMSCHRIJVING_MOET_GEVULD_ZIJN = 'Omschrijving moet gevuld zijn',
  BEGINDATUM_MOET_VOOR_EINDDATUM_LIGGEN = 'Begindatum moet voor einddatum liggen',
  BEGINTIJD_MOET_VOOR_EINDTIJD_LIGGEN = 'Begintijd moet voor eindtijd liggen',
  DATUM_MAG_NIET_IN_HET_VERLEDEN_LIGGEN = 'Datum mag niet in het verleden liggen',
  GESLACHT_MAG_NIET_LEEG_ZIJN = 'Geslacht mag niet leeg zijn',
  SOORT_MAG_NIET_LEEG_ZIJN = 'Soort mag niet leeg zijn',
  WACHTWOORDEN_KOMEN_NIET_OVEREEN = 'De ingevulde wachtwoorden komen niet overeen',
  VUL_BEIDE_VELDEN_IN = 'Vul beide velden in',
  VUL_JE_EMAILADRES_IN_WAARMEE_JE_INLOGT = 'Vul je e-mailadres in waarmee je inlogt'
}

package nl.gezinsapp.integratietesten;

import com.fasterxml.jackson.core.JsonProcessingException;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;
import jakarta.transaction.Transactional;
import lombok.extern.slf4j.Slf4j;
import nl.gezinsapp.model.Geslacht;
import nl.gezinsapp.model.Gezin;
import nl.gezinsapp.model.Gezinslid;
import nl.gezinsapp.model.SoortGezinslid;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.openapi.quarkus.gezin_openapi_yaml.model.AlgemeneResponse;
import org.openapi.quarkus.gezin_openapi_yaml.model.VerwijderGezinslid;

import java.time.Duration;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import static org.awaitility.Awaitility.await;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@Slf4j
@QuarkusTest
@QuarkusTestResource(MockServer.class)
class VerwijderGezinslidIntegratieTest extends AbstractIntegratieTest {
    @Test
    @DisplayName("Test het verwijderen van een Gezinslid")
    void testVerwijder() throws JsonProcessingException {
        maakDatabaseLeeg();

        var gezin = new Gezin();
        gezin.setNaam(UUID.randomUUID().toString());

        opslaanGezin(gezin);

        var gezinslid = new Gezinslid();
        gezinslid.setVoornaam(UUID.randomUUID().toString());
        gezinslid.setAchternaam(UUID.randomUUID().toString());
        gezinslid.setGeslacht(Geslacht.MAN);
        gezinslid.setSoortGezinslid(SoortGezinslid.OUDER);
        gezinslid.setGezin(gezin);

        opslaanGezinslid(gezinslid);

        assertEquals(1, getAantalGezinnen());
        assertEquals(1, getAantalGezinsleden());

        assertNotNull(gezin.getId());
        assertNotNull(gezinslid.getId());

        var accesstoken = maakToken();
        var trackAndTraceId = UUID.randomUUID().toString();
        var jwtResponse = mockCheckJwt(accesstoken,trackAndTraceId);

        postViaApi(AlgemeneResponse.class, "/api/gezin/gezinslid/verwijder", new VerwijderGezinslid().id(gezinslid.getId()), accesstoken,trackAndTraceId);

        assertEquals(1, getAantalGezinnen());
        await()
                .atMost(40, TimeUnit.SECONDS)
                .pollInterval(Duration.ofMillis(500))
                .until(() -> getAantalGezinsleden() == 0);
    }

    @Transactional
    public int getAantalGezinnen() {
        return gezinRepository.findAll().list().size();
    }

    @Transactional
    public int getAantalGezinsleden() {
        return gezinslidRepository.findAll().list().size();
    }
}

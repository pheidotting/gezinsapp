import {Injectable} from '@angular/core';
import {Afspraak, AfspraakSoortAfspraakEnum} from '../../api/kalender/model/afspraak';
import {DatumMetGebeurtenissen} from "../common/model/datum-met-gebeurtenissen.model";
import {Gezinslid} from '../../api/gezin/model/gezinslid';
import {SoortDienst} from '../../api/kalender';
import {SoortDatumEnum} from "../common/model/gebeurtenis.model";
import {TijdMetGebeurtenissen} from "../common/model/tijd-met-gebeurtenissen.model";

@Injectable({
  providedIn: 'root'
})
export class AfspraakNaarDatumMetGebeurtenisService {
  transform(afspraak: Afspraak, bestaandeDatumsMetGebeurtenissen: DatumMetGebeurtenissen[], gezinsleden: Gezinslid[], soortenDiensten: SoortDienst[]): DatumMetGebeurtenissen {
    let datumMetGebeurtenissen: DatumMetGebeurtenissen = this.getDatumMetGebeurtenissen(afspraak, bestaandeDatumsMetGebeurtenissen);

    let tijdMetGebeurtenissen: TijdMetGebeurtenissen = {
      gebeurtenissen: [{}]
    };
    if (afspraak.soortDienst == null) {
      tijdMetGebeurtenissen.gebeurtenissen[0].omschrijving = afspraak.omschrijving;
    } else {
      tijdMetGebeurtenissen.gebeurtenissen[0].omschrijving = soortenDiensten.find((soortDienst: SoortDienst) => {
        return soortDienst.id == afspraak.soortDienst;
      })!.omschrijving;
    }
    if (afspraak.soortAfspraak == AfspraakSoortAfspraakEnum.Huwelijk) {
      tijdMetGebeurtenissen.gebeurtenissen[0].soortDatum = SoortDatumEnum.Huwelijk;
    } else if (afspraak.soortAfspraak == AfspraakSoortAfspraakEnum.Verjaardag) {
      tijdMetGebeurtenissen.gebeurtenissen[0].soortDatum = SoortDatumEnum.Verjaardag;
    } else {
      tijdMetGebeurtenissen.gebeurtenissen[0].soortDatum = SoortDatumEnum.Overig;
    }
    if (afspraak.vanTijd) {
      tijdMetGebeurtenissen.van = afspraak.vanTijd.substring(11, 16);
    }
    if (afspraak.totTijd) {
      tijdMetGebeurtenissen.tot = afspraak.totTijd.substring(11, 16);
    }
    if (gezinsleden != null && afspraak.gezinsleden != null) {
      tijdMetGebeurtenissen.gebeurtenissen[0].gezinsleden = this.getGezinsleden(afspraak, gezinsleden);

      if (gezinsleden.length == 1) {
        tijdMetGebeurtenissen.gebeurtenissen[0].gezinsleden.push(gezinsleden[0]);
      }
    } else {
      tijdMetGebeurtenissen.gebeurtenissen[0].gezinsleden = [];
    }
    datumMetGebeurtenissen.tijdMetGebeurtenissen.push(tijdMetGebeurtenissen);

    return datumMetGebeurtenissen;
  }


  getDatumMetGebeurtenissen(afspraak: Afspraak, bestaandeDatumsMetGebeurtenissen: DatumMetGebeurtenissen[]): DatumMetGebeurtenissen {
    let datumMetGebeurtenissen: DatumMetGebeurtenissen = bestaandeDatumsMetGebeurtenissen.filter((gb: DatumMetGebeurtenissen) => {
      if (afspraak.vanTijd == undefined) {
        return gb.datum == afspraak.vanDatum;
      } else {
        return gb.datum == afspraak.vanTijd.substring(0, 10);
      }
    })[0];
    if (datumMetGebeurtenissen == null) {
      let datum: string | undefined = afspraak.vanDatum ?? afspraak.vanTijd;
      if (datum != undefined && datum.length > 10) {
        datum = datum.substring(0, 10);
      }
      datumMetGebeurtenissen = {
        datum: datum,
        tijdMetGebeurtenissen: []
      };
    }
    return datumMetGebeurtenissen;
  }

  getGezinsleden(afspraak: Afspraak, gezinsleden: Gezinslid[]): Gezinslid[] {
    return gezinsleden.filter((gezinslid: Gezinslid) => {
      let index = afspraak.gezinsleden!.findIndex(a => {
        return a == gezinslid.id;
      });

      return index > -1;
    });
  }
}

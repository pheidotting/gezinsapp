package nl.gezinsapp.integratietesten.common.random;

import nl.gezinsapp.model.Gezinslid;
import uk.co.jemos.podam.api.DefaultClassInfoStrategy;
import uk.co.jemos.podam.api.PodamFactory;
import uk.co.jemos.podam.api.PodamFactoryImpl;

public class RandomGezinslid {
    private static PodamFactory factory;

    private static PodamFactory getFactory() {
        if (factory == null) {
            factory = new PodamFactoryImpl();
            DefaultClassInfoStrategy classInfoStrategy = DefaultClassInfoStrategy.getInstance();
            classInfoStrategy.addExcludedField(Gezinslid.class, "id");
            classInfoStrategy.addExcludedField(Gezinslid.class, "gezin");
            classInfoStrategy.addExcludedField(org.openapi.quarkus.gezin_openapi_yaml.model.Gezinslid.class, "id");
            classInfoStrategy.addExcludedField(org.openapi.quarkus.gezin_openapi_yaml.model.Gezinslid.class, "gezin");
            factory.setClassStrategy(classInfoStrategy);
        }
        return factory;
    }

    public static Gezinslid randomDomainGezinslid() {
        return getFactory().manufacturePojo(Gezinslid.class);
    }

    public static org.openapi.quarkus.gezin_openapi_yaml.model.Gezinslid randomMsgGezinslid() {
        return getFactory().manufacturePojo(org.openapi.quarkus.gezin_openapi_yaml.model.Gezinslid.class);
    }
}

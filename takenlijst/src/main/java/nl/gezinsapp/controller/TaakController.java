package nl.gezinsapp.controller;

import jakarta.transaction.Transactional;
import jakarta.ws.rs.Path;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.gezinsapp.TaakMapper;
import nl.gezinsapp.aspect.ThreadVariabelen;
import nl.gezinsapp.commons.aspect.AlsGezinslid;
import nl.gezinsapp.commons.aspect.MethodeLogging;
import nl.gezinsapp.commons.aspect.TrackAndTraceId;
import nl.gezinsapp.service.TaakService;
import org.openapi.quarkus.takenlijst_openapi_yaml.api.TakenlijstApi;
import org.openapi.quarkus.takenlijst_openapi_yaml.model.AlgemeneResponse;
import org.openapi.quarkus.takenlijst_openapi_yaml.model.LijstTaken;
import org.openapi.quarkus.takenlijst_openapi_yaml.model.Taak;

@Slf4j
@RequiredArgsConstructor
@Path("/api/takenlijst")
public class TaakController implements TakenlijstApi {
    private final TaakMapper taakMapper;
    private final TaakService taakService;

    @Override
    @Transactional
    @TrackAndTraceId
    @AlsGezinslid
    @MethodeLogging
    public LijstTaken alleTaken() {
        var gebruiker = ThreadVariabelen.get();

        return new LijstTaken().taken(
                taakMapper.map(
                        taakService.alleOpenTaken(
                                        gebruiker.getGezin()
                                )
                                .toList()
                )
        );
    }

    @Override
    @Transactional
    @TrackAndTraceId
    @AlsGezinslid
    @MethodeLogging
    public AlgemeneResponse nieuweTaak(Taak taakIn) {
        return new AlgemeneResponse().id(
                taakService.opslaan(
                        taakMapper.map(taakIn),
                        ThreadVariabelen.get()
                )
        );
    }

    @Override
    @Transactional
    @TrackAndTraceId
    @AlsGezinslid
    @MethodeLogging
    public AlgemeneResponse wijzigTaak(Taak taakIn) {
        var taak = taakMapper.update(
                taakIn,
                nl.gezinsapp.domain.Taak.findById(taakIn.getId())
        );

        taakService.opslaan(taak, ThreadVariabelen.get());

        return new AlgemeneResponse().id(
                taakService.opslaan(
                        taak,
                        ThreadVariabelen.get()
                )
        );
    }

    @Override
    @Transactional
    @TrackAndTraceId
    @AlsGezinslid
    @MethodeLogging
    public AlgemeneResponse verwijderTaak(Long id) {
        taakService.verwijder(id, ThreadVariabelen.get().getGezin());

        return new AlgemeneResponse();
    }
}

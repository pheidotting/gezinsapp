package nl.gezinsapp.messaging.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.UUID;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class GezinslidVerwijderd extends AbstractMessagingObject {
    private Long id;

    @Builder
    public GezinslidVerwijderd(String emailadres, Long gezinsId, Long gezinslidId, UUID trackAndTraceId, Long id) {
        super(emailadres, gezinsId, gezinslidId, trackAndTraceId);
        this.id = id;
    }
}

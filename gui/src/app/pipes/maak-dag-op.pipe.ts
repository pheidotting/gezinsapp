import {Pipe, PipeTransform} from '@angular/core';
import moment from "moment/moment";
import 'moment/locale/nl'

@Pipe({
  name: 'maakDagOp',
  standalone: true
})
export class MaakDagOpPipe implements PipeTransform {

  transform(datum: string): string {
    moment.locale('nl');
    return moment(datum).format('dddd');
  }

}

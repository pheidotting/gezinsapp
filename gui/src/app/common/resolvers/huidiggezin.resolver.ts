import {ActivatedRouteSnapshot, ResolveFn, RouterStateSnapshot} from '@angular/router';
import {inject} from '@angular/core';
import {GezinService} from '../../../api/gezin/api/gezin.service';
import {LeesGezin} from '../../../api/gezin/model/leesGezin';

export const huidigGezinResolver: ResolveFn<LeesGezin> = (route: ActivatedRouteSnapshot, state: RouterStateSnapshot) => {
  const gezinService = inject(GezinService);

  return gezinService.leesHuidigGezin();
}

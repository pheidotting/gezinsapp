import {Gezinslid} from './../../../api/gezin/model/gezinslid';

export interface Tijdstip {
  tijdstip: moment.Moment;
  gezinsleden?: Gezinslid[];
  omschrijving?: string;
}

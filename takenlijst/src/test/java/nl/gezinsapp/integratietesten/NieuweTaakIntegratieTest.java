package nl.gezinsapp.integratietesten;

import com.fasterxml.jackson.core.JsonProcessingException;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.openapi.quarkus.takenlijst_openapi_yaml.model.AlgemeneResponse;

import java.util.List;

import static nl.gezinsapp.integratietesten.random.RandomTaak.randomTaak;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@Slf4j
@QuarkusTest
@QuarkusTestResource(MockServer.class)
class NieuweTaakIntegratieTest extends AbstractIntegratieTest {
    @Test
    @DisplayName("Sla een nieuwe Taak op zonder dat deze gekoppeld is aan een gezinslid")
    void test() throws JsonProcessingException {
        maakDatabaseLeeg();

        var accesstoken = maakToken();
        var jwtResponse = mockCheckJwt(accesstoken);

        var item = randomTaak();
        System.out.println(item);
        System.out.println("item");

        var response = putViaApi(AlgemeneResponse.class, "/api/takenlijst/taak", item, accesstoken);

        var itemGelezen = leesTaak(response.getId());
        System.out.println(itemGelezen);

        assertNotNull(itemGelezen);
        assertEquals(item.getOmschrijving(), itemGelezen.getOmschrijving());
        assertEquals(0, itemGelezen.getGezinsleden().size());
        assertEquals(jwtResponse.getGebruiker().getGezin(), itemGelezen.getGezin());
    }

    @Test
    @DisplayName("Sla een nieuwe Taak op en koppel deze aan aan een gezinslid")
    void testMetGezinslid() throws JsonProcessingException {
        maakDatabaseLeeg();

        var accesstoken = maakToken();
        var jwtResponse = mockCheckJwt(accesstoken);

        var item = randomTaak();
        item.setGezinsleden(List.of(jwtResponse.getGebruiker().getId()));

        var response = putViaApi(AlgemeneResponse.class, "/api/takenlijst/taak", item, accesstoken);

        var itemGelezen = leesTaak(response.getId());
        assertNotNull(itemGelezen);
        assertEquals(item.getOmschrijving(), itemGelezen.getOmschrijving());
        assertEquals(jwtResponse.getGebruiker().getId(), itemGelezen.getGezinsleden().get(0));
        assertEquals(jwtResponse.getGebruiker().getGezin(), itemGelezen.getGezin());
    }
}

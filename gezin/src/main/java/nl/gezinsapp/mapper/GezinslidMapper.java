package nl.gezinsapp.mapper;

import nl.gezinsapp.model.Gezin;
import nl.gezinsapp.model.Gezinslid;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.openapi.quarkus.gezin_openapi_yaml.model.GezinslidMetId;
import org.openapi.quarkus.gezin_openapi_yaml.model.GezinslidZonderId;

@Mapper(componentModel = "CDI")
public abstract class GezinslidMapper {
    public abstract Gezinslid update(@MappingTarget Gezinslid gezinslidMetId, GezinslidMetId updateGezinslidMetId);

    public abstract Gezinslid map(GezinslidZonderId gezinslidZonderId);


    public abstract org.openapi.quarkus.gezin_openapi_yaml.model.Gezinslid map(Gezinslid gezinslid);

    Gezin map(Long value) {
        return null;
    }

    Long map(Gezin value) {
        return value.getId();
    }
}

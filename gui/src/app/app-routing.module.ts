import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AanmeldenComponent} from "./aanmelden/aanmelden.component";
import {DashboardComponent} from "./dashboard/dashboard.component";
import {BewerkGezinsledenComponent} from "./gezin/bewerk-gezinsleden/bewerk-gezinsleden.component";
import {LijstGezinsledenComponent} from "./gezin/lijst-gezinsleden/lijst-gezinsleden.component";
import {SoortenDienstenComponent} from "./werkrooster/soorten-diensten/soorten-diensten.component";
import {AfsprakenComponent} from "./afspraken/afspraken.component";
import {InloggenComponent} from "./inloggen/inloggen.component";
import {WijzigWachtwoordComponent} from "./wijzig-wachtwoord/wijzig-wachtwoord.component";
import {lijstGezinsledenResolver} from "./gezin/lijst-gezinsleden.resolver";
import {boodschappenWidgetResolver} from "./dashboard/boodschappen-widget/boodschappen-widget.resolver";
import {huidigGezinResolver} from "./common/resolvers/huidiggezin.resolver";
import {afspraakServiceAlleAfsprakenResolver} from "./common/resolvers/afspraak-ervice-alle-afspraken.resolver";
import {roosterServiceSoortenDienstenBijGezin} from "./common/resolvers/rooster-service-soorten-diensten-bij-gezin.resolver";
import {LijstTakenResolver} from "./takenlijst/takenlijst.resolver";

export const routes: Routes = [
  {path: 'aanmelden', component: AanmeldenComponent},
  {
    path: 'dashboard',
    component: DashboardComponent,
    resolve: {
      takenlijst: LijstTakenResolver,
      boodschappen: boodschappenWidgetResolver,
      gezin: lijstGezinsledenResolver
    }
  },
  {
    path: 'beheer',
    loadChildren: () =>
      import('./beheer/beheer.routes')
        .then(m => m.BEHEER_ROUTES)
  },
  {
    path: 'gezinsleden',
    component: LijstGezinsledenComponent,
    resolve: {gezin: lijstGezinsledenResolver}
  },
  {
    path: 'soorten-diensten',
    component: SoortenDienstenComponent,
    resolve: {
      gezin: lijstGezinsledenResolver,
      soortenDiensten: roosterServiceSoortenDienstenBijGezin
    }
  },
  {
    path: 'nieuw-gezinslid',
    component: BewerkGezinsledenComponent,
    resolve: {gezin: lijstGezinsledenResolver}
  },
  {
    path: 'bewerk-gezinslid/:id',
    component: BewerkGezinsledenComponent,
    resolve: {gezin: lijstGezinsledenResolver}
  },
  {
    path: 'afspraken',
    component: AfsprakenComponent,
    resolve: {
      gezin: lijstGezinsledenResolver
    }
  },
  {
    path: 'adresboek',
    loadChildren: () =>
      import('./adresboek/adresboek.routes')
        .then(m => m.ADRESBOEK_ROUTES)
  },
  {
    path: 'boodschappen',
    loadChildren: () =>
      import('./boodschappen/boodschappen.routes')
        .then(m => m.BOODSCHAPPEN_ROUTES)
  },
  {path: 'inloggen', component: InloggenComponent},
  {
    path: 'taken',
    loadChildren: () =>
      import('./takenlijst/takenlijst.routes')
        .then(m => m.TAKENLIJST_ROUTES)
  },
  {
    path: 'verlanglijstje',
    loadChildren: () =>
      import('./verlanglijstjes/verlanglijst.routes')
        .then(m => m.VERLANGLIJST_ROUTES)
  },
  {path: 'wijzigwachtwoord', component: WijzigWachtwoordComponent},
  {
    path: '**',
    component: DashboardComponent,
    resolve: {
      boodschappen: boodschappenWidgetResolver,
      gezin: huidigGezinResolver,
      alleAfspraken: afspraakServiceAlleAfsprakenResolver,
      soortenDiensten: roosterServiceSoortenDienstenBijGezin
    }
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)//,
    // RouterModule.forChild([{path: 'adresboek', component: LijstAdresBoekenComponent}]),
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {
}

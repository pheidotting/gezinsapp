import {ComponentFixture, TestBed} from '@angular/core/testing';

import {GezinslidBijAfspraakComponent} from './gezinslid-bij-afspraak.component';
import {By} from "@angular/platform-browser";
import {provideRouter, RouterModule} from "@angular/router";
import {provideHttpClient, withInterceptorsFromDi} from "@angular/common/http";
import {provideHttpClientTesting} from "@angular/common/http/testing";

describe('GezinslidBijAfspraakComponent', () => {
  let component: GezinslidBijAfspraakComponent;
  let fixture: ComponentFixture<GezinslidBijAfspraakComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [GezinslidBijAfspraakComponent,
        RouterModule.forRoot([])],
      providers: [provideRouter([]), provideHttpClient(withInterceptorsFromDi()), provideHttpClientTesting()]
    }).compileComponents();

    fixture = TestBed.createComponent(GezinslidBijAfspraakComponent);
    component = fixture.componentInstance;
  });

  it('niet tonen, want gezinsleden is leeg', () => {
    fixture.detectChanges();
    expect(component).toBeTruthy();

    let gezinsledenDiv = fixture.debugElement.query(By.css('#gezinsleden'));

    expect(gezinsledenDiv).toBeFalsy();
  });

  it('tonen, want gezinsleden is niet leeg', () => {
    component.gezinsleden.slice(0, 0);
    component.gezinsleden.push({checked: false});

    fixture.detectChanges();
    expect(component).toBeTruthy();

    let gezinsledenDiv = fixture.debugElement.query(By.css('#gezinsleden'));
    let gezinslidChecked = fixture.debugElement.queryAll(By.css('input[name="gezinslid-checked"]'));
    let gezinslidNaam = fixture.debugElement.queryAll(By.css('span[name="gezinslid-naam"]'));

    expect(gezinsledenDiv).toBeTruthy();
    expect(gezinslidChecked.length).toBe(2);
    expect(gezinslidNaam.length).toBe(2);
  });
});

package nl.gezinsapp.model;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.apache.commons.lang3.builder.ToStringExclude;

import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "ADRESBOEK")
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class AdresBoek extends PanacheEntityBase {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;
    @Column(name = "GEZINSLID")
    private Long gezinslid;
    @Column(name = "GEZIN")
    private Long gezin;
    @Column(name = "OMSCHRIJVING")
    private String omschrijving;
    @OneToMany(mappedBy = "adresBoek", orphanRemoval = true)
    @ToStringExclude
    private Set<Adres> adressen= new HashSet();
}

import {MaakTijdOpPipe} from './maak-tijd-op.pipe';

describe('MaakMomentTijdOpPipe', () => {
  it('create an instance', () => {
    const pipe = new MaakTijdOpPipe();

    let datum: string = '2024-10-08T14:31';

    expect(pipe).toBeTruthy();
    expect(pipe.transform(datum)).toEqual('14:31');
  });
});

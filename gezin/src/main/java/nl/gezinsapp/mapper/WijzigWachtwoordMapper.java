package nl.gezinsapp.mapper;

import nl.gezinsapp.model.WijzigWachtwoord;
import org.mapstruct.Mapper;

@Mapper(componentModel = "CDI")
public interface WijzigWachtwoordMapper {
    WijzigWachtwoord map(org.openapi.quarkus.gezin_openapi_yaml.model.WijzigWachtwoord wijzigWachtwoord);
}

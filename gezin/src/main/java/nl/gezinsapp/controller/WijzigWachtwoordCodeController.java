package nl.gezinsapp.controller;

import jakarta.ws.rs.Path;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.gezinsapp.commons.aspect.MethodeLogging;
import nl.gezinsapp.commons.aspect.TrackAndTraceId;
import nl.gezinsapp.mapper.WijzigWachtwoordCodeMapper;
import nl.gezinsapp.service.WijzigWachtwoordService;
import org.openapi.quarkus.gezin_openapi_yaml.api.WijzigWachtwoordCodeApi;
import org.openapi.quarkus.gezin_openapi_yaml.model.ControleerWijzigWachtwoordCode;
import org.openapi.quarkus.gezin_openapi_yaml.model.ControleerWijzigWachtwoordCodeResultaat;
import org.openapi.quarkus.gezin_openapi_yaml.model.VraagWijzigWachtwoordCodeOp;
import org.openapi.quarkus.gezin_openapi_yaml.model.WijzigWachtwoordCode;

@Slf4j
@Path("/api/gezin/wijzigwachtwoordcode")
@RequiredArgsConstructor
public class WijzigWachtwoordCodeController implements WijzigWachtwoordCodeApi {
    private final WijzigWachtwoordService wijzigWachtwoordService;
    private final WijzigWachtwoordCodeMapper wijzigWachtwoordCodeMapper;

    @Override
    @MethodeLogging
    @TrackAndTraceId
    public ControleerWijzigWachtwoordCodeResultaat controleerWijzigWachtwoordCode(ControleerWijzigWachtwoordCode controleerWijzigWachtwoordCode) {
        return new ControleerWijzigWachtwoordCodeResultaat().resultaat(
                wijzigWachtwoordService.controleerWijzigWachtwoordCode(
                        controleerWijzigWachtwoordCode.getId(),
                        controleerWijzigWachtwoordCode.getCode()));
    }

    @Override
    @MethodeLogging
    @TrackAndTraceId
    public WijzigWachtwoordCode genereerWijzigWachtwoordCode(VraagWijzigWachtwoordCodeOp vraagWijzigWachtwoordCodeOp) {
        return wijzigWachtwoordCodeMapper.map(wijzigWachtwoordService.genereerWijzigWachtwoordCode(vraagWijzigWachtwoordCodeOp.getEmail()));
    }
}

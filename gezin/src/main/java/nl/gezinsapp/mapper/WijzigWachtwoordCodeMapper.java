package nl.gezinsapp.mapper;

import nl.gezinsapp.model.WijzigWachtwoordCode;
import org.mapstruct.Mapper;

@Mapper(componentModel = "CDI")
public interface WijzigWachtwoordCodeMapper {
    org.openapi.quarkus.gezin_openapi_yaml.model.WijzigWachtwoordCode map(WijzigWachtwoordCode activatieCode);
}

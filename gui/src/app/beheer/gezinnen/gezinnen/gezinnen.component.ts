import {Component, OnInit} from '@angular/core';
import {Gezin} from '../../../../api/gezin';
import {AvatarComponent} from "../../../common/avatar/avatar.component";
import {MaakDatumOpPipe} from "../../../pipes/maak-datum-op.pipe";
import {MaakGeslachtEnumOpPipe} from "../../../pipes/maak-geslacht-enum-op.pipe";
import {MaakSoortOuderEnumOpPipe} from "../../../pipes/maak-soort-ouder-enum-op.pipe";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-gezinnen',
  imports: [
    AvatarComponent,
    MaakDatumOpPipe,
    MaakGeslachtEnumOpPipe,
    MaakSoortOuderEnumOpPipe
  ],
  templateUrl: './gezinnen.component.html'
})
export class GezinnenComponent implements OnInit {
  gezinnen: Gezin[] = [];
  tonen: string[] = [];

  constructor(
    private readonly activatedRoute: ActivatedRoute
  ) {
  }

  ngOnInit(): void {
    this.activatedRoute.data.subscribe((response: any) => {
      this.gezinnen = response.gezinnen.gezinnen;
      this.gezinnen.forEach((gezin: Gezin) => {
        this.tonen[gezin.id!] = '';
      });
    });
  }

  toggle(id: number) {
    if (this.tonen[id] == '') {
      this.tonen[id] = 'show';
    } else {
      this.tonen[id] = '';
    }
  }
}

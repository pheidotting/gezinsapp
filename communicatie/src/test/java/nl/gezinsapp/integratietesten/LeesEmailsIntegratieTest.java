package nl.gezinsapp.integratietesten;

import com.fasterxml.jackson.core.JsonProcessingException;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;
import lombok.extern.slf4j.Slf4j;
import nl.gezinsapp.domain.Email;
import nl.gezinsapp.integratietesten.common.TestGegevens;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.openapi.quarkus.communicatie_openapi_yaml.model.Emails;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@Slf4j
@QuarkusTest
@QuarkusTestResource(MockServer.class)
class LeesEmailsIntegratieTest extends AbstractIntegratieTest {
    @ConfigProperty(name = "rabbitmq-host")
    private String rabbitHost;
    @ConfigProperty(name = "rabbitmq-port")
    private String rabbitPort;
    @ConfigProperty(name = "rabbitmq-username")
    private String rabbitUsername;
    @ConfigProperty(name = "rabbitmq-password")
    private String rabbitPassword;

    @Test
    @DisplayName("Test het ophalen van e-mails")
    void test() throws JsonProcessingException {
        maakDatabaseLeeg();

        var accesstoken = maakToken();
        mockCheckJwt(accesstoken, true);

        TestGegevens.setRabbitHost(rabbitHost);
        TestGegevens.setRabbitPort(Integer.valueOf(rabbitPort));
        TestGegevens.setRabbitUser(rabbitUsername);
        TestGegevens.setRabbitPassword(rabbitPassword);

        var email = new Email();
        email.setOnderwerp("Activatiecode");
        email.setTijdstipverstuurd(LocalDateTime.now());
        email.setTijdstipaangemaakt(LocalDateTime.now());
        email.setEmailadres("a@b.c");

        opslaan(email);

        var emails = leesViaApi(Emails.class, "/api/communicatie/leesemails/ACTIVATIECODE", accesstoken);

        assertEquals(1, emails.getEmails().size());
        assertEquals("a@b.c", emails.getEmails().get(0).getEmailadres());
        assertNotNull(emails.getEmails().get(0).getTijdstipaangemaakt());
        assertNotNull(emails.getEmails().get(0).getTijdstipverstuurd());
    }
}

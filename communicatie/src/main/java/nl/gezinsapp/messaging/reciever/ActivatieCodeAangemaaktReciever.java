package nl.gezinsapp.messaging.reciever;

import io.smallrye.common.annotation.Blocking;
import jakarta.enterprise.context.ApplicationScoped;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.gezinsapp.commons.aspect.MethodeLogging;
import nl.gezinsapp.mapper.ActivatieCodeAangemaaktMapper;
import nl.gezinsapp.messaging.model.ActivatieCodeAangemaakt;
import nl.gezinsapp.service.ActivatieCodeAangemaaktService;
import org.eclipse.microprofile.reactive.messaging.Incoming;
import org.eclipse.microprofile.reactive.messaging.Message;
import org.slf4j.MDC;

import java.util.concurrent.CompletionStage;

import nl.gezinsapp.commons.messaging.AbstractReciever;

@Slf4j
@ApplicationScoped
@RequiredArgsConstructor
public class ActivatieCodeAangemaaktReciever extends AbstractReciever<ActivatieCodeAangemaakt> {
    private final ActivatieCodeAangemaaktService activatieCodeAangemaaktService;
    private final ActivatieCodeAangemaaktMapper activatieCodeAangemaaktMapper;

    @Incoming("activatiecode-aangemaakt")
    @MethodeLogging
    @Blocking
    public CompletionStage<Void> verwerk(Message<ActivatieCodeAangemaakt> m) {
        var activatieCodeAangemaakt = getObject(m, ActivatieCodeAangemaakt.class);

        if(activatieCodeAangemaakt.getTrackAndTraceId()!=null){
            MDC.put("trackAndTraceId",activatieCodeAangemaakt.getTrackAndTraceId().toString());
        }

        log.info("Ontvangen : {}", activatieCodeAangemaakt);

        activatieCodeAangemaaktService.maakEmail(activatieCodeAangemaaktMapper.map(activatieCodeAangemaakt));

        return m.ack();
    }
}

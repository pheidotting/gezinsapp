package nl.gezinsapp.service;

import jakarta.enterprise.context.RequestScoped;
import lombok.RequiredArgsConstructor;
import nl.gezinsapp.messaging.model.Jarige;
import nl.gezinsapp.messaging.model.JarigenVandaag;
import nl.gezinsapp.messaging.sender.Sender;
import nl.gezinsapp.model.Gezinslid;

import java.time.LocalDate;

import static nl.gezinsapp.service.CommunicatieMessagingObjectUtil.vulObject;

@RequestScoped
@RequiredArgsConstructor
public class BepaalJarigenService {
    private final Sender sender;

    public void verstuurJarigen() {
        var dagVandaag = LocalDate.now().getDayOfMonth();
        var maandVandaag = LocalDate.now().getMonthValue();

        var jarigenVandaag = new JarigenVandaag(
                Gezinslid.findAll().stream()
                        .map(panacheEntityBase -> (Gezinslid) panacheEntityBase)
                        .filter(gezinslid -> gezinslid.getGeboorteDatum().getMonthValue() == maandVandaag
                                && gezinslid.getGeboorteDatum().getDayOfMonth() == dagVandaag)
                        .map(gezinslid -> {
                            var jarige = vulObject(gezinslid, Jarige.builder().build());
                            return jarige;
                        })
                        .toList());

        sender.verstuurJarigenVandaag(jarigenVandaag);
    }
}

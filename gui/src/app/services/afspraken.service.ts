import {Injectable} from '@angular/core';
import {Afspraak, AfspraakSoortAfspraakEnum, AfspraakTerugkeerEnum, AlgemeneResponse, SoortDienst} from '../../api/kalender';
import {DateSelectArg, EventClickArg, EventInput} from "@fullcalendar/core";
import moment from "moment/moment";
import {Foutmeldingen} from "../model/foutmeldingen";

@Injectable({
  providedIn: 'root'
})
export class AfsprakenService {
  teVerwijderenAfspraak: {
    id?: number;
    omschrijving?: string;
    heledag?: boolean;
    vanTijd?: string;
    totTijd?: string;
    vanDatum?: string;
    totDatum?: string;
  } = {};

  nieuweOfTeWijzigenAfspraak: {
    id?: number;
    omschrijving: string;
    heledag: boolean;
    vanTijd?: string;
    totTijd?: string;
    vanDatum?: string;
    totDatum?: string;
    terugkeer?: AfspraakTerugkeerEnum;
    soortAfspraak?: AfspraakSoortAfspraakEnum;
    soortDienst?: number;
    gezinsleden: Array<number>;
  } = {omschrijving: '', heledag: false, gezinsleden: []};
  alleSoortenDiensten: SoortDienst[] = [];

  constructor() {
  }

  gezinsleden: [{ id?: number, naam?: string, checked: boolean }] = [{checked: false}];

  mapAfspraakNaarEventInput(afspraak: Afspraak): EventInput {
    let afsp: EventInput = {
      id: afspraak.id + "",
      title: afspraak.omschrijving,
      allDay: afspraak.heledag
    };
    if (afspraak.soortDienst != null) {
      const sd: SoortDienst | undefined = this.alleSoortenDiensten.find((soortDienst: SoortDienst) => {
        return soortDienst.id == afspraak.soortDienst;
      });
      if (sd != undefined) {
        afsp.title = sd.omschrijving;
      } else {
        afsp.title = afspraak.soortDienst + '';
      }
    }
    if (afspraak.heledag) {
      afsp.start = afspraak.vanDatum;
      afsp.end = afspraak.totDatum;
    } else {
      afsp.start = afspraak.vanTijd;
      afsp.end = afspraak.totTijd;
    }
    let extendedProps: { soortAfspraak: string | undefined, terugkeer: string | undefined, gezinsleden: number[] | undefined } = {
      soortAfspraak: undefined,
      terugkeer: undefined,
      gezinsleden: undefined
    };
    if (afspraak.soortAfspraak != undefined) {
      extendedProps.soortAfspraak = afspraak.soortAfspraak;
    }
    extendedProps.gezinsleden = afspraak.gezinsleden;
    extendedProps.terugkeer = afspraak.terugkeer;
    afsp.extendedProps = extendedProps;

    return afsp;
  }

  setNieuweOfTeWijzigenAfspraakVanDateSelectArg(selectInfo: DateSelectArg): void {
    this.nieuweOfTeWijzigenAfspraak.heledag = selectInfo.allDay;
    this.nieuweOfTeWijzigenAfspraak.vanDatum = selectInfo.startStr;
    this.nieuweOfTeWijzigenAfspraak.totDatum = selectInfo.startStr;
    this.nieuweOfTeWijzigenAfspraak.vanTijd = selectInfo.startStr + "T09:00";
    this.nieuweOfTeWijzigenAfspraak.totTijd = selectInfo.startStr + "T10:00";
  }

  setNieuweOfTeWijzigenAfspraak(clickInfo: EventClickArg): void {
    this.gezinsleden.forEach((gezinslid) => {
      gezinslid.checked = false;
    });
    this.nieuweOfTeWijzigenAfspraak.id = +clickInfo.event.id;
    this.nieuweOfTeWijzigenAfspraak.heledag = clickInfo.event.allDay;
    this.nieuweOfTeWijzigenAfspraak.vanDatum = moment(clickInfo.event.startStr).format('YYYY-MM-DD');
    this.nieuweOfTeWijzigenAfspraak.totDatum = moment(clickInfo.event.startStr).format('YYYY-MM-DD');
    this.nieuweOfTeWijzigenAfspraak.vanTijd = moment(clickInfo.event.startStr).format('YYYY-MM-DDTHH:mm');
    this.nieuweOfTeWijzigenAfspraak.totTijd = moment(clickInfo.event.endStr).format('YYYY-MM-DDTHH:mm');
    this.nieuweOfTeWijzigenAfspraak.omschrijving = clickInfo.event.title;
    this.nieuweOfTeWijzigenAfspraak.soortAfspraak = clickInfo.event.extendedProps['soortAfspraak'] as AfspraakSoortAfspraakEnum;
    this.nieuweOfTeWijzigenAfspraak.terugkeer = clickInfo.event.extendedProps['terugkeer'];
    this.nieuweOfTeWijzigenAfspraak.gezinsleden = clickInfo.event.extendedProps['gezinsleden'];
    this.gezinsleden.forEach((gezinslid) => {
      let index: number = clickInfo.event.extendedProps['gezinsleden'].findIndex((a: number) => {
        return a == gezinslid.id;
      });
      if (index > -1) {
        gezinslid.checked = true;
      }
    });
  }

  checkAfspraak(): string[] {
    let meldingen: string[] = [];

    if (this.nieuweOfTeWijzigenAfspraak.soortDienst == null) {
      if (this.nieuweOfTeWijzigenAfspraak.omschrijving == null || this.nieuweOfTeWijzigenAfspraak.omschrijving == '') {
        meldingen.push(Foutmeldingen.OMSCHRIJVING_MOET_GEVULD_ZIJN);
      }
      if (moment(this.nieuweOfTeWijzigenAfspraak.vanDatum).isAfter(moment(this.nieuweOfTeWijzigenAfspraak.totDatum))) {
        meldingen.push(Foutmeldingen.BEGINDATUM_MOET_VOOR_EINDDATUM_LIGGEN)
      }
      if (this.nieuweOfTeWijzigenAfspraak.vanTijd != undefined && this.nieuweOfTeWijzigenAfspraak.totTijd != undefined) {
        if (moment(this.nieuweOfTeWijzigenAfspraak.vanTijd).isAfter(moment(this.nieuweOfTeWijzigenAfspraak.totTijd))) {
          meldingen.push(Foutmeldingen.BEGINTIJD_MOET_VOOR_EINDTIJD_LIGGEN)
        }
      }
    }

    return meldingen;
  }

  zetKlaarVoorOpslaan(): void {
    if (!this.nieuweOfTeWijzigenAfspraak.heledag) {
      this.nieuweOfTeWijzigenAfspraak.vanTijd = this.nieuweOfTeWijzigenAfspraak.vanTijd + ':00-02:00';
      this.nieuweOfTeWijzigenAfspraak.totTijd = this.nieuweOfTeWijzigenAfspraak.totTijd + ':00-02:00';
      this.nieuweOfTeWijzigenAfspraak.vanDatum = undefined;
      this.nieuweOfTeWijzigenAfspraak.totDatum = undefined;
    } else {
      this.nieuweOfTeWijzigenAfspraak.vanTijd = undefined;
      this.nieuweOfTeWijzigenAfspraak.totTijd = undefined;
    }
    this.gezinsleden.filter((gezinslid) => {
      return gezinslid.checked;
    }).forEach((gezinslid) => {
      this.nieuweOfTeWijzigenAfspraak.gezinsleden.push(gezinslid.id!)
    });
  }

  verwerkAfspraak(response: AlgemeneResponse): EventInput {
    let nw: EventInput = this.mapAfspraakNaarEventInput(this.nieuweOfTeWijzigenAfspraak);
    nw.id = response.id + '';
    this.nieuweOfTeWijzigenAfspraak = {omschrijving: '', heledag: false, gezinsleden: [0]};

    return nw;
  }
}

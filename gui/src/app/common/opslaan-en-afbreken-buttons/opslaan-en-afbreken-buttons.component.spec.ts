import {ComponentFixture, TestBed} from '@angular/core/testing';

import {OpslaanEnAfbrekenButtonsComponent} from './opslaan-en-afbreken-buttons.component';

describe('OpslaanEnAfbrekenButtonsComponent', () => {
  let component: OpslaanEnAfbrekenButtonsComponent;
  let fixture: ComponentFixture<OpslaanEnAfbrekenButtonsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [OpslaanEnAfbrekenButtonsComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(OpslaanEnAfbrekenButtonsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

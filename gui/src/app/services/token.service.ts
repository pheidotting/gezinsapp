import {Inject, Injectable, PLATFORM_ID} from '@angular/core';
import {JwtHelperService} from "@auth0/angular-jwt";
import {JwtResponse, JwtService} from "../../api/authenticatie";
import {isPlatformBrowser} from "@angular/common";
import {Router} from "@angular/router";
import {Observable, Observer} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class TokenService {
  private _accessToken: string | null;
  private _refreshToken: string | null;

  private jwtHelperService = new JwtHelperService();

  constructor(
    @Inject(PLATFORM_ID) private platformId: object,
    private jwtService: JwtService,
    private router: Router
  ) {
    this._accessToken = null;
    this._refreshToken = null;
  }

  get refreshToken(): string | null {
    if (this._refreshToken == null) {
      if (isPlatformBrowser(this.platformId)) {
        this._refreshToken = localStorage.getItem("refreshtoken") as string;
      }
    }
    return this._refreshToken;
  }

  get accessToken(): Observable<string | null> {
    if (this._accessToken == null) {
      if (isPlatformBrowser(this.platformId)) {
        this._accessToken = sessionStorage.getItem("accesstoken") as string;
      }
    }
    if (this._accessToken != null) {
      if (this.jwtHelperService.isTokenExpired(this._accessToken)) {
        this._accessToken = null;
      }
    }
    if (this._accessToken == null && this.refreshToken != null) {
      return new Observable((observer: Observer<string | null>) => {
        this.jwtService.renewJwt({refreshToken: this._refreshToken!}).subscribe({
          next: (jwtResponse: JwtResponse) => {
            this.setAccessToken(jwtResponse.accessToken);
            observer.next(this._accessToken);
          },
          error: (error) => {
            this.router.navigate(['inloggen'])
          }
        });
      });
    } else if (this._accessToken == null) {
      if (isPlatformBrowser(this.platformId)) {
        this.router.navigate(['inloggen']);
      }
      return new Observable((observer: Observer<string | null>) => {
        observer.next(this._accessToken);
      });
    } else {
      return new Observable((observer: Observer<string | null>) => {
        observer.next(this._accessToken);
      });
    }
  }

  set accessToken(value: string | null) {
    this.setAccessToken(value);
  }

  setAccessToken(value: string | null | undefined) {
    if (value == null || value == "null") {
      sessionStorage.removeItem("accesstoken");
      value = null;
    } else {
      sessionStorage.setItem("accesstoken", value);
    }
    this._accessToken = value
    if (this._accessToken == null) {
      this.router.navigate(['/inloggen']);
    }
  }

  setRefreshToken(value: string | null | undefined) {
    if (value == null || value == "null") {
      localStorage.removeItem("refreshtoken");
      value = null;
    } else {
      localStorage.setItem("refreshtoken", value);
    }
    this._refreshToken = value;
  }
}

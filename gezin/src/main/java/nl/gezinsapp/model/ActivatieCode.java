package nl.gezinsapp.model;

import lombok.Builder;
import lombok.Getter;

import java.util.UUID;

@Getter
@Builder
public class ActivatieCode {
    private String code;
    private UUID id;
}

package nl.gezinsapp.integratietesten;

import com.fasterxml.jackson.core.JsonProcessingException;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.openapi.quarkus.gezin_openapi_yaml.model.LeesGezin;

import java.util.UUID;

import static nl.gezinsapp.integratietesten.common.random.RandomGezin.randomDomainGezin;
import static org.junit.jupiter.api.Assertions.assertEquals;

@QuarkusTest
@QuarkusTestResource(MockServer.class)
class LeesHuidigGezinIntegratieTest extends AbstractIntegratieTest {
    @Test
    @DisplayName("Test het ophalen van het huidige gezin (van de ingelogde gebruiker)")
    void testNieuw() throws JsonProcessingException {
        maakDatabaseLeeg();

        var domGezin = randomDomainGezin();
        opslaanGezin(domGezin);

        var trackAndTraceId = UUID.randomUUID().toString();
        var accesstoken = maakToken();
        var jwtResponse = mockCheckJwt(accesstoken, domGezin.getId(),trackAndTraceId);

        var result = leesViaApi(LeesGezin.class, "/api/gezin/huidig", accesstoken,trackAndTraceId);

        assertEquals(domGezin.getNaam(), result.getNaam());

    }

//    @Test
//    @DisplayName("Test het ophalen van het huidige gezin (van de ingelogde gebruiker) (eigenlijk is deze alleen om sonar te pleasen)")
//    void testNieuw2() {
//        maakDatabaseLeeg();
//
//        mockCheckJwt(2L);
//
//        var result = leesViaApi(LeesGezin.class, "/api/gezin/huidig", maakToken());
//
//        assertEquals("Heidotting & Co", result.getNaam());
//
//    }
}


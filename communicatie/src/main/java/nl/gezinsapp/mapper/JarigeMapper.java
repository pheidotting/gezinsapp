package nl.gezinsapp.mapper;

import nl.gezinsapp.messaging.model.Jarige;
import nl.gezinsapp.model.CommJarige;
import org.mapstruct.Mapper;

@Mapper(componentModel = "CDI")
public interface JarigeMapper {
    CommJarige map(Jarige jarige);
}

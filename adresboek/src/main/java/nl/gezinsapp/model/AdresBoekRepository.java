package nl.gezinsapp.model;

import io.quarkus.hibernate.orm.panache.PanacheRepositoryBase;
import jakarta.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class AdresBoekRepository implements PanacheRepositoryBase<AdresBoek, Long> {
}

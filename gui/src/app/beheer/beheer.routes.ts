import {Routes} from '@angular/router';
import {GezinnenComponent} from "./gezinnen/gezinnen/gezinnen.component";
import {BeheerComponent} from "./beheer/beheer.component";
import {alleGezinnenResolver} from "../common/resolvers/allegezinnen.resolver";
import {ActivatieMailsComponent} from "./activatie-mails/activatie-mails.component";
import {alleActivatieEmailsResolver} from "../common/resolvers/alle-activatie-emails.resolver";
import {beheerGuard} from "../guards/beheer.guard";

export const BEHEER_ROUTES: Routes = [
  {
    path: '',
    component: BeheerComponent,
  },
  {
    path: 'gezinnen',
    component: GezinnenComponent,
    canActivate: [beheerGuard],
    resolve: {
      gezinnen: alleGezinnenResolver
    }
  },
  {
    path: 'emails',
    component: ActivatieMailsComponent,
    canActivate: [beheerGuard],
    resolve: {
      emails: alleActivatieEmailsResolver
    }
  }
]

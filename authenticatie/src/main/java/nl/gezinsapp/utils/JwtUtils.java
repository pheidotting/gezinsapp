package nl.gezinsapp.utils;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import jakarta.enterprise.context.ApplicationScoped;
import lombok.RequiredArgsConstructor;
import nl.gezinsapp.commons.aspect.MethodeLogging;
import nl.gezinsapp.model.GebruikerImpl;

import java.util.Date;

@ApplicationScoped
@RequiredArgsConstructor
public class JwtUtils {
    @MethodeLogging
    public String generateJwtToken(GebruikerImpl gebruiker) {
        return generateTokenFromUsername(gebruiker);
    }

    @MethodeLogging
    public String generateTokenFromUsername(GebruikerImpl gebruiker) {
        return Jwts.builder()
                .setSubject(gebruiker.getEmailadres())
                .setIssuedAt(new Date())
                .claim("userId", gebruiker.getId())
                .claim("soortGebruiker", gebruiker.getSoort().name())
                .setExpiration(new Date((new Date()).getTime() + (7200 * 1000)))
                .signWith(SignatureAlgorithm.HS512, gebruiker.getSalt())
                .compact();
    }
}
import {Component, ElementRef, ViewChild} from '@angular/core';
import {WijzigWachtwoordCodeService} from '../../api/gezin/api/wijzigWachtwoordCode.service';
import {WijzigWachtwoordCode} from '../../api/gezin/model/wijzigWachtwoordCode';
import {ControleerWijzigWachtwoordCodeResultaat} from '../../api/gezin/model/controleerWijzigWachtwoordCodeResultaat';
import {Foutmeldingen} from "../model/foutmeldingen";
import {WijzigWachtwoordService} from '../../api/gezin/api/wijzigWachtwoord.service';
import {AlgemeneResponse} from '../../api/gezin';
import {Router} from '@angular/router';
import {WachtwoordSterkteService} from '../../api/authenticatie/api/wachtwoordSterkte.service';
import {WachtwoordSterkte} from '../../api/authenticatie/model/wachtwoordSterkte';
import {MeldingenComponent} from "../common/meldingen/meldingen.component";
import {FormsModule} from "@angular/forms";
import {NgForOf, NgIf} from "@angular/common";

@Component({
  selector: 'app-wijzig-wachtwoord',
  templateUrl: './wijzig-wachtwoord.component.html',
  styleUrl: './wijzig-wachtwoord.component.css',
  imports: [
    MeldingenComponent,
    FormsModule,
    NgIf,
    NgForOf
  ]
})
export class WijzigWachtwoordComponent {
  email: string = '';
  foutmeldingen: string[] = [];
  stapEenGereed: boolean = false;
  codeId: string | undefined;
  code: string | undefined;
  codeOk: boolean = false;
  passwordOk: boolean = false;
  passwordStrength: number = 0;
  wachtwoordSterkte: WachtwoordSterkte = {'valide': false, 'meldingen': []};

  wachtwoord: string = '';
  wachtwoordnogmaals: string = '';

  @ViewChild('codeDeelEenE') codeDeelEenElement?: ElementRef;
  @ViewChild('codeDeelTweeE') codeDeelTweeElement?: ElementRef;
  @ViewChild('codeDeelDrieE') codeDeelDrieElement?: ElementRef;
  @ViewChild('codeDeelVierE') codeDeelVierElement?: ElementRef;
  @ViewChild('codeDeelVijfE') codeDeelVijfElement?: ElementRef;
  @ViewChild('codeDeelZesE') codeDeelZesElement?: ElementRef;
  codeDeelEen: string = '';
  codeDeelTwee: string = '';
  codeDeelDrie: string = '';
  codeDeelVier: string = '';
  codeDeelVijf: string = '';
  codeDeelZes: string = '';

  constructor(
    private wijzigWachtwoordCodeService: WijzigWachtwoordCodeService,
    private wachtwoordSterkteApiService: WachtwoordSterkteService,
    private wijzigWachtwoordService: WijzigWachtwoordService,
    private router: Router
  ) {
  }

  doorgaanNaInvullenEmail(): void {
    this.foutmeldingen = [];
    if (this.email == '') {
      this.foutmeldingen.push(Foutmeldingen.VUL_JE_EMAILADRES_IN_WAARMEE_JE_INLOGT);
    } else {
      this.wijzigWachtwoordCodeService.genereerWijzigWachtwoordCode({'email': this.email}).subscribe((wijzigWachtwoordCode: WijzigWachtwoordCode) => {
        this.stapEenGereed = true;
        this.codeId = wijzigWachtwoordCode.id;
      });
    }
  }

  controlerenActiveringsCode(index: number) {
    if (index == 1 && this.codeDeelEen != '') {
      this.codeDeelTweeElement!.nativeElement.focus();
    } else if (index == 2) {
      this.codeDeelDrieElement!.nativeElement.focus();
    } else if (index == 3) {
      this.codeDeelVierElement!.nativeElement.focus();
    } else if (index == 4) {
      this.codeDeelVijfElement!.nativeElement.focus();
    } else if (index == 5) {
      this.codeDeelZesElement!.nativeElement.focus();
    } else if (index == 6) {
      let code = this.codeDeelEen +
        this.codeDeelTwee +
        this.codeDeelDrie +
        this.codeDeelVier +
        this.codeDeelVijf +
        this.codeDeelZes;

      this.code = code;
      this.wijzigWachtwoordCodeService.controleerWijzigWachtwoordCode({
        'id': this.codeId!,
        'code': code
      }).subscribe((resultaat: ControleerWijzigWachtwoordCodeResultaat) => {
        this.codeOk = resultaat.resultaat;
      });
    }
  }

  wachtwoordOpslaan(): void {
    this.foutmeldingen = [];
    if (this.wachtwoord == '' || this.wachtwoordnogmaals == '') {
      this.foutmeldingen.push(Foutmeldingen.VUL_BEIDE_VELDEN_IN);
    }
    if (this.foutmeldingen.length == 0) {
      if (this.wachtwoord != this.wachtwoordnogmaals) {
        this.foutmeldingen.push(Foutmeldingen.WACHTWOORDEN_KOMEN_NIET_OVEREEN);
      }
    }
    if (this.foutmeldingen.length == 0) {
      this.wijzigWachtwoordService.wijzigWachtwoord({
        'email': this.email,
        'wachtwoord': this.wachtwoord,
        'codeid': this.codeId!,
        'code': this.code!
      }).subscribe((response: AlgemeneResponse) => {
        this.router.navigate(['inloggen']);
      });
    }
  }

  onChangeCheckPassword(): void {
    if (this.wachtwoord != null) {
      this.wachtwoordSterkteApiService.wachtwoordSterkte(this.wachtwoord).subscribe((wachtwoordSterkte: WachtwoordSterkte) => {
        this.passwordStrength = 5 - wachtwoordSterkte.meldingen.length;
        this.wachtwoordSterkte = wachtwoordSterkte;
        this.passwordOk = wachtwoordSterkte.valide && this.wachtwoord == this.wachtwoordnogmaals;
      });
    } else {
      this.wachtwoordSterkte = {'valide': false, 'meldingen': []};
    }
  }
}

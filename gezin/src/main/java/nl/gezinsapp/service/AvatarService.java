package nl.gezinsapp.service;

import jakarta.enterprise.context.ApplicationScoped;
import lombok.extern.slf4j.Slf4j;
import nl.gezinsapp.commons.aspect.NietIngelogdException;
import nl.gezinsapp.model.Gezinslid;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.openapi.quarkus.authenticatie_openapi_yaml.model.Gebruiker;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Slf4j
@ApplicationScoped
public class AvatarService {
    @ConfigProperty(name = "avatar.locatie")
    private String avatarLocatie;

    public File download(Long gezinslidId) {
        return new File(locatieAvatar(leesGezinslid(gezinslidId)));
    }

    public void upload(Gebruiker ingelogdeGebruiker, Long gezinslidId, File avatar) throws IOException {
        var gebruikerDb = leesGezinslid(gezinslidId);

        checkGezin(gebruikerDb, ingelogdeGebruiker);

        if (!new File(avatarLocatie).exists()) {
            Files.createDirectory(Paths.get(avatarLocatie));
        }
        if (!new File(basisLocatieAvatars(gebruikerDb)).exists()) {
            Files.createDirectory(Paths.get(basisLocatieAvatars(gebruikerDb)));
        }
        var file = new File(locatieAvatar(gebruikerDb));
        if (file.createNewFile()) {

            try (OutputStream os = new FileOutputStream(file)) {
                os.write(Files.readAllBytes(avatar.toPath()));
            }

            gebruikerDb.setAvatarAanwezig(true);
            gebruikerDb.persist();
        } else {
            throw new IOException("Bestand kon niet worden aangemaakt.");
        }
    }

    public void verwijder(Gebruiker ingelogdeGebruiker, Long gezinslidId) throws IOException {
        var gebruikerDb = leesGezinslid(gezinslidId);

        checkGezin(gebruikerDb, ingelogdeGebruiker);

        Files.delete(Path.of(locatieAvatar(gebruikerDb)));

        gebruikerDb.setAvatarAanwezig(false);
        gebruikerDb.persist();
    }

    private void checkGezin(Gezinslid gebruikerDb, Gebruiker ingelogdeGebruiker) {
        if (gebruikerDb.getGezin().getId() != ingelogdeGebruiker.getGezin()) {
            throw new NietIngelogdException();
        }
    }

    private Gezinslid leesGezinslid(Long id) {
        return Gezinslid.findById(id);
    }

    private String basisLocatieAvatars(Gezinslid gezinslid) {
        return avatarLocatie + File.separator + gezinslid.getGezin().getId();
    }

    private String locatieAvatar(Gezinslid gezinslid) {
        return basisLocatieAvatars(gezinslid) + File.separator + gezinslid.getId();
    }
}

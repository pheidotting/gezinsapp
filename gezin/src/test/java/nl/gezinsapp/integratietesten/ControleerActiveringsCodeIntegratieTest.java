package nl.gezinsapp.integratietesten;

import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;
import jakarta.inject.Inject;
import lombok.extern.slf4j.Slf4j;
import nl.gezinsapp.model.repository.ActivatieCodeRepository;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.openapi.quarkus.gezin_openapi_yaml.model.ControleerActiveringsCode;
import org.openapi.quarkus.gezin_openapi_yaml.model.ControleerActiveringsCodeResultaat;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

@Slf4j
@QuarkusTest
@QuarkusTestResource(MockServer.class)
class ControleerActiveringsCodeIntegratieTest extends AbstractIntegratieTest {
    @Inject
    private ActivatieCodeRepository activatieCodeRepository;

    @Test
    @DisplayName("Controleer een goede code")
    void test1() {
        var activatieCode = activatieCodeRepository.set("123456");

        var controleerActiveringsCode = new ControleerActiveringsCode();
        controleerActiveringsCode.code("123456");
        controleerActiveringsCode.setId(activatieCode.getId());

        var controleerActiveringsCodeResultaat = postViaApi(ControleerActiveringsCodeResultaat.class, "/api/gezin/activeringscode/controleer", controleerActiveringsCode, null, null);

        assertTrue(controleerActiveringsCodeResultaat.getResultaat());
    }

    @Test
    @DisplayName("Controleer een foute code")
    void test2() {
        var activatieCode = activatieCodeRepository.set("123456");

        var controleerActiveringsCode = new ControleerActiveringsCode();
        controleerActiveringsCode.code("123457");
        controleerActiveringsCode.setId(activatieCode.getId());

        var controleerActiveringsCodeResultaat = postViaApi(ControleerActiveringsCodeResultaat.class, "/api/gezin/activeringscode/controleer", controleerActiveringsCode, null, null);

        assertFalse(controleerActiveringsCodeResultaat.getResultaat());
    }

    @Test
    @DisplayName("Controleer een code die niet bestaat")
    void test3() {
        var controleerActiveringsCode = new ControleerActiveringsCode();
        controleerActiveringsCode.code("123456");
        controleerActiveringsCode.setId(UUID.randomUUID());

        var controleerActiveringsCodeResultaat = postViaApi(ControleerActiveringsCodeResultaat.class, "/api/gezin/activeringscode/controleer", controleerActiveringsCode, null, null);

        assertFalse(controleerActiveringsCodeResultaat.getResultaat());
    }
}

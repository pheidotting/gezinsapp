package nl.gezinsapp.controller;

import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.gezinsapp.commons.aspect.AlsGezinslid;
import nl.gezinsapp.commons.aspect.MethodeLogging;
import nl.gezinsapp.commons.aspect.TrackAndTraceId;
import nl.gezinsapp.domain.Verlanglijst;
import nl.gezinsapp.domain.VerlanglijstItem;
import nl.gezinsapp.mapper.VerlanglijstItemMapper;
import org.openapi.quarkus.verlanglijstje_openapi_yaml.api.*;
import org.openapi.quarkus.verlanglijstje_openapi_yaml.model.*;
import java.time.LocalDateTime;

@Slf4j
@RequiredArgsConstructor
public class VerlanglijstItemController extends AbstractController implements VerlanglijstItemApi {
    private final VerlanglijstItemMapper verlanglijstItemMapper;

    @Override
    @Transactional
    @TrackAndTraceId
    @AlsGezinslid
    @MethodeLogging
    public AlgemeneResponse nieuwVerlanglijstItem(VerlanglijstjeItem verlanglijstjeItem) {
        var verlanglijst = (Verlanglijst) Verlanglijst.findById(verlanglijstjeItem.getVerlanglijstje());

        if (isVanHuidigGezin(verlanglijst)) {
            var item = verlanglijstItemMapper.map(verlanglijstjeItem);
            item.setVerlanglijst(verlanglijst);
            item.setDatumToegevoegd(LocalDateTime.now());
            item.persist();

            return new AlgemeneResponse().id(item.getId());
        } else {
            return new AlgemeneResponse().foutmeldingen(new Foutmeldingen().addMeldingenItem("Betreffende verlanglijst is geen onderdeel van het huidige gezin."));
        }
    }

    @Override
    @Transactional
    @TrackAndTraceId
    @AlsGezinslid
    @MethodeLogging
    public AlgemeneResponse verwijderVerlanglijstItem(Long verlanglijstItemId) {
        log.info("{}", verlanglijstItemId);
        var verlanglijstItem = (VerlanglijstItem) VerlanglijstItem.findById(verlanglijstItemId);

        if (isVanHuidigGezin(verlanglijstItem)) {
            verlanglijstItem.delete();

            return new AlgemeneResponse().id(verlanglijstItemId);
        } else {
            return new AlgemeneResponse().foutmeldingen(new Foutmeldingen().addMeldingenItem("Betreffende verlanglijst is geen onderdeel van het huidige gezin."));
        }
    }

    @Override
    @Transactional
    @TrackAndTraceId
    @AlsGezinslid
    @MethodeLogging
    public AlgemeneResponse wijzigVerlanglijstItem(VerlanglijstjeItem verlanglijstjeItem) {
        var verlanglijstItem = (VerlanglijstItem) VerlanglijstItem.findById(verlanglijstjeItem.getId());

        if (isVanHuidigGezin(verlanglijstItem)) {
            var item = verlanglijstItemMapper.update(verlanglijstjeItem, verlanglijstItem);
            item.persist();

            return new AlgemeneResponse().id(item.getId());
        } else {
            return new AlgemeneResponse().foutmeldingen(new Foutmeldingen().addMeldingenItem("Betreffende verlanglijst item is geen onderdeel van het huidige gezin."));
        }
    }
}

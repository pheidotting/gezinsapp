package nl.gezinsapp.controller;

import io.quarkus.mailer.Mail;
import io.quarkus.mailer.Mailer;
import io.quarkus.runtime.Startup;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.transaction.Transactional;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import lombok.extern.slf4j.Slf4j;
import nl.gezinsapp.domain.Email;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.slf4j.MDC;

import java.time.LocalDateTime;

@Slf4j
@Startup
@ApplicationScoped
@Path("/")
public class VerstuurmailController {
    @ConfigProperty(name = "quarkus.application.name")
    private String applicatieNaam;
    @ConfigProperty(name = "quarkus.application.omgeving")
    private String omgeving;

    @Inject
    Mailer mailer;

    @GET
    @Path("/verstuurmails")
    @Transactional
    public void verstuurmails() {
        MDC.put("applicatie-naam", applicatieNaam);
        MDC.put("omgeving", omgeving);

        log.debug("Check mail...");
        Email.findAll()
                .stream()
                .map(panacheEntityBase -> (Email) panacheEntityBase)
                .filter(email -> email.getTijdstipverstuurd() == null)
                .map(email -> {
                    email.setTijdstipverstuurd(LocalDateTime.now());
                    email.persist();
                    return Mail.withText(email.getEmailadres(),
                            email.getOnderwerp(),
                            email.getTekst());
                })
                .forEach(mail -> {
                    if (mail.getTo() == null || mail.getTo().contains("") || mail.getTo().contains(null)) {
                        log.error("Er wordt mogelijk een e-mail verstuurd naar een leeg e-mailadres, onderwerp is {}", mail.getSubject());
                    } else {
                        log.info("Stuur mail naar {}", mail.getTo());
                        mailer.send(mail);
                    }
                });
    }
}

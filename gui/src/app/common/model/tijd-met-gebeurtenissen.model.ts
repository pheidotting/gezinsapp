import {Gebeurtenis} from "./gebeurtenis.model";

export class TijdMetGebeurtenissen {
  van?: string;
  tot?: string;
  gebeurtenissen: Gebeurtenis[] = [];
}

package nl.gezinsapp.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.HashMap;
import java.util.Map;

@Setter
@Getter
@NoArgsConstructor
@ToString
public class CommWijzigWachtwoordCodeAangemaakt extends CommObject {
    private String code;

    @Override
    public Map<String, Object> velden() {
        Map<String, Object> velden = new HashMap<>();
        velden.put("emailadres", emailadres);
        velden.put("code", code);
        return velden;
    }
}

import {ChangeDetectorRef, Component, signal, ViewChild, WritableSignal} from '@angular/core';
import {Calendar, CalendarApi, CalendarOptions, DateSelectArg, EventApi, EventClickArg, EventInput} from '@fullcalendar/core';
import interactionPlugin from '@fullcalendar/interaction';
import dayGridPlugin from '@fullcalendar/daygrid';
import nlLocale from '@fullcalendar/core/locales/nl';
import timeGridPlugin from '@fullcalendar/timegrid';
import listPlugin from '@fullcalendar/list';
import {Afspraak, AfspraakService, AfspraakSoortAfspraakEnum, LijstAfspraken, RoosterService, SoortDienst, SoortenDiensten} from "../../api/kalender";
import moment from "moment";
import {FullCalendarComponent, FullCalendarModule} from "@fullcalendar/angular";
import {AfsprakenService} from "../services/afspraken.service";
import {Gezin} from '../../api/gezin/model/gezin';
import {Gezinslid} from '../../api/gezin/model/gezinslid';
import {GezinslidUtil} from "../services/gezinslid-util.service";
import {SideBarComponent} from "../common/side-bar/side-bar.component";
import {FormsModule} from "@angular/forms";
import {KalenderItemComponent} from "./kalender-item/kalender-item.component";
import {AfsprakenModalComponent} from "./afspraken-modal/afspraken-modal.component";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-afspraken',
  templateUrl: './afspraken.component.html',
  imports: [
    SideBarComponent,
    FullCalendarModule,
    FormsModule,
    KalenderItemComponent,
    AfsprakenModalComponent
  ]
})
export class AfsprakenComponent {
  gezin: Gezin = {
    naam: '',
    gezinsleden: [],
  };

  afspraken: EventInput[] = [];
  @ViewChild('calendar') calendarComponent!: FullCalendarComponent;
  modalTonen: string = "none";
  foutmeldingen: string[] = [];
  meldingen: string[] = [];
  goedmeldingen: string[] = [];

  get afsprakenService(): AfsprakenService {
    return this._afsprakenService;
  }

  calendarVisible: WritableSignal<boolean> = signal(true);
  calendarOptions: WritableSignal<CalendarOptions> = signal<CalendarOptions>({
    plugins: [
      interactionPlugin,
      dayGridPlugin,
      timeGridPlugin,
      listPlugin
    ],
    locale: nlLocale,
    headerToolbar: {
      left: 'prev,next today',
      center: 'title',
      right: 'dayGridMonth,timeGridWeek,timeGridDay,listWeek'
    },
    initialView: 'dayGridMonth',
    events: this.afspraken,
    weekends: true,
    editable: true,
    selectable: true,
    selectMirror: true,
    dayMaxEvents: true,
    select: this.handleDateSelect.bind(this),
    eventClick: this.handleEventClick.bind(this),
    eventsSet: this.handleEvents.bind(this),
    datesSet: event => {
      this.haalAfspraken(moment(event.startStr), moment(event.endStr));
    }
  });

  currentEvents: WritableSignal<EventApi[]> = signal<EventApi[]>([]);

  constructor(
    private readonly activatedRoute: ActivatedRoute,
    private readonly changeDetector: ChangeDetectorRef,
    private readonly afspraakService: AfspraakService,
    private readonly _afsprakenService: AfsprakenService,
    private readonly gezinslidUtil: GezinslidUtil,
    private readonly roosterService: RoosterService
  ) {
    this.activatedRoute.data.subscribe((response: any) => {
      this.gezin = response.gezin;
      this.afsprakenService.gezinsleden.splice(0, this.afsprakenService.gezinsleden.length);
      this.gezin.gezinsleden.forEach((gezinslid: Gezinslid) => {
        this._afsprakenService.gezinsleden.push({id: gezinslid.id, naam: this.gezinslidUtil.maakNaamOp(gezinslid) as string, checked: false});
      });
    });
  }

  haalAfspraken(start: moment.Moment, eind: moment.Moment): void {
    this.roosterService.soortenDienstenBijGezin().subscribe((soortenDiensten: SoortenDiensten) => {
      this._afsprakenService.alleSoortenDiensten = soortenDiensten.soortenDiensten;

      this.afspraakService.alleAfspraken(start.format('YYYY-MM-DD'), eind.format('YYYY-MM-DD')).subscribe((lijstAfspraken: LijstAfspraken) => {
        this.afspraken = [];
        let api: Calendar | undefined;
        if (this.calendarComponent != null) {
          api = this.calendarComponent.getApi();
          api.removeAllEvents();
        }
        lijstAfspraken.afspraken.forEach((afspraak: Afspraak) => {
          let nw: EventInput = this._afsprakenService.mapAfspraakNaarEventInput(afspraak);
          this.afspraken.push(nw);
          if (api != undefined) {
            api.addEvent(nw);
          }
        });
      });
    });
  }

  handleDateSelect(selectInfo: DateSelectArg) {
    // const title = prompt('Please enter a new title for your event');
    const calendarApi: CalendarApi = selectInfo.view.calendar;

    calendarApi.unselect(); // clear date selection

    this._afsprakenService.setNieuweOfTeWijzigenAfspraakVanDateSelectArg(selectInfo);
    this.modalTonen = 'block';
  }

  handleEventClick(clickInfo: EventClickArg) {
    this.modalTonen = 'block';
    this._afsprakenService.setNieuweOfTeWijzigenAfspraak(clickInfo);
  }

  handleEvents(events: EventApi[]) {
    this.currentEvents.set(events);
    this.changeDetector.detectChanges(); // workaround for pressionChangedAfterItHasBeenCheckedError
  }

  afspraakGemaaktOfGewijzigd(event: EventInput) {
    this.afspraken.push(event);
  }


  soortenDiensten(): SoortDienst[] {
    let soorten: SoortDienst[] = [];
    if (this._afsprakenService.gezinsleden.length == 1) {
      this._afsprakenService.alleSoortenDiensten.forEach((soortDienst: SoortDienst) => {
        soorten.push(soortDienst);
      });
    } else {
      this._afsprakenService.gezinsleden.filter((gezinslid: { id?: number, naam?: String, checked: boolean }) => {
        return gezinslid.checked;
      }).forEach((gezinslid: { id?: number, naam?: String, checked: boolean }) => {
        this._afsprakenService.alleSoortenDiensten.filter((soortDienst: SoortDienst) => {
          return soortDienst.gezinslid == gezinslid.id;
        }).forEach((soortDienst: SoortDienst) => {
          soorten.push(soortDienst);
        });
      });
    }

    return soorten;
  }


  protected readonly AfspraakSoortAfspraakEnum = AfspraakSoortAfspraakEnum;
}

package nl.gezinsapp.domain;

import io.quarkus.hibernate.orm.panache.PanacheEntity;
import io.quarkus.hibernate.orm.panache.PanacheEntityBase;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.time.LocalDateTime;

@Entity
@Table(name = "VERLANGLIJSTITEM")
@Getter
@Setter
@ToString(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
public class VerlanglijstItem extends PanacheEntityBase {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;
    @JoinColumn(name = "VERLANGLIJST")
    @ManyToOne
    private Verlanglijst verlanglijst;
    @Column(name = "URL")
    private String url;
    @Column(name = "OMSCHRIJVING")
    private String omschrijving;
    @Column(name = "DATUMTOEGEVOEGD")
    private LocalDateTime datumToegevoegd;
    @Column(name = "DATUMAFGESTREEPT")
    private LocalDateTime datumAfgestreept;
}

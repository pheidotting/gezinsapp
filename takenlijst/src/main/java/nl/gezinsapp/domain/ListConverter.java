package nl.gezinsapp.domain;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.persistence.AttributeConverter;
import jakarta.persistence.Converter;

import java.util.ArrayList;
import java.util.List;

@Converter(autoApply = true)
public class ListConverter implements AttributeConverter<List<Long>, String> {
    private ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public String convertToDatabaseColumn(List<Long> longs) {
        try {
            return objectMapper.writeValueAsString(longs);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<Long> convertToEntityAttribute(String s) {
        try {
            var longs = objectMapper.readValue(s, new TypeReference<List<Long>>() {
            });
            if (longs != null && longs.size() == 1 && longs.get(0) == null) {
                return new ArrayList<>();
            }
            return longs;
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }
}

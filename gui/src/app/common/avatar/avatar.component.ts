import {Component, Input} from '@angular/core';
import {GezinService, Gezinslid, LeesGezin} from "../../../api/gezin";
import {NgIf} from "@angular/common";

@Component({
  selector: 'app-avatar',
  imports: [
    NgIf
  ],
  templateUrl: './avatar.component.html'
})
export class AvatarComponent {
  @Input() gezinslidId: number | undefined;
  @Input() gezinslid: Gezinslid | undefined;
  @Input() timestamp: number = (new Date()).getTime();

  constructor(
    private gezinService: GezinService
  ) {
  }

  getGezinslid(): Gezinslid | undefined {
    if (this.gezinslid == null && this.gezinslidId != null) {
      this.gezinService.leesHuidigGezin().subscribe((gezin: LeesGezin) => {
        this.gezinslid = gezin.gezinsleden.filter((lid: Gezinslid) => {
          return lid.id == this.gezinslidId;
        })[0];
      });
    }
    return this.gezinslid;
  }

  bepaalInitialen(lid: Gezinslid | undefined): string {
    let initialen: string = '';
    if (lid != undefined) {
      if (lid.voornaam != null) {
        initialen += lid.voornaam.substring(0, 1);
      }
      if (lid.achternaam != null) {
        initialen += lid.achternaam.substring(0, 1);
      }
    }
    return initialen;
  }
}

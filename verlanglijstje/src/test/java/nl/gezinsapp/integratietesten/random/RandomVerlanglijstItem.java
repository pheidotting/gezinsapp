package nl.gezinsapp.integratietesten.random;

import nl.gezinsapp.domain.VerlanglijstItem;
import org.openapi.quarkus.verlanglijstje_openapi_yaml.model.VerlanglijstjeItem;
import uk.co.jemos.podam.api.DefaultClassInfoStrategy;
import uk.co.jemos.podam.api.PodamFactory;
import uk.co.jemos.podam.api.PodamFactoryImpl;

public class RandomVerlanglijstItem {
    private static PodamFactory factory;

    private static PodamFactory getFactory() {
        if (factory == null) {
            factory = new PodamFactoryImpl();
            DefaultClassInfoStrategy classInfoStrategy = DefaultClassInfoStrategy.getInstance();
            classInfoStrategy.addExcludedField(VerlanglijstjeItem.class, "id");
            classInfoStrategy.addExcludedField(VerlanglijstjeItem.class, "gezinslid");
            classInfoStrategy.addExcludedField(VerlanglijstjeItem.class, "VerlanglijstItemItems");
            classInfoStrategy.addExcludedField(VerlanglijstItem.class, "id");
            classInfoStrategy.addExcludedField(VerlanglijstItem.class, "gezinslid");
            classInfoStrategy.addExcludedField(VerlanglijstItem.class, "VerlanglijstItemItems");
            factory.setClassStrategy(classInfoStrategy);
        }
        return factory;
    }

    public static VerlanglijstjeItem randomMsgVerlanglijstItem() {
        return getFactory().manufacturePojo(VerlanglijstjeItem.class);
    }

    public static VerlanglijstItem randomDomainVerlanglijstItem() {
        var lijst = getFactory().manufacturePojo(VerlanglijstItem.class);

        return lijst;
    }
}

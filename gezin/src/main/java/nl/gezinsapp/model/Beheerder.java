package nl.gezinsapp.model;

import jakarta.persistence.DiscriminatorValue;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@ToString
@DiscriminatorValue(value = "B")
public class Beheerder extends Gebruiker {
    @Override
    public String rol() {
        return this.getClass().getSimpleName();
    }
}

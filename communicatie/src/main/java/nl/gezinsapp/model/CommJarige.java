package nl.gezinsapp.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.HashMap;
import java.util.Map;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class CommJarige extends CommObject {
    private String voornaam;
    private String tussenvoegsel;
    private String achternaam;

    public CommJarige(String emailadres, Long gezinsId, Long gezinslidId, String voornaam, String tussenvoegsel, String achternaam) {
        super(emailadres, gezinsId, gezinslidId);
        this.voornaam = voornaam;
        this.tussenvoegsel = tussenvoegsel;
        this.achternaam = achternaam;
    }

    @Override
    public Map<String, Object> velden() {
        Map<String, Object> velden = new HashMap<>();
        velden.put("emailadres", emailadres);
        velden.put("voornaam", voornaam);
        velden.put("tussenvoegsel", tussenvoegsel);
        velden.put("achternaam", achternaam);
        return velden;
    }
}

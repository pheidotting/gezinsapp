package nl.gezinsapp.integratietest.random;

import org.openapi.quarkus.adresboek_openapi_yaml.model.Adres;
import org.openapi.quarkus.adresboek_openapi_yaml.model.AdresBoek;
import uk.co.jemos.podam.api.DefaultClassInfoStrategy;
import uk.co.jemos.podam.api.PodamFactory;
import uk.co.jemos.podam.api.PodamFactoryImpl;

public class RandomAdresBoek {
    private static PodamFactory factory;

    private static PodamFactory getFactory() {
        if (factory == null) {
            factory = new PodamFactoryImpl();
            DefaultClassInfoStrategy classInfoStrategy = DefaultClassInfoStrategy.getInstance();
            classInfoStrategy.addExcludedField(Adres.class, "id");
            classInfoStrategy.addExcludedField(AdresBoek.class, "id");
            classInfoStrategy.addExcludedField(nl.gezinsapp.model.AdresBoek.class, "id");
            classInfoStrategy.addExcludedField(nl.gezinsapp.model.Adres.class, "id");
            factory.setClassStrategy(classInfoStrategy);
        }
        return factory;
    }

    public static AdresBoek randomDtoAdresBoek() {
        return getFactory().manufacturePojo(AdresBoek.class);
    }

    public static Adres randomDtoAdres() {
        return getFactory().manufacturePojo(Adres.class);
    }

    public static nl.gezinsapp.model.AdresBoek randomAdresBoek() {
        return getFactory().manufacturePojo(nl.gezinsapp.model.AdresBoek.class);
    }

    public static nl.gezinsapp.model.Adres randomAdres() {
        var adres = getFactory().manufacturePojo(nl.gezinsapp.model.Adres.class);
        adres.setPostcode(adres.getPostcode().substring(0, 6));
        return adres;
    }

}

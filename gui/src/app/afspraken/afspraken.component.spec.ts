// import {ComponentFixture, TestBed} from '@angular/core/testing';
// import {CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA} from "@angular/core";
// import {AfspraakService, AfspraakSoortAfspraakEnum, AfspraakTerugkeerEnum, AlgemeneResponse, LijstAfspraken} from '../../api/kalender';
// import {AfsprakenComponent} from "./afspraken.component";
// import {Subject} from "rxjs";
// import {HttpEvent, HttpResponse} from "@angular/common/http";
// import {Calendar} from "@fullcalendar/core";
// import {FullCalendarComponent} from "@fullcalendar/angular";
// import {EventImpl} from "@fullcalendar/core/internal";
// import {AfsprakenService} from "../services/afspraken.service";
// import {GezinslidUtil} from "../services/gezinslid-util.service";
// import { GezinService } from '../../api/gezin/api/gezin.service';
// import { LeesGezin } from '../../api/gezin/model/leesGezin';
//
// describe('AfsprakenComponent', () => {
//   let component: AfsprakenComponent;
//   let fixture: ComponentFixture<AfsprakenComponent>;
//
//   let afspraakService: jasmine.SpyObj<AfspraakService>;
//   let calendarComponent: jasmine.SpyObj<FullCalendarComponent>;
//   let calendar: jasmine.SpyObj<Calendar>;
//   let afsprakenService: jasmine.SpyObj<AfsprakenService>;
//   let gezinService: jasmine.SpyObj<GezinService>;
//   let gezinslidUtil: jasmine.SpyObj<GezinslidUtil>;
//
//   beforeEach(async () => {
//     afspraakService = jasmine.createSpyObj('AfspraakService', ['alleAfspraken', 'nieuweAfspraak', 'opslaanAfspraak', 'verwijderAfspraak']);
//     calendarComponent = jasmine.createSpyObj('FullCalendarComponent', ['getApi', 'unselect']);
//     calendar = jasmine.createSpyObj('Calendar', ['getEvents', 'removeAllEvents', 'addEvent', 'unselect']);
//     afsprakenService = jasmine.createSpyObj('AfsprakenService', ['nieuweOfTeWijzigenAfspraak', 'teVerwijderenAfspraak', 'zetKlaarVoorOpslaan', 'checkAfspraak', 'setNieuweOfTeWijzigenAfspraak', 'nieuweAfspraak', 'mapAfspraakNaarEventInput', 'setNieuweOfTeWijzigenAfspraakVanDateSelectArg', 'verwerkAfspraak']);
//     gezinService = jasmine.createSpyObj('GezinService', ['leesHuidigGezin', 'teVerwijderenAfspraak', 'zetKlaarVoorOpslaan', 'checkAfspraak', 'setNieuweOfTeWijzigenAfspraak', 'nieuweAfspraak', 'mapAfspraakNaarEventInput', 'setNieuweOfTeWijzigenAfspraakVanDateSelectArg', 'verwerkAfspraak']);
//     gezinslidUtil = jasmine.createSpyObj('GezinslidUtil', ['maakNaamOp', 'teVerwijderenAfspraak', 'zetKlaarVoorOpslaan', 'checkAfspraak', 'setNieuweOfTeWijzigenAfspraak', 'nieuweAfspraak', 'mapAfspraakNaarEventInput', 'setNieuweOfTeWijzigenAfspraakVanDateSelectArg', 'verwerkAfspraak']);
//
//     await TestBed.configureTestingModule({
//       declarations: [AfsprakenComponent],
//       schemas: [NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA],
//       providers: [
//         {provide: AfspraakService, useValue: afspraakService},
//         {provide: AfsprakenService, useValue: afsprakenService},
//         {provide: GezinService, useValue: gezinService},
//         {provide: GezinslidUtil, useValue: gezinslidUtil},
//       ]
//     }).compileComponents();
//
//     // component = TestBed.inject(AfspraakService);
//
//     fixture = TestBed.createComponent(AfsprakenComponent);
//     component = fixture.componentInstance;
//
//     let lijstAfspraken: LijstAfspraken = {
//       afspraken: [{omschrijving: 'dummy', heledag: false}]
//     };
//     let event: HttpEvent<LijstAfspraken> = new HttpResponse<LijstAfspraken>({body: lijstAfspraken});
//
//     let lijstAfsprakenSubject: Subject<HttpEvent<LijstAfspraken>> = new Subject<HttpEvent<LijstAfspraken>>();
//
//     afspraakService.alleAfspraken.and.returnValue(lijstAfsprakenSubject);
//     lijstAfsprakenSubject.next(event)
//
//     fixture.detectChanges();
//   });
//
//   it('Test het opslaan van een afspraak, er zijn fouten aangetroffen', () => {
//     let event: HttpEvent<LeesGezin> = new HttpResponse<LeesGezin>({body: {naam:'',gezinsleden:[]}});
//     let huidigGezinSubject: Subject<HttpEvent<LeesGezin>> = new Subject<HttpEvent<LeesGezin>>();
//     gezinService.leesHuidigGezin.and.returnValue(huidigGezinSubject);
//     huidigGezinSubject.next(event)
//
//     afsprakenService.checkAfspraak.and.returnValue(['fout!']);
//
//     component.opslaanAfspraak();
//
//     expect(afsprakenService.zetKlaarVoorOpslaan).toHaveBeenCalled();
//     expect(afsprakenService.checkAfspraak).toHaveBeenCalled();
//   });
//
//   it('Test het opslaan van een nieuwe afspraak, er zijn geen fouten aangetroffen', () => {
//     component.calendarComponent = calendarComponent;
//     afsprakenService.checkAfspraak.and.returnValue([]);
//     calendarComponent.getApi.and.returnValue(calendar);
//
//     let response: AlgemeneResponse = {
//       foutmeldingen: {
//         meldingen: []
//       }, id: 46
//     };
//     let event: HttpEvent<AlgemeneResponse> = new HttpResponse<AlgemeneResponse>({body: response});
//     let responseSubject: Subject<HttpEvent<AlgemeneResponse>> = new Subject<HttpEvent<AlgemeneResponse>>();
//     afspraakService.nieuweAfspraak.and.returnValue(responseSubject);
//
//     afsprakenService.verwerkAfspraak.and.returnValue({});
//
//     component.opslaanAfspraak();
//     responseSubject.next(event);
//
//     expect(afsprakenService.zetKlaarVoorOpslaan).toHaveBeenCalled();
//     expect(afsprakenService.checkAfspraak).toHaveBeenCalled();
//     expect(calendarComponent.getApi).toHaveBeenCalled();
//     expect(afsprakenService.verwerkAfspraak).toHaveBeenCalled();
//     expect(calendar.addEvent).toHaveBeenCalled();
//     expect(component.afspraken.length).toEqual(1);
//     expect(component.modalTonen).toEqual('none');
//   });
//   it('Test het opslaan van een bestaande afspraak, er zijn geen fouten aangetroffen', () => {
//     component.calendarComponent = calendarComponent;
//     afsprakenService.nieuweOfTeWijzigenAfspraak.id = 3;
//     afsprakenService.checkAfspraak.and.returnValue([]);
//     calendarComponent.getApi.and.returnValue(calendar);
//
//     let response: AlgemeneResponse = {
//       foutmeldingen: {
//         meldingen: []
//       }, id: 46
//     };
//     let event: HttpEvent<AlgemeneResponse> = new HttpResponse<AlgemeneResponse>({body: response});
//     let responseSubject: Subject<HttpEvent<AlgemeneResponse>> = new Subject<HttpEvent<AlgemeneResponse>>();
//     afspraakService.opslaanAfspraak.and.returnValue(responseSubject);
//
//     afsprakenService.verwerkAfspraak.and.returnValue({});
//     let events: EventImpl[] = [];
//     let event1: any = {id: '1'};
//     let event2: any = {id: '2'};
//     events.push(event1);
//     events.push(event2);
//     calendar.getEvents.and.returnValue(events);
//
//     component.opslaanAfspraak();
//     responseSubject.next(event);
//
//     expect(afsprakenService.zetKlaarVoorOpslaan).toHaveBeenCalled();
//     expect(afsprakenService.checkAfspraak).toHaveBeenCalled();
//     expect(calendarComponent.getApi).toHaveBeenCalled();
//     expect(afsprakenService.verwerkAfspraak).toHaveBeenCalled();
//     expect(calendar.addEvent).toHaveBeenCalled();
//     expect(component.afspraken.length).toEqual(1);
//     expect(component.modalTonen).toEqual('none');
//     expect(calendar.getEvents).toHaveBeenCalled();
//   });
//   it('Test het verwijderen van een bestaande afspraak', () => {
//     component.calendarComponent = calendarComponent;
//     component.afsprakenService.nieuweOfTeWijzigenAfspraak.id = 5;
//
//     let response: AlgemeneResponse = {
//       foutmeldingen: {
//         meldingen: []
//       }
//     };
//     let event: HttpEvent<AlgemeneResponse> = new HttpResponse<AlgemeneResponse>({body: response});
//
//     let responseSubject: Subject<HttpEvent<AlgemeneResponse>> = new Subject<HttpEvent<AlgemeneResponse>>();
//
//     afspraakService.verwijderAfspraak.and.returnValue(responseSubject);
//
//     afspraakService.opslaanAfspraak.and.returnValue(responseSubject);
//     calendarComponent.getApi.and.returnValue(calendar);
//     let events: EventImpl[] = [];
//     let event1: any = {id: '1'};
//     let event2: any = {id: '2'};
//     events.push(event1);
//     events.push(event2);
//     calendar.getEvents.and.returnValue(events);
//
//     component.doeVerwijderAfspraak();
//     responseSubject.next(event);
//
//     expect(afspraakService.verwijderAfspraak).toHaveBeenCalled();
//     expect(component.modalTonen).toEqual('block');
//     expect(component.verwijderModalTonen).toEqual('none');
//   });
//   it('Test het open van de verwijder modal', () => {
//     expect(component.verwijderModalTonen = 'none');
//     component.afsprakenService.nieuweOfTeWijzigenAfspraak.vanDatum = '09:00';
//     component.afsprakenService.nieuweOfTeWijzigenAfspraak.omschrijving = 'bladiebla';
//
//     component.verwijderAfspraak();
//
//     expect(component.afsprakenService.teVerwijderenAfspraak.vanDatum = '09:00');
//     expect(component.afsprakenService.teVerwijderenAfspraak.omschrijving = 'bladiebla');
//     expect(component.verwijderModalTonen = 'block');
//   });
//   // it('Test verwijdertekst', () => {
//   //   component.afsprakenService.teVerwijderenAfspraak.omschrijving = 'abc';
//   //   component.afsprakenService.teVerwijderenAfspraak.vanTijd = '2024-07-23T08:00:00+02:00';
//   //   component.afsprakenService.teVerwijderenAfspraak.heledag = false;
//   //
//   //   expect(component.verwijderTekst()).toEqual('Weet je zeker dat je de afspraak van 23-07-2024 om 08:00 wilt verwijderen?');
//   // });
//   it('Test verwijdertekst, hele dag', () => {
//     component.afsprakenService.teVerwijderenAfspraak.vanDatum = '2024-07-31';
//     component.afsprakenService.teVerwijderenAfspraak.heledag = true;
//
//     expect(component.verwijderTekst()).toEqual('Weet je zeker dat je de afspraak van 31-07-2024 wilt verwijderen?');
//   });
//   it('handleDateSelect', () => {
//     let selectInfo: any = {
//       allDay: true,
//       startStr: '2024-07-04T14:30',
//       endStr: '2024-07-04T15:40',
//       title: 'Test',
//       display: 'HUWELIJK',
//       view: {
//         calendar: calendar
//       }
//     };
//
//     calendarComponent.getApi.and.returnValue(calendar);
//
//     component.handleDateSelect(selectInfo);
//
//     expect(calendar.unselect).toHaveBeenCalled();
//     expect(afsprakenService.setNieuweOfTeWijzigenAfspraakVanDateSelectArg).toHaveBeenCalled();
//     expect(component.modalTonen).toEqual('block');
//   });
//   it('soortAfspraakGewijzigd Huwelijk', () => {
//     expect(component.afsprakenService.nieuweOfTeWijzigenAfspraak.terugkeer).toBeUndefined();
//     component.afsprakenService.nieuweOfTeWijzigenAfspraak.soortAfspraak = AfspraakSoortAfspraakEnum.Huwelijk;
//
//     component.soortAfspraakGewijzigd();
//
//     expect(component.afsprakenService.nieuweOfTeWijzigenAfspraak.terugkeer).toEqual(AfspraakTerugkeerEnum.Jaarlijks);
//   });
//   it('soortAfspraakGewijzigd Verjaardag', () => {
//     expect(component.afsprakenService.nieuweOfTeWijzigenAfspraak.terugkeer).toBeUndefined();
//     component.afsprakenService.nieuweOfTeWijzigenAfspraak.soortAfspraak = AfspraakSoortAfspraakEnum.Verjaardag;
//
//     component.soortAfspraakGewijzigd();
//
//     expect(component.afsprakenService.nieuweOfTeWijzigenAfspraak.terugkeer).toEqual(AfspraakTerugkeerEnum.Jaarlijks);
//   });
//   it('soortAfspraakGewijzigd Overig', () => {
//     expect(component.afsprakenService.nieuweOfTeWijzigenAfspraak.terugkeer).toBeUndefined();
//     component.afsprakenService.nieuweOfTeWijzigenAfspraak.soortAfspraak = AfspraakSoortAfspraakEnum.Overig;
//
//     component.soortAfspraakGewijzigd();
//
//     expect(component.afsprakenService.nieuweOfTeWijzigenAfspraak.terugkeer).toBeUndefined();
//   });
//
// });

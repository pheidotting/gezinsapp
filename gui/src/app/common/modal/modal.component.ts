import {Component, EventEmitter, Input, Output} from '@angular/core';
import {NgStyle} from "@angular/common";

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  imports: [
    NgStyle
  ],
  styleUrl: './modal.component.css'
})
export class ModalComponent {
  @Input() tekst: string | undefined;
  @Input() tonen: boolean = false;

  @Output() jaActieEmitter: EventEmitter<void> = new EventEmitter();
  @Output() neeActieEmitter: EventEmitter<void> = new EventEmitter();

  neeActie(): void {
    this.neeActieEmitter.emit();
  }

  jaActie(): void {
    this.jaActieEmitter.emit();
  }
}

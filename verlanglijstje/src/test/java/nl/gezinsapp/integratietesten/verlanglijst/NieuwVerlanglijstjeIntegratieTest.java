package nl.gezinsapp.integratietesten.verlanglijst;

import com.fasterxml.jackson.core.JsonProcessingException;
import io.quarkus.test.junit.QuarkusTest;
import lombok.extern.slf4j.Slf4j;
import nl.gezinsapp.integratietesten.AbstractIntegratieTest;
import nl.gezinsapp.integratietesten.MockServer;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.openapi.quarkus.verlanglijstje_openapi_yaml.model.AlgemeneResponse;

import static nl.gezinsapp.integratietesten.random.RandomVerlanglijst.randomMsgVerlanglijst;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import io.quarkus.test.common.QuarkusTestResource;

@Slf4j
@QuarkusTest
@QuarkusTestResource(MockServer.class)
class NieuwVerlanglijstjeIntegratieTest extends AbstractIntegratieTest {
    @Test
    @DisplayName("Sla een nieuwe Verlanglijst op")
    void test() throws JsonProcessingException {
        maakDatabaseLeeg();

        var accesstoken = maakToken();
        var jwtResponse = mockCheckJwt(accesstoken);

        var item = randomMsgVerlanglijst();
        item.setGezinslid(jwtResponse.getGebruiker().getId());

        var response = putViaApi(AlgemeneResponse.class, "/api/verlanglijstje/verlanglijstjes", item, accesstoken);

        var itemGelezen = leesVerlanglijst(response.getId());
        assertNotNull(itemGelezen);
        assertEquals(item.getOmschrijving(), itemGelezen.getOmschrijving());
    }
}

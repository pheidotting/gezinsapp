package nl.gezinsapp.service;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

@ExtendWith(MockitoExtension.class)
class GezinslidVerwijderdServiceTest {
    @InjectMocks
    GezinslidVerwijderdService gezinslidVerwijderdService;

    @Mock
    AfspraakService afspraakService;
    @Mock
    SoortDienstService soortDienstService;

    @Test
    void verwerkVerwijderdgezinslid() {
        var id = 33L;

        gezinslidVerwijderdService.verwerkVerwijderdgezinslid(id);

        verify(afspraakService).verwijderAfsprakenBijGezinslid(id);
        verifyNoMoreInteractions(afspraakService);
        verify(soortDienstService).verwijderSoortDienstBijGezinslid(id);
        verifyNoMoreInteractions(soortDienstService);
    }
}
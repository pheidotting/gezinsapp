package nl.gezinsapp.mapper;

import org.mapstruct.Mapper;
import org.openapi.quarkus.communicatie_openapi_yaml.model.Email;

import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;

@Mapper(componentModel = "CDI")
public abstract class EmailMapper {
    public abstract Email map(nl.gezinsapp.domain.Email email);

    protected OffsetDateTime map(LocalDateTime value) {
        return value == null ? null : OffsetDateTime.of(value, ZoneOffset.ofHours(2));
    }
}

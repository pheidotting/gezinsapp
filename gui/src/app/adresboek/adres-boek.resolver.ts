import {ActivatedRouteSnapshot, Resolve} from '@angular/router';
import {AdresboekService} from '../../api/adresboek';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class AdresBoekResolver implements Resolve<any> {
  constructor(
    private adresboekService: AdresboekService
  ) {
  }

  resolve(route: ActivatedRouteSnapshot): Observable<any> {
    const id = +route.url[0].path;
    return this.adresboekService.leesAdresBoek(id);
  }
}

package nl.gezinsapp.model.repository;

import io.quarkus.redis.datasource.RedisDataSource;
import io.quarkus.redis.datasource.value.ValueCommands;
import jakarta.enterprise.context.ApplicationScoped;
import nl.gezinsapp.model.WijzigWachtwoordCode;

import java.util.Optional;
import java.util.UUID;

import static java.util.Optional.ofNullable;

@ApplicationScoped
public class WijzigWachtwoordCodeRepository {
    private ValueCommands<UUID, String> valueCommands;

    public WijzigWachtwoordCodeRepository(RedisDataSource ds) {
        valueCommands = ds.value(UUID.class, String.class);
    }

    public WijzigWachtwoordCode set(String code) {
        var activatieCode = WijzigWachtwoordCode.builder().code(code).id(UUID.randomUUID()).build();
        valueCommands.setex(activatieCode.getId(), 600, activatieCode.getCode());
        return activatieCode;
    }

    public Optional<String> get(UUID key) {
        return ofNullable(valueCommands.get(key));
    }
}

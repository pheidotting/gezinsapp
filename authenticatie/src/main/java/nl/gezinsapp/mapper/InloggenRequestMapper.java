package nl.gezinsapp.mapper;

import nl.gezinsapp.model.InloggenRequest;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "CDI")
public interface InloggenRequestMapper {
    InloggenRequestMapper INSTANCE = Mappers.getMapper(InloggenRequestMapper.class);

    org.openapi.quarkus.authenticatie_openapi_yaml.model.InloggenRequest map(InloggenRequest inloggenRequest);

    InloggenRequest map(org.openapi.quarkus.authenticatie_openapi_yaml.model.InloggenRequest inloggenRequest);
}

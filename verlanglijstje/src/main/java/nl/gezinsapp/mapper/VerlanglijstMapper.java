package nl.gezinsapp.mapper;

import nl.gezinsapp.domain.Verlanglijst;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.openapi.quarkus.verlanglijstje_openapi_yaml.model.Verlanglijstje;
import org.openapi.quarkus.verlanglijstje_openapi_yaml.model.Verlanglijstje1;

import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;

@Mapper(componentModel = "CDI")
public abstract class VerlanglijstMapper {
    public abstract Verlanglijst map(org.openapi.quarkus.verlanglijstje_openapi_yaml.model.Verlanglijstje verlanglijstje);

    public abstract Verlanglijst update(org.openapi.quarkus.verlanglijstje_openapi_yaml.model.Verlanglijstje verlanglijstje, @MappingTarget Verlanglijst verlanglijst);

    public abstract Verlanglijstje1 map(Verlanglijst verlanglijst);
    public abstract Verlanglijstje maps(Verlanglijst verlanglijst);

    protected LocalDateTime map(OffsetDateTime value) {
        return value == null ? null : value.toLocalDateTime();
    }

    protected OffsetDateTime map(LocalDateTime value) {
        return value == null ? null : OffsetDateTime.of(value, ZoneOffset.ofHours(2));
    }
}

import {ActivatedRouteSnapshot, ResolveFn, RouterStateSnapshot} from '@angular/router';
import {inject} from '@angular/core';
import {GezinService} from '../../../api/gezin/api/gezin.service';
import {Gezinnen} from '../../../api/gezin';

export const alleGezinnenResolver: ResolveFn<Gezinnen> = (route: ActivatedRouteSnapshot, state: RouterStateSnapshot) => {
  const gezinService = inject(GezinService);

  return gezinService.leesGezinnen();
}

package nl.gezinsapp.integratietesten;

import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;
import jakarta.transaction.Transactional;
import lombok.extern.slf4j.Slf4j;
import nl.gezinsapp.model.Gezin;
import nl.gezinsapp.model.Gezinslid;
import org.junit.jupiter.api.Test;
import org.openapi.quarkus.gezin_openapi_yaml.model.Gezinnen;
import uk.co.jemos.podam.api.PodamFactoryImpl;

import java.util.HashSet;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static org.awaitility.Awaitility.await;
import static org.junit.jupiter.api.Assertions.assertEquals;

@Slf4j
@QuarkusTest
@QuarkusTestResource(MockServer.class)
class OphalenGezinIntegratieTest extends AbstractIntegratieTest {

    private List<Gezin> gezinnen;

    @Test
    void getGezin() {
        maakDatabaseLeeg();

        var aantalNu = aantalBedrijvenInDb();

        var opgeslagenGezin = opslaanGezin();
        var papa = opslaanGezinslid(opgeslagenGezin);
        System.out.println(papa);
        System.out.println(papa.getGezin());
        var mama = opslaanGezinslid(opgeslagenGezin);

        assertEquals(1, alleGezinnen().size());
        assertEquals(2, alleGezinsleden().size());

        await()
                .pollDelay(500, TimeUnit.MILLISECONDS)
                .pollInterval(500, TimeUnit.MILLISECONDS)
                .atMost(1, TimeUnit.MINUTES)
                .until(() -> leesApi().getGezinnen().size() > aantalNu);

        var opgehaaldeGezinnen = leesApi().getGezinnen();
        assertEquals(1, opgehaaldeGezinnen.size());
        var opgehaaldGezin = opgehaaldeGezinnen.get(0);
        assertEquals(opgeslagenGezin.getNaam(), opgehaaldGezin.getNaam());
        System.out.println(opgehaaldGezin);

        assertEquals(2, opgehaaldGezin.getGezinsleden().size());
        var opehaaldePapa = opgehaaldGezin.getGezinsleden().stream().filter(gezinslid -> gezinslid.getAchternaam().equals(papa.getAchternaam())).findFirst().get();
        assertEquals(papa.getAchternaam(), opehaaldePapa.getAchternaam());
        assertEquals(papa.getVoornaam(), opehaaldePapa.getVoornaam());
        var opehaaldeMama = opgehaaldGezin.getGezinsleden().stream().filter(gezinslid -> gezinslid.getAchternaam().equals(mama.getAchternaam())).findFirst().get();
        assertEquals(mama.getAchternaam(), opehaaldeMama.getAchternaam());
        assertEquals(mama.getVoornaam(), opehaaldeMama.getVoornaam());

        schoonGezinTabel();
    }

    @Transactional
    public long aantalBedrijvenInDb() {
        this.gezinnen = gezinRepository.findAll().stream().toList();
        return this.gezinnen.size();
    }

    @Transactional
    void schoonGezinTabel() {
        gezinslidRepository.deleteAll();
        gezinRepository.deleteAll();
    }

    @Transactional
    public Gezin opslaanGezin() {
        var gezin = new PodamFactoryImpl().manufacturePojo(Gezin.class);
        gezin.setId(null);
        gezin.setGezinsleden(new HashSet<>());

        gezinRepository.persist(gezin);

        return gezin;
    }

    @Transactional
    public Gezinslid opslaanGezinslid(Gezin gezin) {
        var gezinslid = new PodamFactoryImpl().manufacturePojo(Gezinslid.class);
        gezinslid.setId(null);
        gezin.getGezinsleden().add(gezinslid);
        gezinslid.setGezin(gezin);

        gezinslidRepository.persist(gezinslid);

        return gezinslid;
    }

    private Gezinnen leesApi() {
        try {
            return leesViaApi(Gezinnen.class, "/api/gezin", null, null);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
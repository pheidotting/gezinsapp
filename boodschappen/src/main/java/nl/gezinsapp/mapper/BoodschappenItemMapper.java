package nl.gezinsapp.mapper;

import nl.gezinsapp.model.BoodschappenItem;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;

@Mapper(componentModel = "CDI")
public abstract class BoodschappenItemMapper {
    public abstract BoodschappenItem map(org.openapi.quarkus.boodschappen_openapi_yaml.model.BoodschappenItem item);

    public abstract BoodschappenItem update(@MappingTarget BoodschappenItem itemAanwezig, org.openapi.quarkus.boodschappen_openapi_yaml.model.BoodschappenItem item);

    public abstract org.openapi.quarkus.boodschappen_openapi_yaml.model.BoodschappenItem map(BoodschappenItem item);
}

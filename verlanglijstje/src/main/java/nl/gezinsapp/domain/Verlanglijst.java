package nl.gezinsapp.domain;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;
import jakarta.persistence.*;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "VERLANGLIJST")
@Getter
@Setter
@ToString(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
public class Verlanglijst extends PanacheEntityBase {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;
    @Column(name = "GEZINSLID")
    private Long gezinslid;
    @Column(name = "OMSCHRIJVING")
    private String omschrijving;
    @Column(name = "CODE")
    private String code;
    @OneToMany(mappedBy = "verlanglijst")
    private Set<VerlanglijstItem> verlanglijstItems = new HashSet<>();
}

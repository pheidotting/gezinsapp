import {ComponentFixture, fakeAsync, TestBed} from '@angular/core/testing';

import {TakenWidgetComponent} from './taken-widget.component';
import {ActivatedRoute, provideRouter, RouterModule} from "@angular/router";
import {provideHttpClient, withInterceptorsFromDi} from "@angular/common/http";
import {provideHttpClientTesting} from "@angular/common/http/testing";
import {of} from "rxjs";

describe('TakenWidgetComponent', () => {
  let component: TakenWidgetComponent;
  let fixture: ComponentFixture<TakenWidgetComponent>;
  let activatedRoute: ActivatedRoute;
  let app: TakenWidgetComponent;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [TakenWidgetComponent,
        RouterModule.forRoot([])],
      providers: [provideRouter([]), provideHttpClient(withInterceptorsFromDi()), provideHttpClientTesting()]
    });

    fixture = TestBed.createComponent(TakenWidgetComponent);
    activatedRoute = fixture.debugElement.injector.get(ActivatedRoute);

    activatedRoute.data = of({
      takenlijst: {
        taken: [
          {
            omschrijving: "Afwassen",
            gezinslid: 2
          }
        ]
      }
    });

    fixture.detectChanges();

    app = fixture.debugElement.componentInstance;
  });
  it('should create', fakeAsync(() => {
    expect(app).toBeTruthy();
  }));

  it('Test ophalen gegevens', fakeAsync(() => {
    expect(app.taken.length).toEqual(1);
    expect(app.taken[0].omschrijving).toEqual('Afwassen');
  }));
});

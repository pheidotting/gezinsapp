package nl.gezinsapp.commons.aspect;

import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.annotation.Priority;
import jakarta.interceptor.AroundInvoke;
import jakarta.interceptor.Interceptor;
import jakarta.interceptor.InvocationContext;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.slf4j.MDC;

import java.time.Duration;
import java.util.Arrays;

import static java.time.LocalDateTime.now;

@Slf4j
@MethodeLogging
@Priority(2019)
@Interceptor
@RequiredArgsConstructor
public class MethodeLoggingAspect {
    @ConfigProperty(name = "quarkus.application.name")
    private String applicatieNaam;
    @ConfigProperty(name = "quarkus.application.omgeving")
    private String omgeving;

    @AroundInvoke
    Object logInvocation(InvocationContext context) throws Exception {
        MDC.put("applicatie-naam", applicatieNaam);
        MDC.put("omgeving", omgeving);

        Object obj;
        var nu = now();

        var classNaam = bepaalClassNaam(context);
        var method = context.getMethod();

        MDC.put("methodname", method.getName());
        String params = "";
        if (context.getParameters() != null) {
            MDC.put("parameters", new ObjectMapper().findAndRegisterModules().writeValueAsString(Arrays.stream(context.getParameters())
                    .filter(o -> o != null)
                    .map(Object::toString)
                    .toList()));
            params = ", params : " + Arrays.stream(context.getParameters())
                    .filter(o -> o != null)
                    .map(Object::toString)
                    .toList().toString();
        }

        if (classNaam.endsWith("Controller") || classNaam.endsWith("Reciever")) {
            log.info("{}.{} - {}", classNaam, method.getName(), params);
        } else {
            log.info("{}.{} - {}", classNaam, method.getName(), params);
        }

        obj = context.proceed();

        if (classNaam.endsWith("Controller") || classNaam.endsWith("Reciever")) {
            var duration = Duration.between(nu, now());
            MDC.put("duration", duration.toMillis() + "");
            log.info("Einde, {} duration : {}", method.getName(), duration);
        }

        return obj;
    }

    private String bepaalClassNaam(InvocationContext context) {
        var classNaam = context.getTarget().getClass().getSimpleName();
        if (classNaam.contains("$$")) {
            classNaam = classNaam.substring(0, classNaam.indexOf("$$"));
        }
        return classNaam.replace("_Subclass", "");
    }
}

package nl.gezinsapp.mapper;

import nl.gezinsapp.model.Adres;
import nl.gezinsapp.model.AdresBoek;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;

@Mapper(componentModel = "CDI")
public abstract class AdresMapper {
    public abstract Adres map(org.openapi.quarkus.adresboek_openapi_yaml.model.Adres adres);

    public abstract Adres update(org.openapi.quarkus.adresboek_openapi_yaml.model.Adres adres, @MappingTarget Adres bestaand);

    AdresBoek map(Long value) {
        return null;
    }
}

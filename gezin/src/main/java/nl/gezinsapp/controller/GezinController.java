package nl.gezinsapp.controller;

import io.smallrye.common.annotation.RunOnVirtualThread;
import jakarta.persistence.EntityNotFoundException;
import jakarta.ws.rs.Path;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.gezinsapp.ActivatieCodeNietMeegegevenException;
import nl.gezinsapp.ActivatieCodeOnjuistException;
import nl.gezinsapp.aspect.ThreadVariabelen;
import nl.gezinsapp.commons.aspect.AlsGezinslid;
import nl.gezinsapp.commons.aspect.MethodeLogging;
import nl.gezinsapp.commons.aspect.TrackAndTraceId;
import nl.gezinsapp.mapper.GezinMapper;
import nl.gezinsapp.model.Gezin;
import nl.gezinsapp.service.GezinService;
import org.openapi.quarkus.gezin_openapi_yaml.api.GezinApi;
import org.openapi.quarkus.gezin_openapi_yaml.model.AanmeldenGezin;
import org.openapi.quarkus.gezin_openapi_yaml.model.AlgemeneResponse;
import org.openapi.quarkus.gezin_openapi_yaml.model.Foutmeldingen;
import org.openapi.quarkus.gezin_openapi_yaml.model.Gezinnen;
import org.openapi.quarkus.gezin_openapi_yaml.model.LeesGezin;
import org.openapi.quarkus.gezin_openapi_yaml.model.OpslaanGezin;

@Slf4j
@Path("/api/gezin")
@RequiredArgsConstructor
public class GezinController implements GezinApi {
    private final GezinService gezinService;
    private final GezinMapper gezinMapper;

    @Override
    @MethodeLogging
//    @AlsBeheerder
    @TrackAndTraceId
    public LeesGezin leesGezin(Long id) {
        return gezinMapper.mapLeesGezin(gezinService.findById(id).orElseThrow(() -> new EntityNotFoundException("Gezin met id " + id + " niet gevonden.")));
    }

    @Override
    @MethodeLogging
    @TrackAndTraceId
    public Gezinnen leesGezinnen() {
        return new Gezinnen().gezinnen(gezinService.findAll()
                .stream()
                .map(gezinMapper::map)
                .toList());
    }

    @Override
    @RunOnVirtualThread
    @TrackAndTraceId
    @AlsGezinslid
    @MethodeLogging
    public LeesGezin leesHuidigGezin() {
        return gezinMapper.mapLeesGezin(gezinService.findById(ThreadVariabelen.get().getGezin()).get());
    }

    @Override
    @MethodeLogging
    @TrackAndTraceId
    public AlgemeneResponse aanmeldenGezin(AanmeldenGezin aanmeldenGezin) {
        try {
            return new AlgemeneResponse().id(gezinService.aanmeldenGezin(gezinMapper.map(aanmeldenGezin), aanmeldenGezin.getActiveringsCode()).getId());
        } catch (ActivatieCodeOnjuistException e) {
            return new AlgemeneResponse().foutmeldingen(new Foutmeldingen().addMeldingenItem("Activatiecode niet juist"));
        } catch (ActivatieCodeNietMeegegevenException e) {
            return new AlgemeneResponse().foutmeldingen(new Foutmeldingen().addMeldingenItem("Activatiecode niet ingevuld"));
        }
    }

    @Override
    @RunOnVirtualThread
    @AlsGezinslid
    @MethodeLogging
    @TrackAndTraceId
    public AlgemeneResponse opslaanGezin(OpslaanGezin opslaanGezin) {
        log.info("Opslaan Gezin: {}", opslaanGezin);
        Gezin.findByIdOptional(opslaanGezin.getId())
                .ifPresent(bestaand -> Gezin.persist(gezinMapper.map(opslaanGezin, (Gezin) bestaand)));

        return new AlgemeneResponse();
    }
}

import {TestBed} from '@angular/core/testing';

import {ActivatieMailsComponent} from './activatie-mails.component';
import {ActivatedRoute, provideRouter, RouterModule} from "@angular/router";
import {provideHttpClient, withInterceptorsFromDi} from "@angular/common/http";
import {provideHttpClientTesting} from "@angular/common/http/testing";
import {of} from "rxjs";

describe('ActivatieMailsComponent', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ActivatieMailsComponent,
        RouterModule.forRoot([])],
      providers: [provideRouter([]), provideHttpClient(withInterceptorsFromDi()), provideHttpClientTesting()]
    });
  });

  it('should create and load data', () => {
    let fixture = TestBed.createComponent(ActivatieMailsComponent);

    let activatedRoute: ActivatedRoute = fixture.debugElement.injector.get(ActivatedRoute);

    activatedRoute.data = of({
      emails: {
        emails: [
          {
            emailadres: 'A'
          },
          {
            emailadres: 'B'
          }
        ]
      }
    });

    fixture.detectChanges();

    let app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
    expect(app.emails.length).toBe(2);
  });
});

package nl.gezinsapp.service;

import io.quarkus.qute.Template;
import jakarta.enterprise.context.ApplicationScoped;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.gezinsapp.model.CommWijzigWachtwoordCodeAangemaakt;
import org.openapi.quarkus.gezin_openapi_yaml.model.LeesGezin;

@Slf4j
@ApplicationScoped
@RequiredArgsConstructor
public class WijzigWachtwoordCodeAangemaaktService extends AbstractService<CommWijzigWachtwoordCodeAangemaakt> {
    private final Template wijzigWachtwoordCodeAangemaakt;

    @Override
    public void vulCommObject(CommWijzigWachtwoordCodeAangemaakt commWijzigWachtwoordCodeAangemaakt, LeesGezin gezin) {

    }

    @Override
    public Template getTemplate() {
        return wijzigWachtwoordCodeAangemaakt;
    }

    @Override
    public String getOnderwerp() {
        return "Code voor het wijzigen van je wachtwoord";
    }
}

import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {Email} from '../../../api/communicatie';
import {MaakDatumOpPipe} from "../../pipes/maak-datum-op.pipe";
import {MaakTijdOpPipe} from "../../pipes/maak-tijd-op.pipe";

@Component({
  selector: 'app-activatie-mails',
  imports: [
    MaakDatumOpPipe,
    MaakTijdOpPipe
  ],
  templateUrl: './activatie-mails.component.html'
})
export class ActivatieMailsComponent implements OnInit {
  emails: Email[] = [];

  constructor(
    private readonly activatedRoute: ActivatedRoute
  ) {
  }

  ngOnInit(): void {
    this.activatedRoute.data.subscribe((response: any) => {
      this.emails = response.emails.emails;
    });
  }

}

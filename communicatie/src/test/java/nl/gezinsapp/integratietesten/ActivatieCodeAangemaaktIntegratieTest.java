package nl.gezinsapp.integratietesten;

import io.quarkus.test.junit.QuarkusTest;
import lombok.extern.slf4j.Slf4j;
import nl.gezinsapp.integratietesten.common.QueueUtil;
import nl.gezinsapp.integratietesten.common.TestGegevens;
import nl.gezinsapp.messaging.model.ActivatieCodeAangemaakt;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.time.Duration;
import java.util.concurrent.TimeUnit;
import io.quarkus.test.common.QuarkusTestResource;

import static io.restassured.RestAssured.given;
import static org.awaitility.Awaitility.await;
import static org.junit.jupiter.api.Assertions.*;

@Slf4j
@QuarkusTest
@QuarkusTestResource(MockServer.class)
public class ActivatieCodeAangemaaktIntegratieTest extends AbstractIntegratieTest {
    @ConfigProperty(name = "mp.messaging.incoming.activatiecode-aangemaakt.exchange.name")
    private String exchange;
    @ConfigProperty(name = "rabbitmq-host")
    private String rabbitHost;
    @ConfigProperty(name = "rabbitmq-port")
    private String rabbitPort;
    @ConfigProperty(name = "rabbitmq-username")
    private String rabbitUsername;
    @ConfigProperty(name = "rabbitmq-password")
    private String rabbitPassword;

    @Test
    @DisplayName("Test een correcte aanmelding van het gezin")
    void test() {
        maakDatabaseLeeg();
        given()         .when().delete("http://localhost:" + mailpitContainer.getMappedPort(8025) + "/api/v1/messages");

        TestGegevens.setRabbitHost(rabbitHost);
        TestGegevens.setRabbitPort(Integer.valueOf(rabbitPort));
        TestGegevens.setRabbitUser(rabbitUsername);
        TestGegevens.setRabbitPassword(rabbitPassword);

        var queueUtil = mmaakActivatieCodeAangemaaktQueueUtil();

        var bericht = new ActivatieCodeAangemaakt();
        bericht.setEmailadres("a@b.c");
        bericht.setCode("123456");
        bericht.setFamilienaam("ActivatieCodeAangemaakt : The Simpsons");

        queueUtil.stuurBerichtNaarExchange(bericht);

        await()
                .atMost(40, TimeUnit.SECONDS)
                .pollInterval(Duration.ofMillis(500))
                .until(() -> {
                    var email = leesEmail();
                    assertEquals("Activatiecode", email.getOnderwerp());
                    assertTrue(email.getTekst().contains("<h3>Hoi ActivatieCodeAangemaakt : The Simpsons, leuk dat je Gezinsapp een kans wilt geven.</h3>\n" +
                            "<p>Om je te kunnen aanmelden heb je een activatiecode nodig, deze kun je hieronder vinden:</p>\n" +
                            "<h3>123456</h3>\n" +
                            "<p>Vul deze in om je aanmeldingsproces te voltooien, tot snel!</p>\n" +
                            "<p>Met vriendelijke groet,</p>\n" +
                            "<p>Team Gezinsapp</p>"));
                    assertNotNull(email.getTijdstipaangemaakt());
                    assertNull(email.getTijdstipverstuurd());
                    return true;
                });

    given().when().get("/verstuurmails");

        var response = given()
                .when().get("http://localhost:" + mailpitContainer.getMappedPort(8025) + "/api/v1/messages");
        assertTrue(response.getBody().prettyPrint().contains("\"messages_count\": 1,"));
        assertTrue(response.getBody().prettyPrint().contains("\"Address\": \"gezinsapp@heidotting.nl\""));
        assertTrue(response.getBody().prettyPrint().contains("\"Address\": \"a@b.c\""));
        assertTrue(response.getBody().prettyPrint().contains("\"Subject\": \"Activatiecode\","));
        var email = leesEmail();
        assertNotNull(email.getTijdstipaangemaakt());
        assertNotNull(email.getTijdstipverstuurd());
    }

    private QueueUtil<ActivatieCodeAangemaakt> mmaakActivatieCodeAangemaaktQueueUtil() {
        return new QueueUtil<>() {
            @Override
            public String getExchange() {
                return exchange;
            }

            @Override
            public Class<ActivatieCodeAangemaakt> getClazz() {
                return ActivatieCodeAangemaakt.class;
            }
        };
    }

}

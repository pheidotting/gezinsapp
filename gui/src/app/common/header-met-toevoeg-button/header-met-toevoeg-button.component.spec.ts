import {ComponentFixture, TestBed} from '@angular/core/testing';

import {HeaderMetToevoegButtonComponent} from './header-met-toevoeg-button.component';

describe('HeaderMetToevoegButtonComponent', () => {
  let component: HeaderMetToevoegButtonComponent;
  let fixture: ComponentFixture<HeaderMetToevoegButtonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [HeaderMetToevoegButtonComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(HeaderMetToevoegButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

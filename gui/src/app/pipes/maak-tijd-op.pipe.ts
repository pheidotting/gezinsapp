import {Pipe, PipeTransform} from '@angular/core';
import moment from "moment";

@Pipe({
  name: 'maakTijdOp',
  standalone: true
})
export class MaakTijdOpPipe implements PipeTransform {

  transform(datum: string | undefined): string {
    if (datum == undefined) {
      return '';
    }
    return moment(datum).format('HH:mm');
  }

}

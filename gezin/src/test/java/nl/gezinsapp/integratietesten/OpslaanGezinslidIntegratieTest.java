package nl.gezinsapp.integratietesten;

import com.fasterxml.jackson.core.JsonProcessingException;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.openapi.quarkus.gezin_openapi_yaml.model.AlgemeneResponse;
import org.openapi.quarkus.gezin_openapi_yaml.model.GezinslidZonderId;

import java.util.UUID;

import static nl.gezinsapp.integratietesten.common.random.RandomGezin.randomDomainGezin;
import static nl.gezinsapp.integratietesten.common.random.RandomGezinslid.randomDomainGezinslid;
import static nl.gezinsapp.integratietesten.common.random.RandomGezinslid.randomMsgGezinslid;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@QuarkusTest
@QuarkusTestResource(MockServer.class)
class OpslaanGezinslidIntegratieTest extends AbstractIntegratieTest {
    @Test
    @DisplayName("Test het opslaan van een nieuw Gezinslid")
    void testNieuw() throws JsonProcessingException {
        maakDatabaseLeeg();

        var gezin = randomDomainGezin();
        opslaanGezin(gezin);

        var gezinslid = randomMsgGezinslid();
        gezinslid.setGezin(gezin.getId());

        var accesstoken = maakToken();
        var trackAndTraceId = UUID.randomUUID().toString();
        var jwtResponse = mockCheckJwt(accesstoken,trackAndTraceId);

        var result = putViaApi(AlgemeneResponse.class, "/api/gezin/gezinslid/opslaan", gezinslid, accesstoken,trackAndTraceId);

        assertEquals(1, gezinslidRepository.findAll().list().size());
        assertTrue(result.getFoutmeldingen().getMeldingen().isEmpty());

    }

    @Test
    @DisplayName("Test het opslaan van een nieuw Gezinslid zonder de verplichte velden")
    void testNieuwZonderVerplichteVelden() throws JsonProcessingException {
        maakDatabaseLeeg();

        var gezin = randomDomainGezin();
        opslaanGezin(gezin);

        var gezinslid = new GezinslidZonderId();
        gezinslid.setGezin(gezin.getId());

        var accesstoken = maakToken();
        var trackAndTraceId = UUID.randomUUID().toString();
        var jwtResponse = mockCheckJwt(accesstoken,trackAndTraceId);

        var result = putViaApi(AlgemeneResponse.class, "/api/gezin/gezinslid/opslaan", gezinslid, accesstoken,trackAndTraceId);

        assertTrue(gezinslidRepository.findAll().list().isEmpty());
        assertEquals(1, result.getFoutmeldingen().getMeldingen().size());
        assertEquals("Achternaam mag niet leeg zijn.", result.getFoutmeldingen().getMeldingen().get(0));

        gezinslid.setAchternaam("");

        assertTrue(gezinslidRepository.findAll().list().isEmpty());
        assertEquals(1, result.getFoutmeldingen().getMeldingen().size());
        assertEquals("Achternaam mag niet leeg zijn.", result.getFoutmeldingen().getMeldingen().get(0));

        gezinslid.setAchternaam(UUID.randomUUID().toString());

        result = putViaApi(AlgemeneResponse.class, "/api/gezin/gezinslid/opslaan", gezinslid, accesstoken,trackAndTraceId);

        assertTrue(gezinslidRepository.findAll().list().isEmpty());
        assertEquals(1, result.getFoutmeldingen().getMeldingen().size());
        assertEquals("Voornaam mag niet leeg zijn.", result.getFoutmeldingen().getMeldingen().get(0));

        gezinslid.setVoornaam(UUID.randomUUID().toString());

        result = putViaApi(AlgemeneResponse.class, "/api/gezin/gezinslid/opslaan", gezinslid, accesstoken,trackAndTraceId);

        assertTrue(gezinslidRepository.findAll().list().isEmpty());
        assertEquals(1, result.getFoutmeldingen().getMeldingen().size());
        assertEquals("Geslacht mag niet leeg zijn.", result.getFoutmeldingen().getMeldingen().get(0));

        gezinslid.setGeslacht(GezinslidZonderId.GeslachtEnum.MAN);

        gezinslid.setSoortGezinslid(GezinslidZonderId.SoortGezinslidEnum.OUDER);

        result = putViaApi(AlgemeneResponse.class, "/api/gezin/gezinslid/opslaan", gezinslid, accesstoken,trackAndTraceId);

        assertEquals(1, gezinslidRepository.findAll().list().size());
        assertTrue(result.getFoutmeldingen().getMeldingen().isEmpty());
    }

    @Test
    @DisplayName("Test het opslaan van een bestaand Gezinslid")
    void testBestaand() throws JsonProcessingException {
        maakDatabaseLeeg();

        var gezin = randomDomainGezin();
        opslaanGezin(gezin);

        var gezinslid = randomDomainGezinslid();
        gezinslid.setGezin(gezin);
        opslaanGezinslid(gezinslid);

        assertEquals(1, gezinslidRepository.findAll().list().size());

        var gezinslidNw = randomMsgGezinslid();
        gezinslidNw.setId(gezinslid.getId());
        gezinslidNw.setGezin(gezin.getId());

        var accesstoken = maakToken();
        var trackAndTraceId = UUID.randomUUID().toString();
        var jwtResponse = mockCheckJwt(accesstoken,trackAndTraceId);

        var result = postViaApi(AlgemeneResponse.class, "/api/gezin/gezinslid/opslaan", gezinslidNw, accesstoken,trackAndTraceId);

        assertEquals(1, gezinslidRepository.findAll().list().size());
        assertTrue(result.getFoutmeldingen().getMeldingen().isEmpty());
    }
}

package nl.gezinsapp.integratietesten.afspraak;

import com.fasterxml.jackson.core.JsonProcessingException;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;
import lombok.extern.slf4j.Slf4j;
import nl.gezinsapp.integratietesten.AbstractIntegratieTest;
import nl.gezinsapp.integratietesten.MockServer;
import nl.gezinsapp.model.Afspraak;
import nl.gezinsapp.model.enums.Terugkeer;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockserver.model.JsonBody;
import org.openapi.quarkus.authenticatie_openapi_yaml.model.JwtResponse;
import org.openapi.quarkus.gezin_openapi_yaml.model.Gezin;
import org.openapi.quarkus.kalender_openapi_yaml.model.LijstAfspraken;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import static nl.gezinsapp.integratietesten.common.random.SoortDienstDatum.randomSoortDienst;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockserver.model.HttpRequest.request;
import static org.mockserver.model.HttpResponse.response;

@Slf4j
@QuarkusTest
@QuarkusTestResource(MockServer.class)
class LeesAfsprakenIntegratieTest extends AbstractIntegratieTest {
    @Test
    @DisplayName("Haal alle afspraken op")
    void test() throws JsonProcessingException {
        maakDatabaseLeeg();

        var accesstoken = maakToken();
        var jwtResponse = mockCheckJwt(accesstoken);

        var soortDienst = randomSoortDienst();
        opslaan(soortDienst);

        var afspraak1 = new Afspraak(null, "afsp1", true, null, null, LocalDate.of(2024, 1, 5), LocalDate.of(2024, 1, 7), null, Terugkeer.NIET, jwtResponse.getGebruiker().getGezin(), soortDienst, List.of(1L, 3L));
        var afspraak2 = new Afspraak(null, "afsp2", true, null, null, LocalDate.of(2024, 1, 9), LocalDate.of(2024, 1, 9), null, Terugkeer.NIET, jwtResponse.getGebruiker().getGezin(), soortDienst, null);
        var afspraak3 = new Afspraak(null, "afsp3", true, null, null, LocalDate.of(2024, 1, 11), LocalDate.of(2024, 1, 11), null, Terugkeer.JAARLIJKS, jwtResponse.getGebruiker().getGezin(), soortDienst, null);
        var afspraak4 = new Afspraak(null, "afsp4", true, null, null, LocalDate.of(2024, 1, 12), LocalDate.of(2024, 1, 13), null, Terugkeer.JAARLIJKS, jwtResponse.getGebruiker().getGezin(), soortDienst, null);
        var afspraak8 = new Afspraak(null, "afsp8", true, null, null, LocalDate.of(2023, 12, 12), LocalDate.of(2022, 12, 13), null, Terugkeer.MAANDELIJKS, jwtResponse.getGebruiker().getGezin(), soortDienst, null);
        var afspraak5 = new Afspraak(null, "afsp5", false, LocalDateTime.of(2024, 1, 14, 8, 0), LocalDateTime.of(2024, 1, 14, 12, 0), null, null, null, Terugkeer.NIET, jwtResponse.getGebruiker().getGezin(), soortDienst, null);
        var afspraak6 = new Afspraak(null, "afsp6", false, LocalDateTime.of(2024, 1, 15, 8, 0), LocalDateTime.of(2024, 1, 16, 12, 0), null, null, null, Terugkeer.NIET, jwtResponse.getGebruiker().getGezin(), soortDienst, null);
        var afspraak7 = new Afspraak(null, "afsp7", false, LocalDateTime.of(2024, 1, 17, 8, 0), LocalDateTime.of(2024, 1, 17, 12, 0), null, null, null, Terugkeer.JAARLIJKS, jwtResponse.getGebruiker().getGezin(), soortDienst, null);
        var afspraak9 = new Afspraak(null, "afsp9", false, LocalDateTime.of(2023, 10, 17, 8, 0), LocalDateTime.of(2023, 10, 17, 12, 0), null, null, null, Terugkeer.MAANDELIJKS, jwtResponse.getGebruiker().getGezin(), soortDienst, null);

        opslaan(afspraak1);
        opslaan(afspraak2);
        opslaan(afspraak3);
        opslaan(afspraak4);
        opslaan(afspraak5);
        opslaan(afspraak6);
        opslaan(afspraak7);
        opslaan(afspraak8);
        opslaan(afspraak9);

        var gezin = new Gezin();

        MockServer.getMockServerClient()
                .when(
                        request()
                                .withMethod("GET")
                                .withPath("/api/gezin/" + jwtResponse.getGebruiker().getGezin())
                )
                .respond(
                        response()
                                .withStatusCode(200)
                                .withHeader("Content-Type", "application/json")
                                .withBody(JsonBody.json(gezin))
                );

        testAfspraak1(afspraak1, jwtResponse, accesstoken);
        testAfspraak2(afspraak1, afspraak2, jwtResponse, accesstoken);
        testAfspraak5en6(afspraak4, afspraak5, afspraak6, afspraak8, jwtResponse, accesstoken);
        testTerugkerend(afspraak3, afspraak4, afspraak7, afspraak8, afspraak9, jwtResponse, accesstoken);
    }

    private void testAfspraak1(Afspraak afspraak, JwtResponse jwtResponse, String accesstoken) {
        var response = leesViaApi(LijstAfspraken.class, "/api/kalender/afspraak/2024-01-05/2024-01-05", accesstoken);
        assertEquals(1, response.getAfspraken().size());
        assertEquals(afspraak.getOmschrijving(), response.getAfspraken().get(0).getOmschrijving());
        assertEquals(2,response.getAfspraken().get(0).getGezinsleden().size());
        assertEquals(jwtResponse.getGebruiker().getGezin(), response.getAfspraken().get(0).getGezin());

        response = leesViaApi(LijstAfspraken.class, "/api/kalender/afspraak/2024-01-06/2024-01-06", accesstoken);
        assertEquals(1, response.getAfspraken().size());
        assertEquals(afspraak.getOmschrijving(), response.getAfspraken().get(0).getOmschrijving());
        assertEquals(jwtResponse.getGebruiker().getGezin(), response.getAfspraken().get(0).getGezin());

        response = leesViaApi(LijstAfspraken.class, "/api/kalender/afspraak/2024-01-07/2024-01-07", accesstoken);
        assertEquals(1, response.getAfspraken().size());
        assertEquals(afspraak.getOmschrijving(), response.getAfspraken().get(0).getOmschrijving());
        assertEquals(jwtResponse.getGebruiker().getGezin(), response.getAfspraken().get(0).getGezin());

        response = leesViaApi(LijstAfspraken.class, "/api/kalender/afspraak/2024-01-07/2024-01-08", accesstoken);
        assertEquals(1, response.getAfspraken().size());
        assertEquals(afspraak.getOmschrijving(), response.getAfspraken().get(0).getOmschrijving());
        assertEquals(jwtResponse.getGebruiker().getGezin(), response.getAfspraken().get(0).getGezin());

        response = leesViaApi(LijstAfspraken.class, "/api/kalender/afspraak/2024-01-04/2024-01-08", accesstoken);
        assertEquals(1, response.getAfspraken().size());
        assertEquals(afspraak.getOmschrijving(), response.getAfspraken().get(0).getOmschrijving());
        assertEquals(jwtResponse.getGebruiker().getGezin(), response.getAfspraken().get(0).getGezin());

        response = leesViaApi(LijstAfspraken.class, "/api/kalender/afspraak/2024-01-08/2024-01-08", accesstoken);
        assertEquals(0, response.getAfspraken().size());
    }

    private void testAfspraak2(Afspraak afspraak1, Afspraak afspraak2, JwtResponse jwtResponse, String accesstoken) {
        var response = leesViaApi(LijstAfspraken.class, "/api/kalender/afspraak/2024-01-05/2024-01-09", accesstoken);
        assertEquals(2, response.getAfspraken().size());
        assertEquals(afspraak1.getOmschrijving(), response.getAfspraken().get(0).getOmschrijving());
        assertEquals(jwtResponse.getGebruiker().getGezin(), response.getAfspraken().get(0).getGezin());
        assertEquals(afspraak2.getOmschrijving(), response.getAfspraken().get(1).getOmschrijving());
        assertEquals(jwtResponse.getGebruiker().getGezin(), response.getAfspraken().get(1).getGezin());

        response = leesViaApi(LijstAfspraken.class, "/api/kalender/afspraak/2024-01-07/2024-01-09", accesstoken);
        assertEquals(2, response.getAfspraken().size());
        assertEquals(afspraak1.getOmschrijving(), response.getAfspraken().get(0).getOmschrijving());
        assertEquals(jwtResponse.getGebruiker().getGezin(), response.getAfspraken().get(0).getGezin());
        assertEquals(afspraak2.getOmschrijving(), response.getAfspraken().get(1).getOmschrijving());
        assertEquals(jwtResponse.getGebruiker().getGezin(), response.getAfspraken().get(1).getGezin());
    }

    private void testAfspraak5en6(Afspraak afspraak4, Afspraak afspraak5, Afspraak afspraak6, Afspraak afspraak8, JwtResponse jwtResponse, String accesstoken) {
        var response = leesViaApi(LijstAfspraken.class, "/api/kalender/afspraak/2024-01-14/2024-01-14", accesstoken);
        assertEquals(1, response.getAfspraken().size());
        assertEquals(afspraak5.getOmschrijving(), response.getAfspraken().get(0).getOmschrijving());
        assertEquals(jwtResponse.getGebruiker().getGezin(), response.getAfspraken().get(0).getGezin());

        response = leesViaApi(LijstAfspraken.class, "/api/kalender/afspraak/2024-01-13/2024-01-16", accesstoken);
        assertEquals(4, response.getAfspraken().size());
        assertEquals(afspraak4.getOmschrijving(), response.getAfspraken().get(0).getOmschrijving());
        assertEquals(jwtResponse.getGebruiker().getGezin(), response.getAfspraken().get(0).getGezin());
        assertEquals(afspraak5.getOmschrijving(), response.getAfspraken().get(1).getOmschrijving());
        assertEquals(jwtResponse.getGebruiker().getGezin(), response.getAfspraken().get(1).getGezin());
        assertEquals(afspraak6.getOmschrijving(), response.getAfspraken().get(2).getOmschrijving());
        assertEquals(jwtResponse.getGebruiker().getGezin(), response.getAfspraken().get(2).getGezin());
        assertEquals(afspraak8.getOmschrijving(), response.getAfspraken().get(3).getOmschrijving());
        assertEquals(jwtResponse.getGebruiker().getGezin(), response.getAfspraken().get(3).getGezin());
    }

    private void testTerugkerend(Afspraak afspraak3, Afspraak afspraak4, Afspraak afspraak7, Afspraak afspraak8, Afspraak afspraak9, JwtResponse jwtResponse, String accesstoken) {
        var response = leesViaApi(LijstAfspraken.class, "/api/kalender/afspraak/2025-01-11/2025-01-14", accesstoken);
        assertEquals(3, response.getAfspraken().size());
        assertEquals(afspraak3.getOmschrijving(), response.getAfspraken().get(0).getOmschrijving());
        assertEquals(jwtResponse.getGebruiker().getGezin(), response.getAfspraken().get(0).getGezin());
        assertEquals(afspraak4.getOmschrijving(), response.getAfspraken().get(1).getOmschrijving());
        assertEquals(jwtResponse.getGebruiker().getGezin(), response.getAfspraken().get(1).getGezin());
        assertEquals(afspraak8.getOmschrijving(), response.getAfspraken().get(2).getOmschrijving());
        assertEquals(jwtResponse.getGebruiker().getGezin(), response.getAfspraken().get(2).getGezin());

        response = leesViaApi(LijstAfspraken.class, "/api/kalender/afspraak/2025-01-17/2025-01-17", accesstoken);
        assertEquals(2, response.getAfspraken().size());
        assertEquals(afspraak7.getOmschrijving(), response.getAfspraken().get(0).getOmschrijving());
        assertEquals(jwtResponse.getGebruiker().getGezin(), response.getAfspraken().get(0).getGezin());
        assertEquals(afspraak9.getOmschrijving(), response.getAfspraken().get(1).getOmschrijving());
        assertEquals(jwtResponse.getGebruiker().getGezin(), response.getAfspraken().get(1).getGezin());
    }
}

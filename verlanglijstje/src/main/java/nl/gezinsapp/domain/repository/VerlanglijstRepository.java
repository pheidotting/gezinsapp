package nl.gezinsapp.domain.repository;

import io.quarkus.hibernate.orm.panache.PanacheRepositoryBase;
import jakarta.enterprise.context.ApplicationScoped;
import nl.gezinsapp.domain.Verlanglijst;

@ApplicationScoped
public class VerlanglijstRepository implements PanacheRepositoryBase<Verlanglijst, Long> {
}

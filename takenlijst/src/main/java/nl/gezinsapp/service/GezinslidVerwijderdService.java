package nl.gezinsapp.service;

import jakarta.enterprise.context.ApplicationScoped;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.gezinsapp.commons.aspect.MethodeLogging;

@Slf4j
@ApplicationScoped
@RequiredArgsConstructor
public class GezinslidVerwijderdService {
    private final TaakService taakService;

    @MethodeLogging
    public void verwerkVerwijderdgezinslid(Long gezinslidId) {
        taakService.verwerkVerwijderdGezinslid(gezinslidId);
    }
}

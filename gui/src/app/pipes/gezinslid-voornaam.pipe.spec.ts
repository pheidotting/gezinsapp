import {GezinslidVoornaamPipe} from './gezinslid-voornaam.pipe';
import {GezinslidGeslachtEnum, GezinslidSoortGezinslidEnum} from "../../api/gezin/model/gezinslid";

describe('GezinslidVoornaamPipe', () => {
  it('should return voornaam', () => {
    const pipe = new GezinslidVoornaamPipe();
    expect(pipe).toBeTruthy();

    expect(pipe.transform(
      {
        voornaam: 'John',
        achternaam: 'Doe',
        id: 3,
        soortGezinslid: GezinslidSoortGezinslidEnum.Ouder,
        geslacht: GezinslidGeslachtEnum.Man
      })).toEqual('John');
  });
});

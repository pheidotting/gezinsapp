package nl.gezinsapp.domain.repository;

import io.quarkus.hibernate.orm.panache.PanacheRepositoryBase;
import jakarta.enterprise.context.ApplicationScoped;
import nl.gezinsapp.domain.Taak;

@ApplicationScoped
public class TaakRepository implements PanacheRepositoryBase<Taak, Long> {
}

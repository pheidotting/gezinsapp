package nl.gezinsapp.model.enums;

public enum SoortAfspraak {
    HUWELIJK,
    VERJAARDAG,
    OVERIG
}

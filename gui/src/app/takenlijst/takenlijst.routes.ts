import {Routes} from '@angular/router';
import {LijstTakenComponent} from "./lijst-taken/lijst-taken.component";
import {lijstGezinsledenResolver} from "../gezin/lijst-gezinsleden.resolver";
import {LijstTakenResolver} from "./takenlijst.resolver";

export const TAKENLIJST_ROUTES: Routes = [
  {
    path: '',
    component: LijstTakenComponent,
    // canActivate: [adresBoekGuard],
    resolve: {
      takenlijst: LijstTakenResolver,
      gezin: lijstGezinsledenResolver
    }
    // },
    // {
    //   path: ':id',
    //   component: AdresBoekComponent,
    //   canActivate: [adresBoekGuard],
    //   resolve: {
    //     adresBoek: AdresBoekResolver,
    //     gezin: lijstGezinsledenResolver
    //   }
    // },
    // {
    //   path: ':id/nieuw',
    //   component: AdresBoekComponent,
    //   canActivate: [adresBoekGuard],
    //   resolve: {
    //     gezin: lijstGezinsledenResolver
    //   }
  }
]

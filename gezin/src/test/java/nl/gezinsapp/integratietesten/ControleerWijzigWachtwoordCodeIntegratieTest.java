package nl.gezinsapp.integratietesten;

import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;
import jakarta.inject.Inject;
import lombok.extern.slf4j.Slf4j;
import nl.gezinsapp.model.repository.WijzigWachtwoordCodeRepository;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.openapi.quarkus.gezin_openapi_yaml.model.ControleerWijzigWachtwoordCode;
import org.openapi.quarkus.gezin_openapi_yaml.model.ControleerWijzigWachtwoordCodeResultaat;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

@Slf4j
@QuarkusTest
@QuarkusTestResource(MockServer.class)
class ControleerWijzigWachtwoordCodeIntegratieTest extends AbstractIntegratieTest {
    @Inject
    private WijzigWachtwoordCodeRepository WijzigWachtwoordCodeRepository;

    @Test
    @DisplayName("Controleer een goede code")
    void test1() {
        var WijzigWachtwoordCode = WijzigWachtwoordCodeRepository.set("123456");

        var controleerWijzigWachtwoordCode = new ControleerWijzigWachtwoordCode();
        controleerWijzigWachtwoordCode.code("123456");
        controleerWijzigWachtwoordCode.setId(WijzigWachtwoordCode.getId());

        var controleerWijzigWachtwoordCodeResultaat = postViaApi(ControleerWijzigWachtwoordCodeResultaat.class, "/api/gezin/wijzigwachtwoordcode/controleer", controleerWijzigWachtwoordCode, null, null);

        assertTrue(controleerWijzigWachtwoordCodeResultaat.getResultaat());
    }

    @Test
    @DisplayName("Controleer een foute code")
    void test2() {
        var WijzigWachtwoordCode = WijzigWachtwoordCodeRepository.set("123456");

        var controleerWijzigWachtwoordCode = new ControleerWijzigWachtwoordCode();
        controleerWijzigWachtwoordCode.code("123457");
        controleerWijzigWachtwoordCode.setId(WijzigWachtwoordCode.getId());

        var controleerWijzigWachtwoordCodeResultaat = postViaApi(ControleerWijzigWachtwoordCodeResultaat.class, "/api/gezin/wijzigwachtwoordcode/controleer", controleerWijzigWachtwoordCode, null, null);

        assertFalse(controleerWijzigWachtwoordCodeResultaat.getResultaat());
    }

    @Test
    @DisplayName("Controleer een code die niet bestaat")
    void test3() {
        var controleerWijzigWachtwoordCode = new ControleerWijzigWachtwoordCode();
        controleerWijzigWachtwoordCode.code("123456");
        controleerWijzigWachtwoordCode.setId(UUID.randomUUID());

        var controleerWijzigWachtwoordCodeResultaat = postViaApi(ControleerWijzigWachtwoordCodeResultaat.class, "/api/gezin/wijzigwachtwoordcode/controleer", controleerWijzigWachtwoordCode, null, null);

        assertFalse(controleerWijzigWachtwoordCodeResultaat.getResultaat());
    }
}

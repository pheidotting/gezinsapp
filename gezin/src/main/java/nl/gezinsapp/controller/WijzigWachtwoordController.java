package nl.gezinsapp.controller;

import jakarta.transaction.Transactional;
import jakarta.ws.rs.Path;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.gezinsapp.WijzigWachtwoordCodeOnjuistException;
import nl.gezinsapp.commons.aspect.MethodeLogging;
import nl.gezinsapp.commons.aspect.TrackAndTraceId;
import nl.gezinsapp.mapper.WijzigWachtwoordMapper;
import nl.gezinsapp.service.WijzigWachtwoordService;
import org.openapi.quarkus.gezin_openapi_yaml.api.WijzigWachtwoordApi;
import org.openapi.quarkus.gezin_openapi_yaml.model.AlgemeneResponse;
import org.openapi.quarkus.gezin_openapi_yaml.model.Foutmeldingen;
import org.openapi.quarkus.gezin_openapi_yaml.model.WijzigWachtwoord;

@Slf4j
@Path("/api/gezin/wijzigwachtwoord")
@RequiredArgsConstructor
public class WijzigWachtwoordController implements WijzigWachtwoordApi {
    private final WijzigWachtwoordService wijzigWachtwoordService;
    private final WijzigWachtwoordMapper wijzigWachtwoordMapper;

    @Override
    @MethodeLogging
    @Transactional
    @TrackAndTraceId
    public AlgemeneResponse wijzigWachtwoord(WijzigWachtwoord wijzigWachtwoord) {
        var response = new AlgemeneResponse();
        try {
            wijzigWachtwoordService.wijzigWachtwoord(wijzigWachtwoordMapper.map(wijzigWachtwoord));
        } catch (WijzigWachtwoordCodeOnjuistException e) {
            response.setFoutmeldingen(new Foutmeldingen().addMeldingenItem("De ingevoerde code is onjuist"));
        }

        return response;
    }
}

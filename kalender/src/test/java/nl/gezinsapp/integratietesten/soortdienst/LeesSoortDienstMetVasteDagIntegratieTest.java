package nl.gezinsapp.integratietesten.soortdienst;

import com.fasterxml.jackson.core.JsonProcessingException;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;
import lombok.extern.slf4j.Slf4j;
import nl.gezinsapp.integratietesten.AbstractIntegratieTest;
import nl.gezinsapp.integratietesten.MockServer;
import nl.gezinsapp.model.enums.Dag;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockserver.model.JsonBody;
import org.openapi.quarkus.gezin_openapi_yaml.model.Gezin;
import org.openapi.quarkus.gezin_openapi_yaml.model.Gezinslid;
import org.openapi.quarkus.kalender_openapi_yaml.model.LijstAfspraken;

import java.time.LocalDate;

import static nl.gezinsapp.integratietesten.common.random.SoortDienstDatum.randomSoortDienst;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockserver.model.HttpRequest.request;
import static org.mockserver.model.HttpResponse.response;

@Slf4j
@QuarkusTest
@QuarkusTestResource(MockServer.class)
class LeesSoortDienstMetVasteDagIntegratieTest extends AbstractIntegratieTest {
    @Test
    @DisplayName("Lees een opgeslagen datum")
    void test() throws JsonProcessingException {
        maakDatabaseLeeg();

        var soortDienst = randomSoortDienst();
        soortDienst.setDag(Dag.MA);

        var accesstoken = maakToken();
        var jwtResponse = mockCheckJwt(accesstoken);

        opslaan(soortDienst);

        var gezin = new Gezin();
        var gezinslid = new Gezinslid();
        gezinslid.setId(soortDienst.getGezinslid());
        gezin.getGezinsleden().add(gezinslid);

        MockServer.getMockServerClient()
                .when(
                        request()
                                .withMethod("GET")
                                .withPath("/api/gezin/" + jwtResponse.getGebruiker().getGezin())
                )
                .respond(
                        response()
                                .withStatusCode(200)
                                .withHeader("Content-Type", "application/json")
                                .withBody(JsonBody.json(gezin))
                );
        var response = leesViaApi(LijstAfspraken.class, "/api/kalender/afspraak/2024-12-02/2024-12-15", accesstoken);

        assertEquals(2, response.getAfspraken().size());
        assertEquals(soortDienst.getOmschrijving(), response.getAfspraken().get(0).getOmschrijving());
        assertEquals(LocalDate.of(2024, 12, 2), response.getAfspraken().get(0).getVanTijd().toLocalDate());
        assertEquals(LocalDate.of(2024, 12, 2), response.getAfspraken().get(0).getTotTijd().toLocalDate());
        assertEquals(soortDienst.getVan().withNano(0).minusHours(2), response.getAfspraken().get(0).getVanTijd().toLocalTime());
        assertEquals(soortDienst.getTot().withNano(0).minusHours(2), response.getAfspraken().get(0).getTotTijd().toLocalTime());
        assertEquals(soortDienst.getOmschrijving(), response.getAfspraken().get(1).getOmschrijving());
        assertEquals(LocalDate.of(2024, 12, 9), response.getAfspraken().get(1).getVanTijd().toLocalDate());
        assertEquals(LocalDate.of(2024, 12, 9), response.getAfspraken().get(1).getTotTijd().toLocalDate());
        assertEquals(soortDienst.getVan().withNano(0).minusHours(2), response.getAfspraken().get(1).getVanTijd().toLocalTime());
        assertEquals(soortDienst.getTot().withNano(0).minusHours(2), response.getAfspraken().get(1).getTotTijd().toLocalTime());

    }
}

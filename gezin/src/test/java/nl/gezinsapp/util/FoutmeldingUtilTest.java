package nl.gezinsapp.util;

import org.hibernate.exception.ConstraintViolationException;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static nl.gezinsapp.util.FoutmeldingUtil.mapExceptionNaarFoutmeldingen;
import static org.junit.jupiter.api.Assertions.assertEquals;

class FoutmeldingUtilTest {
    @ParameterizedTest
    @ValueSource(strings = {"4,18,20"})
    void testConn(String value) {
        var exception = new ConstraintViolationException(String.format("could not execute statement [(conn=%s) Column 'veldnaam' mag niet leeg zijn.", value), null, null);

        var meldingen = mapExceptionNaarFoutmeldingen(exception);
        assertEquals(1, meldingen.size());
        assertEquals("veldnaam mag niet leeg zijn.", meldingen.get(0));
    }
}
import {Component, OnInit} from '@angular/core';
import {
  AlgemeneResponse,
  Gezin,
  GezinService,
  Gezinslid,
  GezinslidGeslachtEnum,
  GezinslidService,
  GezinslidSoortGezinslidEnum,
  LeesGezin,
  VerwijderGezinslid
} from "../../../api/gezin";
import moment from "moment/moment";
import {ActivatedRoute, Router} from "@angular/router";
import {GezinslidUtil} from "../../services/gezinslid-util.service";
import {SideBarComponent} from "../../common/side-bar/side-bar.component";
import {HeaderMetToevoegButtonComponent} from "../../common/header-met-toevoeg-button/header-met-toevoeg-button.component";
import {NgIf} from "@angular/common";
import {ModalComponent} from "../../common/modal/modal.component";
import {MaakDatumOpPipe} from "../../pipes/maak-datum-op.pipe";
import {AvatarComponent} from "../../common/avatar/avatar.component";
import {MaakSoortOuderEnumOpPipe} from "../../pipes/maak-soort-ouder-enum-op.pipe";
import {MaakGeslachtEnumOpPipe} from "../../pipes/maak-geslacht-enum-op.pipe";

@Component({
  selector: 'app-lijst-gezinsleden',
  templateUrl: './lijst-gezinsleden.component.html',
  imports: [
    SideBarComponent,
    HeaderMetToevoegButtonComponent,
    NgIf,
    ModalComponent,
    MaakDatumOpPipe,
    AvatarComponent,
    MaakSoortOuderEnumOpPipe,
    MaakGeslachtEnumOpPipe
  ]
})
export class LijstGezinsledenComponent implements OnInit {
  gezin: Gezin = {gezinsleden: [], naam: ""};
  teVerwijderenGezinslid: Gezinslid = {
    id: 0,
    achternaam: "",
    geslacht: GezinslidGeslachtEnum.Vrouw,
    soortGezinslid: GezinslidSoortGezinslidEnum.Ouder,
    voornaam: ""
  };
  modalTonen: boolean = false;
  benIkDitZelfBln: boolean = false;

  constructor(
    private gezinService: GezinService,
    private gezinslidService: GezinslidService,
    private router: Router,
    private readonly activatedRoute: ActivatedRoute,
    private gezinslidUtil: GezinslidUtil
  ) {
  }

  ngOnInit(): void {
    this.leesGezinUitResolver();
  }

  leesGezinUitDatabase(): void {
    this.gezinService.leesHuidigGezin().subscribe((gezin: LeesGezin) => {
      this.gezin = gezin;
      this.verwerkGezin();
    });
  }

  leesGezinUitResolver(): void {
    this.activatedRoute.data.subscribe((response: any) => {
      this.gezin = response.gezin;
      this.verwerkGezin();
    });
  }

  verwerkGezin(): void {
    this.gezin.gezinsleden = this.gezin.gezinsleden.sort((lid1: Gezinslid, lid2: Gezinslid): number => {
      let datum1: moment.Moment = moment(lid1.geboorteDatum);
      let datum2: moment.Moment = moment(lid2.geboorteDatum);
      if (datum1.isBefore(datum2)) {
        return -1;
      } else if (datum2.isBefore(datum1)) {
        return 1;
      } else {
        return 0;
      }
    });
  }

  maakDatumOp(datum: string | undefined): string {
    if (datum != undefined) {
      return moment(datum).format('DD-MM-YYYY');
    } else {
      return '';
    }
  }

  setVerwijderLid(lid: Gezinslid): void {
    this.modalTonen = true;
    this.teVerwijderenGezinslid = lid;
  }

  maakNaamOp(gezinslid: Gezinslid): String {
    let naam: string = gezinslid.voornaam;

    if (gezinslid.tussenvoegsel != null && gezinslid.tussenvoegsel != undefined) {
      naam += ' ' + gezinslid.tussenvoegsel.trim();
    }
    naam += ' ' + gezinslid.achternaam;

    return naam.trim();
  }

  bewerk(lid: Gezinslid): void {
    this.router.navigate(['bewerk-gezinslid', lid.id])
  }

  verwijderGezinslid() {
    let verwijderGezinslid: VerwijderGezinslid = {id: this.teVerwijderenGezinslid.id!};
    this.gezinslidService.verwijderGezinslid(verwijderGezinslid).subscribe((response: AlgemeneResponse): void => {
      this.teVerwijderenGezinslid = {
        id: 0,
        achternaam: "",
        geslacht: GezinslidGeslachtEnum.Man,
        soortGezinslid: GezinslidSoortGezinslidEnum.Kind,
        voornaam: ""
      };
      this.leesGezinUitDatabase();
      this.modalTonen = false;
    });

  }

  nieuwGezinsLid(): void {
    this.router.navigate(['/nieuw-gezinslid']);
  }

  benIkDitZelf(gl: Gezinslid): boolean {
    this.gezinslidUtil.haalIngelogdGezinslid(this.gezin.gezinsleden).subscribe((gezinslid: Gezinslid) => {
      this.benIkDitZelfBln = gl.id == gezinslid.id;
    });
    return this.benIkDitZelfBln;
  }

  verbergModal(): void {
    this.modalTonen = false;
  }
}


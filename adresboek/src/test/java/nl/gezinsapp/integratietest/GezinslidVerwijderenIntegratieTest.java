package nl.gezinsapp.integratietest;

import com.fasterxml.jackson.core.JsonProcessingException;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;
import lombok.extern.slf4j.Slf4j;
import nl.gezinsapp.integratietest.common.QueueUtil;
import nl.gezinsapp.integratietest.common.TestGegevens;
import nl.gezinsapp.messaging.model.GezinslidVerwijderd;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.UUID;
import java.util.concurrent.TimeUnit;

import static nl.gezinsapp.integratietest.random.RandomAdresBoek.randomAdresBoek;
import static org.awaitility.Awaitility.await;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

@Slf4j
@QuarkusTest
@QuarkusTestResource(MockServer.class)
class GezinslidVerwijderenIntegratieTest extends AbstractIntegratieTest {
    @ConfigProperty(name = "mp.messaging.incoming.gezinslidverwijderd.exchange.name")
    private String exchange;
    @ConfigProperty(name = "rabbitmq-host")
    private String rabbitHost;
    @ConfigProperty(name = "rabbitmq-port")
    private String rabbitPort;
    @ConfigProperty(name = "rabbitmq-username")
    private String rabbitUsername;
    @ConfigProperty(name = "rabbitmq-password")
    private String rabbitPassword;

    @Test
    @DisplayName("Test dat de verlanglijstjes worden verwijderd")
    void test() throws JsonProcessingException {
        TestGegevens.setRabbitHost(rabbitHost);
        TestGegevens.setRabbitPort(Integer.valueOf(rabbitPort));
        TestGegevens.setRabbitUser(rabbitUsername);
        TestGegevens.setRabbitPassword(rabbitPassword);

        maakDatabaseLeeg();

        var gezinslid1 = 33L;
        var gezinslid2 = 46L;

        var accesstoken = maakToken();
        mockCheckJwt(accesstoken);

        var adresBoek1 = randomAdresBoek();
        adresBoek1.setGezinslid(gezinslid1);
        opslaan(adresBoek1);
        var adresBoek2 = randomAdresBoek();
        adresBoek2.setGezinslid(gezinslid2);
        opslaan(adresBoek2);

        queue().stuurBerichtNaarExchange(new GezinslidVerwijderd("a@b.c", 1L, 2L, UUID.randomUUID(), gezinslid1));

        await()
                .atMost(5, TimeUnit.SECONDS)
                .until(() -> {
                    try {
                        assertNull(leesAdresBoek(adresBoek1.getId()));
                        assertNotNull(leesAdresBoek(adresBoek2.getId()));
                        return true;
                    } catch (AssertionError ae) {
                        return false;
                    }
                });
    }

    private QueueUtil<GezinslidVerwijderd> queue() {
        return new QueueUtil<>() {
            @Override
            public String getExchange() {
                return exchange;
            }

            @Override
            public Class<GezinslidVerwijderd> getClazz() {
                return GezinslidVerwijderd.class;
            }
        };
    }
}


package nl.gezinsapp.integratietesten;

import com.fasterxml.jackson.core.JsonProcessingException;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.openapi.quarkus.takenlijst_openapi_yaml.model.AlgemeneResponse;

import java.util.List;

import static nl.gezinsapp.integratietesten.random.RandomTaak.randomDomainTaak;
import static nl.gezinsapp.integratietesten.random.RandomTaak.randomTaak;
import static org.jboss.resteasy.reactive.RestResponse.StatusCode.OK;
import static org.junit.jupiter.api.Assertions.assertEquals;

@Slf4j
@QuarkusTest
@QuarkusTestResource(MockServer.class)
class WijzigTaakIntegratieTest extends AbstractIntegratieTest {
    @Test
    @DisplayName("Verwijder een Taak")
    void test() throws JsonProcessingException {
        maakDatabaseLeeg();

        var accesstoken = maakToken();
        var jwtResponse = mockCheckJwt(accesstoken);

        var taak = randomDomainTaak();
        taak.setGezin(jwtResponse.getGebruiker().getGezin());
        opslaan(taak);

        var gewijzigdeTaak = randomTaak();
        gewijzigdeTaak.setGezin(taak.getGezin());
        gewijzigdeTaak.setId(taak.getId());
        gewijzigdeTaak.setGezinsleden(List.of(3L));

        var repsonse = patchViaApi(AlgemeneResponse.class, "/api/takenlijst/taak", gewijzigdeTaak, accesstoken, OK);

        assertEquals(taak.getId(), repsonse.getId());
        var itemGelezen = leesTaak(taak.getId());
        assertEquals(gewijzigdeTaak.getOmschrijving(), itemGelezen.getOmschrijving());
        assertEquals(gewijzigdeTaak.getGezinsleden(), itemGelezen.getGezinsleden());
        assertEquals(gewijzigdeTaak.getGezin(), itemGelezen.getGezin());
        assertEquals(gewijzigdeTaak.getDeadline().toLocalDateTime().withNano(0), itemGelezen.getDeadline());
        assertEquals(gewijzigdeTaak.getTijdstipAfgestreept().toLocalDateTime().withNano(0), itemGelezen.getTijdstipAfgestreept());
    }

}

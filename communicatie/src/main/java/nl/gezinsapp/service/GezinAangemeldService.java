package nl.gezinsapp.service;

import io.quarkus.qute.Template;
import jakarta.enterprise.context.ApplicationScoped;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.gezinsapp.model.CommGezinAangemeld;
import org.openapi.quarkus.gezin_openapi_yaml.model.LeesGezin;

@Slf4j
@ApplicationScoped
@RequiredArgsConstructor
public class GezinAangemeldService extends AbstractService<CommGezinAangemeld> {
    private final Template gezinAangemeld;

    @Override
    public void vulCommObject(CommGezinAangemeld commGezinAangemeld, LeesGezin gezin) {
        commGezinAangemeld.setNaamGezin(gezin.getNaam());
    }

    @Override
    public Template getTemplate() {
        return gezinAangemeld;
    }

    @Override
    public String getOnderwerp() {
        return "Welkom bij de gezinsapp!";
    }
}

package nl.gezinsapp.exception;

public class GebruikerNietGevondenException extends Exception {
    public GebruikerNietGevondenException(String message) {
        super(message);
    }
}

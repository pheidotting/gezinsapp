import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {AlgemeneResponse, BoodschappenItem, BoodschappenService, LijstBoodschappenItems} from "../../api/boodschappen";
import {SideBarComponent} from "../common/side-bar/side-bar.component";
import {NgIf} from "@angular/common";
import {FormsModule} from "@angular/forms";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-boodschappen',
  templateUrl: './boodschappen.component.html',
  imports: [
    SideBarComponent,
    NgIf,
    FormsModule
  ]
})
export class BoodschappenComponent implements OnInit {
  @ViewChild('nieuweomschrijvingE') nieuweomschrijvingElement?: ElementRef;

  boodschappen: BoodschappenItem[] = [];
  gefilterdeLijstBoodschappen: BoodschappenItem[] = [];

  nieuweomschrijving: string = '';
  nieuwewinkel: string = '';

  wijzigId?: number;

  teFilterenWinkel?: string;

  constructor(
    private readonly boodschappenService: BoodschappenService,
    private readonly activatedRoute: ActivatedRoute
  ) {
  }

  ngOnInit(): void {
    this.activatedRoute.data
      .subscribe((response: any) => this.verwerkResponse(response.boodschappen.boodschappenitems));
  }

  verwerkResponse(items: BoodschappenItem[]) {
    this.boodschappen = items;
    this.gefilterdeLijstBoodschappen = this.boodschappen;
  }

  leesBoodschappen(): void {
    this.boodschappenService.lijst().subscribe((response: LijstBoodschappenItems) => {
      this.verwerkResponse(response.boodschappenitems);
    });
  }

  voegItemToe(): void {
    let item: BoodschappenItem = {
      omschrijving: this.nieuweomschrijving,
      winkel: this.nieuwewinkel
    };
    this.boodschappenService.voegItemToe(item).subscribe((response: AlgemeneResponse) => {
      item.id = response.id;
      this.boodschappen.push(item);
      this.nieuweomschrijving = '';
      this.nieuweomschrijvingElement!.nativeElement.focus();
      this.teFilterenWinkel = '';
      this.filterOpWinkel();
    });
  }

  onKeyDown(event: any): void {
    if (event.key === "Enter") {
      this.voegItemToe();
    }
  }

  vinkItemAf(item: BoodschappenItem): void {
    item.gehaald = true;
    this.boodschappenService.wijzig(item).subscribe((response: AlgemeneResponse) => {
      this.leesBoodschappen();
    });
  }

  wijzigItem(item: BoodschappenItem): void {
    this.wijzigId = item.id;
  }

  opslaanItem(item: BoodschappenItem): void {
    this.boodschappenService.wijzig(item).subscribe((response: AlgemeneResponse) => {
      this.wijzigId = undefined;
      this.leesBoodschappen();
    });
  }

  winkels(): string[] {
    return [...new Set(this.boodschappen.map((b: BoodschappenItem) => {
      return b.winkel!;
    }))].filter((b: string) => {
      return b != undefined && b != '';
    }).sort((a: string, b: string) => {
      return a.localeCompare(b);
    });
  }

  filterOpWinkel(): void {
    if (this.teFilterenWinkel == undefined || this.teFilterenWinkel == '') {
      this.gefilterdeLijstBoodschappen = this.boodschappen;
    } else {
      this.gefilterdeLijstBoodschappen = this.boodschappen.filter((b: BoodschappenItem) => {
        return b.winkel == this.teFilterenWinkel;
      });
    }
  }
}

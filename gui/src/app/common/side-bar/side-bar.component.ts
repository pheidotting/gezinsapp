import {Component, OnInit} from '@angular/core';
import {Gezin, GezinService, Gezinslid, LeesGezin} from "../../../api/gezin";
import {InEnUitloggenService} from '../../../api/authenticatie/api/inEnUitloggen.service';
import {TokenService} from "../../services/token.service";
import {ActivatedRoute, Router} from "@angular/router";
import {GezinslidUtil} from "../../services/gezinslid-util.service";
import {AvatarComponent} from "../avatar/avatar.component";
import {ModalComponent} from "../modal/modal.component";
import {NgIf} from "@angular/common";

@Component({
  selector: 'app-side-bar',
  imports: [
    AvatarComponent,
    ModalComponent,
    NgIf
  ],
  templateUrl: './side-bar.component.html'
})
export class SideBarComponent implements OnInit {
  gezin!: Gezin;
  gezinslid!: Gezinslid;

  uitloggenModalTonen: boolean = false;

  constructor(
    private readonly activatedRoute: ActivatedRoute,
    private readonly gezinService: GezinService,
    private readonly inEnUitloggenService: InEnUitloggenService,
    private readonly router: Router,
    private readonly tokenService: TokenService,
    public readonly gezinslidUtil: GezinslidUtil
  ) {
  }

  ngOnInit(): void {
    this.activatedRoute.data.subscribe((response: any) => {
      let aanwezig: boolean = response.gezin == undefined;
      if (!aanwezig) {
        this.gezinService.leesHuidigGezin().subscribe((gezin: LeesGezin) => {
          this.gezin = gezin;
          this.gezinslidUtil.haalIngelogdGezinslid(this.gezin.gezinsleden).subscribe((gezinslid: Gezinslid) => {
            this.gezinslid = gezinslid;
          });
        });
      } else {
        this.gezin = response.gezin;
        if (this.gezin != undefined) {
          this.gezinslidUtil.haalIngelogdGezinslid(this.gezin.gezinsleden).subscribe((gezinslid: Gezinslid) => {
            this.gezinslid = gezinslid;
          });
        }
      }
    });
  }

  uitloggen() {
    this.uitloggenModalTonen = true;
  }

  doeUitloggen() {
    this.inEnUitloggenService.uitloggen({'refreshToken': this.tokenService.refreshToken!}).subscribe(() => {
      this.tokenService.setRefreshToken(null);
      this.tokenService.setAccessToken(null);
      this.router.navigate(['inloggen']);
    });
  }

  verbergModal(): void {
    this.uitloggenModalTonen = false;
  }
}

package nl.gezinsapp.model;

public enum Geslacht {
    MAN, VROUW, ONBELANGRIJK
}

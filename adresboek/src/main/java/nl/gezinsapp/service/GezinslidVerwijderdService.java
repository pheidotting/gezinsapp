package nl.gezinsapp.service;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import nl.gezinsapp.commons.aspect.MethodeLogging;

@ApplicationScoped
@RequiredArgsConstructor
public class GezinslidVerwijderdService {
    private final AdresService boodschappenService;

    @MethodeLogging
    @Transactional
    public void verwerkVerwijderdgezinslid(Long gezinslidId) {
        boodschappenService.verwerkVerwijderdgezinslid(gezinslidId);
    }
}

import {Routes} from '@angular/router';
import {VerlanglijstjeComponent} from "./verlanglijstje/verlanglijstje.component";
import {VerlanglijstComponent} from "./verlanglijst/verlanglijst.component";
import {verlanglijstjeResolver} from "./verlanglijstje.resolver";
import {huidigGezinResolver} from "../common/resolvers/huidiggezin.resolver";

export const VERLANGLIJST_ROUTES: Routes = [
  {
    path: '',
    component: VerlanglijstjeComponent,
    resolve: {verlanglijstje: verlanglijstjeResolver, gezin: huidigGezinResolver}
  },
  {
    path: ':id',
    component: VerlanglijstComponent,
    resolve: {verlanglijstje: verlanglijstjeResolver, gezin: huidigGezinResolver}
  },
  {
    path: ':id/nieuw',
    component: VerlanglijstComponent,
    resolve: {verlanglijstje: verlanglijstjeResolver, gezin: huidigGezinResolver}
  },
]

import {inject, Pipe, PipeTransform} from '@angular/core';
import {GezinService, Gezinslid, LeesGezin} from "../../api/gezin";
import {Observable} from "rxjs";

@Pipe({
  name: 'gezinslidOpId'
})
export class GezinslidOpIdPipe implements PipeTransform {
  private readonly gezinService: GezinService;

  constructor() {
    this.gezinService = inject(GezinService);
  }

  transform(value: number | undefined, ...args: unknown[]): Observable<Gezinslid | null> {
    return new Observable((observer) => {
      if (value != null) {
        this.gezinService.leesHuidigGezin().subscribe((gezin: LeesGezin) => {
          gezin.gezinsleden.filter((gezinslid: Gezinslid) => {
            if (gezinslid.id == value) {
              observer.next(gezinslid);
            }
          })[0];
        });
      } else {
        observer.next(null);
      }
    });
  }

}

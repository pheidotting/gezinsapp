package nl.gezinsapp.aspect;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.openapi.quarkus.authenticatie_openapi_yaml.model.Gebruiker;

import java.util.HashMap;
import java.util.Map;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class ThreadVariabelen {
    private static Map<Long, Gebruiker> gebruikers = new HashMap<>();

    public static Gebruiker get() {
        return ThreadVariabelen.gebruikers.get(Thread.currentThread().getId());
    }

    public static void set(Gebruiker gebruiker) {
        ThreadVariabelen.gebruikers.put(Thread.currentThread().getId(), gebruiker);
    }

    public static void clear() {
        ThreadVariabelen.gebruikers.remove(Thread.currentThread().getId());
    }

}

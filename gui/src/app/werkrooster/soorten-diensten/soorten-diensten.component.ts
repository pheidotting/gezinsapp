import {Component} from '@angular/core';
import {GezinService, LeesGezin} from "../../../api/gezin";
import {GezinslidUtil} from "../../services/gezinslid-util.service";
import {SoortDienstComponent} from "./soort-dienst/soort-dienst.component";
import {SideBarComponent} from "../../common/side-bar/side-bar.component";
import {NgIf} from "@angular/common";

@Component({
  selector: 'app-soorten-diensten',
  templateUrl: './soorten-diensten.component.html',
  imports: [
    SoortDienstComponent,
    SideBarComponent,
    NgIf
  ]
})
export class SoortenDienstenComponent {
  gezin: LeesGezin | undefined;

  constructor(
    private gezinService: GezinService,
    private gezinslidUtil: GezinslidUtil
  ) {
    this.gezinService.leesHuidigGezin().subscribe((gezin: LeesGezin) => {
      this.gezin = gezin;
      this.gezin.gezinsleden = this.gezinslidUtil.sorteer(this.gezin.gezinsleden);
    });
  }
}

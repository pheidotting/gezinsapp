package nl.gezinsapp.service;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.gezinsapp.commons.aspect.MethodeLogging;
import nl.gezinsapp.domain.repository.VerlanglijstRepository;

@Slf4j
@ApplicationScoped
@RequiredArgsConstructor
public class VerlanglijstService {
    private final VerlanglijstRepository verlanglijstRepository;

    @MethodeLogging
    @Transactional
    public void verwerkVerwijderdGezinslid(Long gezinslidId) {
        verlanglijstRepository.findAll().stream()
                .filter(verlanglijst -> verlanglijst.getGezinslid().equals(gezinslidId))
                .map(verlanglijst -> {
                    verlanglijst.setGezinslid(null);
                    return verlanglijst;
                })
                .forEach(verlanglijstRepository::persist);
    }
}

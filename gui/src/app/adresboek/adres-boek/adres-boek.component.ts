import {Component, CUSTOM_ELEMENTS_SCHEMA, OnInit} from '@angular/core';
import {FormControl, FormGroup, FormsModule, ReactiveFormsModule, Validators} from "@angular/forms";
import {Adres, AdresBijPostcode, AdresBoekMetAdressen, AdresboekService, AlgemeneResponse} from '../../../api/adresboek';
import {ActivatedRoute, Router} from "@angular/router";
import {CommonModule} from "@angular/common";
import {LijstAdressenComponent} from "../lijst-adressen/lijst-adressen.component";
import {SideBarComponent} from "../../common/side-bar/side-bar.component";
import {MeldingenComponent} from "../../common/meldingen/meldingen.component";
import {OpslaanEnAfbrekenButtonsComponent} from "../../common/opslaan-en-afbreken-buttons/opslaan-en-afbreken-buttons.component";

@Component({
  selector: 'app-adres-boek',
  templateUrl: './adres-boek.component.html',
  styleUrl: './adres-boek.component.css',
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SideBarComponent,
    MeldingenComponent,
    OpslaanEnAfbrekenButtonsComponent,
    LijstAdressenComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AdresBoekComponent implements OnInit {
  foutmeldingen: string[] = [];
  meldingen: string[] = [];
  goedmeldingen: string[] = [];

  toevoegTonen: boolean = false;
  editMode: boolean = false;
  id: number | undefined;
  omschrijving: string | undefined;
  adressen: Adres[] = [];
  resolving: boolean = false;

  adresBoekForm: FormGroup = new FormGroup({
      omschrijving: new FormControl('')
    }
  );

  adresForm: FormGroup = new FormGroup({
    naam: new FormControl('', Validators.required),
    straat: new FormControl('', Validators.required),
    huisnummer: new FormControl('', Validators.pattern('([0-9]|[1-9][0-9]|[1-9][0-9][0-9])')),
    toevoeging: new FormControl(''),
    postcode: new FormControl('', Validators.pattern('^[1-9][0-9]{3} ?(?!sa|sd|ss|SA|SD|SS)[A-Za-z]{2}$')),
    plaats: new FormControl('', Validators.required),
    id: new FormControl(''),
    adresBoek: new FormControl('')
  });

  constructor(
    private activatedRoute: ActivatedRoute,
    private adresboekService: AdresboekService,
    private router: Router
  ) {
  }

  ngOnInit(): void {
    this.activatedRoute.data.subscribe((response: any) => {
      this.adressen = response.adresBoek.adressen;
      this.editMode = true;
      this.id = response.adresBoek.id;
      this.omschrijving = response.adresBoek.omschrijving;
      this.adresBoekForm.setValue({'omschrijving': response.adresBoek.omschrijving});
    });
  }

  opslaanAdresBoek() {
    if (this.editMode) {
      let request = this.adresBoekForm.value;
      request.id = this.id;
      this.adresboekService.wijzigAdresBoek(request).subscribe((response: AlgemeneResponse) => {
        this.router.navigate(['adresboek']);
      });
    } else {
      this.adresboekService.adresBoekToevoegen(this.adresBoekForm.value).subscribe((response: AlgemeneResponse) => {
        this.router.navigate(['adresboek']);
      });
    }
  }

  resetAdresBoek() {
    this.adresBoekForm.reset();
  }

  adressenGewijzigd() {
    this.adresboekService.leesAdresBoek(this.id!).subscribe((adresBoekMetAdressen: AdresBoekMetAdressen) => {
      this.adressen = adresBoekMetAdressen.adressen!;
    });
  }

  opslaanAdres() {
    this.foutmeldingen = [];
    if (!this.adresForm.valid) {
      if (!this.adresForm.value.postcode.valid) {
        this.foutmeldingen.push('Postcode ' + this.adresForm.value.postcode + ' is niet in formaat 1111AA');
      }
      if (!this.adresForm.value.huisnummer.valid) {
        this.foutmeldingen.push('Huisnummer moet een getal zijn');
      }
      if (!this.adresForm.value.naam.valid) {
        this.foutmeldingen.push('Naam moet gevuld zijn');
      }
      if (!this.adresForm.value.straat.valid) {
        this.foutmeldingen.push('Straat moet gevuld zijn');
      }
      if (!this.adresForm.value.plaats.valid) {
        this.foutmeldingen.push('Plaats moet gevuld zijn');
      }
    } else {
      let adres = this.adresForm.value;
      adres.adresBoek = this.id;
      if (adres.id == null || adres.id == '') {
        if (adres.id == '') {
          adres.id = null;
        }
        this.adresboekService.adresToevoegen(adres).subscribe((response: AlgemeneResponse) => {
          this.adressenGewijzigd();
          this.toevoegTonen = false;
        });
      } else {
        this.adresboekService.wijzigAdres(adres).subscribe((response: AlgemeneResponse) => {
          this.adressenGewijzigd();
          this.toevoegTonen = false;
        });
      }
      this.adresForm.reset();
    }
  }

  resetAdres() {
    this.adresForm.reset();
    this.toevoegTonen = false;
  }

  onChangeCheckPostcode(): void {
    this.haalAdresOp();
  }

  onChangeCheckHuisNummer(): void {
    this.haalAdresOp();
  }

  nieuwAdres() {
    this.toevoegTonen = true;
  }

  haalAdresOp(): void {
    let postcode = this.adresForm.value.postcode;
    postcode = postcode.replaceAll(' ', '');
    if (postcode.length > 6) {
      postcode = postcode.substring(0, 6);
    }
    let huisnummer = this.adresForm.value.huisnummer;
    if (huisnummer != null && huisnummer != '' && postcode != null && postcode != '') {
      this.resolving = true;
      this.adresboekService.adresBijPostcode(postcode, huisnummer).subscribe((adresBijPostcode: AdresBijPostcode) => {
        this.adresForm.patchValue({
          straat: adresBijPostcode.straat,
          huisnummer: adresBijPostcode.huisnummer,
          postcode: adresBijPostcode.postcode,
          plaats: adresBijPostcode.plaats
        })
        this.resolving = false;
      }, () => {
        this.resolving = false;
      });
    }
  }

  startBewerken(id: number): void {
    this.editMode = true;
    this.toevoegTonen = true;
    this.adresForm.setValue(this.adressen.filter((adres: Adres) => {
      return adres.id == id;
    })[0]);
  }
}

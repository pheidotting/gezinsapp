package nl.gezinsapp.model;

public enum Resultaat {
    GELUKT, FOUT_WACHTWOORD, TWEE_FACTOR_NODIG
}

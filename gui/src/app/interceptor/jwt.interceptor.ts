import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from "rxjs";
import {TokenService} from "../services/token.service";

@Injectable()
export class jWTInterceptor implements HttpInterceptor {
  constructor(
    private tokenService: TokenService
  ) {
  }

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    if (
      request.urlWithParams.indexOf('api/') &&
      !request.urlWithParams.endsWith('/api/authorisatie/inloggen') &&
      !request.urlWithParams.endsWith('/api/authorisatie/renewjwt') &&
      !request.urlWithParams.endsWith('/api/gezin/activeringscode/genereer') &&
      !request.urlWithParams.endsWith('/api/gezin/activeringscode/controleer') &&
      !request.urlWithParams.endsWith('/api/gezin/wijzigwachtwoordcode/genereer') &&
      !request.urlWithParams.endsWith('/api/gezin/wijzigwachtwoordcode/controleer') &&
      !request.urlWithParams.endsWith('/api/gezin/wijzigwachtwoord') &&
      (request.urlWithParams.indexOf('/wachtwoordsterkte/') == -1) &&
      !(request.urlWithParams.endsWith('/api/gezin') && request.method.toUpperCase() == 'PUT')
    ) {
      this.tokenService.accessToken.subscribe((token: string | null) => {
        request = this.headersToevoegen(request, token);
        return next.handle(request);
      });
    }
    return next.handle(request);
  }

  private headersToevoegen(request: HttpRequest<any>, accessToken: string | null): HttpRequest<any> {
    return request.clone({
      setHeaders: {
        Authorization: `${accessToken}`,
        'Access-Control-Expose-Headers': 'Authorization'
      }
    });
  }
}

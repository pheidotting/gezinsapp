package nl.gezinsapp.model;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.Map;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode
public abstract class CommObject {
    protected String emailadres;
    protected Long gezinsId;
    private Long gezinslidId;

    public abstract Map<String, Object> velden();
}

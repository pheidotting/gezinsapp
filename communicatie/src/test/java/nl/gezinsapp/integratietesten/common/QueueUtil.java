package nl.gezinsapp.integratietesten.common;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DeliverCallback;
import io.vertx.core.json.JsonObject;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;

import static java.util.Optional.ofNullable;
import static java.util.concurrent.TimeUnit.MINUTES;
import static org.awaitility.Awaitility.await;

@Slf4j
public abstract class QueueUtil<T> {
    public abstract String getExchange();

    public abstract Class<T> getClazz();

    private ObjectMapper objectMapper;

    @SneakyThrows
    public QueueUtil() {
        objectMapper = new ObjectMapper();
        getChannel().exchangeDeclare(getExchange(), "fanout", true);
        declareQueue();
    }

    @SneakyThrows
    private Channel getChannel() {
        if (TestGegevens.getRabbitHost() == null) {
            throw new NullPointerException("TestGegevens.rabbitHost (nog) niet gezet");
        }
        if (TestGegevens.getChannel() == null) {
            var factory = new ConnectionFactory();
            factory.setHost(TestGegevens.getRabbitHost());
            factory.setPort(TestGegevens.getRabbitPort());
            factory.setUsername(TestGegevens.getRabbitUser());
            factory.setPassword(TestGegevens.getRabbitPassword());
            var connection = factory.newConnection();
            TestGegevens.setChannel(connection.createChannel());
        }
        return (Channel) TestGegevens.getChannel();
    }

    @SneakyThrows
    private void declareQueue() {
        var channel = getChannel();
        var queueName = channel.queueDeclare().getQueue();
        channel.queueBind(queueName, getExchange(), "");

        TestGegevens.getQueues().put(getExchange(), queueName);
    }

    @SneakyThrows
    public Optional<T> vangBericht() {
        var queueName = TestGegevens.getQueues().get(getExchange());
        log.info("Wachten op een bericht op " + queueName + " via " + getExchange());
        AtomicReference<T> result = new AtomicReference<>();
        DeliverCallback deliverCallback = (consumerTag, delivery) -> {
            var message = new String(delivery.getBody(), "UTF-8");
            result.set(objectMapper.readValue(message, getClazz()));
            log.info(" [x] Received '" + (T) result.get() + "'");
        };

        getChannel().basicConsume(queueName, true, deliverCallback, consumerTag -> {
        });
        await().atMost(2, MINUTES).until(() -> result.get() != null);
        declareQueue();
        return ofNullable(result.get());
    }

    @SneakyThrows
   public void stuurBerichtNaarExchange(T bericht) {
        stuurBerichtNaarExchange(objectMapper.writeValueAsString(bericht));
    }
    @SneakyThrows
    public  void stuurBerichtNaarExchange(String berichtTekst) {
        log.info("Stuur naar {} : ", getExchange());
        log.info(berichtTekst);
        getChannel().basicPublish(getExchange(), "", null, berichtTekst.getBytes());
    }
}

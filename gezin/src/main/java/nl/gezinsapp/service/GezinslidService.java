package nl.gezinsapp.service;

import jakarta.enterprise.context.ApplicationScoped;
import lombok.RequiredArgsConstructor;
import nl.gezinsapp.commons.aspect.MethodeLogging;
import nl.gezinsapp.messaging.model.GezinslidVerwijderd;
import nl.gezinsapp.messaging.sender.Sender;
import nl.gezinsapp.model.Gezinslid;
import nl.gezinsapp.model.repository.GezinRepository;
import nl.gezinsapp.model.repository.GezinslidRepository;

@ApplicationScoped
@RequiredArgsConstructor
public class GezinslidService {
    private final GezinslidRepository gezinslidRepository;
    private final GezinRepository gezinRepository;
    private final Sender sender;

    @MethodeLogging
    public void opslaan(Gezinslid gezinslid, Long gezinsId) {
        gezinslid.setGezin(gezinRepository.findById(gezinsId));
        gezinslidRepository.persist(gezinslid);
    }

    @MethodeLogging
    public void verwijder(Long id) {
        var lid = gezinslidRepository.findById(id);
        var gezin = gezinRepository.findById(lid.getGezin().getId());

        gezin.getGezinsleden().remove(gezin.getGezinsleden().stream().filter(gezinslid -> gezinslid.getId().equals(id)).findFirst().get());

        gezinRepository.persist(gezin);

        sender.verstuurGezinslidVerwijderd(new GezinslidVerwijderd(id));
    }
}

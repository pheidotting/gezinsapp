package nl.gezinsapp.integratietest;

import com.fasterxml.jackson.core.JsonProcessingException;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.openapi.quarkus.adresboek_openapi_yaml.model.AlgemeneResponse;

import static nl.gezinsapp.integratietest.random.RandomAdresBoek.*;
import static org.jboss.resteasy.reactive.RestResponse.StatusCode.FORBIDDEN;
import static org.jboss.resteasy.reactive.RestResponse.StatusCode.OK;
import static org.junit.jupiter.api.Assertions.*;

@Slf4j
@QuarkusTest
@QuarkusTestResource(MockServer.class)
class AdresWijzigenIntegratieTest extends AbstractIntegratieTest {
    @Test
    @DisplayName("Sla een gewijzigd Adres op")
    void test() throws JsonProcessingException {
        var accesstoken = maakToken();
        var jwtResponse = mockCheckJwt(accesstoken);

        maakDatabaseLeeg();

        var item = randomAdresBoek();
        item.setGezin(jwtResponse.getGebruiker().getGezin());
        opslaan(item);

        var adres = randomAdres();
        adres.setAdresBoek(item);
        opslaan(adres);

        var gewijzigd = randomDtoAdres();
        gewijzigd.setId(adres.getId());
        gewijzigd.setAdresBoek(item.getId());
        var response = patchViaApi(AlgemeneResponse.class, "/api/adresboek/adres", gewijzigd, accesstoken, OK);

        var itemGelezen = leesAdres(adres.getId());
        assertNotNull(itemGelezen);
        assertEquals(gewijzigd.getPlaats(), itemGelezen.getPlaats());
        assertEquals(gewijzigd.getPostcode().substring(0, 6), itemGelezen.getPostcode());
        assertEquals(gewijzigd.getHuisnummer().longValue(), itemGelezen.getHuisnummer().longValue());
        assertEquals(gewijzigd.getStraat(), itemGelezen.getStraat());
        assertEquals(gewijzigd.getToevoeging(), itemGelezen.getToevoeging());
    }

    @Test
    @DisplayName("Sla een gewijzigd Adres op, gezin klopt niet")
    void testFoutGezin() throws JsonProcessingException {
        var accesstoken = maakToken();
        mockCheckJwt(accesstoken);

        maakDatabaseLeeg();

        var item = randomAdresBoek();
        item.setGezin(99L);
        opslaan(item);

        var adres = randomAdres();
        adres.setAdresBoek(item);
        opslaan(adres);

        var gewijzigd = randomDtoAdres();
        gewijzigd.setId(adres.getId());
        gewijzigd.setAdresBoek(item.getId());
        assertNull(patchViaApi(null, "/api/adresboek/adres", gewijzigd, accesstoken, FORBIDDEN));
    }
}

import {fakeAsync, TestBed, tick} from '@angular/core/testing';
import {ActivatedRoute, provideRouter, Router, RouterModule} from "@angular/router";
import {of} from "rxjs";
import {provideHttpClientTesting} from "@angular/common/http/testing";
import {provideHttpClient, withInterceptorsFromDi} from '@angular/common/http';
import {BoodschappenWidgetComponent} from "./boodschappen-widget.component";
import {By} from "@angular/platform-browser";

describe('BoodschappenWidgetComponent', () => {
  let router: Router;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [BoodschappenWidgetComponent,
        RouterModule.forRoot([])],
      providers: [provideRouter([]), provideHttpClient(withInterceptorsFromDi()), provideHttpClientTesting()]
    });
    router = TestBed.inject(Router);
  });

  it('should create', fakeAsync(() => {
    let fixture = TestBed.createComponent(BoodschappenWidgetComponent);

    let app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));

  it('should load data on startup and show table', fakeAsync(() => {
    let fixture = TestBed.createComponent(BoodschappenWidgetComponent);

    let activatedRoute: ActivatedRoute = fixture.debugElement.injector.get(ActivatedRoute);
    activatedRoute.data = of({
      boodschappen: {
        boodschappenitems: [
          {
            omschrijving: 'Rock & Roll',
            winkel: 'Sligro',
          },
          {
            omschrijving: 'Bier'
          },
          {
            omschrijving: 'Patat',
            winkel: 'Jumbo'
          }
        ]
      }
    });

    fixture.detectChanges();
    tick();

    let app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
    expect(app.boodschappen.length).toBe(3);
    expect(app.boodschappen[0].omschrijving).toBe('Bier');
    expect(app.boodschappen[1].omschrijving).toBe('Patat');
    expect(app.boodschappen[2].omschrijving).toBe('Rock & Roll');
    let boodschappentabel = fixture.debugElement.query(By.css('#boodschappentabel'));
    expect(boodschappentabel).toBeTruthy();
  }));
  it('should load data on startup and not show table', fakeAsync(() => {
    let fixture = TestBed.createComponent(BoodschappenWidgetComponent);

    let activatedRoute: ActivatedRoute = fixture.debugElement.injector.get(ActivatedRoute);
    activatedRoute.data = of({
      boodschappen: {
        boodschappenitems: []
      }
    });

    fixture.detectChanges();
    tick();

    let app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
    expect(app.boodschappen.length).toBe(0);
    let boodschappentabel = fixture.debugElement.query(By.css('#boodschappentabel'));
    expect(boodschappentabel).toBeFalsy();
  }));

});

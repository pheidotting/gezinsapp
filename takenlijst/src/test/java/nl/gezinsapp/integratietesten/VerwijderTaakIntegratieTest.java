package nl.gezinsapp.integratietesten;

import com.fasterxml.jackson.core.JsonProcessingException;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.openapi.quarkus.takenlijst_openapi_yaml.model.AlgemeneResponse;

import static nl.gezinsapp.integratietesten.random.RandomTaak.randomDomainTaak;
import static org.jboss.resteasy.reactive.RestResponse.StatusCode.FORBIDDEN;
import static org.jboss.resteasy.reactive.RestResponse.StatusCode.OK;
import static org.junit.Assert.assertNull;

@Slf4j
@QuarkusTest
@QuarkusTestResource(MockServer.class)
class VerwijderTaakIntegratieTest extends AbstractIntegratieTest {
    @Test
    @DisplayName("Verwijder een Taak")
    void test() throws JsonProcessingException {
        maakDatabaseLeeg();

        var accesstoken = maakToken();
        var jwtResponse = mockCheckJwt(accesstoken);

        var taak = randomDomainTaak();

        taak.setGezin(jwtResponse.getGebruiker().getGezin());
        opslaan(taak);

        deleteViaApi(AlgemeneResponse.class, "/api/takenlijst/taak", taak.getId(), accesstoken, OK);

        var itemGelezen = leesTaak(taak.getId());
        assertNull(itemGelezen);
    }

    @Test
    @DisplayName("Verwijder een Taak, maar Gezin is niet ingelogd Gezin")
    void testFoutGezin() throws JsonProcessingException {
        maakDatabaseLeeg();

        var accesstoken = maakToken();
        mockCheckJwt(accesstoken);

        var taak = randomDomainTaak();

        taak.setGezin(99L);
        opslaan(taak);

        assertNull(deleteViaApi(null, "/api/takenlijst/taak", taak.getId(), accesstoken, FORBIDDEN));
    }
}

package nl.gezinsapp.model.repository;

import io.quarkus.redis.datasource.RedisDataSource;
import io.quarkus.redis.datasource.value.ValueCommands;
import io.vertx.redis.client.RedisAPI;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import nl.gezinsapp.model.RefreshToken;

import java.util.Optional;
import java.util.UUID;

import static java.util.Optional.ofNullable;

@ApplicationScoped
public class RefreshTokenRepository {
    private ValueCommands<UUID, RefreshToken> valueCommands;
    private ValueCommands<String, RefreshToken> valueCommandsAccessToken;
    private ValueCommands<Long, RefreshToken> valueCommandsOpGezinslid;

    @Inject
    RedisAPI lowLevelClient;

    public RefreshTokenRepository(RedisDataSource ds) {
        valueCommands = ds.value(UUID.class, RefreshToken.class);
        valueCommandsAccessToken = ds.value(String.class, RefreshToken.class);
        valueCommandsOpGezinslid = ds.value(Long.class, RefreshToken.class);
    }

    public void set(RefreshToken refreshToken, UUID token) {
        valueCommands.setex(token, 86400L, refreshToken);
        valueCommandsAccessToken.setex(refreshToken.getAccessToken(), 86400L, refreshToken);
        valueCommandsOpGezinslid.setex(refreshToken.getUserId(), 86400L, refreshToken);
    }

    public Optional<RefreshToken> get(UUID key) {
        return ofNullable(valueCommands.get(key));
    }

    public Optional<RefreshToken> get(String key) {
        return ofNullable(valueCommandsAccessToken.get(key));
    }

    public Optional<RefreshToken> get(Long gezinslid) {
        return ofNullable(valueCommandsOpGezinslid.get(gezinslid));
    }

    public void remove(UUID token) {
        var refreshToken = get(token);
        valueCommands.getdel(token);
        valueCommandsAccessToken.getdel(refreshToken.get().getAccessToken());
        valueCommandsOpGezinslid.getdel(refreshToken.get().getUserId());
    }

    public void remove(String accessToken) {
        var refreshToken = get(accessToken);
        valueCommandsAccessToken.getdel(refreshToken.get().getAccessToken());
        valueCommandsOpGezinslid.getdel(refreshToken.get().getUserId());
    }
}

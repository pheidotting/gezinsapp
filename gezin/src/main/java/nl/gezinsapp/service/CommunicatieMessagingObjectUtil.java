package nl.gezinsapp.service;

import nl.gezinsapp.messaging.model.AbstractMessagingObject;
import nl.gezinsapp.model.Gebruiker;
import nl.gezinsapp.model.Gezinslid;
import org.slf4j.MDC;

import java.util.UUID;

public class CommunicatieMessagingObjectUtil {
    public static <T extends AbstractMessagingObject> T vulObject(Gebruiker gebruiker, T t) {
        if (gebruiker != null) {
            t.setEmailadres(gebruiker.getEmailadres());
            if (gebruiker instanceof Gezinslid gezinslid) {
                t.setGezinsId(gezinslid.getGezin().getId());
            }
            t.setGezinslidId(gebruiker.getId());
        }
        var trackAndTraceId = MDC.get("trackAndTraceId");
        if (trackAndTraceId != null) {
            t.setTrackAndTraceId(UUID.fromString(trackAndTraceId));
        }

        return t;
    }

}

package nl.gezinsapp.integratietesten.verlanglijstzonderautorisatie;

import io.quarkus.test.junit.QuarkusTest;
import lombok.extern.slf4j.Slf4j;
import nl.gezinsapp.integratietesten.AbstractIntegratieTest;
import nl.gezinsapp.integratietesten.MockServer;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.openapi.quarkus.verlanglijstje_openapi_yaml.model.Verlanglijstje;

import static nl.gezinsapp.integratietesten.random.RandomVerlanglijst.randomDomainVerlanglijst;
import static nl.gezinsapp.integratietesten.random.RandomVerlanglijstItem.randomDomainVerlanglijstItem;
import static nl.gezinsapp.integratietesten.random.RandomVerlanglijstItem.randomMsgVerlanglijstItem;
import static org.junit.jupiter.api.Assertions.*;
import io.quarkus.test.common.QuarkusTestResource;

@Slf4j
@QuarkusTest
@QuarkusTestResource(MockServer.class)
class LeesVerlanglijstjeIntegratieTest extends AbstractIntegratieTest {
    @Test
    @DisplayName("Lees een verlanglijstje met code zonder ingelogd te zijn")
    void test() {
        maakDatabaseLeeg();

        var verlanglijst = randomDomainVerlanglijst(99L);
        assertNotNull(verlanglijst.getCode());
        opslaan(verlanglijst);

        var domainItem = randomDomainVerlanglijstItem();
        domainItem.setVerlanglijst(verlanglijst);
        opslaan(domainItem);

        var response = leesViaApi(Verlanglijstje.class, "/api/verlanglijstje/verlanglijstje-zonder-autorisatie/" + verlanglijst.getCode(), null);
        assertNotNull(response);
        assertEquals(verlanglijst.getOmschrijving(), response.getOmschrijving());
        assertEquals(1, response.getVerlanglijstItems().size());
        assertEquals(domainItem.getOmschrijving(), response.getVerlanglijstItems().get(0).getOmschrijving());
    }

    @Test
    @DisplayName("Lees een verlanglijstje met code zonder ingelogd te zijn met onjuiste code")
    void test2() {
        maakDatabaseLeeg();

        var verlanglijst = randomDomainVerlanglijst(99L);
        assertNotNull(verlanglijst.getCode());
        opslaan(verlanglijst);

        var domainItem = randomDomainVerlanglijstItem();
        domainItem.setVerlanglijst(verlanglijst);
        opslaan(domainItem);

        var response = leesViaApi(Verlanglijstje.class, "/api/verlanglijstje/verlanglijstje-zonder-autorisatie/abc", null);
        assertNotNull(response);
        assertNull(response.getId());
        assertNull(response.getOmschrijving());
    }
}

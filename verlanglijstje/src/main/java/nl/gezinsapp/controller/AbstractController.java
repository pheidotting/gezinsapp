package nl.gezinsapp.controller;

import nl.gezinsapp.aspect.ThreadVariabelen;
import nl.gezinsapp.domain.Verlanglijst;
import nl.gezinsapp.domain.VerlanglijstItem;
import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.openapi.quarkus.gezin_openapi_yaml.api.GezinApi;
import org.openapi.quarkus.gezin_openapi_yaml.model.Gezinslid;

public abstract class AbstractController {
    @RestClient
    protected GezinApi gezinApi;

    protected boolean isVanHuidigGezin(Verlanglijst verlanglijst) {
        var gezinsledenIds = gezinApi.leesGezin(ThreadVariabelen.get().getGezin()).getGezinsleden().stream()
                .map(Gezinslid::getId).toList();
        return gezinsledenIds.contains(verlanglijst.getGezinslid());
    }

    protected boolean isVanHuidigGezin(VerlanglijstItem verlanglijstItem) {
        var gezinsledenIds = gezinApi.leesGezin(ThreadVariabelen.get().getGezin()).getGezinsleden().stream()
                .map(Gezinslid::getId).toList();
        return gezinsledenIds.contains(verlanglijstItem.getVerlanglijst().getGezinslid());
    }
}

package nl.gezinsapp.client;

import io.vertx.core.json.JsonObject;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.QueryParam;
import org.eclipse.microprofile.rest.client.annotation.ClientHeaderParam;
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

@RegisterRestClient(configKey = "postcode-api")
public interface PostcodeClient {
    @GET
    @ClientHeaderParam(name = "token", value = "f3cec253-8d9f-4f87-99b1-408293ff1c9e")
    JsonObject haalAdresOp(@QueryParam("postcode") String postcode, @QueryParam("number") String huisnummer);

}

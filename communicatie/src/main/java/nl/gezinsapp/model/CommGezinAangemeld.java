package nl.gezinsapp.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.HashMap;
import java.util.Map;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class CommGezinAangemeld extends CommObject {
    private String naamGezin;

    public CommGezinAangemeld(String emailadres, Long gezinsId, Long gezinslidId) {
        super(emailadres, gezinsId, gezinslidId);
    }

    public Map<String, Object> velden() {
        Map<String, Object> velden = new HashMap<>();
        velden.put("emailadres", emailadres);
        velden.put("naamGezin", naamGezin);
        return velden;
    }
}

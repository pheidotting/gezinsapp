import {ComponentFixture, fakeAsync, TestBed} from '@angular/core/testing';

import {BoodschappenComponent} from './boodschappen.component';
import {of} from "rxjs";
import {ActivatedRoute, provideRouter, Router, RouterModule} from "@angular/router";
import {HttpTestingController, provideHttpClientTesting} from "@angular/common/http/testing";
import {BoodschappenItem} from '../../api/boodschappen/model/boodschappenItem';
import {provideHttpClient, withInterceptorsFromDi} from '@angular/common/http';

describe('BoodschappenComponent', () => {
  let router: Router;
  let fixture: ComponentFixture<BoodschappenComponent>;
  let activatedRoute: ActivatedRoute;
  let app: BoodschappenComponent;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [BoodschappenComponent,
        RouterModule.forRoot([])],
      providers: [provideRouter([]), provideHttpClient(withInterceptorsFromDi()), provideHttpClientTesting()]
    });
    router = TestBed.inject(Router);

    fixture = TestBed.createComponent(BoodschappenComponent);
    activatedRoute = fixture.debugElement.injector.get(ActivatedRoute);
    // httpTestingController = TestBed.inject(HttpTestingController);

    activatedRoute.data = of({
      boodschappen: {
        boodschappenitems: [{omschrijving: "Brood"}]
      }
    });

    fixture.detectChanges();

    app = fixture.debugElement.componentInstance;
  });
  it('should create', fakeAsync(() => {
    expect(app).toBeTruthy();
  }));

  it('Test ophalen gegevens', fakeAsync(() => {
    expect(app.boodschappen.length).toEqual(1);
    expect(app.boodschappen[0].omschrijving).toEqual('Brood');
  }));

  it('Test opslaan nieuwe item', fakeAsync(() => {
    let httpTestingController: HttpTestingController = TestBed.inject(HttpTestingController);
    app.nieuwewinkel = 'jumbo';
    app.nieuweomschrijving = 'brood';

    app.voegItemToe();

    const reqPutBoodschappenItem = httpTestingController.expectOne((req) => {
      return req.url.endsWith('/api/boodschappen/item');
    });
    expect(reqPutBoodschappenItem.request.method).toEqual('PUT');
    reqPutBoodschappenItem.flush({
      id: 10,
      foutmeldingen: {meldingen: []}
    });

    expect(app.boodschappen.length).toEqual(2);
  }));
  it('Test opslaan nieuwe item', fakeAsync(() => {
    let httpTestingController: HttpTestingController = TestBed.inject(HttpTestingController);
    app.nieuwewinkel = 'jumbo';
    app.nieuweomschrijving = 'brood';

    app.onKeyDown({key: 'Enter'});

    const reqPutBoodschappenItem = httpTestingController.expectOne((req) => {
      return req.url.endsWith('/api/boodschappen/item');
    });
    expect(reqPutBoodschappenItem.request.method).toEqual('PUT');
    reqPutBoodschappenItem.flush({
      id: 10,
      foutmeldingen: {meldingen: []}
    });

    expect(app.boodschappen.length).toEqual(2);
  }));

  it('vinkItemAf', fakeAsync(() => {
    let httpTestingController: HttpTestingController = TestBed.inject(HttpTestingController);
    let item: BoodschappenItem = {
      omschrijving: 'Ja'
    };
    app.nieuwewinkel = 'jumbo';
    app.nieuweomschrijving = 'brood';

    app.vinkItemAf(item);
    const reqPatchBoodschappenItem = httpTestingController.expectOne((req) => {
      return req.url.endsWith('/api/boodschappen/item');
    });
    expect(reqPatchBoodschappenItem.request.method).toEqual('PATCH');
    expect(JSON.stringify(reqPatchBoodschappenItem.request.body)).toEqual('{"omschrijving":"Ja","gehaald":true}');
    reqPatchBoodschappenItem.flush({
      foutmeldingen: {meldingen: []}
    });
  }));

  it('wijzigItem', () => {
    expect(app.wijzigId).toBeUndefined();

    app.wijzigItem({
      id: 4,
      omschrijving: ''
    })

    expect(app.wijzigId).toEqual(4);
  });

  it('opslaanItem', () => {
    let httpTestingController: HttpTestingController = TestBed.inject(HttpTestingController);

    let item: BoodschappenItem = {
      omschrijving: 'Ja'
    };
    app.nieuwewinkel = 'jumbo';
    app.nieuweomschrijving = 'brood';

    app.opslaanItem(item);
    const reqPatchBoodschappenItem = httpTestingController.expectOne((req) => {
      return req.url.endsWith('/api/boodschappen/item');
    });
    expect(reqPatchBoodschappenItem.request.method).toEqual('PATCH');
    expect(JSON.stringify(reqPatchBoodschappenItem.request.body)).toEqual('{"omschrijving":"Ja"}');
    reqPatchBoodschappenItem.flush({
      foutmeldingen: {meldingen: []}
    });
  });

  it('winkels', () => {
    app.boodschappen.push({
      winkel: 'Jumbo',
      omschrijving: ''
    }, {
      winkel: 'Lidl',
      omschrijving: ''
    }, {
      winkel: 'Jumbo',
      omschrijving: ''
    });

    let result: string[] = app.winkels();
    expect(result.length).toEqual(2);
    expect(result).toContain('Jumbo');
    expect(result).toContain('Lidl');
  });

  it('filterOpWinkel', () => {
    app.boodschappen = [];
    app.boodschappen.push({
      winkel: 'Jumbo',
      omschrijving: ''
    }, {
      winkel: 'Lidl',
      omschrijving: ''
    }, {
      winkel: 'Jumbo',
      omschrijving: ''
    });
    app.teFilterenWinkel = 'Jumbo';

    app.filterOpWinkel();

    expect(app.gefilterdeLijstBoodschappen.length).toEqual(2);

    app.teFilterenWinkel = 'Lidl';

    app.filterOpWinkel();

    expect(app.gefilterdeLijstBoodschappen.length).toEqual(1);

    app.teFilterenWinkel = '';

    app.filterOpWinkel();

    expect(app.gefilterdeLijstBoodschappen.length).toEqual(3);
  });
});

package nl.gezinsapp.controller;

import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.gezinsapp.commons.aspect.MethodeLogging;
import nl.gezinsapp.commons.aspect.TrackAndTraceId;
import nl.gezinsapp.mapper.GebruikerMapper;
import org.openapi.quarkus.gezin_openapi_yaml.api.GebruikerApi;
import org.openapi.quarkus.gezin_openapi_yaml.model.Gebruiker;

@Slf4j
@RequiredArgsConstructor
public class GebruikerController implements GebruikerApi {
    private final GebruikerMapper gebruikerMapper;

    @Override
    @Transactional
    @MethodeLogging
    @TrackAndTraceId
    public Gebruiker leesGebruiker(String email) {
        return gebruikerMapper.map(nl.gezinsapp.model.Gebruiker.find("emailadres", email).firstResult());
    }

    @Override
    @Transactional
    @MethodeLogging
    @TrackAndTraceId
    public Gebruiker leesGebruikerOpId(Long id) {
        return gebruikerMapper.map(nl.gezinsapp.model.Gebruiker.findById(id));
    }
}

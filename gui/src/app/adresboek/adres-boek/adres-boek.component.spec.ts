import {fakeAsync, flush, TestBed, tick} from '@angular/core/testing';

import {AdresBoekComponent} from './adres-boek.component';
import {AdresboekService} from '../../../api/adresboek';
import {ActivatedRoute, provideRouter, Router, RouterModule} from "@angular/router";
import {of} from "rxjs";
import {HttpTestingController, provideHttpClientTesting} from "@angular/common/http/testing";
import {By} from "@angular/platform-browser";
import {provideHttpClient, withInterceptorsFromDi} from '@angular/common/http';

describe('AdresBoekComponent', () => {
  let router: Router;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [AdresBoekComponent,
        RouterModule.forRoot([])],
      providers: [provideRouter([]), provideHttpClient(withInterceptorsFromDi()), provideHttpClientTesting()]
    });
    router = TestBed.inject(Router);
  });
  it('should create', fakeAsync(() => {
    let fixture = TestBed.createComponent(AdresBoekComponent);

    let app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));

  it('should load data on startup', fakeAsync(() => {
    let fixture = TestBed.createComponent(AdresBoekComponent);

    let activatedRoute: ActivatedRoute = fixture.debugElement.injector.get(ActivatedRoute);

    activatedRoute.data = of({
      adresBoek: {
        omschrijving: 'omschrijving',
        adressen: [{
          'naam': 'naampje'
        }]
      }
    });

    fixture.detectChanges();
    tick();

    let app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
    expect(app.adressen.length).toBe(1);
    expect(app.adressen[0].naam).toBe('naampje');
    expect(app.adresBoekForm.value.omschrijving).toBe('omschrijving');
  }));

  it('opslaanAdresBoek, nieuw adresBoek', fakeAsync(() => {
    let fixture = TestBed.createComponent(AdresBoekComponent);
    let httpTestingController = TestBed.inject(HttpTestingController);

    let activatedRoute: ActivatedRoute = fixture.debugElement.injector.get(ActivatedRoute);
    const routerSpy = spyOn(router, 'navigate');

    activatedRoute.data = of({
      adresBoek: {
        omschrijving: 'omschrijving',
        adressen: [{
          'naam': 'naampje'
        }]
      }
    });

    let app = fixture.debugElement.componentInstance;
    app.opslaanAdresBoek();

    const reqPutAdresBoek = httpTestingController.expectOne((req) => {
      return req.url.endsWith('/api/adresboek/adresboek');
    });
    expect(reqPutAdresBoek.request.method).toEqual('PUT');
    reqPutAdresBoek.flush({
      foutmeldingen: {
        meldingen: []
      }
    });

    fixture.detectChanges();
    tick();

    expect(app).toBeTruthy();
    expect(app.adressen.length).toBe(1);
    expect(app.adressen[0].naam).toBe('naampje');
    expect(app.adresBoekForm.value.omschrijving).toBe('omschrijving');
    expect(routerSpy).toHaveBeenCalledWith(['adresboek']);
  }));

  it('opslaanAdresBoek, bestaand adresBoek', fakeAsync(() => {
    let fixture = TestBed.createComponent(AdresBoekComponent);
    let httpTestingController = TestBed.inject(HttpTestingController);

    let activatedRoute: ActivatedRoute = fixture.debugElement.injector.get(ActivatedRoute);
    const routerSpy = spyOn(router, 'navigate');

    activatedRoute.data = of({
      adresBoek: {
        omschrijving: 'omschrijving',
        adressen: [{
          'naam': 'naampje'
        }]
      }
    });

    let app = fixture.debugElement.componentInstance;
    app.editMode = true;
    app.opslaanAdresBoek();

    const reqPutAdresBoek = httpTestingController.expectOne((req) => {
      return req.url.endsWith('/api/adresboek/adresboek');
    });
    expect(reqPutAdresBoek.request.method).toEqual('PATCH');
    reqPutAdresBoek.flush({
      foutmeldingen: {
        meldingen: []
      }
    });

    fixture.detectChanges();
    tick();

    expect(app).toBeTruthy();
    expect(app.adressen.length).toBe(1);
    expect(app.adressen[0].naam).toBe('naampje');
    expect(app.adresBoekForm.value.omschrijving).toBe('omschrijving');
    expect(routerSpy).toHaveBeenCalledWith(['adresboek']);
  }));

  it('opslaanAdres, niks goed, dus alle foutmeldingen worden getoond', fakeAsync(() => {
    let fixture = TestBed.createComponent(AdresBoekComponent);

    let activatedRoute: ActivatedRoute = fixture.debugElement.injector.get(ActivatedRoute);
    let adresboekService: AdresboekService = fixture.debugElement.injector.get(AdresboekService);
    let adresToevoegenSpy = spyOn(adresboekService, 'adresToevoegen');
    let wijzigAdresSpy = spyOn(adresboekService, 'wijzigAdres');

    activatedRoute.data = of({
      adresBoek: {
        omschrijving: 'omschrijving',
        adressen: [{
          'naam': 'naampje'
        }]
      }
    });

    let app = fixture.debugElement.componentInstance;
    app.editMode = true;

    app.toevoegTonen = true;
    fixture.detectChanges();
    app.adresForm.controls.postcode.setValue('1234');

    let compiled = fixture.debugElement.nativeElement;
    let btn = fixture.debugElement.queryAll(By.css('button[name="opslaanButton"]'));
    btn[0].nativeElement.click();

    fixture.detectChanges();
    tick();

    expect(app).toBeTruthy();
    expect(app.adressen.length).toBe(1);
    expect(app.adressen[0].naam).toBe('naampje');
    expect(app.adresBoekForm.value.omschrijving).toBe('omschrijving');
    expect(adresToevoegenSpy).not.toHaveBeenCalled();
    expect(wijzigAdresSpy).not.toHaveBeenCalled();

    expect(app.foutmeldingen.length).toBe(5);
    expect(app.foutmeldingen[0]).toBe('Postcode 1234 is niet in formaat 1111AA');
    expect(app.foutmeldingen[1]).toBe('Huisnummer moet een getal zijn');
    expect(app.foutmeldingen[2]).toBe('Naam moet gevuld zijn');
    expect(app.foutmeldingen[3]).toBe('Straat moet gevuld zijn');
    expect(app.foutmeldingen[4]).toBe('Plaats moet gevuld zijn');

    expect(compiled.querySelector('.alert-danger').textContent)
      .toContain('Sorry, er ging iets mis - Postcode 1234 is niet in formaat 1111AA - Huisnummer moet een getal zijn - Naam moet gevuld zijn - Straat moet gevuld zijn - Plaats moet gevuld zijn');
  }));

  it('opslaanAdres, opslaan nieuw adres, zonder foutmeldingen', fakeAsync(() => {
    let fixture = TestBed.createComponent(AdresBoekComponent);
    let httpTestingController = TestBed.inject(HttpTestingController);

    let activatedRoute: ActivatedRoute = fixture.debugElement.injector.get(ActivatedRoute);
    let adresboekService: AdresboekService = fixture.debugElement.injector.get(AdresboekService);
    let wijzigAdresSpy = spyOn(adresboekService, 'wijzigAdres');

    activatedRoute.data = of({
      adresBoek: {
        omschrijving: 'omschrijving',
        adressen: [{
          'naam': 'naampje'
        }]
      }
    });

    let app = fixture.debugElement.componentInstance;
    app.toevoegTonen = true;
    app.editMode = true;
    app.adresForm.controls.postcode.setValue('1234AB');
    app.adresForm.controls.huisnummer.setValue('12');
    app.adresForm.controls.straat.setValue('straatnaam');
    app.adresForm.controls.plaats.setValue('plaats');
    app.adresForm.controls.naam.setValue('naam');
    fixture.detectChanges();
    app.id = 6;

    let btn = fixture.debugElement.queryAll(By.css('button[name="opslaanButton"]'));
    btn[0].nativeElement.click();

    tick();

    expect(app).toBeTruthy();
    expect(app.adressen.length).toBe(1);
    expect(app.adressen[0].naam).toBe('naampje');
    expect(app.adresBoekForm.value.omschrijving).toBe('omschrijving');
    expect(wijzigAdresSpy).not.toHaveBeenCalled();
    const reqToevoegenAdres = httpTestingController.expectOne((req) => {
      return req.url.endsWith('/api/adresboek/adres');
    });
    expect(reqToevoegenAdres.request.method).toEqual('PUT');
    reqToevoegenAdres.flush({
      foutmeldingen: {
        meldingen: []
      }
    });
    const reqLeesAdres = httpTestingController.expectOne((req) => {
      return req.url.endsWith('/api/adresboek/adresboek/' + 6);
    });
    expect(reqLeesAdres.request.method).toEqual('GET');
    reqLeesAdres.flush([]);

    expect(app.foutmeldingen.length).toBe(0);
  }));
  it('opslaanAdres, opslaan bestaand adres, zonder foutmeldingen', fakeAsync(() => {
    let fixture = TestBed.createComponent(AdresBoekComponent);
    let httpTestingController = TestBed.inject(HttpTestingController);

    let activatedRoute: ActivatedRoute = fixture.debugElement.injector.get(ActivatedRoute);
    let adresboekService: AdresboekService = fixture.debugElement.injector.get(AdresboekService);
    let adresToevoegenSpy = spyOn(adresboekService, 'adresToevoegen');

    activatedRoute.data = of({
      adresBoek: {
        omschrijving: 'omschrijving',
        adressen: [{
          'naam': 'naampje'
        }]
      }
    });

    let app = fixture.debugElement.componentInstance;
    app.toevoegTonen = true;
    app.editMode = true;
    app.adresForm.controls.postcode.setValue('1234AB');
    app.adresForm.controls.huisnummer.setValue('12');
    app.adresForm.controls.straat.setValue('straatnaam');
    app.adresForm.controls.plaats.setValue('plaats');
    app.adresForm.controls.naam.setValue('naam');
    app.adresForm.controls.id.setValue(1);
    fixture.detectChanges();
    app.id = 5;

    let btn = fixture.debugElement.queryAll(By.css('button[name="opslaanButton"]'));
    btn[0].nativeElement.click();

    tick();

    expect(app).toBeTruthy();
    expect(app.adressen.length).toBe(1);
    expect(app.adressen[0].naam).toBe('naampje');
    expect(app.adresBoekForm.value.omschrijving).toBe('omschrijving');
    expect(adresToevoegenSpy).not.toHaveBeenCalled();

    const reqWijzigAdres = httpTestingController.expectOne((req) => {
      return req.url.endsWith('/api/adresboek/adres');
    });
    expect(reqWijzigAdres.request.method).toEqual('PATCH');
    reqWijzigAdres.flush({
      foutmeldingen: {
        meldingen: []
      }
    });
    const reqLeesAdres = httpTestingController.expectOne((req) => {
      return req.url.endsWith('/api/adresboek/adresboek/' + 5);
    });
    expect(reqLeesAdres.request.method).toEqual('GET');
    reqLeesAdres.flush([]);

    expect(app.foutmeldingen.length).toBe(0);

    flush();
  }));
});

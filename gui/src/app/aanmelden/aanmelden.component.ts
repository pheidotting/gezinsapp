import {Component, ElementRef, Output, ViewChild} from '@angular/core';
import {
  AanmeldenGezin,
  ActiveringsCode,
  ActiveringsCodeService,
  AlgemeneResponse,
  ControleerActiveringsCode,
  ControleerActiveringsCodeResultaat,
  GezinService,
  Gezinslid,
  GezinslidGeslachtEnum,
  GezinslidSoortGezinslidEnum,
  VraagActiveringsCodeOp
} from "../../api/gezin";
import {Router} from "@angular/router";
import {Subject} from "rxjs";
import {Foutmeldingen} from "../model/foutmeldingen";
import {WachtwoordSterkte} from '../../api/authenticatie/model/wachtwoordSterkte';
import {WachtwoordSterkteService} from '../../api/authenticatie/api/wachtwoordSterkte.service';
import {MeldingenComponent} from "../common/meldingen/meldingen.component";
import {FormsModule} from "@angular/forms";
import {NgForOf, NgIf} from "@angular/common";
import {GezinslidComponent} from "../common/gezinslid/gezinslid.component";

@Component({
  selector: 'app-aanmelden',
  templateUrl: './aanmelden.component.html',
  styleUrl: './aanmelden.component.css',
  imports: [
    MeldingenComponent,
    FormsModule,
    NgIf,
    GezinslidComponent,
    NgForOf
  ]
})
export class AanmeldenComponent {
  vorigeAchternaam: string = '';
  vorigeTussenvoegsel?: string;
  wachtwoordnogmaals?: string;
  wachtwoordakkoord: boolean = false;
  passwordOk: boolean = false;
  passwordStrength: number = 0;
  wachtwoordSterkte: WachtwoordSterkte = {'valide': false, 'meldingen': []};

  controleerActiveringsCode: ControleerActiveringsCode = {
    code: '',
    id: ''
  };
  @Output() preOpslaanEventEmitter: Subject<void> = new Subject<void>();

  @ViewChild('codeDeelEenE') codeDeelEenElement?: ElementRef;
  @ViewChild('codeDeelTweeE') codeDeelTweeElement?: ElementRef;
  @ViewChild('codeDeelDrieE') codeDeelDrieElement?: ElementRef;
  @ViewChild('codeDeelVierE') codeDeelVierElement?: ElementRef;
  @ViewChild('codeDeelVijfE') codeDeelVijfElement?: ElementRef;
  @ViewChild('codeDeelZesE') codeDeelZesElement?: ElementRef;
  codeDeelEen: string = '';
  codeDeelTwee: string = '';
  codeDeelDrie: string = '';
  codeDeelVier: string = '';
  codeDeelVijf: string = '';
  codeDeelZes: string = '';

  codeVerstuurd: boolean = false;
  codeGecontroleerd: boolean = false;
  codeOk: boolean = false;

  foutmeldingen: string[] = [];
  meldingen: string[] = [];
  goedmeldingen: string[] = [];

  gezin!: AanmeldenGezin;

  opgeslagen: boolean = false;

  constructor(
    private activeringsCodeService: ActiveringsCodeService,
    private gezinService: GezinService,
    private wachtwoordSterkteApiService: WachtwoordSterkteService,
    private router: Router
  ) {
    // @ts-ignore
    this.gezin = {
      gezinsleden: [
        {
          id: 0,
          achternaam: "",
          geslacht: GezinslidGeslachtEnum.Vrouw,
          soortGezinslid: GezinslidSoortGezinslidEnum.Ouder,
          voornaam: ""
        }
      ], naam: ""
    };
  }

  getEersteGezinslid(): Gezinslid {
    return this.gezin.gezinsleden[0];
  }

  voegGezinslidToe(): void {
    let aantalOuders = this.gezin.gezinsleden.filter((gezinslid: Gezinslid) => {
      return gezinslid.soortGezinslid == GezinslidSoortGezinslidEnum.Ouder;
    }).length;

    let soortLid: GezinslidSoortGezinslidEnum = aantalOuders > 1 ? GezinslidSoortGezinslidEnum.Kind : GezinslidSoortGezinslidEnum.Ouder;

    this.gezin.gezinsleden.push({
      id: 0,
      voornaam: '',
      tussenvoegsel: this.getEersteGezinslid().tussenvoegsel,
      achternaam: this.getEersteGezinslid().achternaam,
      soortGezinslid: soortLid,
      geslacht: GezinslidGeslachtEnum.Onbelangrijk
    })
  }

  opslaan(): void {
    this.foutmeldingen = [];
    this.isCorrectGevuld();
    if (this.foutmeldingen.length == 0) {
      this.preOpslaanEventEmitter.next();
    }
  }

  doeOpslaan(): void {
    if (!this.opgeslagen) {
      this.opgeslagen = true;
      this.gezinService.aanmeldenGezin(this.gezin).subscribe((algemeneResponse: AlgemeneResponse) => {
        if (algemeneResponse.foutmeldingen != null) {
          algemeneResponse.foutmeldingen.meldingen!.forEach((melding: string) => {
            this.foutmeldingen.push(melding);
          });
        } else {
          this.router.navigate(['inloggen']);
        }
      });
    }
  }

  isCorrectGevuld(): void {
    this.checkWachtwoord();
    if (this.gezin.naam == null || this.gezin.naam == '') {
      this.foutmeldingen.push(Foutmeldingen.GEZINSNAAM_MAG_NIET_LEEG_ZIJN);
    }
    if (this.gezin.gezinsleden.length == 0) {
      this.foutmeldingen.push(Foutmeldingen.ER_MOETEN_WEL_GEZINSLEDEN_AANWEZIG_ZIJN);
    }
    if (!this.wachtwoordakkoord) {
      this.foutmeldingen.push(Foutmeldingen.WACHTWOORDEN_KOMEN_NIET_OVEREEN);
    }
    this.gezin.gezinsleden.forEach((gezinslid: Gezinslid) => {
      if (gezinslid.voornaam == null || gezinslid.voornaam == '') {
        this.foutmeldingen.push(Foutmeldingen.VOORNAAM_MAG_NIET_LEEG_ZIJN);
      }
      if (gezinslid.achternaam == null || gezinslid.achternaam == '') {
        this.foutmeldingen.push(Foutmeldingen.ACHTERNAAM_MAG_NIET_LEEG_ZIJN);
      }
    });
    if (this.foutmeldingen.length > 0) {
      window.scroll({
        top: 0,
        left: 0,
        behavior: 'smooth'
      });
    }
  }

  maakNaamOp(gezinslid: Gezinslid): String {
    let naam: string = gezinslid.voornaam;

    if (gezinslid.tussenvoegsel != null && gezinslid.tussenvoegsel != undefined) {
      naam += ' ' + gezinslid.tussenvoegsel.trim();
    }
    naam += ' ' + gezinslid.achternaam;

    if (naam.trim() != '') {
      naam = "(" + naam.trim();
      if (gezinslid.soortGezinslid != null) {
        naam += ', ' + gezinslid.soortGezinslid.substring(0, 1).toUpperCase() + gezinslid.soortGezinslid.substring(1).toLowerCase();
      }
      naam += ")";
    }

    return naam.trim();
  }

  achternaamGewijzigd(): void {
    if (this.gezin.gezinsleden[0].achternaam != this.vorigeAchternaam) {
      this.gezin.gezinsleden.forEach((gezinslid: Gezinslid) => {
        if (gezinslid.achternaam == null || gezinslid.achternaam == '') {
          gezinslid.achternaam = this.gezin.gezinsleden[0].achternaam;
        }
      });
    }
    this.vorigeAchternaam = this.gezin.gezinsleden[0].achternaam;
  }

  tussenvoegselGewijzigd(): void {
    if (this.gezin.gezinsleden[0].tussenvoegsel != this.vorigeTussenvoegsel) {
      this.gezin.gezinsleden.forEach((gezinslid: Gezinslid) => {
        if (gezinslid.tussenvoegsel == null || gezinslid.tussenvoegsel == '') {
          gezinslid.tussenvoegsel = this.gezin.gezinsleden[0].tussenvoegsel;
        }
      });
    }
    this.vorigeTussenvoegsel = this.gezin.gezinsleden[0].tussenvoegsel;
  }

  stuurActivatieCode(): void {
    let vraagActiveringsCodeOp: VraagActiveringsCodeOp = new class implements VraagActiveringsCodeOp {
      email: string;
      familienaam: string;

      constructor(email: string, familienaam: string) {
        this.email = email;
        this.familienaam = familienaam;
      }
    }(this.gezin.gezinsleden[0].emailadres!, this.gezin.naam);

    this.activeringsCodeService.genereerActiveringsCode(vraagActiveringsCodeOp).subscribe((activeringsCode: ActiveringsCode) => {
      this.gezin.activeringsCode = {id: '', code: ''};
      this.gezin.activeringsCode!.id = activeringsCode.id;
      this.controleerActiveringsCode.id = activeringsCode.id;
      this.codeVerstuurd = true;
      this.codeDeelEenElement!.nativeElement.focus();
    });
  }

  controlerenActiveringsCode(index: number) {
    this.codeGecontroleerd = false;
    if (index == 1 && this.codeDeelEen != '') {
      this.codeDeelTweeElement!.nativeElement.focus();
    } else if (index == 2) {
      this.codeDeelDrieElement!.nativeElement.focus();
    } else if (index == 3) {
      this.codeDeelVierElement!.nativeElement.focus();
    } else if (index == 4) {
      this.codeDeelVijfElement!.nativeElement.focus();
    } else if (index == 5) {
      this.codeDeelZesElement!.nativeElement.focus();
    } else if (index == 6) {
      let code = this.codeDeelEen +
        this.codeDeelTwee +
        this.codeDeelDrie +
        this.codeDeelVier +
        this.codeDeelVijf +
        this.codeDeelZes;

      this.controleerActiveringsCode.code = code;
      this.gezin.activeringsCode!.code = code;
      this.activeringsCodeService.controleerActiveringsCode(this.controleerActiveringsCode).subscribe((resultaat: ControleerActiveringsCodeResultaat) => {
        this.codeOk = resultaat.resultaat;
        this.codeGecontroleerd = true;
      });
    }
  }

  checkWachtwoord(): void {
    if (this.getEersteGezinslid() != undefined) {
      if (this.getEersteGezinslid().wachtwoord != undefined && this.wachtwoordnogmaals != undefined) {
        this.wachtwoordakkoord = this.getEersteGezinslid().wachtwoord === this.wachtwoordnogmaals;
      }
    } else {
      this.wachtwoordakkoord = true;
    }
  }

  onChangeCheckPassword(): void {
    if (this.gezin.gezinsleden[0].wachtwoord != null) {
      this.wachtwoordSterkteApiService.wachtwoordSterkte(this.gezin.gezinsleden[0].wachtwoord).subscribe((wachtwoordSterkte: WachtwoordSterkte) => {
        this.passwordStrength = 5 - wachtwoordSterkte.meldingen.length;
        this.wachtwoordSterkte = wachtwoordSterkte;
        this.passwordOk = wachtwoordSterkte.valide;
        this.passwordOk = wachtwoordSterkte.valide && this.gezin.gezinsleden[0].wachtwoord == this.wachtwoordnogmaals;
      });
    } else {
      this.wachtwoordSterkte = {'valide': false, 'meldingen': []};
    }
  }

  familenaamGewijzigd(): void {
    this.gezin.gezinsleden[0].achternaam = this.gezin.naam;
  }
}

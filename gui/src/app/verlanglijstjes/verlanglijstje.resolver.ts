import {ActivatedRouteSnapshot, ResolveFn, RouterStateSnapshot} from '@angular/router';
import {inject} from '@angular/core';
import {Verlanglijstjes, VerlanglijstjeService} from '../../api/verlanglijstje';

export const verlanglijstjeResolver: ResolveFn<Verlanglijstjes> = (route: ActivatedRouteSnapshot, state: RouterStateSnapshot) => {
  const verlanglijstjeService = inject(VerlanglijstjeService);

  return verlanglijstjeService.alleVerlanglijstjes();
}

package nl.gezinsapp.messaging.reciever;

import io.smallrye.common.annotation.Blocking;
import jakarta.enterprise.context.ApplicationScoped;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.gezinsapp.commons.aspect.MethodeLogging;
import nl.gezinsapp.messaging.model.GezinslidVerwijderd;
import nl.gezinsapp.service.GezinslidVerwijderdService;
import org.eclipse.microprofile.reactive.messaging.Incoming;
import org.eclipse.microprofile.reactive.messaging.Message;
import org.slf4j.MDC;

import java.util.concurrent.CompletionStage;

import nl.gezinsapp.commons.messaging.AbstractReciever;

@Slf4j
@ApplicationScoped
@RequiredArgsConstructor
public class GezinslidVerwijderdReciever extends AbstractReciever<GezinslidVerwijderd> {
    private final GezinslidVerwijderdService gezinslidVerwijderdService;

    @Incoming("gezinslidverwijderd")
    @MethodeLogging
    @Blocking
    public CompletionStage<Void> verwerk(Message<GezinslidVerwijderd> m) {
        var gezinslidVerwijderd = getObject(m, GezinslidVerwijderd.class);

        if (gezinslidVerwijderd.getTrackAndTraceId() != null) {
            MDC.put("trackAndTraceId", gezinslidVerwijderd.getTrackAndTraceId().toString());
        }

        log.info("Ontvangen : {}", gezinslidVerwijderd);

        gezinslidVerwijderdService.verwerkVerwijderdgezinslid(gezinslidVerwijderd.getId());

        return m.ack();
    }
}

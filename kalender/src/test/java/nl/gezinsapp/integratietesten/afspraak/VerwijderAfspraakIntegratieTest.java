package nl.gezinsapp.integratietesten.afspraak;

import com.fasterxml.jackson.core.JsonProcessingException;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;
import lombok.extern.slf4j.Slf4j;
import nl.gezinsapp.integratietesten.AbstractIntegratieTest;
import nl.gezinsapp.integratietesten.MockServer;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.openapi.quarkus.kalender_openapi_yaml.model.AlgemeneResponse;

import java.time.Duration;
import java.util.concurrent.TimeUnit;

import static nl.gezinsapp.integratietesten.common.random.RandomAfspraak.randomAfspraak;
import static org.awaitility.Awaitility.await;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

@Slf4j
@QuarkusTest
@QuarkusTestResource(MockServer.class)
class VerwijderAfspraakIntegratieTest extends AbstractIntegratieTest {
    @Test
    @DisplayName("Verwijder een bestaande afspraak")
    void test() throws JsonProcessingException {
        maakDatabaseLeeg();

        var accesstoken = maakToken();
        var jwtResponse = mockCheckJwt(accesstoken);

        var afspraak = randomAfspraak();
        afspraak.setGezin(99L);
        opslaan(afspraak);

        var response = deleteViaApi(AlgemeneResponse.class, "/api/kalender/afspraak", afspraak.getId(), accesstoken);
        assertNull(response.getFoutmeldingen());

        await()
                .atMost(40, TimeUnit.SECONDS)
                .pollInterval(Duration.ofMillis(500))
                .until(() -> {
                    var afspraken = leesAlleAfspraken();
                    assertTrue(afspraken.isEmpty());
                    return true;
                });

    }
}

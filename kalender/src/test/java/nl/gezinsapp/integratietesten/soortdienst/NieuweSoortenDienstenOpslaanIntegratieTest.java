package nl.gezinsapp.integratietesten.soortdienst;

import com.fasterxml.jackson.core.JsonProcessingException;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;
import lombok.extern.slf4j.Slf4j;
import nl.gezinsapp.integratietesten.AbstractIntegratieTest;
import nl.gezinsapp.integratietesten.MockServer;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.openapi.quarkus.kalender_openapi_yaml.model.AlgemeneResponse;

import java.util.UUID;

import static nl.gezinsapp.integratietesten.common.random.SoortDienstDatum.randomMsgSoortDienst;
import static org.junit.jupiter.api.Assertions.*;

@Slf4j
@QuarkusTest
@QuarkusTestResource(MockServer.class)
class NieuweSoortenDienstenOpslaanIntegratieTest extends AbstractIntegratieTest {

    @Test
    @DisplayName("Lees een opgeslagen datum")
    void test() throws JsonProcessingException {
        maakDatabaseLeeg();

        var accesstoken = maakToken();
        var jwtResponse = mockCheckJwt(accesstoken);

        var msgSoortDienst = randomMsgSoortDienst();

        var response = putViaApi(AlgemeneResponse.class, "/api/kalender/rooster/soort-diensten", msgSoortDienst, accesstoken);
        assertNull(response.getFoutmeldingen());
        assertNotNull(response.getId());

        var alleSoorten=leesAlleSoortDienst();
        assertEquals(1,alleSoorten.size());
        var soort = alleSoorten.get(0);
        assertEquals(msgSoortDienst.getDag().name(),soort.getDag().name());
        assertEquals(msgSoortDienst.getOmschrijving(),soort.getOmschrijving());
        assertEquals(msgSoortDienst.getVan().substring(0,8),soort.getVan().toString());
        assertEquals(msgSoortDienst.getTot().substring(0,8),soort.getTot().toString());
        assertEquals(msgSoortDienst.getGezinslid(), soort.getGezinslid());

        msgSoortDienst.setOmschrijving(UUID.randomUUID().toString());
        msgSoortDienst.setId(response.getId());

        var response2 = patchViaApi(AlgemeneResponse.class, "/api/kalender/rooster/soort-diensten", msgSoortDienst, accesstoken);
        assertNull(response2.getFoutmeldingen());

         alleSoorten=leesAlleSoortDienst();
        assertEquals(1,alleSoorten.size());
         soort = alleSoorten.get(0);
        assertEquals(msgSoortDienst.getDag().name(),soort.getDag().name());
        assertEquals(msgSoortDienst.getOmschrijving(),soort.getOmschrijving());
        assertEquals(msgSoortDienst.getVan().substring(0,8),soort.getVan().toString());
        assertEquals(msgSoortDienst.getTot().substring(0,8),soort.getTot().toString());
    }
}

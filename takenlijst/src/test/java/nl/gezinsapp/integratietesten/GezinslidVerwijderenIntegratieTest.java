package nl.gezinsapp.integratietesten;

import com.fasterxml.jackson.core.JsonProcessingException;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;
import lombok.extern.slf4j.Slf4j;
import nl.gezinsapp.integratietesten.common.QueueUtil;
import nl.gezinsapp.integratietesten.common.TestGegevens;
import nl.gezinsapp.messaging.model.GezinslidVerwijderd;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.UUID;
import java.util.concurrent.TimeUnit;

import static nl.gezinsapp.integratietesten.random.RandomTaak.randomDomainTaak;
import static org.awaitility.Awaitility.await;
import static org.junit.jupiter.api.Assertions.assertEquals;

@Slf4j
@QuarkusTest
@QuarkusTestResource(MockServer.class)
class GezinslidVerwijderenIntegratieTest extends AbstractIntegratieTest {
    @ConfigProperty(name = "mp.messaging.incoming.gezinslidverwijderd.exchange.name")
    private String exchange;
    @ConfigProperty(name = "rabbitmq-host")
    private String rabbitHost;
    @ConfigProperty(name = "rabbitmq-port")
    private String rabbitPort;
    @ConfigProperty(name = "rabbitmq-username")
    private String rabbitUsername;
    @ConfigProperty(name = "rabbitmq-password")
    private String rabbitPassword;

    @Test
    @DisplayName("Test dat de taak die is toegewezen aan een gezinslid ge'unassigned' wordt")
    void test() throws JsonProcessingException {
        TestGegevens.setRabbitHost(rabbitHost);
        TestGegevens.setRabbitPort(Integer.valueOf(rabbitPort));
        TestGegevens.setRabbitUser(rabbitUsername);
        TestGegevens.setRabbitPassword(rabbitPassword);

        maakDatabaseLeeg();

        var gezinslid1 = 33L;
        var gezinslid2 = 46L;

        var accesstoken = maakToken();
        var jwtResponse = mockCheckJwt(accesstoken);

        var taak1 = randomDomainTaak();
        taak1.setGezin(jwtResponse.getGebruiker().getGezin());
        taak1.getGezinsleden().add(gezinslid1);
        opslaan(taak1);
        var taak2 = randomDomainTaak();
        taak2.setGezin(3L);
        taak2.getGezinsleden().add(gezinslid2);
        opslaan(taak2);

        queue().stuurBerichtNaarExchange(new GezinslidVerwijderd("a@b.c", 1L, 2L, UUID.randomUUID(), gezinslid1));

        await()
                .atMost(5, TimeUnit.SECONDS)
                .until(() -> {
                    try {
                        System.out.println(leesTaak(taak1.getId()));
                        System.out.println(leesTaak(taak2.getId()));
                        assertEquals(0, leesTaak(taak1.getId()).getGezinsleden().size());
                        assertEquals(1, leesTaak(taak2.getId()).getGezinsleden().size());
                        return true;
                    } catch (AssertionError ae) {
                        return false;
                    }
                });
    }

    private QueueUtil<GezinslidVerwijderd> queue() {
        return new QueueUtil<>() {
            @Override
            public String getExchange() {
                return exchange;
            }

            @Override
            public Class<GezinslidVerwijderd> getClazz() {
                return GezinslidVerwijderd.class;
            }
        };
    }
}


package nl.gezinsapp.service;

import nl.gezinsapp.messaging.sender.Sender;
import nl.gezinsapp.model.repository.ActivatieCodeRepository;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ActivatieCodeServiceTest {
    @InjectMocks
    ActivatieCodeService activatieCodeService;

    @Mock
    ActivatieCodeRepository activatieCodeRepository;
    @Mock
    GenereerCodeService genereerCodeService;
    @Mock
    Sender activatieCodeAangemaaktSender;

    @Test
    @DisplayName("Test een juiste code")
    void controleerActiveringsCode() {
        var code = "234567";
        var id = UUID.randomUUID();

        when(activatieCodeRepository.get(id)).thenReturn(Optional.of(code));

        var result = activatieCodeService.controleerActiveringsCode(id, code);

        verify(activatieCodeRepository).get(id);

        assertTrue(result);
    }

    @Test
    @DisplayName("Test een onjuiste code")
    void controleerActiveringsCode2() {
        var code = "234567";
        var codeFout = "345678";
        var id = UUID.randomUUID();

        when(activatieCodeRepository.get(id)).thenReturn(Optional.of(codeFout));

        var result = activatieCodeService.controleerActiveringsCode(id, code);

        verify(activatieCodeRepository).get(id);

        assertFalse(result);
    }

    @Test
    @DisplayName("Test een niet voorkomende code")
    void controleerActiveringsCode3() {
        var code = "234567";
        var id = UUID.randomUUID();

        when(activatieCodeRepository.get(id)).thenReturn(Optional.empty());

        var result = activatieCodeService.controleerActiveringsCode(id, code);

        verify(activatieCodeRepository).get(id);

        assertFalse(result);
    }
}
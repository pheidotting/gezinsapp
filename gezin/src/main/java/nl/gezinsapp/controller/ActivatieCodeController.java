package nl.gezinsapp.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.gezinsapp.commons.aspect.MethodeLogging;
import nl.gezinsapp.commons.aspect.TrackAndTraceId;
import nl.gezinsapp.mapper.ActivatieCodeMapper;
import nl.gezinsapp.service.ActivatieCodeService;
import org.openapi.quarkus.gezin_openapi_yaml.api.ActiveringsCodeApi;
import org.openapi.quarkus.gezin_openapi_yaml.model.ActiveringsCode;
import org.openapi.quarkus.gezin_openapi_yaml.model.ControleerActiveringsCode;
import org.openapi.quarkus.gezin_openapi_yaml.model.ControleerActiveringsCodeResultaat;
import org.openapi.quarkus.gezin_openapi_yaml.model.VraagActiveringsCodeOp;


@Slf4j
@RequiredArgsConstructor
public class ActivatieCodeController implements ActiveringsCodeApi {
    private final ActivatieCodeService activatieCodeService;
    private final ActivatieCodeMapper activatieCodeMapper;

    @Override
    @TrackAndTraceId
    @MethodeLogging
    public ActiveringsCode genereerActiveringsCode(VraagActiveringsCodeOp vraagActiveringsCodeOp) {
        return activatieCodeMapper.map(activatieCodeService.genereerActiveringsCode(vraagActiveringsCodeOp));
    }

    @Override
    @TrackAndTraceId
    @MethodeLogging
    public ControleerActiveringsCodeResultaat controleerActiveringsCode(ControleerActiveringsCode controleerActiveringsCode) {
        return new ControleerActiveringsCodeResultaat().resultaat(
                activatieCodeService.controleerActiveringsCode(
                        controleerActiveringsCode.getId(),
                        controleerActiveringsCode.getCode()));
    }
}

import {HttpPipe} from './http.pipe';

describe('HttpPipe', () => {
  it('create an instance', () => {
    const pipe = new HttpPipe();
    expect(pipe).toBeTruthy();

    expect(pipe.transform('ab')).toEqual('http://ab');
    expect(pipe.transform('http://ab')).toEqual('http://ab');
  });
});

import {Component, EventEmitter, Input, Output} from '@angular/core';
import {NgIf} from "@angular/common";

@Component({
  selector: 'app-opslaan-en-afbreken-buttons',
  templateUrl: './opslaan-en-afbreken-buttons.component.html',
  imports: [
    NgIf
  ]
})
export class OpslaanEnAfbrekenButtonsComponent {
  @Input() id: number | undefined;

  @Output() opslaanEventEmitter: EventEmitter<number> = new EventEmitter();
  @Output() afbrekenEventEmitter: EventEmitter<number> = new EventEmitter();
  @Output() verwijderenEventEmitter: EventEmitter<number> = new EventEmitter();
  @Output() bewerkenEventEmitter: EventEmitter<number> = new EventEmitter();
  @Input() disabled: boolean = false;

  @Input() opslaanButtonTonen: boolean = true;
  @Input() afbrekenButtonTonen: boolean = true;
  @Input() verwijderButtonTonen: boolean = false;
  @Input() bewerkenButtonTonen: boolean = false;

  @Input() nieuweRegel: boolean = true;

  @Input() kleineButtons: boolean = false;

  @Input() reactive: boolean = false;

  opslaan(): void {
    this.opslaanEventEmitter.emit(this.id);
  }

  afbreken(): void {
    this.afbrekenEventEmitter.emit(this.id);
  }

  verwijderen(): void {
    this.verwijderenEventEmitter.emit(this.id);
  }

  bewerken(): void {
    this.bewerkenEventEmitter.emit(this.id);
  }

  extraStyle(): string {
    return this.kleineButtons ? ' btn-sm' : '';
  }
}

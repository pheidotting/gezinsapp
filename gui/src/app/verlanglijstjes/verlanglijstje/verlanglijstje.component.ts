import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {NgIf} from "@angular/common";
import {HeaderMetToevoegButtonComponent} from "../../common/header-met-toevoeg-button/header-met-toevoeg-button.component";
import {SideBarComponent} from "../../common/side-bar/side-bar.component";
import {OpslaanEnAfbrekenButtonsComponent} from "../../common/opslaan-en-afbreken-buttons/opslaan-en-afbreken-buttons.component";
import {ModalComponent} from "../../common/modal/modal.component";
import {AlgemeneResponse, Verlanglijstje, Verlanglijstje1, Verlanglijstjes, VerlanglijstjeService} from '../../../api/verlanglijstje';
import {GezinslidUtil} from "../../services/gezinslid-util.service";
import {Gezin, Gezinslid} from '../../../api/gezin';

@Component({
  selector: 'app-verlanglijstje',
  templateUrl: './verlanglijstje.component.html',
  imports: [
    HeaderMetToevoegButtonComponent,
    SideBarComponent,
    NgIf,
    OpslaanEnAfbrekenButtonsComponent,
    ModalComponent
  ]
})
export class VerlanglijstjeComponent implements OnInit {
  verlanglijstjes: Verlanglijstje1[] | undefined = [];
  gezin: Gezin | undefined;
  modalTonen: boolean = false;
  teVerwijderenLijst: Verlanglijstje1 | undefined;

  constructor(
    private readonly activatedRoute: ActivatedRoute,
    private readonly verlanglijstjeService: VerlanglijstjeService,
    private readonly gezinslidUtil: GezinslidUtil,
    private readonly router: Router
  ) {
  }

  ngOnInit(): void {
    this.activatedRoute.data.subscribe((response: any) => {
      this.gezin = response.gezin;
      this.verlanglijstjes = response.verlanglijstje.verlanglijstjes;
    });
  }

  leesVerlanglijstjes(): void {
    this.verlanglijstjeService.alleVerlanglijstjes().subscribe((verlanglijstjes: Verlanglijstjes) => {
      this.verlanglijstjes = verlanglijstjes.verlanglijstjes;
    });
  }

  gezinslid(gezinslidId: number): string {
    return this.gezinslidUtil.maakNaamOp(this.gezin!.gezinsleden.filter((gezinslid: Gezinslid) => {
      return gezinslid.id == gezinslidId;
    })[0]) as string;
  }

  verwijder(id: number): void {
    this.teVerwijderenLijst = this.verlanglijstjes!.filter((verlanglijst: Verlanglijstje1) => {
      return verlanglijst.id == id;
    })[0];
    this.modalTonen = true;
  }

  verbergModal() {
    this.modalTonen = false;
  }

  doeVerwijder() {
    this.verlanglijstjeService.verwijder(this.teVerwijderenLijst!.id!).subscribe((algemeneResponse: AlgemeneResponse) => {
      this.modalTonen = false;
      this.leesVerlanglijstjes();
    });
  }

  voegLijstToe(): void {
    let lijst: Verlanglijstje = {
      gezinslid: 0,
      verlanglijstItems: []
    };
    this.verlanglijstjeService.nieuwVerlanglijstje(lijst).subscribe((response: AlgemeneResponse) => {
      this.router.navigate(['verlanglijstje', response.id, 'nieuw']);
    });
  }

  getOmschrijving(verlanglijstje: Verlanglijstje1 | undefined): string {
    if (verlanglijstje != null) {
      if (verlanglijstje.omschrijving != null) {
        return verlanglijstje.omschrijving;
      } else {
        return '';
      }
    } else {
      return '';
    }
  }
}

package nl.gezinsapp.mapper;

import nl.gezinsapp.model.GebruikerImpl;
import org.mapstruct.Mapper;
import org.openapi.quarkus.gezin_openapi_yaml.model.Gebruiker;

@Mapper(componentModel = "CDI")

public abstract class GebruikerMapper {

    public abstract GebruikerImpl map(Gebruiker gebruiker);

    public abstract org.openapi.quarkus.authenticatie_openapi_yaml.model.Gebruiker map(GebruikerImpl gebruiker);
}

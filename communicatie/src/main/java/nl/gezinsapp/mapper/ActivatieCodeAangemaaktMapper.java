package nl.gezinsapp.mapper;

import nl.gezinsapp.messaging.model.ActivatieCodeAangemaakt;
import nl.gezinsapp.model.CommActivatieCodeAangemaakt;
import org.mapstruct.Mapper;

@Mapper(componentModel = "CDI")
public interface ActivatieCodeAangemaaktMapper {
    CommActivatieCodeAangemaakt map(ActivatieCodeAangemaakt activatieCodeAangemaakt);
}

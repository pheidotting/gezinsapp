package nl.gezinsapp.util;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.hibernate.exception.ConstraintViolationException;

import java.util.List;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class FoutmeldingUtil {
    public static List<String> mapExceptionNaarFoutmeldingen(Exception e) {
        if (e instanceof ConstraintViolationException) {
            var m = e.getMessage().replace("could not execute statement [(", "").trim();
            m = m.substring(m.indexOf(")") + 1).trim();
            m = m.replace("Column '", "");
            m = m.substring(0, m.indexOf("'")).trim();
            m = m.substring(0, 1) + m.substring(1, m.length()).toLowerCase();
            return List.of(m + " mag niet leeg zijn.");
        }
        return List.of();
    }
}

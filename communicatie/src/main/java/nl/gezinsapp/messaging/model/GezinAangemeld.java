package nl.gezinsapp.messaging.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.UUID;

@Setter
@Getter
@AllArgsConstructor
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class GezinAangemeld extends AbstractMessagingObject {

    @Builder
    public GezinAangemeld(String emailadres, Long gezinsId, Long gezinslidId, UUID trackAndTraceId) {
        super(emailadres, gezinsId, gezinslidId,trackAndTraceId);
    }
}

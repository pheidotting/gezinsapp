import {Component, OnInit} from '@angular/core';
import {BoodschappenItem} from '../../../api/boodschappen/model/boodschappenItem';
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-boodschappen-widget',
  templateUrl: './boodschappen-widget.component.html',
})
export class BoodschappenWidgetComponent implements OnInit {
  boodschappen: BoodschappenItem[] = [];

  constructor(
    private readonly activatedRoute: ActivatedRoute,
  ) {
  }

  ngOnInit(): void {
    this.leesBoodschappen();
  }

  leesBoodschappen(): void {
    this.activatedRoute.data.subscribe((response: any) => {
      this.boodschappen = response.boodschappen.boodschappenitems
        .filter((boodschappenItem: BoodschappenItem) => {
          return !boodschappenItem.gehaald;
        })
        .sort((a: BoodschappenItem, b: BoodschappenItem) => {
          if (a.winkel == null) {
            return -1;
          }
          if (b.winkel == null) {
            return 1;
          }
          return a.winkel.localeCompare(b.winkel);
        });
    });
  }
}

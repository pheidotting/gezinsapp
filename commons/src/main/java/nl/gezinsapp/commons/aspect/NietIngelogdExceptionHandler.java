package nl.gezinsapp.commons.aspect;

import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.ext.ExceptionMapper;
import jakarta.ws.rs.ext.Provider;

@Provider
public class NietIngelogdExceptionHandler implements ExceptionMapper<NietIngelogdException> {
    @Override
    @MethodeLogging
    public Response toResponse(NietIngelogdException e) {
        return Response.status(Response.Status.UNAUTHORIZED).build();
    }
}
package nl.gezinsapp.integratietesten;

import io.quarkus.test.junit.QuarkusTest;
import lombok.extern.slf4j.Slf4j;
import nl.gezinsapp.integratietesten.common.QueueUtil;
import nl.gezinsapp.integratietesten.common.TestGegevens;
import nl.gezinsapp.messaging.model.WijzigWachtwoordCodeAangemaakt;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.time.Duration;
import java.util.concurrent.TimeUnit;

import static io.restassured.RestAssured.given;
import static org.awaitility.Awaitility.await;
import static org.junit.jupiter.api.Assertions.*;
import io.quarkus.test.common.QuarkusTestResource;

@Slf4j
@QuarkusTest
@QuarkusTestResource(MockServer.class)
public class WijzigWachtwoordCodeAangemaaktIntegratieTest extends AbstractIntegratieTest {
    @ConfigProperty(name = "mp.messaging.incoming.wijzigwachtwoordcode-aangemaakt.exchange.name")
    private String exchange;
    @ConfigProperty(name = "rabbitmq-host")
    private String rabbitHost;
    @ConfigProperty(name = "rabbitmq-port")
    private String rabbitPort;
    @ConfigProperty(name = "rabbitmq-username")
    private String rabbitUsername;
    @ConfigProperty(name = "rabbitmq-password")
    private String rabbitPassword;

    @Test
    @DisplayName("Test het versturen van een code voor het wijzigen van het wachtwoord")
    void test() {
        maakDatabaseLeeg();
        given().when().delete("http://localhost:" + mailpitContainer.getMappedPort(8025) + "/api/v1/messages");

        TestGegevens.setRabbitHost(rabbitHost);
        TestGegevens.setRabbitPort(Integer.valueOf(rabbitPort));
        TestGegevens.setRabbitUser(rabbitUsername);
        TestGegevens.setRabbitPassword(rabbitPassword);

        var queueUtil = mmaakWijzigWachtwoordCodeAangemaaktQueueUtil();

        var bericht = new WijzigWachtwoordCodeAangemaakt();
        bericht.setEmailadres("a@b.c");
        bericht.setCode("123456");

        queueUtil.stuurBerichtNaarExchange(bericht);

        await()
                .atMost(40, TimeUnit.SECONDS)
                .pollInterval(Duration.ofMillis(500))
                .until(() -> {
                    var email = leesEmail();
                    assertEquals("Code voor het wijzigen van je wachtwoord", email.getOnderwerp());
                    assertEquals("<h1>123456</h1>\n" +
                            "<h1>a@b.c</h1>", email.getTekst());
                    assertNotNull(email.getTijdstipaangemaakt());
                    assertNull(email.getTijdstipverstuurd());
                    return true;
                });

        given().when().get("/verstuurmails");

        var response = given()
                .when().get("http://localhost:" + mailpitContainer.getMappedPort(8025) + "/api/v1/messages");
        assertTrue(response.getBody().prettyPrint().contains("\"messages_count\": 1,"));
        assertTrue(response.getBody().prettyPrint().contains("\"Address\": \"gezinsapp@heidotting.nl\""));
        assertTrue(response.getBody().prettyPrint().contains("\"Address\": \"a@b.c\""));
        assertTrue(response.getBody().prettyPrint().contains("\"Subject\": \"Code voor het wijzigen van je wachtwoord\","));
        assertTrue(response.getBody().prettyPrint().contains("\"Snippet\": \"<h1>123456</h1> <h1>a@b.c</h1>\""));

        var email = leesEmail();
        assertNotNull(email.getTijdstipaangemaakt());
        assertNotNull(email.getTijdstipverstuurd());
    }

    private QueueUtil<WijzigWachtwoordCodeAangemaakt> mmaakWijzigWachtwoordCodeAangemaaktQueueUtil() {
        return new QueueUtil<>() {
            @Override
            public String getExchange() {
                return exchange;
            }

            @Override
            public Class<WijzigWachtwoordCodeAangemaakt> getClazz() {
                return WijzigWachtwoordCodeAangemaakt.class;
            }
        };
    }

}

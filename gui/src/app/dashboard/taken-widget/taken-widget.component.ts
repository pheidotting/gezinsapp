import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {Taak} from '../../../api/takenlijst';
import {GezinslidOpIdPipe} from "../../pipes/gezinslid-op-id.pipe";
import {AsyncPipe} from "@angular/common";
import {GezinslidVoornaamPipe} from "../../pipes/gezinslid-voornaam.pipe";

@Component({
  selector: 'app-taken-widget',
  imports: [
    GezinslidOpIdPipe,
    AsyncPipe,
    GezinslidVoornaamPipe
  ],
  templateUrl: './taken-widget.component.html'
})
export class TakenWidgetComponent implements OnInit {
  taken: Taak[] = [];

  constructor(
    private readonly activatedRoute: ActivatedRoute,
  ) {
  }

  ngOnInit(): void {
    this.activatedRoute.data.subscribe((response: any) => {
      this.taken = response.takenlijst.taken;
    });
  }

}

package nl.gezinsapp.controller;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.gezinsapp.service.BepaalJarigenService;
import org.eclipse.microprofile.config.inject.ConfigProperty;

@Slf4j
@ApplicationScoped
@RequiredArgsConstructor
@Path("/cron")
public class CronController {
    @ConfigProperty(name = "quarkus.application.name")
    private String applicatieNaam;
    @ConfigProperty(name = "quarkus.application.omgeving")
    private String omgeving;

    private final BepaalJarigenService bepaalJarigenService;

    @GET
    @Path("/bepaaljarigenvandaag")
    public void bepaalJarigenVandaag() {
        bepaalJarigenService.verstuurJarigen();
    }
}

import {Component, EventEmitter, Input, Output} from '@angular/core';
import {NgIf} from "@angular/common";

@Component({
  selector: 'app-header-met-toevoeg-button',
  templateUrl: './header-met-toevoeg-button.component.html',
  styleUrl: './header-met-toevoeg-button.component.css',
  imports: [
    NgIf
  ]
})
export class HeaderMetToevoegButtonComponent {
  @Input() headerTekst: string = '';
  @Input() wijzigKnopTonen: boolean = false;
  @Input() toevoegKnopTonen: boolean = false;
  @Output() toevoegActieEmitter: EventEmitter<void> = new EventEmitter();
  @Output() wijzigActieEmitter: EventEmitter<void> = new EventEmitter();

  public toevoegActie(): void {
    this.toevoegActieEmitter.emit();
  }

  public wijzigActie(): void {
    this.wijzigActieEmitter.emit();
  }
}

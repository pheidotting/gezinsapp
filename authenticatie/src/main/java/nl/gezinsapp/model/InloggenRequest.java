package nl.gezinsapp.model;

public record InloggenRequest(
        String emailadres,
        String wachtwoord
) {

}

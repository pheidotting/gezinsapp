package nl.gezinsapp.integratietesten;

import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;
import lombok.extern.slf4j.Slf4j;
import nl.gezinsapp.integratietesten.common.QueueUtil;
import nl.gezinsapp.integratietesten.common.TestGegevens;
import nl.gezinsapp.messaging.model.JarigenVandaag;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static io.restassured.RestAssured.given;
import static nl.gezinsapp.integratietesten.common.random.RandomGezin.randomDomainGezin;
import static nl.gezinsapp.integratietesten.common.random.RandomGezinslid.randomDomainGezinslid;
import static org.junit.jupiter.api.Assertions.assertEquals;

@Slf4j
@QuarkusTest
@QuarkusTestResource(MockServer.class)
public class BepaalJarigenIntegratieTest extends AbstractIntegratieTest {
    @ConfigProperty(name = "mp.messaging.outgoing.jarigenvandaag.exchange.name")
    private String exchange;
    @ConfigProperty(name = "rabbitmq-host")
    private String rabbitHost;
    @ConfigProperty(name = "rabbitmq-port")
    private String rabbitPort;
    @ConfigProperty(name = "rabbitmq-username")
    private String rabbitUsername;
    @ConfigProperty(name = "rabbitmq-password")
    private String rabbitPassword;

    @Test
    @DisplayName("Test een correcte aanmelding van het gezin")
    void test() {
        TestGegevens.setRabbitHost(rabbitHost);
        TestGegevens.setRabbitPort(Integer.valueOf(rabbitPort));
        TestGegevens.setRabbitUser(rabbitUsername);
        TestGegevens.setRabbitPassword(rabbitPassword);

        var queueUtil = maakJarigenVandaagQueueUtil();

        maakDatabaseLeeg();

        var gezin1 = randomDomainGezin();
        var gezin2 = randomDomainGezin();
        var gezin3 = randomDomainGezin();
        opslaanGezin(gezin1);
        opslaanGezin(gezin2);
        opslaanGezin(gezin3);

        var gezinslid1 = randomDomainGezinslid();
        gezinslid1.setGezin(gezin1);
        gezinslid1.setGeboorteDatum(LocalDate.now().minusYears(20));
        var gezinslid2 = randomDomainGezinslid();
        gezinslid2.setGezin(gezin2);
        gezinslid2.setGeboorteDatum(LocalDate.now().minusYears(20).plusDays(1));
        var gezinslid3 = randomDomainGezinslid();
        gezinslid3.setGezin(gezin3);
        gezinslid3.setGeboorteDatum(LocalDate.now().minusYears(30));
        opslaanGezinslid(gezinslid1);
        opslaanGezinslid(gezinslid2);
        opslaanGezinslid(gezinslid3);

        given().when().get("/cron/bepaaljarigenvandaag");

        var jarigenVandaag = queueUtil.vangBericht();
        assertEquals(2, jarigenVandaag.get().getJarigen().size());
    }

    private QueueUtil<JarigenVandaag> maakJarigenVandaagQueueUtil() {
        return new QueueUtil<>() {
            @Override
            public String getExchange() {
                return exchange;
            }

            @Override
            public Class<JarigenVandaag> getClazz() {
                return JarigenVandaag.class;
            }
        };
    }
}

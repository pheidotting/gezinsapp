import {ActivatedRouteSnapshot, ResolveFn, RouterStateSnapshot} from '@angular/router';
import {inject} from '@angular/core';
import {BoodschappenService, LijstBoodschappenItems} from '../../../api/boodschappen';

export const boodschappenWidgetResolver: ResolveFn<LijstBoodschappenItems> = (route: ActivatedRouteSnapshot, state: RouterStateSnapshot) => {
  const boodschappenService = inject(BoodschappenService);

  return boodschappenService.lijst();
}

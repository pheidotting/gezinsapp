package nl.gezinsapp.integratietesten;

import com.fasterxml.jackson.core.JsonProcessingException;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.openapi.quarkus.boodschappen_openapi_yaml.model.AlgemeneResponse;

import static nl.gezinsapp.integratietesten.random.RandomBoodschappenItem.randomDtoBoodschappenItem;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@Slf4j
@QuarkusTest
@QuarkusTestResource(MockServer.class)
class BoodschappenItemToevoegenIntegratieTest extends AbstractIntegratieTest {
    @Test
    @DisplayName("Sla een nieuwe BoodschappenItem op")
    void test() throws JsonProcessingException {
        var accesstoken = maakToken();
        var jwtResponse = mockCheckJwt(accesstoken);

        maakDatabaseLeeg();

        var item = randomDtoBoodschappenItem();
        var response = putViaApi(AlgemeneResponse.class, "/api/boodschappen/item", item, accesstoken);

        var itemGelezen = leesBoodschappenItem(response.getId());
        assertNotNull(itemGelezen);
        assertEquals(item.getGehaald(), itemGelezen.isGehaald());
        assertEquals(item.getOmschrijving(), itemGelezen.getOmschrijving());
        assertEquals(item.getWinkel(), itemGelezen.getWinkel());
        assertEquals(jwtResponse.getGebruiker().getGezin(), itemGelezen.getGezin());
    }
}

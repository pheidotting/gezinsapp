package nl.gezinsapp.model;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.time.LocalDateTime;

@Entity
@Table(name = "INLOGPOGING")
@Getter
@Setter
@ToString
public class InlogPoging extends PanacheEntityBase {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;
    @Column(name = "GEZINSLID")
    private Long gezinslid;
    @Column(name = "TIJDSTIP")
    private LocalDateTime tijdstip;
    @Column(name = "RESULTAAT")
    @Enumerated(EnumType.STRING)
    private Resultaat resultaat;
    @Column(name = "BROWSER")
    private String browser;
    @Column(name = "BROWSERTYPE")
    private String browserType;
    @Column(name = "BROWSERMAJORVERSION")
    private String browserMajorVersion;
    @Column(name = "DEVICETYPE")
    private String deviceType;
    @Column(name = "PLATFORM")
    private String platform;
    @Column(name = "PLATFORMVERSION")
    private String platformVersion;
    @Column(name = "IPADRES")
    private String ipAdres;

    public InlogPoging() {
        this.tijdstip = LocalDateTime.now();
    }
}

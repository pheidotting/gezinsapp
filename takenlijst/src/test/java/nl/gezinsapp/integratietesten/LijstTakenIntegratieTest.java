package nl.gezinsapp.integratietesten;

import com.fasterxml.jackson.core.JsonProcessingException;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.openapi.quarkus.takenlijst_openapi_yaml.model.LijstTaken;

import java.time.LocalDateTime;

import static nl.gezinsapp.integratietesten.random.RandomTaak.randomDomainTaak;
import static org.jboss.resteasy.reactive.RestResponse.StatusCode.OK;
import static org.junit.jupiter.api.Assertions.assertEquals;

@Slf4j
@QuarkusTest
@QuarkusTestResource(MockServer.class)
class LijstTakenIntegratieTest extends AbstractIntegratieTest {
    @Test
    @DisplayName("Sla een nieuwe Taak op zonder dat deze gekoppeld is aan een gezinslid")
    void test() throws JsonProcessingException {
        maakDatabaseLeeg();

        var accesstoken = maakToken();
        var jwtResponse = mockCheckJwt(accesstoken);

        var taak1 = randomDomainTaak();
        taak1.setGezin(jwtResponse.getGebruiker().getGezin());
        taak1.setTijdstipAfgestreept(null);
        opslaan(taak1);
        var taak2 = randomDomainTaak();
        taak2.setGezin(3L);
        taak2.setTijdstipAfgestreept(null);
        opslaan(taak2);
        var taak3 = randomDomainTaak();
        taak3.setGezin(jwtResponse.getGebruiker().getGezin());
        taak3.setTijdstipAfgestreept(LocalDateTime.now());
        opslaan(taak3);

        var response = getViaApi(LijstTaken.class, "/api/takenlijst/taken", accesstoken, OK);

        assertEquals(1, response.getTaken().size());
        assertEquals(taak1.getOmschrijving(), response.getTaken().get(0).getOmschrijving());
        assertEquals(taak1.getGezin(), jwtResponse.getGebruiker().getGezin());
    }
}

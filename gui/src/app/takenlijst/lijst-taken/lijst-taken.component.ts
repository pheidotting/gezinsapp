import {Component} from '@angular/core';
import {SideBarComponent} from "../../common/side-bar/side-bar.component";
import {AlgemeneResponse, LijstTaken, Taak, TakenlijstService} from '../../../api/takenlijst';
import {HeaderMetToevoegButtonComponent} from "../../common/header-met-toevoeg-button/header-met-toevoeg-button.component";
import {OpslaanEnAfbrekenButtonsComponent} from "../../common/opslaan-en-afbreken-buttons/opslaan-en-afbreken-buttons.component";
import {ActivatedRoute} from "@angular/router";
import {FormsModule} from "@angular/forms";
import moment from "moment";
import {GezinService, Gezinslid, LeesGezin} from '../../../api/gezin';
import {GezinslidOpIdPipe} from "../../pipes/gezinslid-op-id.pipe";
import {AsyncPipe} from "@angular/common";
import {GezinslidVoornaamPipe} from "../../pipes/gezinslid-voornaam.pipe";
import {TaakOmToeTeWijzen} from "./taak.om.toe.te.wijzen";
import {AvatarComponent} from "../../common/avatar/avatar.component";

@Component({
  selector: 'app-lijst-taken',
  imports: [
    SideBarComponent,
    HeaderMetToevoegButtonComponent,
    OpslaanEnAfbrekenButtonsComponent,
    FormsModule,
    GezinslidOpIdPipe,
    AsyncPipe,
    GezinslidVoornaamPipe,
    AvatarComponent
  ],
  templateUrl: './lijst-taken.component.html'
})
export class LijstTakenComponent {
  taken: TaakOmToeTeWijzen[] = [];
  nieuweTaakModus: boolean = false;
  omschrijving: string = '';
  wijzigId: number = 0;
  gezinsleden: Array<{
    id: number,
    voornaam: string,
    geselecteerd: boolean
  }> = [];
  eerste: boolean = true;

  constructor(
    private readonly activatedRoute: ActivatedRoute,
    private readonly takenlijstService: TakenlijstService,
    private readonly gezinService: GezinService
  ) {
    this.activatedRoute.data.subscribe((response: any) => {
      this.gezinsleden = [];
      this.gezinService.leesHuidigGezin().subscribe((gezin: LeesGezin) => {
        gezin.gezinsleden.forEach((gezinslid: Gezinslid) => {
          this.gezinsleden.push({
            id: gezinslid.id,
            voornaam: gezinslid.voornaam,
            geselecteerd: false
          });
        });
        this.taken = this.map(response.takenlijst.taken);
      });
    });
  }

  isEersteKeer(): boolean {
    let result: boolean = this.eerste;
    if (this.eerste) {
      this.eerste = false;
    }
    return result;
  }

  resetEersteKeer() {
    this.eerste = true;
  }

  nieuweTaak(): void {
    this.nieuweTaakModus = true;
  }

  map(taken: Taak[]): TaakOmToeTeWijzen[] {
    if (taken == undefined) {
      return [];
    }
    return taken.map((taak: Taak) => {
      let taakOmToeTeWijzen: TaakOmToeTeWijzen = {
        id: taak.id,
        gezin: taak.gezin,
        omschrijving: taak.omschrijving,
        deadline: taak.deadline,
        tijdstipAfgestreept: taak.tijdstipAfgestreept,
        gezinsledenToewijzen: []
      };

      if (taak.gezinsleden != undefined) {
        this.gezinsleden.forEach((l: any) => {
          let glt = {
            id: l.id,
            voornaam: l.voornaam,
            geselecteerd: false
          };

          let index: number = taak.gezinsleden!.findIndex((id: number) => {
            return id == l.id;
          });

          glt.geselecteerd = index > -1;
          taakOmToeTeWijzen.gezinsledenToewijzen.push(glt);
        });
      }
      return taakOmToeTeWijzen;
    });
  }

  ophalen() {
    this.takenlijstService.alleTaken().subscribe((taken: LijstTaken) => {
      this.taken = this.map(taken.taken);
    });
  }

  mapGeselecteerdeGezinsledenNaarIds(gezinsleden: Array<{
    id: number,
    voornaam: string,
    geselecteerd: boolean
  }>): number[] {
    return gezinsleden
      .filter((lid: any) => {
        return lid.geselecteerd;
      })
      .map((lid: any) => {
        return lid.id;
      });
  }

  opslaan(): void {
    if (this.nieuweTaakModus) {
      let gezinsleden = this.mapGeselecteerdeGezinsledenNaarIds(this.gezinsleden);
      this.takenlijstService.nieuweTaak({'omschrijving': this.omschrijving, 'gezinsleden': gezinsleden}).subscribe((respone: AlgemeneResponse) => {
        this.afbreken();
        this.ophalen();
      });
    } else {
      let index: number = this.taken.findIndex((t: Taak) => {
        return t.id == this.wijzigId;
      });
      if (index > -1) {
        let taak: TaakOmToeTeWijzen = this.taken.splice(index, 1)[0];
        taak.gezinsleden = this.mapGeselecteerdeGezinsledenNaarIds(taak.gezinsledenToewijzen);
        taak.gezinsleden = taak.gezinsledenToewijzen
          .filter((lid: any) => {
            return lid.geselecteerd;
          })
          .map((lid: any) => {
            return lid.id;
          });
        this.takenlijstService.wijzigTaak(taak).subscribe((respone: AlgemeneResponse) => {
          this.wijzigId = 0;
          this.afbreken();
          this.ophalen();
        });
      }
    }
  }

  afbreken(): void {
    this.omschrijving = '';
    this.nieuweTaakModus = false;
  }

  wijzigItem(taakId: number) {
    this.wijzigId = taakId;
    this.nieuweTaakModus = false;
  }

  verwijderen(taakId: number) {
    this.takenlijstService.verwijderTaak(taakId).subscribe((response: AlgemeneResponse) => {
      this.ophalen();
    });
  }

  afvinken(taak: Taak): void {
    taak.tijdstipAfgestreept = moment().format();
    this.takenlijstService.wijzigTaak(taak).subscribe((response: AlgemeneResponse) => {
      let index = this.taken.findIndex(a => {
        return a.id == taak.id;
      });

      this.taken.splice(index, 1);
    });
  }
}

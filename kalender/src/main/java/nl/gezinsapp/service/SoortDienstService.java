package nl.gezinsapp.service;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;
import jakarta.enterprise.context.ApplicationScoped;
import nl.gezinsapp.commons.aspect.MethodeLogging;
import nl.gezinsapp.model.SoortDienst;

@ApplicationScoped
public class SoortDienstService {
    @MethodeLogging
    public void verwijderSoortDienstBijGezinslid(Long gezinslidId) {
        SoortDienst.findAll().stream()
                .map(SoortDienst.class::cast)
                .filter(soortDienst -> soortDienst.getGezinslid().equals(gezinslidId))
                .forEach(PanacheEntityBase::delete);

    }
}

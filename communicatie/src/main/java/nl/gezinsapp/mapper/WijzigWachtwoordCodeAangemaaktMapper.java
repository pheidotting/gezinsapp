package nl.gezinsapp.mapper;

import nl.gezinsapp.messaging.model.WijzigWachtwoordCodeAangemaakt;
import nl.gezinsapp.model.CommWijzigWachtwoordCodeAangemaakt;
import org.mapstruct.Mapper;

@Mapper(componentModel = "CDI")
public interface WijzigWachtwoordCodeAangemaaktMapper {
    CommWijzigWachtwoordCodeAangemaakt map(WijzigWachtwoordCodeAangemaakt wijzigWachtwoordAangemaakt);
}

package nl.gezinsapp.integratietesten;

import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.openapi.quarkus.gezin_openapi_yaml.model.Gezinslid;

import static nl.gezinsapp.integratietesten.common.random.RandomGezin.randomDomainGezin;
import static nl.gezinsapp.integratietesten.common.random.RandomGezinslid.randomDomainGezinslid;
import static org.junit.jupiter.api.Assertions.assertEquals;

@Slf4j
@QuarkusTest
@QuarkusTestResource(MockServer.class)
class LeesGebruikerOpEmailIntegratieTest extends AbstractIntegratieTest {
    @Test
    @DisplayName("Ophalen van een Gebruiker op e-mailadres")
    void test1() {
        maakDatabaseLeeg();

        var gezin = randomDomainGezin();
        opslaanGezin(gezin);

        var gebruiker1 = randomDomainGezinslid();
        gebruiker1.setGezin(gezin);
        var gebruiker2 = randomDomainGezinslid();
        gebruiker2.setGezin(gezin);

        opslaanGebruiker(gebruiker1);
        opslaanGebruiker(gebruiker2);

        var resultaat1 = leesViaApi(org.openapi.quarkus.gezin_openapi_yaml.model.Gezinslid.class, "/api/gezin/gebruiker/" + gebruiker1.getEmailadres(), null, null);
        assertEquals(gebruiker1.getAchternaam(), resultaat1.getAchternaam());

        var resultaat2 = leesViaApi(Gezinslid.class, "/api/gezin/gebruiker/" + gebruiker2.getEmailadres(), null, null);
        assertEquals(gebruiker2.getAchternaam(), resultaat2.getAchternaam());
    }

}

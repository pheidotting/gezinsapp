package nl.gezinsapp.integratietesten;

import com.fasterxml.jackson.core.JsonProcessingException;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.openapi.quarkus.gezin_openapi_yaml.model.AlgemeneResponse;

import java.time.Duration;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import static nl.gezinsapp.integratietesten.common.random.RandomGezin.randomDomainGezin;
import static nl.gezinsapp.integratietesten.common.random.RandomGezin.randomMsgGezin;
import static org.awaitility.Awaitility.await;
import static org.junit.jupiter.api.Assertions.assertNull;

@QuarkusTest
@QuarkusTestResource(MockServer.class)
class OpslaanGezinIntegratieTest extends AbstractIntegratieTest {
    @Test
    @DisplayName("Test het opslaan van een nieuw Gezin")
    void testNieuw() throws JsonProcessingException {
        maakDatabaseLeeg();

        var accesstoken = maakToken();
        var trackAndTraceId = UUID.randomUUID().toString();
        var jwtResponse = mockCheckJwt(accesstoken,trackAndTraceId);

        var domGezin = randomDomainGezin();
        opslaanGezin(domGezin);
        var gezin = randomMsgGezin();
        gezin.setId(domGezin.getId());

        var result = postViaApi(AlgemeneResponse.class, "/api/gezin", gezin, accesstoken,trackAndTraceId);
        assertNull(result.getFoutmeldingen());

        await()
                .atMost(2, TimeUnit.MINUTES)
                .pollInterval(Duration.ofMillis(500))
                .until(() -> alleGezinnen().size() == 1);

    }
}

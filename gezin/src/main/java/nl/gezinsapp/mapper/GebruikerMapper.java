package nl.gezinsapp.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.openapi.quarkus.gezin_openapi_yaml.model.Gebruiker;

@Mapper(componentModel = "CDI")
public abstract class GebruikerMapper {
    @Mapping(target = "gezin", expression = "java(gebruiker instanceof nl.gezinsapp.model.Gezinslid ? ((nl.gezinsapp.model.Gezinslid) gebruiker).getGezin().getId() : null)")
    @Mapping(target = "soort", expression = "java(gebruiker instanceof nl.gezinsapp.model.Beheerder ? Gebruiker.SoortEnum.BEHEERDER : Gebruiker.SoortEnum.GEZINSLID)")
    public abstract Gebruiker map(nl.gezinsapp.model.Gebruiker gebruiker);

    Long map(nl.gezinsapp.model.Gezin gezin) {
        return gezin.getId();
    }
}

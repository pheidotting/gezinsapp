import {TestBed} from '@angular/core/testing';

import {AanmeldenComponent} from './aanmelden.component';
import {Gezinslid, GezinslidGeslachtEnum, GezinslidSoortGezinslidEnum} from "../../api/gezin";
import {provideRouter, Router, RouterModule} from "@angular/router";
import {provideHttpClient, withInterceptorsFromDi} from "@angular/common/http";
import {provideHttpClientTesting} from "@angular/common/http/testing";
import {By} from "@angular/platform-browser";

describe('AanmeldenComponent', () => {
  let router: Router;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [AanmeldenComponent,
        RouterModule.forRoot([])],
      providers: [provideRouter([]), provideHttpClient(withInterceptorsFromDi()), provideHttpClientTesting()]
    });
    router = TestBed.inject(Router);
  });

  it('should return false, als familienaam NIET gevuld is', () => {
    let fixture = TestBed.createComponent(AanmeldenComponent);

    let component = fixture.debugElement.componentInstance;
    expect(component).toBeTruthy();

    component.gezin.naam = '';
    component.gezin.gezinsleden = [{
      id: 0,
      voornaam: 'Patrick',
      achternaam: 'Heidotting',
      soortGezinslid: GezinslidSoortGezinslidEnum.Ouder,
      geslacht: GezinslidGeslachtEnum.Man,
      wachtwoord: 'a'
    }]
    component.wachtwoordnogmaals = 'a';

    component.isCorrectGevuld();

    expect(component.foutmeldingen.length).toEqual(1);
    expect(component.foutmeldingen[0]).toEqual('Gezinsnaam mag niet leeg zijn');
  });

  it('should return true, als familienaam WEL gevuld is', () => {
    let fixture = TestBed.createComponent(AanmeldenComponent);

    let component = fixture.debugElement.componentInstance;

    component.gezin.naam = 'Heidotting & Co';
    component.gezin.gezinsleden = [{
      id: 0,
      voornaam: 'Patrick',
      achternaam: 'Heidotting',
      soortGezinslid: GezinslidSoortGezinslidEnum.Ouder,
      geslacht: GezinslidGeslachtEnum.Man,
      wachtwoord: 'a'
    }]
    component.wachtwoordnogmaals = 'a';

    component.isCorrectGevuld();

    expect(component.foutmeldingen.length).toEqual(0);
  });

  it('should return false, als er geen familieleden aanwezig zijn', () => {
    let fixture = TestBed.createComponent(AanmeldenComponent);

    let component = fixture.debugElement.componentInstance;

    component.gezin.naam = 'Heidotting & Co';
    component.gezin.gezinsleden = [];

    component.isCorrectGevuld();

    expect(component.foutmeldingen.length).toEqual(1);
    expect(component.foutmeldingen[0]).toEqual('Er moeten wel gezinsleden aanwezig zijn');
  });
  it('should return true, als het enige gezinslid correct gevuld is', () => {
    let fixture = TestBed.createComponent(AanmeldenComponent);

    let component = fixture.debugElement.componentInstance;

    component.gezin.naam = 'Heidotting & Co';
    component.gezin.gezinsleden = [{
      id: 0,
      voornaam: 'Patrick',
      achternaam: '',
      soortGezinslid: GezinslidSoortGezinslidEnum.Ouder,
      geslacht: GezinslidGeslachtEnum.Man,
      wachtwoord: 'a'
    }]
    component.wachtwoordnogmaals = 'a';

    component.isCorrectGevuld();

    expect(component.foutmeldingen.length).toEqual(1);
    expect(component.foutmeldingen[0]).toEqual('Achternaam mag niet leeg zijn');
  });
  it('should return false, als bij het enige gezinslid voornaam niet gevuld is', () => {
    let fixture = TestBed.createComponent(AanmeldenComponent);

    let component = fixture.debugElement.componentInstance;

    component.gezin.naam = 'Heidotting & Co';
    component.gezin.gezinsleden = [{
      id: 0,
      voornaam: '',
      achternaam: 'Heidotting',
      soortGezinslid: GezinslidSoortGezinslidEnum.Ouder,
      geslacht: GezinslidGeslachtEnum.Man,
      wachtwoord: 'a'
    }]
    component.wachtwoordnogmaals = 'a';

    component.isCorrectGevuld();

    expect(component.foutmeldingen.length).toEqual(1);
    expect(component.foutmeldingen[0]).toEqual('Voornaam mag niet leeg zijn');
  });
  it('should return false, als bij het enige gezinslid achternaam niet gevuld is', () => {
    let fixture = TestBed.createComponent(AanmeldenComponent);

    let component = fixture.debugElement.componentInstance;

    component.gezin.naam = 'Heidotting & Co';
    component.gezin.gezinsleden = [{
      id: 0,
      voornaam: 'Patrick',
      achternaam: '',
      soortGezinslid: GezinslidSoortGezinslidEnum.Ouder,
      geslacht: GezinslidGeslachtEnum.Man,
      wachtwoord: 'a'
    }]
    component.wachtwoordnogmaals = 'a';

    component.isCorrectGevuld();

    expect(component.foutmeldingen.length).toEqual(1);
    expect(component.foutmeldingen[0]).toEqual('Achternaam mag niet leeg zijn');
  });
  it('should return false, als het enige gezinslid correct gevuld is, maar het tweede niet', () => {
    let fixture = TestBed.createComponent(AanmeldenComponent);

    let component = fixture.debugElement.componentInstance;

    component.gezin.naam = 'Heidotting & Co';
    component.gezin.gezinsleden = [
      {
        id: 0,
        voornaam: 'Patrick',
        achternaam: 'Heidotting',
        soortGezinslid: GezinslidSoortGezinslidEnum.Ouder,
        geslacht: GezinslidGeslachtEnum.Man,
        wachtwoord: 'a'
      },
      {id: 0, voornaam: '', achternaam: 'Heidotting', soortGezinslid: GezinslidSoortGezinslidEnum.Ouder, geslacht: GezinslidGeslachtEnum.Vrouw}
    ]
    component.wachtwoordnogmaals = 'a';

    component.isCorrectGevuld();

    expect(component.foutmeldingen.length).toEqual(1);
    expect(component.foutmeldingen[0]).toEqual('Voornaam mag niet leeg zijn');
  });
  it('should return true, als beide gezinsleden correct zijn gevuld', () => {
    let fixture = TestBed.createComponent(AanmeldenComponent);

    let component = fixture.debugElement.componentInstance;

    component.gezin.naam = 'Heidotting & Co';
    component.gezin.gezinsleden = [
      {
        id: 0,
        voornaam: 'Patrick',
        achternaam: 'Heidotting',
        soortGezinslid: GezinslidSoortGezinslidEnum.Ouder,
        geslacht: GezinslidGeslachtEnum.Man,
        wachtwoord: 'a'
      },
      {id: 0, voornaam: 'Sabine', achternaam: 'Heidotting', soortGezinslid: GezinslidSoortGezinslidEnum.Ouder, geslacht: GezinslidGeslachtEnum.Vrouw}
    ]
    component.wachtwoordnogmaals = 'a';

    component.isCorrectGevuld();

    expect(component.foutmeldingen.length).toEqual(0);
  });
  it('naam opmaken met alleen voornaam', () => {
    let fixture = TestBed.createComponent(AanmeldenComponent);

    let component = fixture.debugElement.componentInstance;

    let gezinslid: Gezinslid = {
      id: 0,
      voornaam: 'Patrick',
      achternaam: '',
      soortGezinslid: GezinslidSoortGezinslidEnum.Ouder,
      geslacht: GezinslidGeslachtEnum.Man
    };

    expect(component.maakNaamOp(gezinslid)).toEqual('(Patrick, Ouder)');
  });
  it('naam opmaken met voornaam en achternaam', () => {
    let fixture = TestBed.createComponent(AanmeldenComponent);

    let component = fixture.debugElement.componentInstance;

    let gezinslid: Gezinslid = {
      id: 0,
      voornaam: 'Patrick',
      achternaam: 'Heidotting',
      soortGezinslid: GezinslidSoortGezinslidEnum.Kind,
      geslacht: GezinslidGeslachtEnum.Man
    };

    expect(component.maakNaamOp(gezinslid)).toEqual('(Patrick Heidotting, Kind)');
  });
  it('naam opmaken met voornaam, achternaam en tussenvoegsel', () => {
    let fixture = TestBed.createComponent(AanmeldenComponent);

    let component = fixture.debugElement.componentInstance;

    let gezinslid: Gezinslid = {
      id: 0,
      voornaam: 'Patrick',
      achternaam: 'Heidotting',
      tussenvoegsel: 'ter',
      soortGezinslid: GezinslidSoortGezinslidEnum.Ouder,
      geslacht: GezinslidGeslachtEnum.Man
    };

    expect(component.maakNaamOp(gezinslid)).toEqual('(Patrick ter Heidotting, Ouder)');
  });
  it('naam opmaken met alleen achternaam', () => {
    let fixture = TestBed.createComponent(AanmeldenComponent);

    let component = fixture.debugElement.componentInstance;

    let gezinslid: Gezinslid = {
      id: 0,
      voornaam: '',
      achternaam: 'Heidotting',
      soortGezinslid: GezinslidSoortGezinslidEnum.Ouder,
      geslacht: GezinslidGeslachtEnum.Man
    };

    expect(component.maakNaamOp(gezinslid)).toEqual('(Heidotting, Ouder)');
  });
  it('naam opmaken met alleen achternaam en tussenvogsel', () => {
    let fixture = TestBed.createComponent(AanmeldenComponent);

    let component = fixture.debugElement.componentInstance;

    let gezinslid: Gezinslid = {
      id: 0,
      voornaam: '',
      achternaam: 'Heidotting',
      tussenvoegsel: 'de',
      soortGezinslid: GezinslidSoortGezinslidEnum.Ouder,
      geslacht: GezinslidGeslachtEnum.Man
    };

    expect(component.maakNaamOp(gezinslid)).toEqual('(de Heidotting, Ouder)');
  });
  it('naam opmaken met alleen voornaam en tussenvogsel', () => {
    let fixture = TestBed.createComponent(AanmeldenComponent);

    let component = fixture.debugElement.componentInstance;

    let gezinslid: Gezinslid = {
      id: 0,
      voornaam: 'Patrick',
      achternaam: '',
      tussenvoegsel: 'de',
      soortGezinslid: GezinslidSoortGezinslidEnum.Ouder,
      geslacht: GezinslidGeslachtEnum.Man
    };

    expect(component.maakNaamOp(gezinslid)).toEqual('(Patrick de, Ouder)');
  });
  it('wachtwoord niet akkoord, 2 lege velden', () => {
    let fixture = TestBed.createComponent(AanmeldenComponent);

    let component = fixture.debugElement.componentInstance;

    component.gezin.gezinsleden = [
      {id: 0, voornaam: 'Patrick', achternaam: 'Heidotting', soortGezinslid: GezinslidSoortGezinslidEnum.Ouder, geslacht: GezinslidGeslachtEnum.Man},
    ];

    component.checkWachtwoord();
    expect(component.wachtwoordakkoord).toBeFalsy();
  });
  it('wachtwoord niet akkoord, 2 ongelijke velden', () => {
    let fixture = TestBed.createComponent(AanmeldenComponent);

    let component = fixture.debugElement.componentInstance;

    component.gezin.gezinsleden = [
      {
        id: 0,
        voornaam: 'Patrick',
        achternaam: 'Heidotting',
        soortGezinslid: GezinslidSoortGezinslidEnum.Ouder,
        geslacht: GezinslidGeslachtEnum.Man,
        wachtwoord: 'ww'
      },
    ];

    component.checkWachtwoord();
    expect(component.wachtwoordakkoord).toBeFalsy();
  });

  it('wachtwoord akkoord, 2 gelijke velden', () => {
    let fixture = TestBed.createComponent(AanmeldenComponent);

    let component = fixture.debugElement.componentInstance;

    component.gezin.gezinsleden = [
      {
        id: 0,
        voornaam: 'Patrick',
        achternaam: 'Heidotting',
        soortGezinslid: GezinslidSoortGezinslidEnum.Ouder,
        geslacht: GezinslidGeslachtEnum.Man,
        wachtwoord: 'ww'
      },
    ];
    component.wachtwoordnogmaals = 'ww';

    component.checkWachtwoord();
    expect(component.wachtwoordakkoord).toBeTruthy();
  });

  it('achternaam wordt overgenomen uit gezinsnaam', async () => {
    let fixture = TestBed.createComponent(AanmeldenComponent);

    let component = fixture.debugElement.componentInstance;

    component.gezin.naam = 'Heidotting';

    component.familenaamGewijzigd();
    fixture.detectChanges();

    expect(component.gezin.gezinsleden[0].achternaam).toEqual('Heidotting');

    fixture.whenStable().then(() => {
      let achternaamVeld = fixture.debugElement.query(By.css('#achternaam'));
      expect(achternaamVeld.nativeElement.value).toEqual('Heidotting');
    });
  });
});

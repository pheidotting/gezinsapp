package nl.gezinsapp.integratietest;

import com.fasterxml.jackson.core.JsonProcessingException;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.openapi.quarkus.adresboek_openapi_yaml.model.AlgemeneResponse;

import static nl.gezinsapp.integratietest.random.RandomAdresBoek.randomDtoAdresBoek;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@Slf4j
@QuarkusTest
@QuarkusTestResource(MockServer.class)
class AdresBoekToevoegenIntegratieTest extends AbstractIntegratieTest {
    @Test
    @DisplayName("Sla een nieuwe AdresBoek op")
    void test() throws JsonProcessingException {
        var accesstoken = maakToken();
        var jwtResponse = mockCheckJwt(accesstoken);

        maakDatabaseLeeg();

        var item = randomDtoAdresBoek();
        var response = putViaApi(AlgemeneResponse.class, "/api/adresboek/adresboek", item, accesstoken);

        var itemGelezen = leesAdresBoek(response.getId());
        assertNotNull(itemGelezen);
        assertEquals(jwtResponse.getGebruiker().getGezin(), itemGelezen.getGezin());
        assertEquals(item.getGezinslid(), itemGelezen.getGezinslid());
        assertEquals(item.getOmschrijving(), itemGelezen.getOmschrijving());
    }
}

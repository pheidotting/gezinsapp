package nl.gezinsapp.commons.aspect;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.ws.rs.client.ClientRequestContext;
import jakarta.ws.rs.client.ClientRequestFilter;
import jakarta.ws.rs.ext.Provider;
import org.slf4j.MDC;

@Provider
@ApplicationScoped
public class RequestUUIDHeaderFactory implements ClientRequestFilter {
    @Override
    public void filter(ClientRequestContext requestContext) {
        var trackAndTraceId = MDC.get("trackAndTraceId");
        if (trackAndTraceId != null) {
            requestContext.getHeaders().add("trackAndTraceId", trackAndTraceId);
        }
    }
}
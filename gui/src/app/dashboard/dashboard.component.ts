import {Component} from '@angular/core';
import {GezinService, LeesGezin} from '../../api/gezin';
import {SideBarComponent} from "../common/side-bar/side-bar.component";
import {KalenderOverzichtComponent} from "./kalender-overzicht/kalender-overzicht.component";
import {BoodschappenWidgetComponent} from "./boodschappen-widget/boodschappen-widget.component";
import {TakenWidgetComponent} from "./taken-widget/taken-widget.component";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  imports: [
    SideBarComponent,
    KalenderOverzichtComponent,
    BoodschappenWidgetComponent,
    TakenWidgetComponent
  ]
})
export class DashboardComponent {
  gezin?: LeesGezin;

  constructor(
    private gezinService: GezinService
  ) {
    this.gezinService.leesHuidigGezin().subscribe((gezin: LeesGezin) => {
      this.gezin = gezin;
    });
  }
}

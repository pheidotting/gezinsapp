package nl.gezinsapp.model;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import nl.gezinsapp.model.enums.Dag;

import java.time.LocalTime;

import static jakarta.persistence.EnumType.STRING;

@Entity
@Table(name = "SOORTDIENST")
@Getter
@Setter
@ToString(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
public class SoortDienst extends PanacheEntityBase {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;
    @Column(name="OMSCHRIJVING")
    private String omschrijving;
    @Column(name="VAN")
    private LocalTime van;
    @Column(name="TOT")
    private LocalTime tot;
    @Column(name="GEZINSLID")
    private Long gezinslid;
    @Enumerated(STRING)
    @Column(name="DAG")
    private Dag dag;
}

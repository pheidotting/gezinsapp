package nl.gezinsapp.integratietest;

import com.fasterxml.jackson.core.JsonProcessingException;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.openapi.quarkus.adresboek_openapi_yaml.model.AdresBoekMetAdressen;
import org.openapi.quarkus.adresboek_openapi_yaml.model.AlgemeneResponse;

import static nl.gezinsapp.integratietest.random.RandomAdresBoek.*;
import static org.jboss.resteasy.reactive.RestResponse.StatusCode.FORBIDDEN;
import static org.jboss.resteasy.reactive.RestResponse.StatusCode.OK;
import static org.junit.jupiter.api.Assertions.*;

@Slf4j
@QuarkusTest
@QuarkusTestResource(MockServer.class)
class VoegAdresToeIntegratieTest extends AbstractIntegratieTest {
    @Test
    @DisplayName("Voeg een Adres toe aan een AdresBoek")
    void test() throws JsonProcessingException {
        var accesstoken = maakToken();
        var jwtResponse = mockCheckJwt(accesstoken);

        maakDatabaseLeeg();

        var adresBoek = randomAdresBoek();
        adresBoek.setGezin(jwtResponse.getGebruiker().getGezin());
        var adres1 = randomAdres();
        var adres2 = randomDtoAdres();

        opslaan(adresBoek);
        adres1.setAdresBoek(adresBoek);
        opslaan(adres1);

        adres2.setAdresBoek(adresBoek.getId());
        var response = putViaApi(AlgemeneResponse.class, "/api/adresboek/adres", adres2, accesstoken);
        assertNotNull(response.getId());

        var gelezenAdresBoek = getViaApi(AdresBoekMetAdressen.class, "/api/adresboek/adresboek/" + adresBoek.getId(), accesstoken, OK);

        assertNotNull(gelezenAdresBoek);
        assertEquals(adresBoek.getOmschrijving(), gelezenAdresBoek.getOmschrijving());
        assertEquals(2, gelezenAdresBoek.getAdressen().size());
    }

    @Test
    @DisplayName("Lees een AdresBoek van een ander Gezin")
    void testFoutGezin() throws JsonProcessingException {
        var accesstoken = maakToken();
        mockCheckJwt(accesstoken);

        maakDatabaseLeeg();

        var adresBoek = randomAdresBoek();
        adresBoek.setGezin(99L);
        var adres1 = randomAdres();
        var adres2 = randomAdres();

        opslaan(adresBoek);
        adres1.setAdresBoek(adresBoek);
        adres2.setAdresBoek(adresBoek);
        opslaan(adres1);
        opslaan(adres2);

        assertNull(getViaApi(null, "/api/adresboek/adresboek/" + adresBoek.getId(), accesstoken, FORBIDDEN));
    }
}

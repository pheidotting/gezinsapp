package nl.gezinsapp.commons.aspect;

import jakarta.annotation.Priority;
import jakarta.interceptor.AroundInvoke;
import jakarta.interceptor.Interceptor;
import jakarta.interceptor.InvocationContext;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.HttpHeaders;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.MDC;

import java.util.UUID;

@Slf4j
@TrackAndTraceId
@Priority(2010)
@Interceptor
@RequiredArgsConstructor
public class TrackAndTraceIdAspect {
    @Context
    HttpHeaders headers;

    @AroundInvoke
    Object logInvocation(InvocationContext context) throws Exception {
        final String TRACKANDTRACEID = "trackAndTraceId";

        var trackAndTraceId = MDC.get(TRACKANDTRACEID);
        if (trackAndTraceId == null) {
            trackAndTraceId = headers.getHeaderString(TRACKANDTRACEID);
        }
        if (trackAndTraceId == null) {
            trackAndTraceId = UUID.randomUUID().toString();
        }

        MDC.put(TRACKANDTRACEID, trackAndTraceId);

        return context.proceed();
    }
}

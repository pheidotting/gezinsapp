import {fakeAsync, TestBed} from '@angular/core/testing';
import {ActivatedRoute, GuardResult, MaybeAsync, Router, RouterStateSnapshot} from '@angular/router';

import {beheerGuard} from './beheer.guard';
import {TokenService} from "../services/token.service";
import {Observable, of} from 'rxjs';

describe('beheerGuard', () => {
  it('beheerder, dus true', fakeAsync(() => {
    let token = {
      soortGebruiker: 'BEHEERDER'
    };
    let decodedToken = 'a.' + window.btoa(JSON.stringify(token));

    TestBed.configureTestingModule({
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: {},
          }
        },
        {
          provide: TokenService,
          useValue: {accessToken: of(decodedToken)}
        }
      ]
    });

    const activatedRoute = TestBed.inject(ActivatedRoute);

    const guardResponse: MaybeAsync<GuardResult> = TestBed.runInInjectionContext(() => {
      return beheerGuard(activatedRoute.snapshot, {} as RouterStateSnapshot);
    });

    (guardResponse as unknown as Observable<string>).subscribe((s: string) => {
      expect(s).toBeTrue();
    });
  }));

  it('geen beheerder, dus false', fakeAsync(() => {
    let token = {
      soortGebruiker: 'GEZINSLID'
    };
    let decodedToken = 'a.' + window.btoa(JSON.stringify(token));

    TestBed.configureTestingModule({
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: {},
          }
        },
        {
          provide: TokenService,
          useValue: {accessToken: of(decodedToken)}
        }
      ]
    });

    const activatedRoute = TestBed.inject(ActivatedRoute);
    const routerSpy = spyOn(TestBed.inject(Router), 'navigate');

    const guardResponse: MaybeAsync<GuardResult> = TestBed.runInInjectionContext(() => {
      return beheerGuard(activatedRoute.snapshot, {} as RouterStateSnapshot);
    });

    (guardResponse as unknown as Observable<string>).subscribe((s: string) => {
      expect(s).toBeFalse();
      expect(routerSpy).toHaveBeenCalledWith(['/inloggen']);
    });
  }));
});

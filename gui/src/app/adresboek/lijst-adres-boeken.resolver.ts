import {ActivatedRouteSnapshot, Resolve} from '@angular/router';
import {AdresboekService} from '../../api/adresboek';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class LijstAdresBoekenResolver implements Resolve<any> {
  constructor(
    private adresboekService: AdresboekService
  ) {
  }

  resolve(route: ActivatedRouteSnapshot): Observable<any> {
    return this.adresboekService.lijstAdresBoeken();
  }
}

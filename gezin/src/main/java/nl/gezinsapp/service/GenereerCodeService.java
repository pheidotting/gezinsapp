package nl.gezinsapp.service;

import jakarta.annotation.PostConstruct;
import jakarta.enterprise.context.ApplicationScoped;
import nl.gezinsapp.commons.aspect.MethodeLogging;

import java.util.Random;

@ApplicationScoped
public class GenereerCodeService {
    private Random random;

    @PostConstruct
    public void init() {
        random = new Random();
    }

    @MethodeLogging
    public String genereerCode() {
        var stringBuilder = new StringBuilder();

        var randomGetal = random.nextInt();

        if (randomGetal < 0) {
            randomGetal = randomGetal * -1;
        }

        stringBuilder.append(randomGetal);
        if (stringBuilder.toString().length() < 6) {
            stringBuilder.append(genereerCode());
        }

        return stringBuilder.toString().substring(0, 6);
    }
}

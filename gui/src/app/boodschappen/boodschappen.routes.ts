import {Routes} from '@angular/router';
import {BoodschappenComponent} from "./boodschappen.component";
import {boodschappenResolver} from "./boodschappen.resolver";
import {lijstGezinsledenResolver} from "../gezin/lijst-gezinsleden.resolver";

export const BOODSCHAPPEN_ROUTES: Routes = [
  {
    path: '',
    component: BoodschappenComponent,
    resolve: {
      boodschappen: boodschappenResolver,
      gezin: lijstGezinsledenResolver

    }
  }
]

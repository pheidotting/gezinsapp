package nl.gezinsapp.service;

import io.quarkus.qute.Template;
import jakarta.enterprise.context.ApplicationScoped;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.gezinsapp.model.CommActivatieCodeAangemaakt;
import org.openapi.quarkus.gezin_openapi_yaml.model.LeesGezin;

@Slf4j
@ApplicationScoped
@RequiredArgsConstructor
public class ActivatieCodeAangemaaktService extends AbstractService<CommActivatieCodeAangemaakt> {
    private final Template activatieCodeAangemaakt;

    @Override
    public void vulCommObject(CommActivatieCodeAangemaakt commActivatieCodeAangemaakt, LeesGezin gezin) {

    }

    @Override
    public Template getTemplate() {
        return activatieCodeAangemaakt;
    }

    @Override
    public String getOnderwerp() {
        return "Activatiecode";
    }
}

package nl.gezinsapp.domain;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.time.LocalDateTime;

@Entity
@Table(name = "EMAIL")
@Getter
@Setter
@ToString(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
public class Email extends PanacheEntityBase {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;
    @Column(name = "GEZINSLID")
    private Long gezinslid;
    @Column(name = "EMAILADRES")
    private String emailadres;
    @Column(name = "TIJDSTIPAANGEMAAKT")
    private LocalDateTime tijdstipaangemaakt;
    @Column(name = "TIJDSTIPVERSTUURD")
    private LocalDateTime tijdstipverstuurd;
    @Column(name = "ONDERWERP")
    private String onderwerp;
    @Column(name = "TEKST")
    private String tekst;
}

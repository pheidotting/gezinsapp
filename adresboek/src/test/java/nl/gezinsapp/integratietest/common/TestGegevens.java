package nl.gezinsapp.integratietest.common;

import lombok.Getter;
import lombok.Setter;

import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import static com.google.common.collect.Maps.newHashMap;

public class TestGegevens {
    private TestGegevens() {
    }

    @Getter
    @Setter
    private static int localPort;
    @Getter
    @Setter
    private static String code;
    private static String token;
    private static UUID trackAndTraceId;
    private static UUID symplexSessieId;
    private static Map<Class, UUID> uitgaandeOpdrachtIds = newHashMap();
    @Getter
    private static Map<String, String> queues = newHashMap();
    @Getter
    private static Map<String, String> topics = newHashMap();
    private static Object channel;
    private static Map<String, String> teksten = newHashMap();
    private static Connection dbConnection;
    private static Long id;
    private static UUID uuidId;
    private static Object opgeslagenObject;

    private static String rabbitHost;
    private static int rabbitPort;
    private static String rabbitUser;
    private static String rabbitPassword;

    private static String mailhogHost;
    private static int mailhogPort;

    private static Long revId;

    private static Object mockServerClient;

    private static Map<String, QueueUtil<?>> queueUtils = new HashMap<>();

    public static UUID randomTrackAndTraceId() {
        trackAndTraceId = UUID.randomUUID();
        return trackAndTraceId;
    }

    public static UUID randomSymplexSessieId() {
        symplexSessieId = UUID.randomUUID();
        return symplexSessieId;
    }

    public static String getToken() {
        return token;
    }

    public static void setToken(String token) {
        TestGegevens.token = token;
    }

    public static UUID getTrackAndTraceId() {
        if (trackAndTraceId == null) {
            return randomTrackAndTraceId();
        }
        return trackAndTraceId;
    }

    public static UUID getSymplexSessieId() {
        if (symplexSessieId == null) {
            return randomSymplexSessieId();
        }
        return symplexSessieId;
    }

    public static void setTrackAndTraceId(UUID trackAndTraceId) {
        TestGegevens.trackAndTraceId = trackAndTraceId;
    }

    public static Map<Class, UUID> getUitgaandeOpdrachtIds() {
        return uitgaandeOpdrachtIds;
    }

    public static Map<String, String> getQueues() {
        return queues;
    }

    public static Object getChannel() {
        return channel;
    }

    public static void setChannel(Object channel) {
        TestGegevens.channel = channel;
    }

    public static Map<String, String> getTeksten() {
        return teksten;
    }

    public static Connection getDbConnection() {
        return dbConnection;
    }

    public static void setDbConnection(Connection dbConnection) {
        TestGegevens.dbConnection = dbConnection;
    }

    public static Long getId() {
        return id;
    }

    public static void setId(Long id) {
        TestGegevens.id = id;
    }

    public static UUID getUuidId() {
        return uuidId;
    }

    public static void setUuidId(UUID uuidId) {
        TestGegevens.uuidId = uuidId;
    }

    public static Object getOpgeslagenObject() {
        return opgeslagenObject;
    }

    public static void setOpgeslagenObject(Object opgeslagenObject) {
        TestGegevens.opgeslagenObject = opgeslagenObject;
    }

    public static String getRabbitHost() {
        return rabbitHost;
    }

    public static void setRabbitHost(String rabbitHost) {
        TestGegevens.rabbitHost = rabbitHost;
    }

    public static int getRabbitPort() {
        return rabbitPort;
    }

    public static void setRabbitPort(int rabbitPort) {
        TestGegevens.rabbitPort = rabbitPort;
    }

    public static String getRabbitUser() {
        return rabbitUser;
    }

    public static void setRabbitUser(String rabbitUser) {
        TestGegevens.rabbitUser = rabbitUser;
    }

    public static String getRabbitPassword() {
        return rabbitPassword;
    }

    public static void setRabbitPassword(String rabbitPassword) {
        TestGegevens.rabbitPassword = rabbitPassword;
    }

    public static String getMailhogHost() {
        return mailhogHost;
    }

    public static void setMailhogHost(String mailhogHost) {
        TestGegevens.mailhogHost = mailhogHost;
    }

    public static int getMailhogPort() {
        return mailhogPort;
    }

    public static void setMailhogPort(int mailhogPort) {
        TestGegevens.mailhogPort = mailhogPort;
    }

    public static Long getRevId() {
        return revId;
    }

    public static void setRevId(Long revId) {
        TestGegevens.revId = revId;
    }

    public static Map<String, QueueUtil<?>> getQueueUtils() {
        return queueUtils;
    }

    public static Object mockServerClient() {
        return mockServerClient;
    }

    public static void setMockServerClient(Object mockServerClient) {
        TestGegevens.mockServerClient = mockServerClient;
    }
}

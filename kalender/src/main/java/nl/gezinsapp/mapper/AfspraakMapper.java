package nl.gezinsapp.mapper;

import nl.gezinsapp.model.Afspraak;
import nl.gezinsapp.model.SoortDienst;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;

@Mapper(componentModel = "CDI")
public abstract class AfspraakMapper {
    @Mapping(target = "soortDienst", ignore = true)
    public abstract Afspraak map(org.openapi.quarkus.kalender_openapi_yaml.model.Afspraak afspraak);

    @Mapping(target = "soortDienst", ignore = true)
    public abstract Afspraak update(@MappingTarget Afspraak bestaandeAfspraak, org.openapi.quarkus.kalender_openapi_yaml.model.Afspraak afspraak);

    public abstract org.openapi.quarkus.kalender_openapi_yaml.model.Afspraak map(Afspraak afspraak);

    Long map(SoortDienst value) {
        return value == null ? null : value.getId();
    }
    protected LocalDateTime map(OffsetDateTime value) {
        return value == null ? null : value.toLocalDateTime();
    }

    protected OffsetDateTime map(LocalDateTime value) {
        return value == null ? null : OffsetDateTime.of(value, ZoneOffset.ofHours(2));
    }
}


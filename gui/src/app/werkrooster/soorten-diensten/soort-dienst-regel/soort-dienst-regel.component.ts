import {Component, EventEmitter, Input, Output} from '@angular/core';
import {AlgemeneResponse, RoosterService, SoortDienst, SoortDienstDagEnum} from "../../../../api/kalender";
import {NgIf} from "@angular/common";
import {FormsModule} from "@angular/forms";

@Component({
  selector: '[app-soort-dienst-regel]',
  templateUrl: './soort-dienst-regel.component.html',
  imports: [
    NgIf,
    FormsModule
  ]
})
export class SoortDienstRegelComponent {
  @Input() soortDienst!: SoortDienst;
  bewerkModus: boolean = false;
  @Output() foutmeldingToevoegenEventEmitter: EventEmitter<string> = new EventEmitter;
  @Output() foutmeldingResetEventEmitter: EventEmitter<void> = new EventEmitter;
  @Output() verwijderenSoortDienstEventEmitter: EventEmitter<SoortDienst> = new EventEmitter;
  @Output() regelOpgeslagenEventEmitter: EventEmitter<void> = new EventEmitter;

  verwijderModalTonen: boolean = false;

  constructor(
    private roosterService: RoosterService
  ) {
  }

  maakDagOp(dag: SoortDienstDagEnum | undefined): string {
    if (dag != null) {
      if (dag == SoortDienstDagEnum.Ma) {
        return 'Maandag';
      } else if (dag == SoortDienstDagEnum.Di) {
        return 'Dinsdag';
      } else if (dag == SoortDienstDagEnum.Wo) {
        return 'Woensdag';
      } else if (dag == SoortDienstDagEnum.Do) {
        return 'Donderdag';
      } else if (dag == SoortDienstDagEnum.Vr) {
        return 'Vrijdag';
      } else if (dag == SoortDienstDagEnum.Za) {
        return 'Zaterdag';
      } else if (dag == SoortDienstDagEnum.Zo) {
        return 'Zondag';
      } else {
        return 'Geen vaste dag';
      }
    }
    return 'Geen vaste dag';
  }

  bewerk(): void {
    this.bewerkModus = true;
  }

  setVerwijderSoortDienst(): void {
    this.verwijderenSoortDienstEventEmitter.emit(this.soortDienst);
  }

  opslaan(): void {
    this.foutmeldingResetEventEmitter.emit();
    let allesOk: boolean = true;
    if (this.soortDienst.van == null || this.soortDienst.van == '') {
      this.foutmeldingToevoegenEventEmitter.emit('Van tijd is niet (juist) gevuld');
      allesOk = false;
    }
    if (this.soortDienst.tot == null || this.soortDienst.tot == '') {
      this.foutmeldingToevoegenEventEmitter.emit('Tot tijd is niet (juist) gevuld');
      allesOk = false;
    }
    if (allesOk && (this.soortDienst.id == null || this.soortDienst.id == 0)) {
      if (this.soortDienst.dag != null && this.soortDienst.dag.toString() == '') {
        this.soortDienst.dag = undefined;
      }
      this.roosterService.nieuweSoortenDienstenOpslaan(this.soortDienst).subscribe((response: AlgemeneResponse) => {
        this.bewerkModus = false;
        this.soortDienst.id = response.id!;
        this.regelOpgeslagenEventEmitter.emit();
      });
    } else if (allesOk) {
      this.roosterService.bestaandeSoortenDienstenOpslaan(this.soortDienst).subscribe((response: AlgemeneResponse) => {
        this.bewerkModus = false;
        this.soortDienst.id = response.id!;
        this.regelOpgeslagenEventEmitter.emit();
      });
    }
  }

  maakTijdOp(tijd: string) {
    return tijd.substring(0, 5);
  }
}

package nl.gezinsapp.model;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;
import jakarta.persistence.Column;
import jakarta.persistence.Convert;
import jakarta.persistence.Entity;
import jakarta.persistence.Enumerated;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.Singular;
import lombok.ToString;
import nl.gezinsapp.model.enums.SoortAfspraak;
import nl.gezinsapp.model.enums.Terugkeer;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import static jakarta.persistence.EnumType.STRING;

@Entity
@Table(name = "AFSPRAAK")
@Getter
@Setter
@ToString(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Afspraak extends PanacheEntityBase {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;
    @Column(name = "OMSCHRIJVING")
    private String omschrijving;
    @Column(name = "HELEDAG")
    private boolean heledag;
    @Column(name = "VANTIJD")
    private LocalDateTime vanTijd;
    @Column(name = "TOTTIJD")
    private LocalDateTime totTijd;
    @Column(name = "VANDATUM")
    private LocalDate vanDatum;
    @Column(name = "TOTDATUM")
    private LocalDate totDatum;
    @Enumerated(STRING)
    @Column(name = "SOORTAFSPRAAK")
    private SoortAfspraak soortAfspraak;
    @Enumerated(STRING)
    @Column(name = "TERUGKEER")
    private Terugkeer terugkeer;
    @Column(name = "GEZIN")
    private Long gezin;
    @JoinColumn(name = "SOORTDIENST")
    @ManyToOne(fetch = FetchType.LAZY)
    private SoortDienst soortDienst;
    @Column(name = "GEZINSLEDEN")
    @Convert(converter = ListConverter.class)
    @Singular("gezinslid")
    private List<Long> gezinsleden;


    public static List<Afspraak> allesBijGezin(Long gezin) {
        return list("gezin", gezin);
    }

    public Afspraak(Afspraak afspraak) {
        id = afspraak.id;
        omschrijving = afspraak.omschrijving;
        heledag = afspraak.heledag;
        vanTijd = afspraak.vanTijd;
        totTijd = afspraak.totTijd;
        vanDatum = afspraak.vanDatum;
        totDatum = afspraak.totDatum;
        soortAfspraak = afspraak.soortAfspraak;
        terugkeer = afspraak.terugkeer;
        gezin = afspraak.gezin;
    }
}

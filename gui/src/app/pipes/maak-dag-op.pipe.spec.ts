import {MaakDagOpPipe} from './maak-dag-op.pipe';

describe('MaakMomentDagOpPipe', () => {
  it('create an instance', () => {
    const pipe = new MaakDagOpPipe();

    let datum: string = '2024-10-08T14:31';

    expect(pipe).toBeTruthy();
    expect(pipe.transform(datum)).toEqual('dinsdag');
  });
});

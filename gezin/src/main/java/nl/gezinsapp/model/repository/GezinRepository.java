package nl.gezinsapp.model.repository;

import io.quarkus.hibernate.orm.panache.PanacheRepository;
import jakarta.enterprise.context.ApplicationScoped;
import nl.gezinsapp.model.Gezin;

@ApplicationScoped
public class GezinRepository implements PanacheRepository<Gezin> {
}

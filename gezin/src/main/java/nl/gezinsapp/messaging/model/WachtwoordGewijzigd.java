package nl.gezinsapp.messaging.model;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.UUID;

@Setter
@Getter
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class WachtwoordGewijzigd extends AbstractMessagingObject {
    @Builder
    public WachtwoordGewijzigd(String emailadres, Long gezinsId, Long gezinslidId, UUID trackAndTraceId) {
        super(emailadres, gezinsId, gezinslidId,trackAndTraceId);
    }
}

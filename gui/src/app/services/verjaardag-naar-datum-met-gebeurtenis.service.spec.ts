import {TestBed} from '@angular/core/testing';

import {VerjaardagNaarDatumMetGebeurtenisService} from './verjaardag-naar-datum-met-gebeurtenis.service';

describe('VerjaardagNaarDatumMetGebeurtenisService', () => {
  let service: VerjaardagNaarDatumMetGebeurtenisService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(VerjaardagNaarDatumMetGebeurtenisService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

import {Pipe, PipeTransform} from '@angular/core';
import {Gezinslid} from '../../api/gezin/model/gezinslid';

@Pipe({
  name: 'gezinslidVoornaam',
  standalone: true
})
export class GezinslidVoornaamPipe implements PipeTransform {

  transform(gezinslid: Gezinslid | null): unknown {
    if (gezinslid == null) {
      return '';
    }
    return gezinslid.voornaam;
  }

}

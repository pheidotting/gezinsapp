package nl.gezinsapp.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class WijzigWachtwoord {
    private String email;
    private String wachtwoord;
    private String codeid;
    private String code;
}

package nl.gezinsapp.integratietesten.common.random;

import nl.gezinsapp.model.Gezin;
import uk.co.jemos.podam.api.DefaultClassInfoStrategy;
import uk.co.jemos.podam.api.PodamFactory;
import uk.co.jemos.podam.api.PodamFactoryImpl;

public class RandomGezin {
    private static PodamFactory factory;

    private static PodamFactory getFactory() {
        if (factory == null) {
            factory = new PodamFactoryImpl();
            DefaultClassInfoStrategy classInfoStrategy = DefaultClassInfoStrategy.getInstance();
            classInfoStrategy.addExcludedField(Gezin.class, "id");
            classInfoStrategy.addExcludedField(Gezin.class, "gezinsleden");
            classInfoStrategy.addExcludedField(org.openapi.quarkus.gezin_openapi_yaml.model.Gezin.class, "id");
            classInfoStrategy.addExcludedField(org.openapi.quarkus.gezin_openapi_yaml.model.Gezin.class, "gezinsleden");
            factory.setClassStrategy(classInfoStrategy);
        }
        return factory;
    }

    public static Gezin randomDomainGezin() {
        return getFactory().manufacturePojo(Gezin.class);
    }

    public static org.openapi.quarkus.gezin_openapi_yaml.model.Gezin randomMsgGezin() {
        return getFactory().manufacturePojo(org.openapi.quarkus.gezin_openapi_yaml.model.Gezin.class);
    }
}

package nl.gezinsapp.mapper;

import nl.gezinsapp.messaging.model.GezinAangemeld;
import nl.gezinsapp.model.CommGezinAangemeld;
import org.mapstruct.Mapper;

@Mapper(componentModel = "CDI")
public interface GezinAangemeldMapper {
    CommGezinAangemeld map(GezinAangemeld gezinAangemeld);
}

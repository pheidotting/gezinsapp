package nl.gezinsapp.controller;

import io.smallrye.common.annotation.RunOnVirtualThread;
import jakarta.transaction.Transactional;
import jakarta.ws.rs.Path;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.gezinsapp.aspect.ThreadVariabelen;
import nl.gezinsapp.commons.aspect.AlsGezinslid;
import nl.gezinsapp.commons.aspect.MethodeLogging;
import nl.gezinsapp.commons.aspect.TrackAndTraceId;
import nl.gezinsapp.mapper.BoodschappenItemMapper;
import org.openapi.quarkus.boodschappen_openapi_yaml.api.BoodschappenApi;
import org.openapi.quarkus.boodschappen_openapi_yaml.model.AlgemeneResponse;
import org.openapi.quarkus.boodschappen_openapi_yaml.model.BoodschappenItem;
import org.openapi.quarkus.boodschappen_openapi_yaml.model.LijstBoodschappenItems;
import org.openapi.quarkus.boodschappen_openapi_yaml.model.VerwijderBoodschappenItem;

@Slf4j
@RequiredArgsConstructor
//@ApplicationScoped
@Path("/api/boodschappen/item")
public class BoodschappenController implements BoodschappenApi {
    private final BoodschappenItemMapper boodschappenItemMapper;

    @Override
    @Transactional
    @TrackAndTraceId
    @AlsGezinslid
    @RunOnVirtualThread
    @MethodeLogging
    public AlgemeneResponse voegItemToe(BoodschappenItem boodschappenItem) {
        var item = boodschappenItemMapper.map(boodschappenItem);
        item.setGezin(ThreadVariabelen.get().getGezin());
        item.persist();

        return new AlgemeneResponse().id(item.getId());
    }

    @Override
    @Transactional
    @TrackAndTraceId
    @AlsGezinslid
    @RunOnVirtualThread
    @MethodeLogging
    public LijstBoodschappenItems lijst() {
        return new LijstBoodschappenItems()
                .boodschappenitems(
                        nl.gezinsapp.model.BoodschappenItem
                                .find("gezin", ThreadVariabelen.get().getGezin())
                                .stream()
                                .map(nl.gezinsapp.model.BoodschappenItem.class::cast)
                                .filter(boodschappenItem -> !boodschappenItem.isGehaald())
                                .map(boodschappenItemMapper::map)
                                .toList()
                );
    }

    @Override
    @Transactional
    @TrackAndTraceId
    @AlsGezinslid
    @RunOnVirtualThread
    @MethodeLogging
    public AlgemeneResponse verwijder(VerwijderBoodschappenItem verwijderBoodschappenItem) {
        nl.gezinsapp.model.BoodschappenItem.deleteById(verwijderBoodschappenItem.getId());

        return new AlgemeneResponse().id(verwijderBoodschappenItem.getId());
    }

    @Override
    @Transactional
    @TrackAndTraceId
    @AlsGezinslid
    @RunOnVirtualThread
    @MethodeLogging
    public AlgemeneResponse wijzig(BoodschappenItem boodschappenItem) {
        var item = boodschappenItemMapper.update(nl.gezinsapp.model.BoodschappenItem.findById(boodschappenItem.getId()), boodschappenItem);
        item.setGezin(ThreadVariabelen.get().getGezin());
        item.persist();

        return new AlgemeneResponse().id(boodschappenItem.getId());
    }
}

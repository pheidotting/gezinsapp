package nl.gezinsapp.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.time.Instant;


@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class RefreshToken implements Serializable {
    private String accessToken;
    private Long userId;
    private Instant expiryDate;

}

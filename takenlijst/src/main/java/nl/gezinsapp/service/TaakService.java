package nl.gezinsapp.service;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.transaction.Transactional;
import jakarta.ws.rs.ForbiddenException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.gezinsapp.commons.aspect.MethodeLogging;
import nl.gezinsapp.domain.Taak;
import nl.gezinsapp.domain.repository.TaakRepository;
import org.openapi.quarkus.authenticatie_openapi_yaml.model.Gebruiker;

import java.util.stream.Stream;

@Slf4j
@ApplicationScoped
@RequiredArgsConstructor
public class TaakService {
    private final TaakRepository taakRepository;

    @MethodeLogging
    public Stream<Taak> alleOpenTaken(Long gezinId) {
        return taakRepository.find("gezin", gezinId)
                .stream()
                .filter(taak -> taak.getTijdstipAfgestreept() == null);
    }

    @MethodeLogging
    public Long opslaan(Taak taak, Gebruiker gebruiker) {
        taak.setGezin(gebruiker.getGezin());
        taak.persist();

        return taak.getId();
    }

    @MethodeLogging
    public void verwijder(Long id, Long gezinId) {
        taakRepository.findByIdOptional(id)
                .ifPresent(taak -> {
                    if (!taak.getGezin().equals(gezinId)) {
                        throw new ForbiddenException();
                    }

                    taakRepository.deleteById(id);
                });
    }

    @MethodeLogging
    @Transactional
    public void verwerkVerwijderdGezinslid(Long gezinslidId) {
        taakRepository.findAll().stream()
                .filter(soortDienst -> soortDienst.getGezinsleden().contains(gezinslidId))
                .map(taak -> {
                    taak.getGezinsleden().remove(gezinslidId);
                    return taak;
                })
                .forEach(taak -> taak.persist());
    }
}

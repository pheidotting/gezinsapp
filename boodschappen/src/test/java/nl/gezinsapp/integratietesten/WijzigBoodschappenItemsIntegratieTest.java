package nl.gezinsapp.integratietesten;

import com.fasterxml.jackson.core.JsonProcessingException;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.openapi.quarkus.boodschappen_openapi_yaml.model.AlgemeneResponse;

import static nl.gezinsapp.integratietesten.random.RandomBoodschappenItem.randomBoodschappenItem;
import static nl.gezinsapp.integratietesten.random.RandomBoodschappenItem.randomDtoBoodschappenItem;
import static org.junit.jupiter.api.Assertions.*;

@Slf4j
@QuarkusTest
@QuarkusTestResource(MockServer.class)
class WijzigBoodschappenItemsIntegratieTest extends AbstractIntegratieTest {
    @Test
    @DisplayName("Wijzig een BoodschappenItem")
    void test() throws JsonProcessingException {
        var accesstoken = maakToken();
        var jwtResponse = mockCheckJwt(accesstoken);

        maakDatabaseLeeg();

        var item1 = randomBoodschappenItem(jwtResponse.getGebruiker().getGezin());

        opslaanBoodschappenItem(item1);
        var itemMsg = randomDtoBoodschappenItem();
        itemMsg.id(item1.getId());

        var response = patchViaApi(AlgemeneResponse.class, "/api/boodschappen/item", itemMsg, accesstoken);
        assertNull(response.getFoutmeldingen());
        assertEquals(item1.getId(), response.getId());

        var itemGelezen = leesBoodschappenItem(item1.getId());

        assertNotNull(itemGelezen);
        assertEquals(itemMsg.getOmschrijving(), itemGelezen.getOmschrijving());
        assertEquals(jwtResponse.getGebruiker().getGezin(), itemGelezen.getGezin());
    }
}

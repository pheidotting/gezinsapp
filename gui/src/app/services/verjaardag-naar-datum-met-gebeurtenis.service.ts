import {Injectable} from '@angular/core';
import {Gebeurtenis, SoortDatumEnum} from "../common/model/gebeurtenis.model";
import {DatumMetGebeurtenissen} from "../common/model/datum-met-gebeurtenissen.model";
import moment from "moment/moment";
import {Gezinslid, LeesGezin} from '../../api/gezin';
import {DatumUtils} from "./datum-utils.service";

@Injectable({
  providedIn: 'root'
})
export class VerjaardagNaarDatumMetGebeurtenisService {

  constructor(
    private readonly datumUtils: DatumUtils
  ) {
  }

  transform(gezin: LeesGezin, lijstGebeurtenissen: DatumMetGebeurtenissen[]): DatumMetGebeurtenissen[] {
    let result: DatumMetGebeurtenissen[] = [];
    gezin.gezinsleden
      .filter((gezinslid: Gezinslid) => {
        return gezinslid.geboorteDatum != null;
      })
      .forEach((gezinslid: Gezinslid) => {
        let gebeurtenis: Gebeurtenis = {
          'omschrijving': 'Verjaardag ' + gezinslid.voornaam + ' (' + this.datumUtils.leeftijd(gezinslid.geboorteDatum) + ')',
          'soortDatum': SoortDatumEnum.Verjaardag
        };
        let datumMetGebeurtenissen: DatumMetGebeurtenissen = lijstGebeurtenissen.filter((gb: DatumMetGebeurtenissen) => {
          return moment(gb.datum) == this.datumUtils.volgendeKeer(gezinslid.geboorteDatum);
        })[0];
        if (datumMetGebeurtenissen == null) {
          datumMetGebeurtenissen = {datum: this.datumUtils.volgendeKeer(gezinslid.geboorteDatum).format('YYYY-MM-DD'), tijdMetGebeurtenissen: []};
        } else {
          let index = lijstGebeurtenissen.findIndex((a: DatumMetGebeurtenissen) => {
            return moment(a.datum).format('YYYY-MM-DD') == this.datumUtils.volgendeKeer(gezinslid.geboorteDatum).format('YYYY-MM-DD');
          });

          lijstGebeurtenissen.splice(index, 1);
        }
        if (datumMetGebeurtenissen.tijdMetGebeurtenissen.length == 0) {
          datumMetGebeurtenissen.tijdMetGebeurtenissen.push({gebeurtenissen: []});
        }
        datumMetGebeurtenissen.tijdMetGebeurtenissen[0].gebeurtenissen.push(gebeurtenis);

        lijstGebeurtenissen.push(datumMetGebeurtenissen)
      });
    return result;
  }
}

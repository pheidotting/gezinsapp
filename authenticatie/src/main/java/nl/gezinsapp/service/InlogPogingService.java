package nl.gezinsapp.service;

import com.blueconic.browscap.ParseException;
import com.blueconic.browscap.UserAgentParser;
import com.blueconic.browscap.UserAgentService;
import jakarta.annotation.PostConstruct;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.ws.rs.core.HttpHeaders;
import lombok.RequiredArgsConstructor;
import nl.gezinsapp.commons.aspect.MethodeLogging;
import nl.gezinsapp.model.InlogPoging;
import nl.gezinsapp.model.Resultaat;
import nl.gezinsapp.model.repository.InlogPogingRepository;

import java.io.IOException;
import java.util.Optional;

import static jakarta.ws.rs.core.HttpHeaders.USER_AGENT;
import static java.util.Optional.ofNullable;

@ApplicationScoped
@RequiredArgsConstructor
public class InlogPogingService {
    private final InlogPogingRepository inlogPogingRepository;
    private UserAgentParser userAgentParser;

    @PostConstruct
    public void init() throws IOException, ParseException {
        this.userAgentParser = new UserAgentService().loadParser();
    }

    @MethodeLogging
    public InlogPoging maakInlogPoging(HttpHeaders headers) {
        var inlogPoging = new InlogPoging();
        final var capabilities = this.userAgentParser.parse(headers.getHeaderString(USER_AGENT));
        inlogPoging.setBrowser(capabilities.getBrowser());
        inlogPoging.setBrowserType(capabilities.getBrowserType());
        inlogPoging.setBrowserMajorVersion(capabilities.getBrowserMajorVersion());
        inlogPoging.setDeviceType(capabilities.getDeviceType());
        inlogPoging.setPlatform(capabilities.getPlatform());
        inlogPoging.setPlatformVersion(capabilities.getPlatformVersion());
        inlogPoging.setIpAdres(headers.getHeaderString("X-Real-IP"));

        return opslaanEnReturnen(inlogPoging);
    }

    @MethodeLogging
    public InlogPoging setGebruiker(InlogPoging inlogPoging, Long gezinslid) {
        var ip = haalOp(inlogPoging);
        ip.setGezinslid(gezinslid);

        return opslaanEnReturnen(ip);
    }

    @MethodeLogging
    public InlogPoging setResultaat(InlogPoging inlogPoging, Resultaat resultaat) {
        var ip = haalOp(inlogPoging);
        ip.setResultaat(resultaat);

        return opslaanEnReturnen(ip);
    }

    @MethodeLogging
    public Optional<InlogPoging> haalLaatsteInlogPoging(Long gezinslid) {
        return ofNullable(inlogPogingRepository.alleInlogPogingenBijGezinslid(gezinslid).stream()
                .sorted((o1, o2) -> o2.getTijdstip().compareTo(o1.getTijdstip()))
                .findFirst()
                .orElse(null));
    }

    private InlogPoging haalOp(InlogPoging inlogPoging) {
        return inlogPogingRepository.findById(inlogPoging.getId());
    }

    private InlogPoging opslaanEnReturnen(InlogPoging inlogPoging) {
        inlogPogingRepository.persist(inlogPoging);

        return inlogPoging;
    }
}

package nl.gezinsapp.integratietesten;


import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;
import lombok.extern.slf4j.Slf4j;
import nl.gezinsapp.model.InlogPoging;
import nl.gezinsapp.model.Resultaat;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockserver.model.JsonBody;
import org.openapi.quarkus.authenticatie_openapi_yaml.model.InloggenRequest;
import org.openapi.quarkus.authenticatie_openapi_yaml.model.JwtResponse;

import java.security.NoSuchAlgorithmException;
import java.time.LocalDateTime;
import java.util.UUID;

import static nl.gezinsapp.common.random.RandomGezinslid.randomGebruikerImpl;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockserver.model.HttpRequest.request;
import static org.mockserver.model.HttpResponse.response;

@Slf4j
@QuarkusTestResource(MockServer.class)
@QuarkusTest
class InloggenIntegratieTest extends AbstractIntegratieTest {
    @Test
    @DisplayName("Test inloggen Onjuist wachtwoord")
    void test() throws NoSuchAlgorithmException {
        var salt = gebruikerService.hash(UUID.randomUUID().toString());

        var mail = "email2";
        var wachtwoord = "wachtwoord";
        var anderwachtwoord = "anderwachtwoord";
        var gezinslid = randomGebruikerImpl();
        gezinslid.setEmailadres(mail);
        gezinslid.setWachtwoord(wachtwoord);
        gezinslid.setSalt(salt);
        gebruikerService.setHashWachtwoord(gezinslid, wachtwoord);
        var trackAndTraceId = UUID.randomUUID().toString();

        MockServer.getMockServerClient()
                .when(
                        request()
                                .withMethod("GET")
                                .withPath("/api/gezin/gebruiker/" + mail)
                                .withHeader("trackAndTraceId", trackAndTraceId)
                )
                .respond(
                        response()
                                .withStatusCode(200)
                                .withHeader("Content-Type", "application/json")
                                .withBody(JsonBody.json(gezinslid))
                );

        var inloggenRequest = new InloggenRequest();
        inloggenRequest.setEmailadres(mail);
        inloggenRequest.setWachtwoord(anderwachtwoord);

        var response = postViaApi(JwtResponse.class, "/api/authorisatie/inloggen", inloggenRequest, null, trackAndTraceId);

        assertEquals("GEBRUIKER_NIET_GEVONDEN_OF_ONJUIST_WACHTWOORD", response.getStatus().name());
        assertNull(response.getRefreshToken());
        assertNull(response.getAccessToken());

        var inlogPogingen = leesInlogPogingenBijGezinslid(gezinslid.getId());
        assertEquals(1, inlogPogingen.size());
        assertNotNull(inlogPogingen.get(0).getTijdstip());
        assertEquals(Resultaat.FOUT_WACHTWOORD, inlogPogingen.get(0).getResultaat());

        MockServer.getMockServerClient()
                .reset();
    }

    @Test
    @DisplayName("Test inloggen Juist wachtwoord")
    void test1() throws NoSuchAlgorithmException {
        var salt = gebruikerService.hash(UUID.randomUUID().toString());

        var mail = "email3";
        var wachtwoord = "wachtwoord";
        var gezinslid = randomGebruikerImpl();
        gezinslid.setEmailadres(mail);
        gezinslid.setWachtwoord(wachtwoord);
        gezinslid.setSalt(salt);
        gebruikerService.setHashWachtwoord(gezinslid, wachtwoord);
        var trackAndTraceId = UUID.randomUUID().toString();

        MockServer.getMockServerClient()

                .when(
                        request()
                                .withMethod("GET")
                                .withPath("/api/gezin/gebruiker/" + mail)
                                .withHeader("trackAndTraceId", trackAndTraceId)
                )
                .respond(
                        response()
                                .withStatusCode(200)
                                .withHeader("Content-Type", "application/json")
                                .withBody(JsonBody.json(gezinslid))
                );

        var inloggenRequest = new InloggenRequest();
        inloggenRequest.setEmailadres(mail);
        inloggenRequest.setWachtwoord(wachtwoord);

        var response = postViaApi(JwtResponse.class, "/api/authorisatie/inloggen", inloggenRequest, null, trackAndTraceId);

        assertEquals("GELUKT", response.getStatus().name());
        assertNotNull(response.getRefreshToken());
        assertNotNull(response.getAccessToken());
        assertEquals(gezinslid.getId(), response.getGebruiker().getId());

        var inlogPogingen = leesInlogPogingenBijGezinslid(gezinslid.getId());
        assertEquals(1, inlogPogingen.size());
        assertNotNull(inlogPogingen.get(0).getTijdstip());
        assertEquals(Resultaat.GELUKT, inlogPogingen.get(0).getResultaat());

        assertTrue(refreshTokenRepository.get(response.getRefreshToken()).isPresent());
    }

    @Test
    @DisplayName("Test inloggen Twee factor nodig")
    void test2() throws NoSuchAlgorithmException {
        var salt = gebruikerService.hash(UUID.randomUUID().toString());

        var mail = "email3";
        var wachtwoord = "wachtwoord";
        var gezinslid = randomGebruikerImpl();
        gezinslid.setEmailadres(mail);
        gezinslid.setWachtwoord(wachtwoord);
        gezinslid.setSalt(salt);
        gebruikerService.setHashWachtwoord(gezinslid, wachtwoord);
        var trackAndTraceId = UUID.randomUUID().toString();

        MockServer.getMockServerClient()

                .when(
                        request()
                                .withMethod("GET")
                                .withPath("/api/gezin/gebruiker/" + mail)
                                .withHeader("trackAndTraceId", trackAndTraceId)
                )
                .respond(
                        response()
                                .withStatusCode(200)
                                .withHeader("Content-Type", "application/json")
                                .withBody(JsonBody.json(gezinslid))
                );

        var inlogpoging = new InlogPoging();
        inlogpoging.setGezinslid(gezinslid.getId());
        inlogpoging.setTijdstip(LocalDateTime.now().minusWeeks(3));
        inlogpoging.setResultaat(Resultaat.GELUKT);
        opslaan(inlogpoging);

        var inloggenRequest = new InloggenRequest();
        inloggenRequest.setEmailadres(mail);
        inloggenRequest.setWachtwoord(wachtwoord);

        var response = postViaApi(JwtResponse.class, "/api/authorisatie/inloggen", inloggenRequest, null, trackAndTraceId);

        assertEquals("TWEE_FACTOR_NODIG", response.getStatus().name());
        assertNull(response.getRefreshToken());
        assertNull(response.getAccessToken());

        var inlogPogingen = leesInlogPogingenBijGezinslid(gezinslid.getId());
        assertEquals(2, inlogPogingen.size());
        assertNotNull(inlogPogingen.get(0).getTijdstip());
        assertEquals(Resultaat.GELUKT, inlogPogingen.get(0).getResultaat());
        assertNotNull(inlogPogingen.get(1).getTijdstip());
        assertEquals(Resultaat.TWEE_FACTOR_NODIG, inlogPogingen.get(1).getResultaat());

        leegmaken();
    }
}

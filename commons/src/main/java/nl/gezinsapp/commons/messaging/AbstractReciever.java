package nl.gezinsapp.commons.messaging;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.vertx.core.json.JsonObject;
import lombok.extern.slf4j.Slf4j;
import nl.gezinsapp.commons.aspect.MethodeLogging;
import org.eclipse.microprofile.reactive.messaging.Message;

import java.io.IOException;

@Slf4j
public abstract class AbstractReciever<T> {
    @MethodeLogging
    protected T map(Object obj, Class<T> clazz) {
        try {
            String string;
            if (obj instanceof String) {
                string = obj.toString();
            } else {
                try {
                    string = new String((byte[]) obj);
                } catch (ClassCastException cce) {
                    return ((JsonObject) obj).mapTo(clazz);
                }
            }
            return new ObjectMapper().findAndRegisterModules().readValue(string, clazz);
        } catch (IOException e) {
            log.error(e.getMessage(), e);
            return null;
        }
    }

    @MethodeLogging
    protected T mapS(String obj, Class<T> clazz) {
        return map(obj, clazz);
    }

    @MethodeLogging
    protected T getObject(Message<T> m, Class<T> clazz) {
        T t;
        try {
            t = clazz.cast(m.getPayload());
        } catch (ClassCastException cce) {
            t = map(m.getPayload(), clazz);
        }

        return t;
    }
}

import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {HeaderMetToevoegButtonComponent} from "../../common/header-met-toevoeg-button/header-met-toevoeg-button.component";
import {SideBarComponent} from "../../common/side-bar/side-bar.component";
import {OpslaanEnAfbrekenButtonsComponent} from "../../common/opslaan-en-afbreken-buttons/opslaan-en-afbreken-buttons.component";
import {ModalComponent} from "../../common/modal/modal.component";
import {
  AlgemeneResponse,
  VerlanglijstItemService,
  Verlanglijstje,
  Verlanglijstje1,
  VerlanglijstjeItem,
  VerlanglijstjeService
} from '../../../api/verlanglijstje';
import {GezinslidUtil} from "../../services/gezinslid-util.service";
import {Gezin, Gezinslid} from '../../../api/gezin';
import {NgClass, NgIf} from "@angular/common";
import {FormsModule} from "@angular/forms";
import {HttpPipe} from "../../pipes/http.pipe";

@Component({
  selector: 'app-verlanglijst',
  templateUrl: './verlanglijst.component.html',
  styleUrl: './verlanglijst.component.css',
  imports: [
    SideBarComponent,
    NgIf,
    FormsModule,
    OpslaanEnAfbrekenButtonsComponent,
    HeaderMetToevoegButtonComponent,
    ModalComponent,
    NgClass,
    HttpPipe
  ]
})
export class VerlanglijstComponent implements OnInit {
  verlanglijstje: Verlanglijstje | undefined = {gezinslid: 0, verlanglijstItems: []};
  gezin: Gezin | undefined;
  bewerkmodus: boolean = false;
  modalTonen: boolean = false;
  teVerwijderenItem: VerlanglijstjeItem | undefined;
  teBewerkenItem: number | undefined;
  headerWijzigen: boolean = false;
  verlanglijstjes: Verlanglijstje1[] | undefined = [];

  constructor(
    private readonly activatedRoute: ActivatedRoute,
    private readonly gezinslidUtil: GezinslidUtil,
    private readonly verlanglijstjeService: VerlanglijstjeService,
    private readonly verlanglijstItemService: VerlanglijstItemService,
    private readonly router: Router,
  ) {
  }

  ngOnInit(): void {
    this.activatedRoute.data.subscribe((response: any) => {
      this.gezin = response.gezin;
      this.verlanglijstjes = response.verlanglijstje.verlanglijstjes;
    });
    this.activatedRoute.url.subscribe(url => {
      if (url.length > 0) {
        let id = url[0].path;
        if (url.length > 1 && url[1].path == 'nieuw') {
          this.bewerkmodus = true;
        }
        this.verlanglijstje = this.verlanglijstjes!.filter((verlanglijstje: Verlanglijstje) => {
          return verlanglijstje.id == null ? false : verlanglijstje.id.toString() == id;
        })[0];
      }
    });
  }

  omschrijving(verlanglijstje: Verlanglijstje): string {
    let omschrijving: string | undefined = verlanglijstje == null ? undefined : verlanglijstje.omschrijving;
    if (omschrijving == undefined) {
      omschrijving = 'Geen omschrijving';
    }

    if (verlanglijstje == null) {
      return omschrijving;
    } else {
      return omschrijving + ' - ' + this.gezinslid(verlanglijstje.gezinslid);
    }
  }

  wijzigHeader(): void {
    this.headerWijzigen = true;
  }

  gezinslid(gezinslidId: number): string {
    if (gezinslidId > 0 && this.gezin != null) {
      return this.gezinslidUtil.maakNaamOp(this.gezin.gezinsleden.filter((gezinslid: Gezinslid) => {
        return gezinslid.id == gezinslidId;
      })[0]) as string;
    }
    return '';
  }

  opslaan(): void {
    if (this.verlanglijstje!.id == null) {
      this.verlanglijstjeService.nieuwVerlanglijstje(this.verlanglijstje!).subscribe((response: AlgemeneResponse) => {
        this.bewerkmodus = false;
        this.router.navigate(['verlanglijstje', response.id]);
      });
    } else {
      this.verlanglijstjeService.wijzigVerlanglijstje(this.verlanglijstje!).subscribe((response: AlgemeneResponse) => {
        this.bewerkmodus = false;
        this.router.navigate(['verlanglijstje', response.id]);
      });
    }
  }

  afbrekenOmschrijvingWijzigen() {
    this.headerWijzigen = false;
  }

  opslaanOmschrijving() {
    this.verlanglijstjeService.wijzigVerlanglijstje(this.verlanglijstje!).subscribe((response: AlgemeneResponse) => {
      this.headerWijzigen = false;
    });
  }

  toevoegenItem(): void {
    let item: VerlanglijstjeItem = {};
    item.verlanglijstje = this.verlanglijstje!.id;
    this.verlanglijstje!.verlanglijstItems.push(item);
  }

  opslaanItem(id: number): void {
    let item: VerlanglijstjeItem = this.verlanglijstje!.verlanglijstItems.filter((verlanglijstjeItem: VerlanglijstjeItem) => {
      return verlanglijstjeItem.id == id;
    })[0];
    if (item.id == null) {
      this.verlanglijstItemService.nieuwVerlanglijstItem(item).subscribe((response: AlgemeneResponse) => {
        item.id = response.id;
      });
    } else {
      this.verlanglijstItemService.wijzigVerlanglijstItem(item).subscribe((response: AlgemeneResponse) => {
        item.id = response.id;
        this.teBewerkenItem = undefined;
      });
    }
  }

  afbreken(): void {
    this.verlanglijstje!.verlanglijstItems.splice(this.verlanglijstje!.verlanglijstItems.length - 1, 1);
  }

  vinkItemAf(item: VerlanglijstjeItem): void {
    item.datumAfgestreept = new Date().toISOString();
    this.verlanglijstItemService.wijzigVerlanglijstItem(item).subscribe((response: AlgemeneResponse) => {
    });
  }

  tonenObvDatumAfgestreept(datumAfgestreept: string | undefined): boolean {
    if (datumAfgestreept != null) {
      let datum: Date = new Date(datumAfgestreept);

      let minusVijfMinuten: Date = new Date();
      minusVijfMinuten.setMinutes(datum.getMinutes() - 5);

      return minusVijfMinuten > datum;
    }
    return true;
  }

  doorstrepen(datumAfgestreept: string | undefined): boolean {
    if (datumAfgestreept != null) {
      let datum: Date = new Date(datumAfgestreept);

      let minusVijfMinuten: Date = new Date();
      minusVijfMinuten.setMinutes(datum.getMinutes() - 5);

      return minusVijfMinuten > datum;
    }
    return false;
  }

  verwijder(id: number): void {
    if (this.verlanglijstje != null) {
      this.teVerwijderenItem = this.verlanglijstje.verlanglijstItems.filter((verlanglijstjeItem: VerlanglijstjeItem) => {
        return verlanglijstjeItem.id == id;
      })[0];
      this.modalTonen = true;
    }
  }

  startBewerken(id: number): void {
    this.teBewerkenItem = id;
  }

  verbergModal(): void {
    this.modalTonen = false;
  }

  doeVerwijder(): void {
    this.verlanglijstItemService.verwijderVerlanglijstItem(this.teVerwijderenItem!.id!).subscribe((response: AlgemeneResponse) => {
      this.modalTonen = false;
      let index = this.verlanglijstje!.verlanglijstItems.findIndex(a => {
        return a.id == this.teVerwijderenItem!.id;
      });

      this.verlanglijstje!.verlanglijstItems.splice(index, 1);
    });
  }

  getOmschrijving(item: VerlanglijstjeItem | undefined): string {
    if (item != undefined) {
      if (item.omschrijving != undefined) {
        return item.omschrijving;
      } else {
        return '';
      }
    }
    return '';
  }
}

import {Component, EventEmitter, Input, Output} from '@angular/core';
import {MeldingenComponent} from "../../common/meldingen/meldingen.component";
import {NgIf, NgStyle} from "@angular/common";
import {OpslaanEnAfbrekenButtonsComponent} from "../../common/opslaan-en-afbreken-buttons/opslaan-en-afbreken-buttons.component";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {DatumUtils} from "../../services/datum-utils.service";
import {AfsprakenService} from "../../services/afspraken.service";
import {AfspraakService, AfspraakSoortAfspraakEnum, AfspraakTerugkeerEnum, AlgemeneResponse, SoortDienst} from '../../../api/kalender';
import moment from "moment/moment";
import {Calendar, EventInput} from "@fullcalendar/core";
import {EventImpl} from "@fullcalendar/core/internal";
import {FullCalendarComponent} from "@fullcalendar/angular";
import {GezinslidBijAfspraakComponent} from "../gezinslid-bij-afspraak/gezinslid-bij-afspraak.component";

@Component({
  selector: 'app-afspraken-modal',
  imports: [
    MeldingenComponent,
    NgIf,
    OpslaanEnAfbrekenButtonsComponent,
    ReactiveFormsModule,
    NgStyle,
    FormsModule,
    GezinslidBijAfspraakComponent
  ],
  templateUrl: './afspraken-modal.component.html'
})
export class AfsprakenModalComponent {
  verwijderModalTonen: string = "none";

  @Input({required: true}) calendarComponent!: FullCalendarComponent;
  @Input() modalTonen: string = "none";
  @Input() foutmeldingen: string[] = [];
  @Input() meldingen: string[] = [];
  @Input() goedmeldingen: string[] = [];
  @Output() afspraakGemaaktOfGewijzigdEventEmitter: EventEmitter<EventInput> = new EventEmitter<EventInput>;

  constructor(
    private readonly afspraakService: AfspraakService,
    private readonly datumUtils: DatumUtils,
    private readonly _afsprakenService: AfsprakenService,
  ) {
  }

  get afsprakenService(): AfsprakenService {
    return this._afsprakenService;
  }

  verbergModal(): void {
    this.modalTonen = 'none';
    this._afsprakenService.nieuweOfTeWijzigenAfspraak = {omschrijving: '', heledag: false, gezinsleden: []};
    this.foutmeldingen = [];
  }

  verbergVerwijderModal(): void {
    this._afsprakenService.teVerwijderenAfspraak = {omschrijving: '', heledag: false};
    this.verwijderModalTonen = 'none';
    this.modalTonen = 'block';
  }

  soortAfspraakGewijzigd(): void {
    if (
      this._afsprakenService.nieuweOfTeWijzigenAfspraak.soortAfspraak == AfspraakSoortAfspraakEnum.Verjaardag
      ||
      this._afsprakenService.nieuweOfTeWijzigenAfspraak.soortAfspraak == AfspraakSoortAfspraakEnum.Huwelijk
    ) {
      this._afsprakenService.nieuweOfTeWijzigenAfspraak.terugkeer = AfspraakTerugkeerEnum.Jaarlijks;
    }
  }

  soortenDienstenTonen(): boolean {
    const aangevinkteGezinsleden: { id?: number, naam?: string, checked: boolean }[] = this._afsprakenService.gezinsleden.filter((gezinslid: {
      id?: number,
      naam?: string,
      checked: boolean
    }) => {
      return gezinslid.checked;
    });

    const soortenDienstenBijAangevinkteGezinsleden: SoortDienst[] = this._afsprakenService.alleSoortenDiensten.filter((soortDienst: SoortDienst) => {
      let gevonden: boolean = false;
      aangevinkteGezinsleden.forEach((gezinslid: { id?: number, naam?: string, checked: boolean }) => {
        if (!gevonden) {
          gevonden = gezinslid.id == soortDienst.gezinslid;
        }
      });
      return gevonden;
    });

    return (soortenDienstenBijAangevinkteGezinsleden.length > 0 && aangevinkteGezinsleden.length == 1) || (this._afsprakenService.gezinsleden.length == 1 && this._afsprakenService.alleSoortenDiensten.length > 0);
  }

  soortenDiensten(): SoortDienst[] {
    let soorten: SoortDienst[] = [];
    if (this._afsprakenService.gezinsleden.length == 1) {
      this._afsprakenService.alleSoortenDiensten.forEach((soortDienst: SoortDienst) => {
        soorten.push(soortDienst);
      });
    } else {
      this._afsprakenService.gezinsleden.filter((gezinslid: { id?: number, naam?: string, checked: boolean }) => {
        return gezinslid.checked;
      }).forEach((gezinslid: { id?: number, naam?: string, checked: boolean }) => {
        this._afsprakenService.alleSoortenDiensten.filter((soortDienst: SoortDienst) => {
          return soortDienst.gezinslid == gezinslid.id;
        }).forEach((soortDienst: SoortDienst) => {
          soorten.push(soortDienst);
        });
      });
    }

    return soorten;
  }

  soortDienstNietGekozen(): boolean {
    return this._afsprakenService.nieuweOfTeWijzigenAfspraak.soortDienst == undefined ||
      this._afsprakenService.nieuweOfTeWijzigenAfspraak.soortDienst.toString() == '';
  }

  startTijdGewijzigd(): void {
    this._afsprakenService.nieuweOfTeWijzigenAfspraak.totTijd = moment(this._afsprakenService.nieuweOfTeWijzigenAfspraak.vanTijd).add(1, 'hour').format('YYYY-MM-DDTHH:mm');
  }

  verwijderAfspraak(): void {
    this._afsprakenService.teVerwijderenAfspraak = this._afsprakenService.nieuweOfTeWijzigenAfspraak;
    this.verwijderModalTonen = 'block';
    this.modalTonen = 'none';
  }

  verwijderTekst(): string {
    if (this._afsprakenService.teVerwijderenAfspraak.heledag) {
      return 'Weet je zeker dat je de afspraak van ' + this.datumUtils.maakDatumOp(this._afsprakenService.teVerwijderenAfspraak.vanDatum) + ' wilt verwijderen?';
    } else {
      return 'Weet je zeker dat je de afspraak van ' + this.datumUtils.maakDatumOp(this._afsprakenService.teVerwijderenAfspraak.vanTijd) + ' om ' + this.datumUtils.maakTijdOp(this._afsprakenService.teVerwijderenAfspraak.vanTijd) + ' wilt verwijderen?';
    }
  }

  doeVerwijderAfspraak(): void {
    this.afspraakService.verwijderAfspraak(this._afsprakenService.nieuweOfTeWijzigenAfspraak.id!).subscribe((response: AlgemeneResponse) => {
      let api: Calendar = this.calendarComponent.getApi();
      let events: EventImpl[] = api.getEvents();
      events = events.filter((event: EventImpl) => {
        return event.id != this._afsprakenService.nieuweOfTeWijzigenAfspraak.id + '';
      });
      api.removeAllEvents();
      events.forEach((event: EventImpl) => {
        api.addEvent(event as EventInput);
      });
      this.verbergModal();
      this.verbergVerwijderModal();
    });
  }

  opslaanAfspraak(): void {
    this._afsprakenService.zetKlaarVoorOpslaan();
    this.foutmeldingen = this._afsprakenService.checkAfspraak();
    if (this.foutmeldingen.length == 0) {
      // @ts-ignore
      if (this._afsprakenService.nieuweOfTeWijzigenAfspraak.soortAfspraak == '' || this._afsprakenService.nieuweOfTeWijzigenAfspraak.soortAfspraak == 'auto') {
        this._afsprakenService.nieuweOfTeWijzigenAfspraak.soortAfspraak = undefined;
      }
      let api: Calendar = this.calendarComponent.getApi();
      if (this._afsprakenService.nieuweOfTeWijzigenAfspraak.id == undefined || this._afsprakenService.nieuweOfTeWijzigenAfspraak.id == 0) {
        this.afspraakService.nieuweAfspraak(this._afsprakenService.nieuweOfTeWijzigenAfspraak).subscribe((response: AlgemeneResponse) => {
          let nw: EventInput = this._afsprakenService.verwerkAfspraak(response);
          this.afspraakGemaaktOfGewijzigdEventEmitter.emit(nw);
          api.addEvent(nw);
          this.modalTonen = 'none';
        });
      } else {
        this.afspraakService.opslaanAfspraak(this._afsprakenService.nieuweOfTeWijzigenAfspraak).subscribe((response: AlgemeneResponse) => {
          let events: EventImpl[] = api.getEvents().filter((event: EventImpl) => {
            return event.id != this._afsprakenService.nieuweOfTeWijzigenAfspraak.id + '';
          });
          api.removeAllEvents();
          events.forEach((event: EventImpl) => {
            api.addEvent(event as EventInput);
          });

          let nw: EventInput = this._afsprakenService.verwerkAfspraak(response);
          this.afspraakGemaaktOfGewijzigdEventEmitter.emit(nw);
          api.addEvent(nw);
        });
      }
      location.reload();
    }
  }
}

// import {ComponentFixture, TestBed} from '@angular/core/testing';
//
// import {WijzigWachtwoordComponent} from './wijzig-wachtwoord.component';
// import {WijzigWachtwoordService} from '../../api/gezin/api/wijzigWachtwoord.service';
// import {WijzigWachtwoordCodeService} from '../../api/gezin/api/wijzigWachtwoordCode.service';
// import {Router} from "@angular/router";
// import {CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA} from "@angular/core";
// import {HttpEvent, HttpResponse} from "@angular/common/http";
// import {Subject} from "rxjs";
// import {WijzigWachtwoordCode} from '../../api/gezin/model/wijzigWachtwoordCode';
// import {ControleerWijzigWachtwoordCodeResultaat} from '../../api/gezin/model/controleerWijzigWachtwoordCodeResultaat';
// import {AlgemeneResponse} from '../../api/gezin';
// import { WachtwoordSterkteService } from '../../api/authenticatie/api/wachtwoordSterkte.service';
//
// describe('WijzigWachtwoordComponent', () => {
//   let component: WijzigWachtwoordComponent;
//   let fixture: ComponentFixture<WijzigWachtwoordComponent>;
//
//   let wijzigWachtwoordCodeService: jasmine.SpyObj<WijzigWachtwoordCodeService>;
//   let wachtwoordSterkteService: jasmine.SpyObj<WachtwoordSterkteService>;
//   let wijzigWachtwoordService: jasmine.SpyObj<WijzigWachtwoordService>;
//   let router: jasmine.SpyObj<Router>;
//
//   beforeEach(async () => {
//     wijzigWachtwoordCodeService = jasmine.createSpyObj('WijzigWachtwoordCodeService', ['genereerWijzigWachtwoordCode', 'controleerWijzigWachtwoordCode']);
//     wachtwoordSterkteService = jasmine.createSpyObj('WachtwoordSterkteService', ['wachtwoordSterkte']);
//     wijzigWachtwoordService = jasmine.createSpyObj('WijzigWachtwoordService', ['wijzigWachtwoord']);
//     router = jasmine.createSpyObj('Router', ['navigate']);
//
//     await TestBed.configureTestingModule({
//       declarations: [WijzigWachtwoordComponent],
//       schemas: [NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA],
//       providers: [
//         {provide: WijzigWachtwoordCodeService, useValue: wijzigWachtwoordCodeService},
//         {provide: WachtwoordSterkteService, useValue: wachtwoordSterkteService},
//         {provide: WijzigWachtwoordService, useValue: wijzigWachtwoordService},
//         {provide: Router, useValue: router},
//       ]
//     })
//       .compileComponents();
//
//     fixture = TestBed.createComponent(WijzigWachtwoordComponent);
//     component = fixture.componentInstance;
//     fixture.detectChanges();
//   });
//
//   it('should create', () => {
//     expect(component).toBeTruthy();
//   });
//
//   it('doorgaanNaInvullenEmail - leeg e-mailadres', () => {
//     component.doorgaanNaInvullenEmail();
//
//     expect(component.stapEenGereed).toBeFalsy();
//     expect(component.foutmeldingen.length).toBe(1);
//     expect(component.foutmeldingen[0]).toBe('Vul je e-mailadres in waarmee je inlogt');
//   });
//
//   it('doorgaanNaInvullenEmail - gevuld e-mailadres', () => {
//     component.email = 'a';
//
//     let event: HttpEvent<WijzigWachtwoordCode> = new HttpResponse<WijzigWachtwoordCode>({body: {'id': 'abcd'}});
//     let responseSubject: Subject<HttpEvent<WijzigWachtwoordCode>> = new Subject<HttpEvent<WijzigWachtwoordCode>>();
//     wijzigWachtwoordCodeService.genereerWijzigWachtwoordCode.and.returnValue(responseSubject);
//
//     component.doorgaanNaInvullenEmail();
//     responseSubject.next(event);
//
//     expect(component.foutmeldingen.length).toBe(0);
//     expect(component.stapEenGereed).toBeTruthy();
//     // expect(component.codeId).toBe('abcd');
//     expect(wijzigWachtwoordCodeService.genereerWijzigWachtwoordCode).toHaveBeenCalled();
//   });
//
//   it('controlerenActiveringsCode', () => {
//     component.codeDeelEen = "1";
//     component.codeDeelTwee = "2";
//     component.codeDeelDrie = "3";
//     component.codeDeelVier = "4";
//     component.codeDeelVijf = "5";
//     component.codeDeelZes = "6";
//
//     let event: HttpEvent<ControleerWijzigWachtwoordCodeResultaat> = new HttpResponse<ControleerWijzigWachtwoordCodeResultaat>({body: {'resultaat': true}});
//     let responseSubject: Subject<HttpEvent<ControleerWijzigWachtwoordCodeResultaat>> = new Subject<HttpEvent<ControleerWijzigWachtwoordCodeResultaat>>();
//     wijzigWachtwoordCodeService.controleerWijzigWachtwoordCode.and.returnValue(responseSubject);
//
//     component.controlerenActiveringsCode(6);
//     responseSubject.next(event);
//
//     expect(component.code).toBe("123456");
//     expect(wijzigWachtwoordCodeService.controleerWijzigWachtwoordCode).toHaveBeenCalled();
//   });
//
//   it('wachtwoordOpslaan met veld 1 leeg', () => {
//     component.wachtwoordnogmaals = 'a';
//
//     component.wachtwoordOpslaan();
//
//     expect(component.foutmeldingen.length).toBe(1);
//     expect(component.foutmeldingen[0]).toBe('Vul beide velden in');
//   });
//
//   it('wachtwoordOpslaan met veld 2 leeg', () => {
//     component.wachtwoord = 'a';
//
//     component.wachtwoordOpslaan();
//
//     expect(component.foutmeldingen.length).toBe(1);
//     expect(component.foutmeldingen[0]).toBe('Vul beide velden in');
//   });
//
//   it('wachtwoordOpslaan met 2 lege velden', () => {
//     component.wachtwoordOpslaan();
//
//     expect(component.foutmeldingen.length).toBe(1);
//     expect(component.foutmeldingen[0]).toBe('Vul beide velden in');
//   });
//
//   it('wachtwoordOpslaan met 2 ongelijk gevuld velden', () => {
//     component.wachtwoord = 'a';
//     component.wachtwoordnogmaals = 'b';
//
//     component.wachtwoordOpslaan();
//
//     expect(component.foutmeldingen.length).toBe(1);
//     expect(component.foutmeldingen[0]).toBe('De ingevulde wachtwoorden komen niet overeen');
//   });
//
//   it('wachtwoordOpslaan met 2 gelijk gevuld velden', () => {
//     component.wachtwoord = 'b';
//     component.wachtwoordnogmaals = 'b';
//
//     let event: HttpEvent<AlgemeneResponse> = new HttpResponse<AlgemeneResponse>({body: {'foutmeldingen': {'meldingen': []}}});
//     let responseSubject: Subject<HttpEvent<AlgemeneResponse>> = new Subject<HttpEvent<AlgemeneResponse>>();
//     wijzigWachtwoordService.wijzigWachtwoord.and.returnValue(responseSubject);
//
//     component.wachtwoordOpslaan();
//     responseSubject.next(event);
//
//     expect(component.foutmeldingen.length).toBe(0);
//     expect(wijzigWachtwoordService.wijzigWachtwoord).toHaveBeenCalled();
//   });
// });

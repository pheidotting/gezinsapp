package nl.gezinsapp.integratietesten.common.random;

import org.openapi.quarkus.kalender_openapi_yaml.model.Afspraak;
import uk.co.jemos.podam.api.DefaultClassInfoStrategy;
import uk.co.jemos.podam.api.PodamFactory;
import uk.co.jemos.podam.api.PodamFactoryImpl;

public class RandomAfspraak {
    private static PodamFactory factory;

    private static PodamFactory getFactory() {
        if (factory == null) {
            factory = new PodamFactoryImpl();
            DefaultClassInfoStrategy classInfoStrategy = DefaultClassInfoStrategy.getInstance();
            classInfoStrategy.addExcludedField(Afspraak.class, "id");
            classInfoStrategy.addExcludedField(nl.gezinsapp.model.Afspraak.class, "id");
            classInfoStrategy.addExcludedField(nl.gezinsapp.model.Afspraak.class, "soortDienst");
            factory.setClassStrategy(classInfoStrategy);
        }
        return factory;
    }

    public static Afspraak randomDtoAfspraak() {
        return getFactory().manufacturePojo(Afspraak.class);
    }

    public static nl.gezinsapp.model.Afspraak randomAfspraak() {
        return getFactory().manufacturePojo(nl.gezinsapp.model.Afspraak.class);
    }
}

import java.text.SimpleDateFormat
import java.util.Date

pipeline {
    environment {
        SONAR_TOKEN = '7f6a00825d74b5369b6f9445f8c07d5ae67dca1d'
        DOCKER_TAG = getGitBranchName()
        NAMESPACE = getNameSpace()
        VERSIENUMMER = getVersieNummer()
        SNAPSHOTNUMMER = getSnapshotNummer()
    }
    agent any
    parameters {
      booleanParam(name: 'releaseMaken', defaultValue: false, description: 'Release maken?')
      booleanParam(name: 'dockerImagesMaken', defaultValue: false, description: 'Docker images maken?')
      booleanParam(name: 'sonar', defaultValue: true, description: 'SonarQube checks uitvoeren?')
      booleanParam(name: 'mergen', defaultValue: false, description: 'MergeRequest mergen?')
    }
    post {
        failure {
            slackSend(channel: "foute_builds", message: env.BRANCH_NAME + " build fout", color: 'wrong')
            updateGitlabCommitStatus name: 'Kwaliteitschecks', state: 'failed'
        }
        success {
            updateGitlabCommitStatus name: 'Kwaliteitschecks', state: 'success'
        }
    }
    options {
        retry(1)
        gitLabConnection('Gitlab-Gezinsapp')
        buildDiscarder(logRotator(artifactNumToKeepStr: '5'))
    }
    triggers {
        gitlab(triggerOnPush: true, triggerOnMergeRequest: true)
        }
    tools {
        maven 'maven-3.9.2'
        nodejs 'node'
    }
    stages {
        stage('Start') {
            steps {
                cleanWs()
                checkout scm
                script {
                    updateGitlabCommitStatus name: 'Kwaliteitschecks', state: 'running'

                    currentBuild.description = getReleaseVersion()
                    currentBuild.displayName = BUILD_NUMBER + ' - ' + commitMessage()
                }
            }
        }
        stage('CleanUp Docker') {
            steps {
                script {
                    try {
                        sh '''
                            docker image prune --all --force && docker system prune -f
                        '''
                        sh '''
                            docker stop $(docker ps -a -q) && docker rm $(docker ps -a -q)
                        '''
                    } catch (err) {
                        echo err.getMessage()
                        echo "Error detected, but we will continue."
                    }
                }
            }
        }
        stage('Kwaliteitschecks & Builden') {
            when {
                expression {
                    def branchNaam = env.BRANCH_NAME;
                    return env.mergen == "true" || env.sonar == "true" || branchNaam.startsWith("renovate/") || branchNaam.startsWith("tag/");
                }
            }
            steps {
                parallel (
                    SonarQubeBackend: {
                        script {
                            try {
                                if(env.BRANCH_NAME == 'development' && commitMessage().startsWith('[maven-release-plugin]')) {
                                    sh '''mvn install -DskipTests=true -T 6'''
                                } else if (env.BRANCH_NAME.startsWith("tag/")) {
                                    sh '''mvn install'''
                                } else {
                                    sh '''
                                        mvn install org.sonarsource.scanner.maven:sonar-maven-plugin:sonar -Pcoverage -Dsonar.projectKey=pheidotting_gezinsapp -Dsonar.branch.name=''' + env.BRANCH_NAME
                                }
                            } finally {
                                allure includeProperties:
                                   false,
                                   jdk: '',
                                   results: [
                                    [path: 'adresboek/target/allure-results'],
                                    [path: 'authenticatie/target/allure-results'],
                                    [path: 'boodschappen/target/allure-results'],
                                    [path: 'communicatie/target/allure-results'],
                                    [path: 'gezin/target/allure-results'],
                                    [path: 'kalender/target/allure-results'],
                                    [path: 'takenlijst/target/allure-results'],
                                    [path: 'verlanglijstje/target/allure-results']
                                   ]
                            }
                        }
                    },
                    SonarQubeFrontend: {
                        script {
                            if(env.BRANCH_NAME == 'development' && commitMessage().startsWith('[maven-release-plugin]')) {
                                sh '''
                                    cd gui &&
                                    npm install --force &&
                                    npm run generate:api &&
                                    npm run build'''
                            } else if (env.BRANCH_NAME.startsWith("tag/")) {
                                sh '''
                                    cd gui &&
                                    npm install --force &&
                                    npm run generate:api &&
                                    npm run build'''
                            } else {
                                sh '''
                                    cd gui &&
                                    npm install --force &&
                                    npm run generate:api &&
                                    npm run build &&
                                    npm run test &&
                                    npx sonarqube-scanner -Dsonar.branch.name=''' + env.BRANCH_NAME
                            }
                        }
                    }
                )
            }
        }
        stage('CleanUp Docker 2') {
            steps {
                script {
                    try {
                        sh '''
                            docker image prune --all --force && docker system prune -f
                        '''
                        sh '''
                            docker stop $(docker ps -a -q) && docker rm $(docker ps -a -q)
                        '''
                    } catch (err) {
                        echo err.getMessage()
                        echo "Error detected, but we will continue."
                    }
                }
            }
        }
        stage('Release') {
            when {
                expression {
                    def branchNaam = env.BRANCH_NAME;
                    return (env.releaseMaken == "true" || branchNaam == 'development') && !commitMessage().startsWith('[maven-release-plugin]') && !commitMessage().startsWith('snapshot versie');
                }
            }
            steps {
                script {
                    currentBuild.displayName = 'Release maken (' + getVersieNummer() + ' en ' + getSnapshotNummer() + ')'
                }
                sh '''
                    cd gui &&
                    npm version $VERSIENUMMER &&
                    cd .. &&
                    git add gui/package-lock.json &&
                    git add gui/package.json &&

                    rm kubernetes/deployments/applicaties/prd/*.yaml &&
                    cp kubernetes/deployments/applicaties/base/*.yaml kubernetes/deployments/applicaties/prd &&
                    cd kubernetes/deployments/applicaties/prd &&
                    sed -i "s/{{TAG}}/''' + getVersieNummer() + '''/g" *.yaml &&
                    cd ../../../.. &&

                    rm kubernetes/deployments/applicaties/dev/*.yaml &&
                    cp kubernetes/deployments/applicaties/base/*.yaml kubernetes/deployments/applicaties/dev &&
                    cd kubernetes/deployments/applicaties/dev &&
                    sed -i "s/{{TAG}}/''' + getVersieNummer() + '''/g" *.yaml &&
                    sed -i "s/: prd/: dev/g" *.yaml &&
                    cd ../../../.. &&

                    git add kubernetes/deployments/applicaties/prd/*.yaml &&
                    git add kubernetes/deployments/applicaties/dev/*.yaml &&
                    git commit -m 'versie ''' + env.VERSIENUMMER + '''' &&
                    mvn release:prepare -B release:perform -Darguments=-DskipTests=true -Dmaven.deploy.skip=true -DreleaseVersion=$VERSIENUMMER -DdevelopmentVersion=$SNAPSHOTNUMMER -Dtag=$VERSIENUMMER &&

                    curl --request POST --header "PRIVATE-TOKEN: glpat-f9AsUTGN_99Zi69k5tGx" \
                      --url "https://gitlab.com/api/v4/projects/58452182/repository/branches?branch=tag%2F$VERSIENUMMER&ref=$VERSIENUMMER"
                  '''
            }
        }
        stage('Build images') {
            when {
                expression {
                    def branchNaam = env.BRANCH_NAME;
                    return env.dockerImagesMaken == "true" || branchNaam.startsWith("tag/");
                }
            }
            steps {
                parallel (
                    Backend: {
                        script {
                            sh 'mvn clean install -Dquarkus.container-image.build=true -Dquarkus.container-image.push=true -Dquarkus.container-image.tag=$DOCKER_TAG -DskipTests=true -T 6'
                        }
                    },
                    Frontend: {
                        script {
                            sh '''
                                cd gui &&
                                npm install --force &&
                                npm run generate:api &&
                                npm run build
                                docker build -t gui . &&
                                docker tag gui:latest registry.gitlab.com/pheidotting/gezinsapp/gui:$DOCKER_TAG &&
                                docker push registry.gitlab.com/pheidotting/gezinsapp/gui:$DOCKER_TAG
                                '''
                        }
                    }
                )
            }
        }
        stage('Branch verwijderen') {
            when {
                expression {
                    def branchNaam = env.BRANCH_NAME;
                    return env.dockerImagesMaken == "true" || branchNaam.startsWith("tag/");
                }
            }
            steps {
                script {
                    if (env.BRANCH_NAME.startsWith("tag/")) {
                        sh 'curl --request DELETE --header "PRIVATE-TOKEN: glpat-f9AsUTGN_99Zi69k5tGx" \
                              --url "https://gitlab.com/api/v4/projects/58452182/repository/branches/tag%2F$DOCKER_TAG"'
                    }
                }
            }
        }
        stage('Push Kubernetes') {
            when {
                expression {
                    def branchNaam = env.BRANCH_NAME;
                    return env.dockerImagesMaken == "true" || branchNaam.startsWith("tag/");
                }
            }
            steps {
                withKubeConfig([credentialsId: 'kubernetes', serverUrl: 'https://k8dcc.8qsj0cjt.k8s.transip.dev:31095']) {
                    sh 'kubectl apply -f kubernetes/rabbitmq/. --namespace $NAMESPACE'

                    sh 'kubectl apply -f kubernetes/deployments/redis/. --namespace $NAMESPACE'
                    sh 'kubectl apply -f kubernetes/services/redis/. --namespace $NAMESPACE'

                    sh 'kubectl apply -f kubernetes/pv/. --namespace $NAMESPACE'
                    sh 'kubectl apply -f kubernetes/pvc/. --namespace $NAMESPACE'

                    sh 'kubectl apply -f kubernetes/configmap/. --namespace $NAMESPACE'
                    sh 'kubectl apply -f kubernetes/deployments/applicaties/prd/. --namespace $NAMESPACE'
                    sh 'kubectl apply -f kubernetes/services/applicaties/. --namespace $NAMESPACE'

                    sh 'kubectl apply -f kubernetes/cronjob/. --namespace $NAMESPACE'
                    sh 'kubectl apply -f kubernetes/cronjob/database-backups/. --namespace $NAMESPACE'

                    sh 'kubectl apply -f kubernetes/phpmyadmin/. --namespace $NAMESPACE'

                    sleep(20)
                    sh 'kubectl scale --replicas=0 deployment/gezin --namespace $NAMESPACE'
                    sleep(10)
                    sh 'kubectl scale --replicas=1 deployment/gezin --namespace $NAMESPACE'
                }
                slackSend(channel: "versies", message: "Deploy nieuwe versie gelukt, versie : $DOCKER_TAG", color: 'good')
            }
        }
        stage('MR Merge') {
            when {
                expression {
                    return env.mergen == "true" || commitMessage().endsWith('#merge');
                }
            }
            steps {
                script {
                    def response = sh(script: 'curl --request GET --header "PRIVATE-TOKEN: glpat-f9AsUTGN_99Zi69k5tGx" \
                              --url "https://gitlab.com/api/v4/projects/58452182/merge_requests?source_branch=' + env.BRANCH_NAME + '"', returnStdout: true)
                    def responseObject = readJSON text: response
                    def IID = "$responseObject.iid"
                    def TITLE = "$responseObject.title"

                    if (responseObject.size() == 0) {
                        def responseMR = sh(script: 'curl --request POST --header "PRIVATE-TOKEN: glpat-f9AsUTGN_99Zi69k5tGx" \
                                  --url "https://gitlab.com/api/v4/projects/58452182/merge_requests?source_branch=' + env.BRANCH_NAME + '&target_branch=development&assignee_id=3276299&title=' + env.BRANCH_NAME + '&squash=true&remove_source_branch=true"', returnStdout: true)
                        def responseObjectMR = readJSON text: responseMR
                        IID = "$responseObjectMR.iid"
                        TITLE = "$responseObjectMR.title"

                        sleep(20)
                    }

                    sh(script: 'curl --request PUT --header "PRIVATE-TOKEN: glpat-f9AsUTGN_99Zi69k5tGx" \
                              --url "https://gitlab.com/api/v4/projects/58452182/merge_requests/' + IID.replace('[','').replace(']','') + '/merge"', returnStdout: true)

                    currentBuild.displayName = 'Merged : ' + TITLE
                }
            }
        }
   }
}
def getGitBranchName() {
    return env.BRANCH_NAME.replace("tag/","")
}

def getNameSpace() {
    return env.BRANCH_NAME.startsWith("tag/") ? 'prd' : 'tst'
}

def getReleaseVersion() {
    def pom = readMavenPom file: 'pom.xml'
    return pom.version
}

def getVersieNummer() {
    def h1 = getReleaseVersion();
    def huidig1 =  h1.replace("-SNAPSHOT","")
    def huidig =  huidig1.tokenize('.');
    int volg = 0;
    for (String h : huidig){
        if(h == ':') {
            h = 10;
        }
        volg = h.toInteger();
    }

    int volgnummer = 0;
    def day = new SimpleDateFormat('DDD').format(new Date())
    def year = new SimpleDateFormat('yy').format(new Date())

    if(year == huidig[0] && day == huidig[1]) {
        volgnummer = volg
        echo 'Nu : ' + volgnummer
    }

    echo year + '.' + day + '.' + volgnummer
    return year + '.' + day + '.' + volgnummer
}

def getSnapshotNummer() {
    def versie = getVersieNummer().tokenize('.');

//     echo 'versie[2]' + versie[2]

//     echo versie[0] + '.' + versie[1] + '.' + (++versie[2]) + '-SNAPSHOT'
    return versie[0] + '.' + versie[1] + '.' + (++versie[2]) + '-SNAPSHOT'
}

def commitMessage() {
    sh(returnStdout: true, script: 'git log -1 --pretty=%B').trim();
}
import {Component, Input} from '@angular/core';
import {NgIf} from "@angular/common";

@Component({
  selector: 'app-meldingen',
  templateUrl: './meldingen.component.html',
  imports: [
    NgIf
  ]
})
export class MeldingenComponent {
  @Input() foutmeldingen: string[] = [];
  @Input() meldingen: string[] = [];
  @Input() goedmeldingen: string[] = [];
}

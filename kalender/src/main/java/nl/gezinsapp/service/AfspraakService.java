package nl.gezinsapp.service;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;
import jakarta.enterprise.context.ApplicationScoped;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.gezinsapp.commons.aspect.MethodeLogging;
import nl.gezinsapp.model.Afspraak;
import nl.gezinsapp.model.SoortDienst;
import nl.gezinsapp.model.enums.Dag;
import nl.gezinsapp.model.enums.Terugkeer;
import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.openapi.quarkus.gezin_openapi_yaml.api.GezinApi;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.YearMonth;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Stream;

@Slf4j
@ApplicationScoped
@RequiredArgsConstructor
public class AfspraakService {
    @RestClient
    private GezinApi gezinApi;

    @MethodeLogging
    public void verwijderAfsprakenBijGezinslid(Long gezinslidId) {
        Afspraak.findAll().stream()
                .map(Afspraak.class::cast)
                .filter(afspraak -> afspraak.getGezinsleden().contains(gezinslidId))
                .forEach(PanacheEntityBase::delete);
    }

    @MethodeLogging
    public List<Afspraak> alleAfspraken(LocalDate start, LocalDate eind, Long gezin) {
        List<Afspraak> afspraken = new ArrayList<>();
        afspraken.addAll(Afspraak.allesBijGezin(gezin)
                .stream().map(Afspraak.class::cast)
                .filter(checkAfspraak(start, eind))
                .toList());

        afspraken.addAll(alleAfsprakenBijGezin(start, eind, gezin));
        afspraken.addAll(alleSoortenDienstenBijGezin(start, eind, gezin));

        return afspraken;
    }

    private List<Afspraak> alleAfsprakenBijGezin(LocalDate start, LocalDate eind, Long gezin) {
        return Afspraak
                .allesBijGezin(gezin)
                .stream()
                .filter(afspraak -> afspraak.getTerugkeer().equals(Terugkeer.MAANDELIJKS))
                .map((Function<Afspraak, List<Afspraak>>) afspraak -> {
                    var afspraken1 = new ArrayList<Afspraak>();

                    getMonths(start, eind).forEach(jaarMaand -> {
                        if (afspraak.isHeledag()) {
                            afspraak.setVanDatum(afspraak.getVanDatum().withYear(jaarMaand.getYear()));
                            afspraak.setTotDatum(afspraak.getTotDatum().withYear(jaarMaand.getYear()));
                            afspraak.setVanDatum(afspraak.getVanDatum().withMonth(jaarMaand.getMonthValue()));
                            afspraak.setTotDatum(afspraak.getTotDatum().withMonth(jaarMaand.getMonthValue()));
                        } else {
                            afspraak.setVanTijd(afspraak.getVanTijd().withYear(jaarMaand.getYear()));
                            afspraak.setTotTijd(afspraak.getTotTijd().withYear(jaarMaand.getYear()));
                            afspraak.setVanTijd(afspraak.getVanTijd().withMonth(jaarMaand.getMonthValue()));
                            afspraak.setTotTijd(afspraak.getTotTijd().withMonth(jaarMaand.getMonthValue()));
                        }
                        afspraken1.add(new Afspraak(afspraak));
                    });

                    return afspraken1;
                })
                .flatMap((Function<List<Afspraak>, Stream<Afspraak>>) Collection::stream)
                .filter(Objects::nonNull)
                .filter(checkAfspraak(start, eind))
                .toList();
    }

    private List<Afspraak> alleSoortenDienstenBijGezin(LocalDate start, LocalDate eind, Long gezinId) {
        List<Afspraak> result = new ArrayList<>();
        var gezin = gezinApi.leesGezin(gezinId);
        gezin.getGezinsleden().forEach(gezinslid -> SoortDienst.find("gezinslid", gezinslid.getId()).stream()
                .map(SoortDienst.class::cast)
                .filter(soortDienst -> valtTussen(soortDienst.getDag(), start, eind) != null)
                .forEach(soortDienst -> {
                    valtTussen(soortDienst.getDag(), start, eind)
                            .forEach(datum -> result.add(Afspraak.builder()
                                    .gezinslid(soortDienst.getGezinslid())
                                    .heledag(false)
                                    .omschrijving(soortDienst.getOmschrijving())
                                    .vanTijd(LocalDateTime.of(datum, soortDienst.getVan()))
                                    .totTijd(LocalDateTime.of(datum, soortDienst.getTot()))
                                    .vanDatum(datum)
                                    .totDatum(datum)
                                    .soortDienst(soortDienst)
                                    .build()));
                }));

        return result;
    }

    protected List<LocalDate> valtTussen(Dag dag, LocalDate eersteDatum, LocalDate totDatum) {
        var result = new ArrayList<LocalDate>();

        var dagIterate = eersteDatum;
        while (!dagIterate.isAfter(totDatum)) {
            if (zetDagOm(dagIterate.getDayOfWeek()) == dag) {
                result.add(dagIterate);
            }
            dagIterate = dagIterate.plusDays(1);
        }

        return result;
    }

    private Dag zetDagOm(DayOfWeek dayOfWeek) {
        switch (dayOfWeek) {
            case MONDAY:
                return Dag.MA;
            case TUESDAY:
                return Dag.DI;
            case WEDNESDAY:
                return Dag.WO;
            case THURSDAY:
                return Dag.DO;
            case FRIDAY:
                return Dag.VR;
            case SATURDAY:
                return Dag.ZA;
            default:
                return Dag.ZO;
        }
    }

    private boolean valtTussen(LocalDate teTestenDatum, LocalDate eersteDatum, LocalDate totDatum) {
        return !eersteDatum.isAfter(teTestenDatum) && !totDatum.isBefore(teTestenDatum);
    }

    private boolean valtTussen(LocalDateTime teTestenDatum, LocalDateTime eersteDatum, LocalDateTime totDatum) {
        return !eersteDatum.isAfter(teTestenDatum) && !totDatum.isBefore(teTestenDatum);
    }

    private boolean voldoet(LocalDate start, LocalDate eind, Afspraak afspraak) {
        return (start.isBefore(afspraak.getVanDatum()) && !eind.isBefore(afspraak.getTotDatum())) ||
                valtTussen(start, afspraak.getVanDatum(), afspraak.getTotDatum()) ||
                valtTussen(eind, afspraak.getVanDatum(), afspraak.getTotDatum()) ||
                (!start.isAfter(afspraak.getTotDatum()) && eind.isAfter(afspraak.getTotDatum()));
    }

    private boolean voldoet(LocalDateTime starttijd, LocalDateTime eindtijd, Afspraak afspraak) {
        return (starttijd.isBefore(afspraak.getVanTijd()) && !eindtijd.isBefore(afspraak.getTotTijd())) ||
                valtTussen(starttijd, afspraak.getVanTijd(), afspraak.getTotTijd()) ||
                valtTussen(eindtijd, afspraak.getVanTijd(), afspraak.getTotTijd()) ||
                (!starttijd.isAfter(afspraak.getTotTijd()) && eindtijd.isAfter(afspraak.getTotTijd()));
    }

    private Predicate<Afspraak> checkAfspraak(LocalDate start, LocalDate eind) {
        return afspraak -> {
            if (afspraak.isHeledag()) {
                if (afspraak.getTerugkeer() != null && afspraak.getTerugkeer().equals(Terugkeer.JAARLIJKS)) {
                    afspraak.setVanDatum(afspraak.getVanDatum().withYear(start.getYear()));
                    afspraak.setTotDatum(afspraak.getTotDatum().withYear(eind.getYear()));
                }
                return AfspraakService.this.voldoet(start, eind, afspraak);
            } else {
                if (afspraak.getTerugkeer() != null && afspraak.getTerugkeer().equals(Terugkeer.JAARLIJKS)) {
                    afspraak.setVanTijd(afspraak.getVanTijd().withYear(start.getYear()));
                    afspraak.setTotTijd(afspraak.getTotTijd().withYear(eind.getYear()));
                }
                var starttijd = LocalDateTime.of(start, LocalTime.MIN);
                var eindtijd = LocalDateTime.of(eind, LocalTime.MAX);

                return AfspraakService.this.voldoet(starttijd, eindtijd, afspraak);
            }
        };
    }

    @MethodeLogging
    protected List<YearMonth> getMonths(LocalDate start, LocalDate eind) {
        var months = new ArrayList<YearMonth>();
        var maand = YearMonth.of(start.getYear(), start.getMonthValue());

        while (!maand.isAfter(YearMonth.of(eind.getYear(), eind.getMonthValue()))) {
            months.add(maand);
            maand = maand.plus(1, ChronoUnit.MONTHS);
        }

        return months;
    }
}

import {Afspraak, AfspraakSoortAfspraakEnum} from '../../api/kalender';
import {AfspraakNaarDatumMetGebeurtenisService} from './afspraak-naar-datum-met-gebeurtenis.service';
import {DatumMetGebeurtenissen} from "../common/model/datum-met-gebeurtenissen.model";
import {Gezinslid, GezinslidGeslachtEnum, GezinslidSoortGezinslidEnum} from '../../api/gezin';
import moment from 'moment';
import {SoortDatumEnum} from "../common/model/gebeurtenis.model";

describe('AfspraakNaarDatumMetGebeurtenisPipe', () => {
  it('create an instance', () => {
    const pipe = new AfspraakNaarDatumMetGebeurtenisService();
    expect(pipe).toBeTruthy();
  });

  it('afspraak met een datum, maar voor die dag nog geen gebeurtenissen', () => {
    const service: AfspraakNaarDatumMetGebeurtenisService = new AfspraakNaarDatumMetGebeurtenisService();
    let datum: string = moment().format('YYYY-MM-DD');

    let afspraak: Afspraak = {
      omschrijving: 'afspraak!',
      soortAfspraak: AfspraakSoortAfspraakEnum.Huwelijk,
      heledag: false,
      vanDatum: datum,
      gezinsleden: [3, 6]
    };

    let gezinsleden: Gezinslid[] = [{
      id: 3,
      voornaam: 'A',
      achternaam: 'C',
      soortGezinslid: GezinslidSoortGezinslidEnum.Ouder,
      geslacht: GezinslidGeslachtEnum.Man
    }, {
      id: 6,
      voornaam: 'D',
      achternaam: 'E',
      soortGezinslid: GezinslidSoortGezinslidEnum.Ouder,
      geslacht: GezinslidGeslachtEnum.Man
    }]


    let result: DatumMetGebeurtenissen = service.transform(afspraak, [], gezinsleden, []);

    expect(result.datum == datum).toBeTruthy();
    expect(result.datum).toEqual(datum);
    expect(result.tijdMetGebeurtenissen.length).toEqual(1);
    expect(result.tijdMetGebeurtenissen[0].gebeurtenissen.length).toEqual(1);
    expect(result.tijdMetGebeurtenissen[0].gebeurtenissen[0].omschrijving).toEqual(afspraak.omschrijving);
    expect(result.tijdMetGebeurtenissen[0].gebeurtenissen[0].soortDatum).toEqual(SoortDatumEnum.Huwelijk);
    expect(result.tijdMetGebeurtenissen[0].gebeurtenissen[0].gezinsleden!.length).toEqual(2);
  });

  it('afspraak met een tijdstip, maar voor die dag nog geen gebeurtenissen', () => {
    const service: AfspraakNaarDatumMetGebeurtenisService = new AfspraakNaarDatumMetGebeurtenisService();
    let datum: string = '2024-12-15';
    let tijdstipStart: string = moment().format('2024-12-15 09:00');
    let tijdstipEind: string = moment().format('2024-12-15 11:00');

    let afspraak: Afspraak = {
      omschrijving: 'afspraak!',
      soortAfspraak: AfspraakSoortAfspraakEnum.Huwelijk,
      heledag: false,
      vanTijd: tijdstipStart,
      totTijd: tijdstipEind,
      gezinsleden: [3, 6]
    };

    let gezinsleden: Gezinslid[] = [{
      id: 3,
      voornaam: 'A',
      achternaam: 'C',
      soortGezinslid: GezinslidSoortGezinslidEnum.Ouder,
      geslacht: GezinslidGeslachtEnum.Man
    }, {
      id: 6,
      voornaam: 'D',
      achternaam: 'E',
      soortGezinslid: GezinslidSoortGezinslidEnum.Ouder,
      geslacht: GezinslidGeslachtEnum.Man
    }]


    let result: DatumMetGebeurtenissen = service.transform(afspraak, [], gezinsleden, []);

    expect(result.datum == datum).toBeTruthy();
    expect(result.datum).toEqual(datum);
    expect(result.tijdMetGebeurtenissen.length).toEqual(1);
    expect(result.tijdMetGebeurtenissen[0].gebeurtenissen.length).toEqual(1);
    expect(result.tijdMetGebeurtenissen[0].van).toEqual('09:00');
    expect(result.tijdMetGebeurtenissen[0].tot).toEqual('11:00');
    expect(result.tijdMetGebeurtenissen[0].gebeurtenissen[0].omschrijving).toEqual(afspraak.omschrijving);
    expect(result.tijdMetGebeurtenissen[0].gebeurtenissen[0].soortDatum).toEqual(SoortDatumEnum.Huwelijk);
    expect(result.tijdMetGebeurtenissen[0].gebeurtenissen[0].gezinsleden!.length).toEqual(2);
  });

  it('afspraak met een datum, voor die dag al een gebeurtenis', () => {
    const service: AfspraakNaarDatumMetGebeurtenisService = new AfspraakNaarDatumMetGebeurtenisService();
    let datum: string = moment().format('YYYY-MM-DD');

    let afspraak: Afspraak = {
      omschrijving: 'afspraak!',
      soortAfspraak: AfspraakSoortAfspraakEnum.Huwelijk,
      heledag: false,
      vanDatum: datum,
      gezinsleden: [3, 6]
    };

    let d: DatumMetGebeurtenissen = {
      datum: datum,
      tijdMetGebeurtenissen: [
        {
          van: '07:00',
          tot: '08:00',
          gebeurtenissen: [
            {
              omschrijving: 'aanwezig'
            }
          ]
        }
      ]
    };

    let gezinsleden: Gezinslid[] = [{
      id: 3,
      voornaam: 'A',
      achternaam: 'C',
      soortGezinslid: GezinslidSoortGezinslidEnum.Ouder,
      geslacht: GezinslidGeslachtEnum.Man
    }, {
      id: 6,
      voornaam: 'D',
      achternaam: 'E',
      soortGezinslid: GezinslidSoortGezinslidEnum.Ouder,
      geslacht: GezinslidGeslachtEnum.Man
    }];

    let result: DatumMetGebeurtenissen = service.transform(afspraak, [d], gezinsleden, []);

    expect(result.datum == d.datum!).toBeTruthy();
    expect(result.datum).toEqual(datum);
    expect(result.tijdMetGebeurtenissen.length).toEqual(2);
    expect(result.tijdMetGebeurtenissen[1].gebeurtenissen.length).toEqual(1);
    expect(result.tijdMetGebeurtenissen[1].gebeurtenissen[0].omschrijving).toEqual(afspraak.omschrijving);
    expect(result.tijdMetGebeurtenissen[1].gebeurtenissen[0].soortDatum).toEqual(SoortDatumEnum.Huwelijk);
    expect(result.tijdMetGebeurtenissen[1].gebeurtenissen[0].gezinsleden!.length).toEqual(2);
  });

  it('afspraak met een tijdstip, voor die dag al een gebeurtenis', () => {
    const service: AfspraakNaarDatumMetGebeurtenisService = new AfspraakNaarDatumMetGebeurtenisService();
    let datum: string = '2024-12-15';
    let tijdstipStart: string = moment().format('2024-12-15 09:00');
    let tijdstipEind: string = moment().format('2024-12-15 11:00');

    let afspraak: Afspraak = {
      omschrijving: 'afspraak!',
      soortAfspraak: AfspraakSoortAfspraakEnum.Huwelijk,
      heledag: false,
      vanTijd: tijdstipStart,
      totTijd: tijdstipEind,
      gezinsleden: [3, 6]
    };

    let d: DatumMetGebeurtenissen = {
      datum: datum,
      tijdMetGebeurtenissen: [
        {
          van: '07:00',
          tot: '08:00',
          gebeurtenissen: [
            {
              omschrijving: 'aanwezig'
            }
          ]
        }
      ]
    };

    let gezinsleden: Gezinslid[] = [{
      id: 3,
      voornaam: 'A',
      achternaam: 'C',
      soortGezinslid: GezinslidSoortGezinslidEnum.Ouder,
      geslacht: GezinslidGeslachtEnum.Man
    }, {
      id: 6,
      voornaam: 'D',
      achternaam: 'E',
      soortGezinslid: GezinslidSoortGezinslidEnum.Ouder,
      geslacht: GezinslidGeslachtEnum.Man
    }]


    let result: DatumMetGebeurtenissen = service.transform(afspraak, [d], gezinsleden, []);

    expect(result.datum == d.datum!).toBeTruthy();
    expect(result.datum).toEqual(datum);
    expect(result.tijdMetGebeurtenissen.length).toEqual(2);
    expect(result.tijdMetGebeurtenissen[1].gebeurtenissen.length).toEqual(1);
    expect(result.tijdMetGebeurtenissen[1].van).toEqual('09:00');
    expect(result.tijdMetGebeurtenissen[1].tot).toEqual('11:00');
    expect(result.tijdMetGebeurtenissen[1].gebeurtenissen[0].omschrijving).toEqual(afspraak.omschrijving);
    expect(result.tijdMetGebeurtenissen[1].gebeurtenissen[0].soortDatum).toEqual(SoortDatumEnum.Huwelijk);
    expect(result.tijdMetGebeurtenissen[1].gebeurtenissen[0].gezinsleden!.length).toEqual(2);
  });
});

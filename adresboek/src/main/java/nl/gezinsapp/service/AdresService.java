package nl.gezinsapp.service;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;
import jakarta.enterprise.context.ApplicationScoped;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.gezinsapp.commons.aspect.MethodeLogging;
import nl.gezinsapp.model.AdresBoekRepository;

@Slf4j
@ApplicationScoped
@RequiredArgsConstructor
public class AdresService {
    private final AdresBoekRepository adresBoekRepository;

    @MethodeLogging
    public void verwerkVerwijderdgezinslid(Long gezinslidId) {
        adresBoekRepository.findAll().stream()
                .filter(adres -> adres.getGezinslid() == gezinslidId)
                .forEach(PanacheEntityBase::delete);
    }
}

package nl.gezinsapp.integratietesten.afspraak;

import com.fasterxml.jackson.core.JsonProcessingException;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;
import lombok.extern.slf4j.Slf4j;
import nl.gezinsapp.integratietesten.AbstractIntegratieTest;
import nl.gezinsapp.integratietesten.MockServer;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.openapi.quarkus.kalender_openapi_yaml.model.AlgemeneResponse;

import java.util.List;

import static nl.gezinsapp.integratietesten.common.random.RandomAfspraak.randomDtoAfspraak;
import static org.junit.jupiter.api.Assertions.*;

@Slf4j
@QuarkusTest
@QuarkusTestResource(MockServer.class)
class NieuweAfspraakIntegratieTest extends AbstractIntegratieTest {
    @Test
    @DisplayName("Sla een nieuwe afspraak op")
    void test() throws JsonProcessingException {
        maakDatabaseLeeg();

        var accesstoken = maakToken();
        var jwtResponse = mockCheckJwt(accesstoken);

        var afspraak = randomDtoAfspraak();
        afspraak.setGezinsleden(List.of(afspraak.getGezinsleden().get(0)));
        afspraak.setGezin(jwtResponse.getGebruiker().getGezin());

        var response = putViaApi(AlgemeneResponse.class, "/api/kalender/afspraak", afspraak, accesstoken);
        assertNull(response.getFoutmeldingen());
        assertNotNull(response.getId());

        var afspraken = leesAlleAfspraken();
        assertEquals(1, afspraken.size());
        assertEquals(afspraak.getGezinsleden().size(),afspraken.get(0).getGezinsleden().size());
        assertEquals(afspraak.getSoortAfspraak().name(), afspraken.get(0).getSoortAfspraak().name());
        assertEquals(jwtResponse.getGebruiker().getGezin(), afspraken.get(0).getGezin());
    }

    @Test
    @DisplayName("Sla een nieuwe afspraak op met lege soort")
    void testLegeSoort() throws JsonProcessingException {
        maakDatabaseLeeg();

        var accesstoken = maakToken();
        var jwtResponse = mockCheckJwt(accesstoken);

        var afspraak = randomDtoAfspraak();
        afspraak.setSoortAfspraak(null);
        afspraak.setGezin(99L);

        var response = putViaApi(AlgemeneResponse.class, "/api/kalender/afspraak", afspraak, accesstoken);
        assertNull(response.getFoutmeldingen());
        assertNotNull(response.getId());

        var afspraken = leesAlleAfspraken();
        assertEquals(1, afspraken.size());
        assertNull(afspraken.get(0).getSoortAfspraak());
        assertEquals(jwtResponse.getGebruiker().getGezin(), afspraken.get(0).getGezin());
    }
}

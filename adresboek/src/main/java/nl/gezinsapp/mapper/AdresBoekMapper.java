package nl.gezinsapp.mapper;

import nl.gezinsapp.model.AdresBoek;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;

@Mapper(componentModel = "CDI")
public abstract class AdresBoekMapper {
    public abstract AdresBoek map(org.openapi.quarkus.adresboek_openapi_yaml.model.AdresBoek adresBoek);
    public abstract org.openapi.quarkus.adresboek_openapi_yaml.model.AdresBoek map(AdresBoek adresBoek);
    public abstract org.openapi.quarkus.adresboek_openapi_yaml.model.AdresBoekMetAdressen mapMetAdressen(AdresBoek adresBoek);
    public abstract AdresBoek update(org.openapi.quarkus.adresboek_openapi_yaml.model.AdresBoek adresBoek,@MappingTarget AdresBoek bestaand);

    Long mapNaarLong(AdresBoek value) {
        return value == null ? null : value.getId();
    }
}

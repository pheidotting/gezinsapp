package nl.gezinsapp.controller;


import jakarta.enterprise.context.ApplicationScoped;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.gezinsapp.commons.aspect.MethodeLogging;
import nl.gezinsapp.commons.aspect.TrackAndTraceId;
import nl.gezinsapp.exception.GebruikerNietGevondenException;
import nl.gezinsapp.exception.TokenRefreshException;
import nl.gezinsapp.mapper.GebruikerMapper;
import nl.gezinsapp.model.GebruikerImpl;
import nl.gezinsapp.model.repository.RefreshTokenRepository;
import nl.gezinsapp.service.GebruikerService;
import nl.gezinsapp.service.RefreshTokenService;
import nl.gezinsapp.utils.JwtUtils;
import org.openapi.quarkus.authenticatie_openapi_yaml.api.JwtApi;
import org.openapi.quarkus.authenticatie_openapi_yaml.model.CheckJWTRequest;
import org.openapi.quarkus.authenticatie_openapi_yaml.model.JwtResponse;
import org.openapi.quarkus.authenticatie_openapi_yaml.model.TokenRefreshRequest;

@Slf4j
@ApplicationScoped
@RequiredArgsConstructor
public class CheckJwtController implements JwtApi {
    public static final long TTL = 86400L;

    private final RefreshTokenService refreshTokenService;
    private final RefreshTokenRepository refreshTokenRepository;
    private final GebruikerService gebruikerService;
    private final GebruikerMapper gebruikerMapper;
    private final JwtUtils jwtUtils;

    @Override
    @MethodeLogging
    @TrackAndTraceId
    public JwtResponse checkJwt(CheckJWTRequest checkJWTRequest) {
        var accessToken = checkJWTRequest.getAccessToken().replace("Bearer", "").trim();

        return refreshTokenRepository.get(accessToken)
                .map(refreshTokenService::verifyExpiration)
                .map(refreshToken -> {
                    GebruikerImpl gebruiker = null;
                    try {
                        gebruiker = gebruikerService.getGebruiker(refreshToken.getUserId());
                    } catch (GebruikerNietGevondenException e) {
                        log.error("Gebruiker met id {} niet gevonden", e);
                    }
                    return new JwtResponse().accessToken(accessToken).gebruiker(gebruikerMapper.map(gebruiker));
                }).orElse(new JwtResponse().accessToken(accessToken));
    }


    @Override
    @MethodeLogging
    @TrackAndTraceId
    public JwtResponse renewJwt(TokenRefreshRequest tokenRefreshRequest) {
        var requestRefreshToken = tokenRefreshRequest.getRefreshToken();

        return refreshTokenRepository.get(requestRefreshToken)
                .map(refreshTokenService::verifyExpiration).map(refreshToken -> {
                    GebruikerImpl gebruiker = null;
                    try {
                        gebruiker = gebruikerService.getGebruiker(refreshToken.getUserId());
                    } catch (GebruikerNietGevondenException e) {
                        log.error("Gebruiker met id {} niet gevonden", e);
                    }
                    var token = jwtUtils.generateTokenFromUsername(gebruiker);
                    refreshTokenService.createRefreshToken(token, gebruiker.getId(), requestRefreshToken);
                    return new JwtResponse().accessToken(token).gebruiker(gebruikerMapper.map(gebruiker)).refreshToken(requestRefreshToken);
                }).orElseThrow(() -> new TokenRefreshException(requestRefreshToken.toString(), "Refresh token is not in database!"));
    }


}

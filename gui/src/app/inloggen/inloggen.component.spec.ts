import {fakeAsync, TestBed} from '@angular/core/testing';

import {InloggenComponent} from './inloggen.component';
import {provideRouter, Router, RouterModule} from "@angular/router";
import {provideHttpClient, withInterceptorsFromDi} from "@angular/common/http";
import {HttpTestingController, provideHttpClientTesting} from "@angular/common/http/testing";
import {By} from "@angular/platform-browser";

describe('InloggenComponent', () => {
  let router: Router;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [InloggenComponent,
        RouterModule.forRoot([])],
      providers: [provideRouter([]), provideHttpClient(withInterceptorsFromDi()), provideHttpClientTesting()]
    });
    router = TestBed.inject(Router);
  });

  it('inloggen met correct gebruikersnaam/wachtwoord van een gezinslid', fakeAsync(() => {
    let fixture = TestBed.createComponent(InloggenComponent);
    let httpTestingController = TestBed.inject(HttpTestingController);
    const routerSpy = spyOn(router, 'navigate');

    let app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();

    app.inloggenRequast.emailadres = 'mail';
    app.inloggenRequast.wachtwoord = 'ww';

    app.inloggen();

    const reqInloggen = httpTestingController.expectOne((req) => {
      return req.url.endsWith('/api/authorisatie/inloggen');
    });
    expect(reqInloggen.request.method).toEqual('POST');
    reqInloggen.flush({
      status: 'GELUKT',
      gebruiker: {
        soort: 'GEZINSLID'
      }
    });

    expect(app.gebruikerNietGevondenOfOnjuistWachtwoord).toBeFalse();
    expect(routerSpy).toHaveBeenCalledWith(['dashboard']);
  }));

  it('inloggen met correct gebruikersnaam/wachtwoord van een beheerder', fakeAsync(() => {
    let fixture = TestBed.createComponent(InloggenComponent);
    let httpTestingController = TestBed.inject(HttpTestingController);
    const routerSpy = spyOn(router, 'navigate');

    let app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();

    app.inloggenRequast.emailadres = 'mail';
    app.inloggenRequast.wachtwoord = 'ww';

    app.inloggen();

    const reqInloggen = httpTestingController.expectOne((req) => {
      return req.url.endsWith('/api/authorisatie/inloggen');
    });
    expect(reqInloggen.request.method).toEqual('POST');
    reqInloggen.flush({
      status: 'GELUKT',
      gebruiker: {
        soort: 'BEHEERDER'
      }
    });

    expect(app.gebruikerNietGevondenOfOnjuistWachtwoord).toBeFalse();
    expect(routerSpy).toHaveBeenCalledWith(['beheer']);
  }));

  it('inloggen met incorrect gebruikersnaam/wachtwoord', fakeAsync(() => {
    let fixture = TestBed.createComponent(InloggenComponent);
    let httpTestingController = TestBed.inject(HttpTestingController);
    const routerSpy = spyOn(router, 'navigate');

    let app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();

    app.inloggenRequast.emailadres = 'mail';
    app.inloggenRequast.wachtwoord = 'ww';

    app.inloggen();

    const reqInloggen = httpTestingController.expectOne((req) => {
      return req.url.endsWith('/api/authorisatie/inloggen');
    });
    expect(reqInloggen.request.method).toEqual('POST');
    reqInloggen.flush({
      status: 'GEBRUIKER_NIET_GEVONDEN_OF_ONJUIST_WACHTWOORD'
    });

    expect(app.gebruikerNietGevondenOfOnjuistWachtwoord).toBeTrue();

    fixture.detectChanges();

    let span = fixture.debugElement.query(By.css('#span-gebruikerNietGevondenOfOnjuistWachtwoord'));
    expect(span.nativeElement.textContent).toEqual(' De ingevulde gebruikersnaam of wachtwoord is onjuist ');
  }));
});

package nl.gezinsapp.integratietesten;

import io.qameta.allure.Description;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockserver.model.JsonBody;
import org.openapi.quarkus.authenticatie_openapi_yaml.model.CheckJWTRequest;
import org.openapi.quarkus.authenticatie_openapi_yaml.model.InloggenRequest;
import org.openapi.quarkus.authenticatie_openapi_yaml.model.JwtResponse;

import java.security.NoSuchAlgorithmException;
import java.util.UUID;

import static nl.gezinsapp.common.random.RandomGezinslid.randomGebruikerImpl;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockserver.model.HttpRequest.request;
import static org.mockserver.model.HttpResponse.response;

@Slf4j
@QuarkusTest
@QuarkusTestResource(MockServer.class)
class CheckJwtIntegratieTest extends AbstractIntegratieTest  {

    @Test
    @DisplayName("Test Check JWT")
    @Description("Test de check van een JWT")
    void test() throws NoSuchAlgorithmException {
        var salt = gebruikerService.hash(UUID.randomUUID().toString());

        var mail = "email4";
        var wachtwoord = "wachtwoord";
        var gezinslid = randomGebruikerImpl();
        gezinslid.setEmailadres(mail);
        gezinslid.setWachtwoord(wachtwoord);
        gezinslid.setSalt(salt);
        gebruikerService.setHashWachtwoord(gezinslid, wachtwoord);
        var trackAndTraceId = UUID.randomUUID().toString();

       MockServer.getMockServerClient()
                .when(
                        request()
                                .withMethod("GET")
                                .withPath("/api/gezin/gebruiker/" + mail)
                                .withHeader("trackAndTraceId", trackAndTraceId)
                )
                .respond(
                        response()
                                .withStatusCode(200)
                                .withHeader("Content-Type", "application/json")
                                .withBody(JsonBody.json(gezinslid))
                );
       MockServer.getMockServerClient()
                .when(
                        request()
                                .withMethod("GET")
                                .withPath("/api/gezin/gebruiker-op-id/" + gezinslid.getId())
                                .withHeader("trackAndTraceId", trackAndTraceId)
                )
                .respond(
                        response()
                                .withStatusCode(200)
                                .withHeader("Content-Type", "application/json")
                                .withBody(JsonBody.json(gezinslid))
                );

        var inloggenRequest = new InloggenRequest();
        inloggenRequest.setEmailadres(mail);
        inloggenRequest.setWachtwoord(wachtwoord);

        var response = postViaApi(JwtResponse.class, "/api/authorisatie/inloggen", inloggenRequest, null, trackAndTraceId);

        assertEquals("GELUKT", response.getStatus().name());

        CheckJWTRequest checkJWTRequest = new CheckJWTRequest();
        checkJWTRequest.setAccessToken(response.getAccessToken());

        var jwtResponse = postViaApi(JwtResponse.class, "/api/authorisatie/checkjwt", checkJWTRequest, null, trackAndTraceId);
        assertEquals(gezinslid.getId(), jwtResponse.getGebruiker().getId());
        assertNotNull(jwtResponse.getAccessToken());
        assertNull(jwtResponse.getRefreshToken());
    }
}

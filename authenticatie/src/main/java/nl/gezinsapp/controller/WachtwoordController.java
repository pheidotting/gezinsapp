package nl.gezinsapp.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.gezinsapp.commons.aspect.MethodeLogging;
import nl.gezinsapp.commons.aspect.TrackAndTraceId;
import nl.gezinsapp.model.GebruikerImpl;
import nl.gezinsapp.service.GebruikerService;
import org.openapi.quarkus.authenticatie_openapi_yaml.api.WachtwoordApi;
import org.openapi.quarkus.authenticatie_openapi_yaml.model.GenereerSaltEnHashWachtwoordRequest;
import org.openapi.quarkus.authenticatie_openapi_yaml.model.GenereerSaltEnHashWachtwoordResponse;

import java.security.NoSuchAlgorithmException;
import java.util.UUID;

@Slf4j
@RequiredArgsConstructor
public class WachtwoordController implements WachtwoordApi {
    private final GebruikerService gebruikerService;

    @Override
    @MethodeLogging
    @TrackAndTraceId
    public GenereerSaltEnHashWachtwoordResponse genereerSaltEnHashWachtwoord(GenereerSaltEnHashWachtwoordRequest genereerSaltEnHashWachtwoordRequest) {
        var gebruiker = new GebruikerImpl();
        if (genereerSaltEnHashWachtwoordRequest.getSalt() == null || "".equals(genereerSaltEnHashWachtwoordRequest.getSalt())) {
            try {
                gebruiker.setSalt(gebruikerService.hash(UUID.randomUUID().toString()));
            } catch (NoSuchAlgorithmException e) {
                log.error("Fout bij instellen salt");
            }
        } else {
            gebruiker.setSalt(genereerSaltEnHashWachtwoordRequest.getSalt());
        }
        gebruikerService.setEmailadresEnWachtwoord(
                gebruiker,
                genereerSaltEnHashWachtwoordRequest.getEmailadres(),
                genereerSaltEnHashWachtwoordRequest.getWachtwoord()
        );
        return new GenereerSaltEnHashWachtwoordResponse()
                .salt(gebruiker.getSalt())
                .emailadres(gebruiker.getEmailadres())
                .hashwachtwoord(gebruiker.getWachtwoord());
    }
}

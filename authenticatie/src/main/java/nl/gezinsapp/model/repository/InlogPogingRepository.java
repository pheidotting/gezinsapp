package nl.gezinsapp.model.repository;

import io.quarkus.hibernate.orm.panache.PanacheRepository;
import jakarta.enterprise.context.ApplicationScoped;
import nl.gezinsapp.commons.aspect.MethodeLogging;
import nl.gezinsapp.model.InlogPoging;

import java.util.List;

@ApplicationScoped
public class InlogPogingRepository implements PanacheRepository<InlogPoging> {
    @MethodeLogging
    public List<InlogPoging> alleInlogPogingenBijGezinslid(Long gezinslid) {
        return list("gezinslid", gezinslid);
    }
}

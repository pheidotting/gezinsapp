import {ComponentFixture, TestBed} from '@angular/core/testing';
import {AfspraakSoortAfspraakEnum} from "../../../api/kalender";
import {KalenderItemComponent} from './kalender-item.component';
import {By} from "@angular/platform-browser";

describe('KalenderItemComponent', () => {
  let component: KalenderItemComponent;
  let fixture: ComponentFixture<KalenderItemComponent>;
  let extendedProps: { soortAfspraak: string | undefined, terugkeer: string | undefined, gezinsleden: number[] | undefined };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [KalenderItemComponent]
    })
      .compileComponents();

    fixture = TestBed.createComponent(KalenderItemComponent);
    component = fixture.componentInstance;

    component.arg = {
      id: '1',
      title: 'omsch',
      allDay: false,
      timeText: 'timeText'
    };
    extendedProps = {
      soortAfspraak: undefined,
      terugkeer: undefined,
      gezinsleden: undefined
    };
    component.arg['event'] = {};
    component.arg['event'].title = 'Titel';
  });

  it('Verjaardag icon tonen', () => {
    extendedProps.soortAfspraak = AfspraakSoortAfspraakEnum.Verjaardag.toString();
    component.arg['event'].extendedProps = extendedProps;

    fixture.detectChanges();
    expect(component).toBeTruthy();
    let huwelijkIcon = fixture.debugElement.query(By.css('#huwelijk-icon'));
    let verjaardagIcon = fixture.debugElement.query(By.css('#verjaardag-icon'));
    let defaultIcon = fixture.debugElement.query(By.css('#default-icon'));
    let timeText = fixture.debugElement.query(By.css('#time-text'));
    let eventTitle = fixture.debugElement.query(By.css('#event-title'));

    expect(huwelijkIcon).toBeFalsy();
    expect(verjaardagIcon).toBeTruthy();
    expect(defaultIcon).toBeTruthy();
    expect(timeText).toBeTruthy();
    expect(timeText.nativeElement.textContent).toContain('timeText');
    expect(eventTitle).toBeTruthy();
    expect(eventTitle.nativeElement.textContent).toContain('Titel');
  });

  it('Huwelijk icon tonen', () => {
    extendedProps.soortAfspraak = AfspraakSoortAfspraakEnum.Huwelijk.toString();
    component.arg['event'].extendedProps = extendedProps;

    fixture.detectChanges();
    expect(component).toBeTruthy();
    let huwelijkIcon = fixture.debugElement.query(By.css('#huwelijk-icon'));
    let verjaardagIcon = fixture.debugElement.query(By.css('#verjaardag-icon'));
    let defaultIcon = fixture.debugElement.query(By.css('#default-icon'));
    let timeText = fixture.debugElement.query(By.css('#time-text'));
    let eventTitle = fixture.debugElement.query(By.css('#event-title'));

    expect(huwelijkIcon).toBeTruthy();
    expect(verjaardagIcon).toBeFalsy();
    expect(defaultIcon).toBeTruthy();
    expect(timeText).toBeTruthy();
    expect(timeText.nativeElement.textContent).toContain('timeText');
    expect(eventTitle).toBeTruthy();
    expect(eventTitle.nativeElement.textContent).toContain('Titel');
  });

  it('Overig icon tonen', () => {
    extendedProps.soortAfspraak = AfspraakSoortAfspraakEnum.Overig.toString();
    component.arg['event'].extendedProps = extendedProps;

    fixture.detectChanges();
    expect(component).toBeTruthy();
    let huwelijkIcon = fixture.debugElement.query(By.css('#huwelijk-icon'));
    let verjaardagIcon = fixture.debugElement.query(By.css('#verjaardag-icon'));
    let defaultIcon = fixture.debugElement.query(By.css('#default-icon'));
    let timeText = fixture.debugElement.query(By.css('#time-text'));
    let eventTitle = fixture.debugElement.query(By.css('#event-title'));

    expect(huwelijkIcon).toBeFalsy();
    expect(verjaardagIcon).toBeFalsy();
    expect(defaultIcon).toBeTruthy();
    expect(timeText).toBeTruthy();
    expect(timeText.nativeElement.textContent).toContain('timeText');
    expect(eventTitle).toBeTruthy();
    expect(eventTitle.nativeElement.textContent).toContain('Titel');
  });
});

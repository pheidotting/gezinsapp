import {Component} from '@angular/core';
import {GebruikerSoortEnum, InEnUitloggenService, InloggenRequest, JwtResponse, JwtResponseStatusEnum} from "../../api/authenticatie";
import {Router} from "@angular/router";
import {TokenService} from "../services/token.service";
import {FormsModule} from "@angular/forms";

@Component({
  selector: 'app-inloggen',
  templateUrl: './inloggen.component.html',
  imports: [
    FormsModule,
  ]
})
export class InloggenComponent {
  inloggenRequast: InloggenRequest = {emailadres: '', wachtwoord: ''};
  gebruikerNietGevondenOfOnjuistWachtwoord: boolean = false;
  showPassword: boolean = false;

  constructor(
    private inEnUitloggenService: InEnUitloggenService,
    private router: Router,
    private tokenService: TokenService
  ) {
  }

  inloggen(): void {
    this.inEnUitloggenService.inloggen(this.inloggenRequast).subscribe((response: JwtResponse) => {
      this.gebruikerNietGevondenOfOnjuistWachtwoord = false;
      if (response.status != JwtResponseStatusEnum.Gelukt) {
        this.gebruikerNietGevondenOfOnjuistWachtwoord = true;
      } else {
        this.tokenService.setAccessToken(response.accessToken);
        this.tokenService.setRefreshToken(response.refreshToken);

        if (response.gebruiker!.soort == GebruikerSoortEnum.Gezinslid) {
          this.router.navigate(['dashboard']);
        } else if (response.gebruiker!.soort == GebruikerSoortEnum.Beheerder) {
          this.router.navigate(['beheer']);
        }
      }
    });
  }

  toonwachtwoord(): void {
    this.showPassword = !this.showPassword;
  }
}

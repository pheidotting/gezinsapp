import {Component, Input} from '@angular/core';
import {AfspraakSoortAfspraakEnum} from '../../../api/kalender';
import {EventInput} from "@fullcalendar/core";

@Component({
  selector: 'app-kalender-item',
  templateUrl: './kalender-item.component.html'
})
export class KalenderItemComponent {
  protected readonly AfspraakSoortAfspraakEnum = AfspraakSoortAfspraakEnum;

  @Input({required: true}) arg: EventInput = {};

}

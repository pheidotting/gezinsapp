package nl.gezinsapp.commons.aspect;

import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.ext.ExceptionMapper;
import jakarta.ws.rs.ext.Provider;

@Provider
public class ForbiddenExceptionHandler implements ExceptionMapper<ForbiddenException> {
    @Override
    @MethodeLogging
    public Response toResponse(ForbiddenException e) {
        return Response.status(Response.Status.FORBIDDEN).build();
    }
}
package nl.gezinsapp.integratietesten.verlanglijstitem;

import com.fasterxml.jackson.core.JsonProcessingException;
import io.quarkus.test.junit.QuarkusTest;
import lombok.extern.slf4j.Slf4j;
import nl.gezinsapp.integratietesten.AbstractIntegratieTest;
import nl.gezinsapp.integratietesten.MockServer;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockserver.model.JsonBody;
import org.openapi.quarkus.gezin_openapi_yaml.model.Gezin;
import org.openapi.quarkus.gezin_openapi_yaml.model.Gezinslid;
import org.openapi.quarkus.verlanglijstje_openapi_yaml.model.AlgemeneResponse;

import static nl.gezinsapp.integratietesten.random.RandomVerlanglijst.randomDomainVerlanglijst;
import static nl.gezinsapp.integratietesten.random.RandomVerlanglijstItem.randomDomainVerlanglijstItem;
import static nl.gezinsapp.integratietesten.random.RandomVerlanglijstItem.randomMsgVerlanglijstItem;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockserver.model.HttpRequest.request;
import static org.mockserver.model.HttpResponse.response;
import io.quarkus.test.common.QuarkusTestResource;

@Slf4j
@QuarkusTest
@QuarkusTestResource(MockServer.class)
class VerlanglijstjeItemVerwijderenIntegratieTest extends AbstractIntegratieTest {
    @Test
    @DisplayName("Verwijder een Verlanglijst Item")
    void test() throws JsonProcessingException {
        maakDatabaseLeeg();

        var accesstoken = maakToken();
        var jwtResponse = mockCheckJwt(accesstoken);

        MockServer.getMockServerClient()
                .when(
                        request()
                                .withMethod("GET")
                                .withPath("/api/gezin/" + jwtResponse.getGebruiker().getGezin())
                )
                .respond(
                        response()
                                .withStatusCode(200)
                                .withHeader("Content-Type", "application/json")
                                .withBody(JsonBody.json(gezin(jwtResponse.getGebruiker().getGezin(), jwtResponse.getGebruiker().getId())))
                );

        var verlanglijst = randomDomainVerlanglijst(jwtResponse.getGebruiker().getId());
        opslaan(verlanglijst);

        var verlanglijstItem = randomDomainVerlanglijstItem();
        verlanglijstItem.setVerlanglijst(verlanglijst);
        opslaan(verlanglijstItem);

        assertEquals(1,aantalVerlanglijstItems());

        var response = deleteViaApi(AlgemeneResponse.class, "/api/verlanglijstje/verlanglijstjeitem", verlanglijstItem.getId(), accesstoken);
        assertEquals(verlanglijstItem.getId(),response.getId());
        assertNull(response.getFoutmeldingen());
        assertEquals(0,aantalVerlanglijstItems());
    }
    private Gezin gezin(Long id, Long eersteLidId) {
        var gezin = new Gezin();
        gezin.setId(id);

        var gezinslid1 = new Gezinslid();
        gezinslid1.setId(eersteLidId);

        var gezinslid2 = new Gezinslid();
        gezinslid2.setId(3L);

        gezin.getGezinsleden().add(gezinslid1);
        gezin.getGezinsleden().add(gezinslid2);

        return gezin;
    }
}

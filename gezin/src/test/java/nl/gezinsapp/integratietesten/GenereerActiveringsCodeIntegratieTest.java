package nl.gezinsapp.integratietesten;

import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;
import jakarta.inject.Inject;
import lombok.extern.slf4j.Slf4j;
import nl.gezinsapp.integratietesten.common.QueueUtil;
import nl.gezinsapp.integratietesten.common.TestGegevens;
import nl.gezinsapp.messaging.model.ActivatieCodeAangemaakt;
import nl.gezinsapp.model.repository.ActivatieCodeRepository;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.junit.jupiter.api.Test;
import org.openapi.quarkus.gezin_openapi_yaml.model.ActiveringsCode;
import org.openapi.quarkus.gezin_openapi_yaml.model.VraagActiveringsCodeOp;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@Slf4j
@QuarkusTest
@QuarkusTestResource(MockServer.class)
class GenereerActiveringsCodeIntegratieTest extends AbstractIntegratieTest {
    @ConfigProperty(name = "mp.messaging.outgoing.activatiecode-aangemaakt.exchange.name")
    private String exchange;
    @ConfigProperty(name = "rabbitmq-host")
    private String rabbitHost;
    @ConfigProperty(name = "rabbitmq-port")
    private String rabbitPort;
    @ConfigProperty(name = "rabbitmq-username")
    private String rabbitUsername;
    @ConfigProperty(name = "rabbitmq-password")
    private String rabbitPassword;
    @Inject
    private ActivatieCodeRepository activatieCodeRepository;

    @Test
    void test() {
        TestGegevens.getQueues().put("opslaanbedrijfrequest", exchange);

        TestGegevens.setRabbitHost(rabbitHost);
        TestGegevens.setRabbitPort(Integer.valueOf(rabbitPort));
        TestGegevens.setRabbitUser(rabbitUsername);
        TestGegevens.setRabbitPassword(rabbitPassword);

        var queueUtil = mmaakActivatieCodeAangemaaktQueueUtil();


        var vraagActiveringsCodeOp = new VraagActiveringsCodeOp()
                .email("a@b.c")
                .familienaam("Heidotting & Co");

        var activeringsCode = postViaApi(ActiveringsCode.class, "/api/gezin/activeringscode/genereer", vraagActiveringsCodeOp, null, null);

        var activatieCode = activatieCodeRepository.get(activeringsCode.getId());
        assertTrue(activatieCode.isPresent());

        var activatieCodeAangemaakt = queueUtil.vangBericht();
        assertEquals(activatieCode.get(), activatieCodeAangemaakt.get().getCode());
        assertEquals(vraagActiveringsCodeOp.getEmail(), activatieCodeAangemaakt.get().getEmailadres());
        assertEquals(vraagActiveringsCodeOp.getFamilienaam(), activatieCodeAangemaakt.get().getFamilienaam());
    }

    private QueueUtil<ActivatieCodeAangemaakt> mmaakActivatieCodeAangemaaktQueueUtil() {
        return new QueueUtil<>() {
            @Override
            public String getExchange() {
                return exchange;
            }

            @Override
            public Class<ActivatieCodeAangemaakt> getClazz() {
                return ActivatieCodeAangemaakt.class;
            }
        };
    }
}

package nl.gezinsapp.integratietesten;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.restassured.RestAssured;
import io.restassured.config.ObjectMapperConfig;
import io.restassured.config.RestAssuredConfig;
import jakarta.inject.Inject;
import jakarta.transaction.Transactional;
import nl.gezinsapp.model.Gebruiker;
import nl.gezinsapp.model.Gezin;
import nl.gezinsapp.model.Gezinslid;
import nl.gezinsapp.model.repository.ActivatieCodeRepository;
import nl.gezinsapp.model.repository.GezinRepository;
import nl.gezinsapp.model.repository.GezinslidRepository;
import org.mockserver.model.JsonBody;
import org.openapi.quarkus.authenticatie_openapi_yaml.model.CheckJWTRequest;
import org.openapi.quarkus.authenticatie_openapi_yaml.model.JwtResponse;

import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.List;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

import static io.restassured.RestAssured.given;
import static jakarta.ws.rs.core.HttpHeaders.AUTHORIZATION;
import static org.awaitility.Awaitility.await;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockserver.model.HttpRequest.request;
import static org.mockserver.model.HttpResponse.response;

public abstract class AbstractIntegratieTest {
    private final String BEARER = "";

    @Inject
    protected ActivatieCodeRepository activatieCodeRepository;
    @Inject
    protected GezinRepository gezinRepository;
    @Inject
    protected GezinslidRepository gezinslidRepository;

    protected <T> T leesViaApi(Class<T> clazz, String url, String token,String trackAndTraceId) {
        AtomicReference<T> gezinF = new AtomicReference<T>();

        await()
                .atMost(2, TimeUnit.MINUTES)
                .pollInterval(Duration.ofMillis(500))
                .until(() -> {
                    var requestSpecification = given()
                            .when();
                    if (token != null) {
                        requestSpecification.header(AUTHORIZATION, String.format("%s%s", BEARER, token));
                    }
                    if(trackAndTraceId != null) {
                    requestSpecification.header("trackAndTraceId", trackAndTraceId);}
                    var response = requestSpecification.get(url);
                    System.out.println("response.statusCode() " + response.statusCode());
                    if (response.statusCode() == 200) {
                        gezinF.set(response.then().extract().response().as(clazz));
                    }
                    return response.statusCode() == 200;
                });

        return gezinF.get();
    }

    protected <T> T postViaApi(Class<T> clazz, String url, Object payload, String token,String trackAndTraceId) {
        RestAssured.config = RestAssuredConfig.config().objectMapperConfig(new ObjectMapperConfig().jackson2ObjectMapperFactory(
                (cls, charset) -> {
                    var df = new SimpleDateFormat("dd-MM-yyyy");
                    var objectMapper = new ObjectMapper().findAndRegisterModules();
                    objectMapper.setDateFormat(df);
                    return objectMapper;
                }
        ));
        var requestSpecification = given()
                .when();
        if (token != null) {
            requestSpecification.header(AUTHORIZATION, String.format("%s%s", BEARER, token));
        }
        if(trackAndTraceId != null) {
        requestSpecification.header("trackAndTraceId", trackAndTraceId);}
        requestSpecification.header("Content-Type", "application/json");
        requestSpecification.body(payload);
        var response = requestSpecification.post(url);
        System.out.println("response.statusCode() " + response.statusCode());

        assertEquals(200, response.statusCode());
        return response.as(clazz);
    }

    protected <T> T putViaApi(Class<T> clazz, String url, Object payload, String token,String trackAndTraceId) {
        RestAssured.config = RestAssuredConfig.config().objectMapperConfig(new ObjectMapperConfig().jackson2ObjectMapperFactory(
                (cls, charset) -> {
                    var df = new SimpleDateFormat("dd-MM-yyyy");
                    var objectMapper = new ObjectMapper().findAndRegisterModules();
                    objectMapper.setDateFormat(df);
                    return objectMapper;
                }
        ));
        var requestSpecification = given()
                .when();
        if (token != null) {
            requestSpecification.header(AUTHORIZATION, String.format("%s%s", BEARER, token));
        }
        requestSpecification.header("Content-Type", "application/json");
        if(trackAndTraceId != null) {
            requestSpecification.header("trackAndTraceId", trackAndTraceId);
        }
        requestSpecification.body(payload);
        var response = requestSpecification.put(url);
        System.out.println("response.statusCode() " + response.statusCode());

        assertEquals(200, response.statusCode());
        return response.as(clazz);
    }

    @Transactional
    void opslaanGezin(Gezin gezin) {
        gezinRepository.persist(gezin);
    }

    @Transactional
    void opslaanGezinslid(Gezinslid gezinslid) {
        gezinslidRepository.persist(gezinslid);
    }

    @Transactional
    void opslaanGebruiker(Gebruiker gebruiker) {
        gebruiker.persist();
    }

    @Transactional
    public void maakDatabaseLeeg() {
        gezinRepository.deleteAll();
        Gezinslid.deleteAll();
    }

    @Transactional
    public List<Gezin> alleGezinnen() {
        return gezinRepository.findAll().stream().toList();
    }

    @Transactional
    public List<Gezinslid> alleGezinsleden() {
        return gezinslidRepository.findAll().stream().toList();
    }


    public String maakToken() {
        return Jwts.builder()
                .setSubject("a@b.c")
                .signWith(SignatureAlgorithm.HS512,
                        UUID.randomUUID().toString().replace("-", "") +
                                UUID.randomUUID().toString().replace("-", "") +
                                UUID.randomUUID().toString().replace("-", "")
                )
                .compact();
    }

    public JwtResponse mockCheckJwt(String token,String trackAndTraceId) throws JsonProcessingException {
        return mockCheckJwt(token, null,trackAndTraceId);
    }

    public JwtResponse mockCheckJwt(String token, Long gezinsId,String trackAndTraceId) throws JsonProcessingException {
        var jwtResponse = new JwtResponse();
        jwtResponse.setGebruiker(new org.openapi.quarkus.authenticatie_openapi_yaml.model.Gebruiker());
        jwtResponse.getGebruiker().setVoornaam("John");
        jwtResponse.getGebruiker().setAchternaam("Doe");
        jwtResponse.getGebruiker().setId(new Random().nextLong());
        jwtResponse.getGebruiker().setGezin(gezinsId == null ? new Random().nextLong() : gezinsId);
        System.out.println(String.format("Gemockte gebruiker: %s : %s %s %s (%s)", jwtResponse.getGebruiker().getId(), jwtResponse.getGebruiker().getVoornaam(), jwtResponse.getGebruiker().getTussenvoegsel(), jwtResponse.getGebruiker().getAchternaam(), jwtResponse.getGebruiker().getGezin()));
        MockServer.getMockServerClient()
                .when(
                        request()
                                .withMethod("POST")
                                .withPath("/api/authorisatie/checkjwt")
                                .withHeader("trackAndTraceId",trackAndTraceId)
                                .withBody(new ObjectMapper().writeValueAsString(new CheckJWTRequest().accessToken(token)))
                )
                .respond(
                        response()
                                .withStatusCode(200)
                                .withHeader("Content-Type", "application/json")
                                .withBody(JsonBody.json(jwtResponse))
                );

        return jwtResponse;
    }
}

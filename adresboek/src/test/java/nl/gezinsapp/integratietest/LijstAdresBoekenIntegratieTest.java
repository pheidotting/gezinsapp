package nl.gezinsapp.integratietest;

import com.fasterxml.jackson.core.JsonProcessingException;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.openapi.quarkus.adresboek_openapi_yaml.model.AdresBoeken;

import static nl.gezinsapp.integratietest.random.RandomAdresBoek.randomAdresBoek;
import static org.jboss.resteasy.reactive.RestResponse.StatusCode.OK;
import static org.junit.jupiter.api.Assertions.assertEquals;

@Slf4j
@QuarkusTest
@QuarkusTestResource(MockServer.class)
class LijstAdresBoekenIntegratieTest extends AbstractIntegratieTest {
    @Test
    @DisplayName("LijstAdresBoekenIntegratieTest")
    void test() throws JsonProcessingException {
        var accesstoken = maakToken();
        var jwtResponse = mockCheckJwt(accesstoken);

        maakDatabaseLeeg();

        var item = randomAdresBoek();
        item.setGezin(jwtResponse.getGebruiker().getGezin());
        var item2 = randomAdresBoek();
        item2.setGezin(jwtResponse.getGebruiker().getGezin());

        opslaan(item);
        opslaan(item2);

        var response = getViaApi(AdresBoeken.class, "/api/adresboek/adresboeken", accesstoken, OK);

        assertEquals(2, response.getAdresBoeken().size());
    }
}

package nl.gezinsapp.messaging.reciever;

import nl.gezinsapp.commons.messaging.AbstractReciever;

import io.smallrye.common.annotation.Blocking;
import jakarta.enterprise.context.ApplicationScoped;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.gezinsapp.commons.aspect.MethodeLogging;
import nl.gezinsapp.mapper.JarigeMapper;
import nl.gezinsapp.messaging.model.JarigenVandaag;
import nl.gezinsapp.service.JarigeVandaagService;
import org.eclipse.microprofile.reactive.messaging.Incoming;
import org.eclipse.microprofile.reactive.messaging.Message;
import org.slf4j.MDC;

import java.util.concurrent.CompletionStage;
@Slf4j
@ApplicationScoped
@RequiredArgsConstructor
public class JarigenVandaagReciever extends AbstractReciever<JarigenVandaag> {
    private final JarigeVandaagService jarigeVandaagService;
    private final JarigeMapper jarigeMapper;

    @Incoming("jarigenvandaag")
    @MethodeLogging
    @Blocking
    public CompletionStage<Void> verwerk(Message<JarigenVandaag> m) {
        var jarigenVandaag = getObject(m, JarigenVandaag.class);

        if(jarigenVandaag.getJarigen().get(0).getTrackAndTraceId()!=null){
            MDC.put("trackAndTraceId",jarigenVandaag.getJarigen().get(0).getTrackAndTraceId().toString());
        }

        log.info("Ontvangen : {}", jarigenVandaag);

        jarigenVandaag.getJarigen()
                .stream().filter(jarige -> !"".equals(jarige.getEmailadres()))
                .forEach(jarige -> jarigeVandaagService.maakEmail(jarigeMapper.map(jarige)));

        return m.ack();
    }
}

package nl.gezinsapp.exception;

import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.ext.ExceptionMapper;
import jakarta.ws.rs.ext.Provider;
import nl.gezinsapp.commons.aspect.MethodeLogging;

@Provider
public class TokenRefreshExceptionHandler implements ExceptionMapper<TokenRefreshException> {
    @Override
    @MethodeLogging
    public Response toResponse(TokenRefreshException e) {
        return Response.status(Response.Status.UNAUTHORIZED).build();
    }
}
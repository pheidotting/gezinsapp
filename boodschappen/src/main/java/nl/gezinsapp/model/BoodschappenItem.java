package nl.gezinsapp.model;

import io.quarkus.hibernate.orm.panache.PanacheEntity;
import io.quarkus.hibernate.orm.panache.PanacheEntityBase;
import jakarta.persistence.*;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "BOODSCHAPPENITEM")
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class BoodschappenItem extends PanacheEntityBase {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;
    @Column(name = "GEZIN")
    private Long gezin;
    @Column(name = "WINKEL")
    private String winkel;
    @Column(name = "OMSCHRIJVING")
    private String omschrijving;
    @Column(name = "GEHAALD")
    private boolean gehaald;
}

package nl.gezinsapp.messaging.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@NoArgsConstructor
@ToString
public class ActivatieCodeAangemaakt extends AbstractMessagingObject {
    private String familienaam;
    private String code;
}

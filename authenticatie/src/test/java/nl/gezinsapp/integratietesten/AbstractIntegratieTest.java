package nl.gezinsapp.integratietesten;


import com.fasterxml.jackson.databind.ObjectMapper;
import io.restassured.RestAssured;
import io.restassured.config.ObjectMapperConfig;
import io.restassured.config.RestAssuredConfig;
import jakarta.inject.Inject;
import jakarta.transaction.Transactional;
import nl.gezinsapp.model.InlogPoging;
import nl.gezinsapp.model.repository.InlogPogingRepository;
import nl.gezinsapp.model.repository.RefreshTokenRepository;
import nl.gezinsapp.service.GebruikerService;

import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

import static io.restassured.RestAssured.given;
import static jakarta.ws.rs.core.HttpHeaders.AUTHORIZATION;
import static org.awaitility.Awaitility.await;
import static org.junit.jupiter.api.Assertions.assertEquals;

public abstract class AbstractIntegratieTest {
    @Inject
    GebruikerService gebruikerService;
    @Inject
    RefreshTokenRepository refreshTokenRepository;
    @Inject
    InlogPogingRepository inlogPogingRepository;

    protected <T> T leesViaApi(Class<T> clazz, String url, String token) {
        AtomicReference<T> gezinF = new AtomicReference<T>();

        await()
                .atMost(2, TimeUnit.MINUTES)
                .pollInterval(Duration.ofMillis(500))
                .until(() -> {
                    var requestSpecification = given()
                            .when();
                    if (token != null) {
                        requestSpecification.header(AUTHORIZATION, String.format("Bearer %s", token));
                    }
                    var response = requestSpecification.get(url);
                    System.out.println("response.statusCode() " + response.statusCode());
                    if (response.statusCode() == 200) {
                        gezinF.set(response.then().extract().response().as(clazz));
                    }
                    return response.statusCode() == 200;
                });

        return gezinF.get();
    }

    protected <T> T postViaApi(Class<T> clazz, String url, Object payload, String token) {
        return postViaApi(clazz, url, payload, token, null);
    }

    protected <T> T postViaApi(Class<T> clazz, String url, Object payload, String token, String trackAndTraceId) {
        RestAssured.config = RestAssuredConfig.config().objectMapperConfig(new ObjectMapperConfig().jackson2ObjectMapperFactory(
                (cls, charset) -> {
                    var df = new SimpleDateFormat("dd-MM-yyyy");
                    var objectMapper = new ObjectMapper().findAndRegisterModules();
                    objectMapper.setDateFormat(df);
                    return objectMapper;
                }
        ));
        var requestSpecification = given()
                .when();
        if (token != null) {
            requestSpecification.header(AUTHORIZATION, String.format("Bearer %s", token));
        }
        if (trackAndTraceId != null) {
            requestSpecification.header("trackAndTraceId", trackAndTraceId);
        }
        requestSpecification.header("Content-Type", "application/json");
        requestSpecification.body(payload);
        var response = requestSpecification.post(url);
        System.out.println("response.statusCode() " + response.statusCode());

        assertEquals(200, response.statusCode());
        return response.as(clazz);
    }

    void postViaApiNoResponse(String url, Object payload, String token) {
        RestAssured.config = RestAssuredConfig.config().objectMapperConfig(new ObjectMapperConfig().jackson2ObjectMapperFactory(
                (cls, charset) -> {
                    var df = new SimpleDateFormat("dd-MM-yyyy");
                    var objectMapper = new ObjectMapper().findAndRegisterModules();
                    objectMapper.setDateFormat(df);
                    return objectMapper;
                }
        ));
        var requestSpecification = given()
                .when();
        if (token != null) {
            requestSpecification.header(AUTHORIZATION, String.format("Bearer %s", token));
        }
        requestSpecification.header("Content-Type", "application/json");
        requestSpecification.body(payload);
        var response = requestSpecification.post(url);
        System.out.println("response.statusCode() " + response.statusCode());

        assertEquals(200, response.statusCode());
    }

    protected <T> T putViaApi(Class<T> clazz, String url, Object payload, String token) {
        RestAssured.config = RestAssuredConfig.config().objectMapperConfig(new ObjectMapperConfig().jackson2ObjectMapperFactory(
                (cls, charset) -> {
                    var df = new SimpleDateFormat("dd-MM-yyyy");
                    var objectMapper = new ObjectMapper().findAndRegisterModules();
                    objectMapper.setDateFormat(df);
                    return objectMapper;
                }
        ));
        var requestSpecification = given()
                .when();
        if (token != null) {
            requestSpecification.header(AUTHORIZATION, String.format("Bearer %s", token));
        }
        requestSpecification.header("Content-Type", "application/json");

        requestSpecification.body(payload);
        var response = requestSpecification.put(url);
        System.out.println("response.statusCode() " + response.statusCode());

        assertEquals(200, response.statusCode());
        return response.as(clazz);
    }

    @Transactional
    public List<InlogPoging> leesInlogPogingenBijGezinslid(Long gezinslid) {
        return inlogPogingRepository.find("gezinslid", gezinslid).stream().toList();
    }

    @Transactional
    public void opslaan(InlogPoging inlogPoging) {
        inlogPogingRepository.persist(inlogPoging);
    }

    @Transactional
    public void leegmaken() {
        inlogPogingRepository.deleteAll();
    }
}

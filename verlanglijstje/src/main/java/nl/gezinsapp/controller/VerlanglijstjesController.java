package nl.gezinsapp.controller;

import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.gezinsapp.aspect.ThreadVariabelen;
import nl.gezinsapp.commons.aspect.AlsGezinslid;
import nl.gezinsapp.commons.aspect.MethodeLogging;
import nl.gezinsapp.commons.aspect.TrackAndTraceId;
import nl.gezinsapp.domain.Verlanglijst;
import nl.gezinsapp.mapper.VerlanglijstMapper;
import org.openapi.quarkus.gezin_openapi_yaml.model.Gezinslid;
import org.openapi.quarkus.verlanglijstje_openapi_yaml.api.VerlanglijstjeApi;
import org.openapi.quarkus.verlanglijstje_openapi_yaml.model.AlgemeneResponse;
import org.openapi.quarkus.verlanglijstje_openapi_yaml.model.Foutmeldingen;
import org.openapi.quarkus.verlanglijstje_openapi_yaml.model.Verlanglijstje;
import org.openapi.quarkus.verlanglijstje_openapi_yaml.model.Verlanglijstjes;

import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.function.Function;
import java.util.stream.Stream;

@Slf4j
@RequiredArgsConstructor
public class VerlanglijstjesController extends AbstractController implements VerlanglijstjeApi {
    private final VerlanglijstMapper verlanglijstMapper;

    @Override
    @Transactional
    @TrackAndTraceId
    @AlsGezinslid
    @MethodeLogging
    public Verlanglijstjes alleVerlanglijstjes() {
        var gezinId = ThreadVariabelen.get().getGezin();

        return new Verlanglijstjes().verlanglijstjes(gezinApi.leesGezin(gezinId).getGezinsleden().stream()
                .map(Gezinslid::getId)
                .map((Function<Long, List<Verlanglijst>>) id -> Verlanglijst.find("gezinslid", id).list())
                .flatMap((Function<List<Verlanglijst>, Stream<Verlanglijst>>) Collection::stream)
                .filter(Objects::nonNull)
                .map(verlanglijstMapper::map)
                .toList());
    }

    @Override
    @Transactional
    @TrackAndTraceId
    @AlsGezinslid
    @MethodeLogging
    public AlgemeneResponse nieuwVerlanglijstje(Verlanglijstje verlanglijstje) {
        var gezinslid = ThreadVariabelen.get().getId();

        var lijst = verlanglijstMapper.map(verlanglijstje);
        lijst.setGezinslid(gezinslid);
        lijst.setCode(UUID.randomUUID().toString().replace("-", ""));
        lijst.persist();

        return new AlgemeneResponse().id(lijst.getId());
    }

    @Override
    @Transactional
    @TrackAndTraceId
    @AlsGezinslid
    @MethodeLogging
    public AlgemeneResponse verwijder(Long id) {
        var verlanglijst = (Verlanglijst) Verlanglijst.findById(id);

        if (isVanHuidigGezin(verlanglijst)) {
            Verlanglijst.deleteById(id);
            return new AlgemeneResponse().id(id);
        } else {
            return new AlgemeneResponse().foutmeldingen(new Foutmeldingen().addMeldingenItem("Betreffende verlanglijst is geen onderdeel van het huidige gezin."));
        }
    }

    @Override
    @Transactional
    @TrackAndTraceId
    @AlsGezinslid
    @MethodeLogging
    public AlgemeneResponse wijzigVerlanglijstje(Verlanglijstje verlanglijstje) {
        var verlanglijst = (Verlanglijst) Verlanglijst.findById(verlanglijstje.getId());

        if (isVanHuidigGezin(verlanglijst)) {
            var gewijzigdeVerlanglijst = verlanglijstMapper.update(verlanglijstje, verlanglijst);
            gewijzigdeVerlanglijst.persist();
            return new AlgemeneResponse().id(verlanglijstje.getId());
        } else {
            return new AlgemeneResponse().foutmeldingen(new Foutmeldingen().addMeldingenItem("Betreffende verlanglijst is geen onderdeel van het huidige gezin."));
        }
    }
}

import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'http',
  standalone: true
})
export class HttpPipe implements PipeTransform {

  transform(value: string): unknown {
    return value.startsWith("http") ? value : 'http://' + value;
  }

}

package nl.gezinsapp.integratietesten.afspraak;

import com.fasterxml.jackson.core.JsonProcessingException;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;
import lombok.extern.slf4j.Slf4j;
import nl.gezinsapp.integratietesten.AbstractIntegratieTest;
import nl.gezinsapp.integratietesten.MockServer;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.openapi.quarkus.kalender_openapi_yaml.model.AlgemeneResponse;

import java.time.Duration;
import java.util.concurrent.TimeUnit;

import static nl.gezinsapp.integratietesten.common.random.RandomAfspraak.randomAfspraak;
import static nl.gezinsapp.integratietesten.common.random.RandomAfspraak.randomDtoAfspraak;
import static org.awaitility.Awaitility.await;
import static org.junit.jupiter.api.Assertions.*;

@Slf4j
@QuarkusTest
@QuarkusTestResource(MockServer.class)
class OpslaanAfspraakIntegratieTest extends AbstractIntegratieTest {
    @Test
    @DisplayName("Sla een bestaande afspraak op")
    void test() throws JsonProcessingException {
        maakDatabaseLeeg();

        var accesstoken = maakToken();
        var jwtResponse = mockCheckJwt(accesstoken);

        var afspraakDmn = randomAfspraak();
        afspraakDmn.setGezin(99L);
        opslaan(afspraakDmn);

        var afspraak = randomDtoAfspraak();
        afspraak.setId(afspraakDmn.getId());

        var response = postViaApi(AlgemeneResponse.class, "/api/kalender/afspraak", afspraak, accesstoken);
        assertNull(response.getFoutmeldingen());
        assertNotNull(response.getId());

        await()
                .atMost(40, TimeUnit.SECONDS)
                .pollInterval(Duration.ofMillis(500))
                .until(() -> {
                    var afspraken = leesAlleAfspraken();
                    assertEquals(1, afspraken.size());
                    assertEquals(afspraak.getOmschrijving(), afspraken.get(0).getOmschrijving());
                    assertEquals(jwtResponse.getGebruiker().getGezin(), afspraken.get(0).getGezin());
                    return true;
                });

    }
}

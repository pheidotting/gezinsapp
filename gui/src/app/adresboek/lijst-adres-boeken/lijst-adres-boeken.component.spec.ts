import {fakeAsync, TestBed, tick} from '@angular/core/testing';

import {LijstAdresBoekenComponent} from './lijst-adres-boeken.component';
import {ActivatedRoute, provideRouter, RouterModule} from "@angular/router";
import {of} from 'rxjs';
import {HttpTestingController, provideHttpClientTesting} from "@angular/common/http/testing";
import {provideHttpClient, withInterceptorsFromDi} from '@angular/common/http';

describe('LijstAdresBoekenComponent', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [LijstAdresBoekenComponent,
        RouterModule.forRoot([])],
      providers: [provideRouter([]), provideHttpClient(withInterceptorsFromDi()), provideHttpClientTesting()]
    });
  });

  it('should create', fakeAsync(() => {
    let fixture = TestBed.createComponent(LijstAdresBoekenComponent);

    let app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));

  it('should load data on startup', fakeAsync(() => {
    let fixture = TestBed.createComponent(LijstAdresBoekenComponent);

    let activatedRoute: ActivatedRoute = fixture.debugElement.injector.get(ActivatedRoute);

    activatedRoute.data = of({
      adresBoeken: {
        adresBoeken: [{
          'omschrijving': 'omsc'
        }]
      }
    });

    fixture.detectChanges();
    tick();

    let app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
    expect(app.adresBoeken.length).toBe(1);
    expect(app.adresBoeken[0].omschrijving).toBe('omsc');
  }));

  it('should delete an adresboek', fakeAsync(() => {
    let fixture = TestBed.createComponent(LijstAdresBoekenComponent);
    let httpTestingController = TestBed.inject(HttpTestingController);

    let activatedRoute: ActivatedRoute = fixture.debugElement.injector.get(ActivatedRoute);

    activatedRoute.data = of({
      adresBoeken: {
        adresBoeken: [{
          'omschrijving': 'omsc'
        }]
      }
    });

    let app = fixture.debugElement.componentInstance;
    const id = 4;

    app.verwijder(id);

    fixture.detectChanges();
    tick(60000);

    const reqDeleteAdresBoek = httpTestingController.expectOne((req) => {
      return req.url.endsWith('/api/adresboek/adresboek');
    });
    expect(reqDeleteAdresBoek.request.method).toEqual('DELETE');
    reqDeleteAdresBoek.flush({
      foutmeldingen: {
        meldingen: []
      }
    });
    const reqGetAdresBoeken = httpTestingController.expectOne((req) => {
      return req.url.endsWith('/api/adresboek/adresboeken');
    });
    expect(reqGetAdresBoeken.request.method).toEqual('GET');
    reqGetAdresBoeken.flush({
      adresBoeken: [{
        'omschrijving': 'omsc2'
      }]
    });

    fixture.whenStable().then(() => {
      expect(app.adresBoeken.length).toBe(1);
      expect(app.adresBoeken[0].omschrijving).toBe('omsc2');
    });
  }));

});

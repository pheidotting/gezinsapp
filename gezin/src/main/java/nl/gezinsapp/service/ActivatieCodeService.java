package nl.gezinsapp.service;

import jakarta.enterprise.context.ApplicationScoped;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.gezinsapp.messaging.model.ActivatieCodeAangemaakt;
import nl.gezinsapp.messaging.sender.Sender;
import nl.gezinsapp.model.ActivatieCode;
import nl.gezinsapp.model.repository.ActivatieCodeRepository;
import org.openapi.quarkus.gezin_openapi_yaml.model.VraagActiveringsCodeOp;

import java.util.UUID;

import static nl.gezinsapp.service.CommunicatieMessagingObjectUtil.vulObject;

@Slf4j
@ApplicationScoped
@RequiredArgsConstructor
public class ActivatieCodeService {
    private final ActivatieCodeRepository activatieCodeRepository;
    private final GenereerCodeService genereerCodeService;
    private final Sender activatieCodeAangemaaktSender;

    public ActivatieCode genereerActiveringsCode(VraagActiveringsCodeOp vraagActiveringsCodeOp) {
        var activatieCode = activatieCodeRepository.set(genereerCodeService.genereerCode());

        var activatieCodeAangemaakt = vulObject(null,new ActivatieCodeAangemaakt());
        activatieCodeAangemaakt.setCode(activatieCode.getCode());
        activatieCodeAangemaakt.setFamilienaam(vraagActiveringsCodeOp.getFamilienaam());
        activatieCodeAangemaakt.setEmailadres(vraagActiveringsCodeOp.getEmail());
        log.info("{}", activatieCodeAangemaakt.getCode());
        activatieCodeAangemaaktSender.verstuurActivatieCodeAangemaakt(activatieCodeAangemaakt);

        return activatieCode;
    }

    public boolean controleerActiveringsCode(UUID id, String code) {
        var codeInDb = activatieCodeRepository.get(id);

        if (codeInDb.isPresent()) {
            return codeInDb.get().equals(code);
        } else {
            return false;
        }
    }
}

package nl.gezinsapp.integratietesten.soortdienst;

import com.fasterxml.jackson.core.JsonProcessingException;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;
import lombok.extern.slf4j.Slf4j;
import nl.gezinsapp.integratietesten.AbstractIntegratieTest;
import nl.gezinsapp.integratietesten.MockServer;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.openapi.quarkus.kalender_openapi_yaml.model.SoortenDiensten;

import java.time.Duration;
import java.util.concurrent.TimeUnit;

import static nl.gezinsapp.integratietesten.common.random.SoortDienstDatum.randomSoortDienst;
import static org.awaitility.Awaitility.await;
import static org.junit.jupiter.api.Assertions.assertEquals;

@Slf4j
@QuarkusTest
@QuarkusTestResource(MockServer.class)
class LeesSoortenDienstenIntegratieTest extends AbstractIntegratieTest {
    @Test
    @DisplayName("Lees een opgeslagen datum")
    void test() throws JsonProcessingException {
        maakDatabaseLeeg();

        var soortDienst = randomSoortDienst();

        var accesstoken = maakToken();
        var jwtResponse = mockCheckJwt(accesstoken);

        opslaan(soortDienst);

        await()
                .atMost(40, TimeUnit.SECONDS)
                .pollInterval(Duration.ofMillis(500))
                .until(() -> {
                    var opgehaaldeSoortDienst = leesViaApi(SoortenDiensten.class, "/api/kalender/rooster/soort-diensten/" + soortDienst.getGezinslid(), accesstoken);
                    if (opgehaaldeSoortDienst != null && !opgehaaldeSoortDienst.getSoortenDiensten().isEmpty()) {
                        return true;
                    }
                    return false;
                });
        var opgehaaldeSoortenDiensten = leesViaApi(SoortenDiensten.class, "/api/kalender/rooster/soort-diensten/" + soortDienst.getGezinslid(), accesstoken);

        assertEquals(1, opgehaaldeSoortenDiensten.getSoortenDiensten().size());
        assertEquals(soortDienst.getId(), opgehaaldeSoortenDiensten.getSoortenDiensten().get(0).getId());
        assertEquals(soortDienst.getGezinslid(), opgehaaldeSoortenDiensten.getSoortenDiensten().get(0).getGezinslid());
        assertEquals(soortDienst.getOmschrijving(), opgehaaldeSoortenDiensten.getSoortenDiensten().get(0).getOmschrijving());
        assertEquals(soortDienst.getVan().withNano(0).toString().substring(0, 5), opgehaaldeSoortenDiensten.getSoortenDiensten().get(0).getVan().substring(0, 5));
        assertEquals(soortDienst.getTot().withNano(0).toString().substring(0, 5), opgehaaldeSoortenDiensten.getSoortenDiensten().get(0).getTot().substring(0, 5));
    }
}

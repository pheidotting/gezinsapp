import {ComponentFixture, TestBed} from '@angular/core/testing';

import {AfsprakenModalComponent} from './afspraken-modal.component';
import {provideRouter, RouterModule} from "@angular/router";
import {provideHttpClient, withInterceptorsFromDi} from "@angular/common/http";
import {provideHttpClientTesting} from "@angular/common/http/testing";

describe('AfsprakenModalComponent', () => {
  let component: AfsprakenModalComponent;
  let fixture: ComponentFixture<AfsprakenModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [AfsprakenModalComponent,
        RouterModule.forRoot([])],
      providers: [provideRouter([]), provideHttpClient(withInterceptorsFromDi()), provideHttpClientTesting()]
    }).compileComponents();

    fixture = TestBed.createComponent(AfsprakenModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  // SoortenDienstenTonen, er moet maximaal 1 gezinslid zijn aangevinkt en er moet een soort dienst aanwezig zijn
  it('soortenDienstenTonen, aan alle condities wordt voldaan', () => {
    expect(component).toBeTruthy();

    component.afsprakenService.gezinsleden.push({
      id: 4,
      checked: true
    });
    component.afsprakenService.alleSoortenDiensten.push({
      id: 1,
      van: '',
      tot: '',
      gezinslid: 4
    });

    fixture.detectChanges();

    expect(component.soortenDienstenTonen()).toBeTrue();
  });
  it('soortenDienstenTonen, geen soorten gekoppeld aan een geselecteerd gezinslid', () => {
    expect(component).toBeTruthy();

    component.afsprakenService.gezinsleden.push({
      id: 4,
      checked: true
    });
    component.afsprakenService.alleSoortenDiensten.push({
      id: 1,
      van: '',
      tot: '',
      gezinslid: 5
    });

    fixture.detectChanges();

    expect(component.soortenDienstenTonen()).toBeFalse();
  });
  it('soortenDienstenTonen, geen geselecteerd gezinslid', () => {
    expect(component).toBeTruthy();

    component.afsprakenService.gezinsleden.push({
      id: 4,
      checked: false
    });
    component.afsprakenService.alleSoortenDiensten.push({
      id: 1,
      van: '',
      tot: '',
      gezinslid: 4
    });

    fixture.detectChanges();

    expect(component.soortenDienstenTonen()).toBeFalse();
  });
  it('soortenDienstenTonen, geen soort dienst aanwezig', () => {
    expect(component).toBeTruthy();

    component.afsprakenService.gezinsleden.push({
      id: 4,
      checked: true
    });

    fixture.detectChanges();

    expect(component.soortenDienstenTonen()).toBeFalse();
  });
  it('soortenDienstenTonen, geen soort dienst aanwezig en geen geselecteerd gezinslid', () => {
    expect(component).toBeTruthy();

    component.afsprakenService.gezinsleden.push({
      id: 4,
      checked: false
    });

    fixture.detectChanges();

    expect(component.soortenDienstenTonen()).toBeFalse();
  });
});
